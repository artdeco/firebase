/* start example */
import {Firebase} from '../../'

/* end example */
;(async () => {
 /* start example */
 let firebase = new Firebase()
 let res = await firebase.run({
  shouldRun: true,
  text: 'hello-world',
 })
 console.log(res) // @artdeco/firebase called with hello-world
 /* end example */
})()