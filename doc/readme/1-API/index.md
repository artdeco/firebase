## API

The package is available by importing its default function and named class:

```js
import firebase, { Firebase } from '@artdeco/firebase'
```

<include-types>types/api.xml</include-types>

%~ width="20"%

<function noArgTypesInToc level="3" name="firebase"/>

<example src="doc/example" />
<fork>doc/example</fork>

%~%

<typedef level="2" name="Firebase">types/index.xml</typedef>

%~%