<public gitlab dir="doc/pub" />
<example-override from="src" to="@artdeco/firebase" />
<div align="center"><p align="center">

# @artdeco/firebase

%NPM: @artdeco/firebase%
<pipeline-badge/>
</p></div>

`@artdeco/firebase` is: Various firebase utils and runtime to download JS files from HTTP hosting for execution in functions (helps to eliminate reploy every so often).

```sh
yarn add @artdeco/firebase
npm i @artdeco/firebase
```

## Table Of Contents

%TOC%

%~%