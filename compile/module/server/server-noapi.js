import { FirebaseAspects } from './server-exports'
 
/** @lazy @api {eco.artd.FirebaseAspects} */
export { FirebaseAspects }