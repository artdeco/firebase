import { FirebaseAspects } from './browser-exports'
 
/** @lazy @api {eco.artd.FirebaseAspects} */
export { FirebaseAspects }