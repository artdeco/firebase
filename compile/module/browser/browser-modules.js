export default [
 1, // Firebase
 2, // FirebaseAspectsInstaller
 3, // AbstractFirebaseAspects
 4, // FirebaseAspects
 5, // AbstractHyperFirebase
 6, // HyperFirebase
 7, // FirebaseFactory
 8, // FirebaseLoggingAspects
 9, // FirebaseProfilingAspects
 10, // AbstractFirebaseProfilingAide
 11, // FirebaseProfilingAide
 12, // FirebaseConditioningAspects
]