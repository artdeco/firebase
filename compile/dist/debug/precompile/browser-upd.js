import {getPackageName, isShared} from '../version'
/**
@license
@LICENSE                      Warning!

This file links to (or embeds, in which case you will see object code
boundaries comments before and after), proprietary code from the package(s):
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
Please make sure you have Commercial License to use this library.
*/
const packageName=getPackageName()

/** @nosideeffects */
function getEmbeddedModule(exports,require,module={},__filename='',__dirname='') {
 const fn=new Function('exports,require,module,__filename,__dirname',`
/*! @embed-object-start {@artdeco/firebase} */
var a = "depack-remove-start", b = a;
try {
  if (a) {
    throw Error();
  }
  Object.setPrototypeOf(b, b);
  b.h = new WeakMap();
  b.map = new Map();
  b.set = new Set();
  Object.getOwnPropertySymbols({});
  Object.getOwnPropertyDescriptors({});
  a.includes("");
  [].keys();
  Object.values({});
  Object.assign({}, {});
} catch (c) {
}
a = "depack-remove-end";

const d = function() {
  return (0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@type.engineering/type-engineer"'));
}();

const e = d.iu;
class f {
  __$constructor() {
    this.example = "ok";
    const {g:c} = g(this);
    h(this, {g:e(c, ""),});
  }
}
class k extends d["372700389812"](f, 16817934092, null, {asIFirebase:1, superFirebase:2,}, !1, {symbol:{g:1},}) {
}
function l() {
}
const g = k.getSymbols, h = k.setSymbols;
k[d["372700389811"]] = [{[d["372700389810"]]:{symbol:1,}, initializer({symbol:c}) {
  h(this, {g:c,});
},}];
async function m() {
  return "example";
}
;var n = class extends k.implements(l.prototype = {run:m,},) {
};
module.exports["50152546450"] = n;
module.exports["50152546451"] = n;
/*! @embed-object-end {@artdeco/firebase} */`)
 module['exports']=module.exports||exports||{}
 fn(exports,require,module,__filename,__dirname)
 const m=module['exports']
 if(require) require[packageName]=m
 return m
}

/** @nosideeffects */
function getSharedModule(exports,req) {
 return req(eval(`/*dequire*/"${packageName}"`))
}

const shared=isShared()

export default shared?getSharedModule:getEmbeddedModule