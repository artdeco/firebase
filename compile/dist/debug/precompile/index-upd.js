import {getPackageName, isShared} from '../version'
/**
@license
@LICENSE                      Warning!

This file links to (or embeds, in which case you will see object code
boundaries comments before and after), proprietary code from the package(s):
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
Please make sure you have Commercial License to use this library.
*/
const packageName=getPackageName()

/** @nosideeffects */
function getEmbeddedModule(exports,require,module={},__filename='',__dirname='') {
 const fn=new Function('exports,require,module,__filename,__dirname',`
/*! @embed-object-start {@artdeco/firebase} */
const a = function() {
  return require(eval('/*dequire*/"@type.engineering/type-engineer"'));
}();

const b = a.iu;
class d {
  __$constructor() {
    this.example = "ok";
    const {g:c} = e(this);
    f(this, {g:b(c, ""),});
  }
}
class g extends a["372700389812"](d, 16817934092, null, {asIFirebase:1, superFirebase:2,}, !1, {symbol:{g:1},}) {
}
function h() {
}
const e = g.getSymbols, f = g.setSymbols;
g[a["372700389811"]] = [{[a["372700389810"]]:{symbol:1,}, initializer({symbol:c}) {
  f(this, {g:c,});
},}];
async function k() {
  return "example";
}
;var l = class extends g.implements(h.prototype = {run:k,},) {
};
module.exports["50152546450"] = l;
module.exports["50152546451"] = l;
/*! @embed-object-end {@artdeco/firebase} */`)
 module['exports']=module.exports||exports||{}
 fn(exports,require,module,__filename,__dirname)
 const m=module['exports']
 if(require) require[packageName]=m
 return m
}

/** @nosideeffects */
function getSharedModule(exports,req) {
 return req(eval(`/*dequire*/"${packageName}"`))
}

const shared=isShared()

export default shared?getSharedModule:getEmbeddedModule