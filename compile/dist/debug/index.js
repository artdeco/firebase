require('@type.engineering/type-engineer/types/typedefs')
require('./types/typedefs')

/**
@license
@LICENSE @artdeco/firebase (c) by Art Deco™ 2024.
Please make sure you have a Commercial License to use this library.
*/

/**
 * Various firebase utils and runtime to download JS files from HTTP hosting for execution in functions (helps to eliminate reploy every so often).
 * @extends {eco.artd.Firebase}
 */
class Firebase extends (class {/* lazy-loaded */}) {}

module.exports.Firebase = Firebase

Object.defineProperties(module.exports, {
 'Firebase': {get: () => require('./compile')[50152546451]},
 [50152546451]: {get: () => module.exports['Firebase']},
})