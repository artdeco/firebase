import '@artdeco/firebase.h'
import Module from './node'

/**
@license
@LICENSE @artdeco/firebase (c) by Art Deco™ 2024.
Please make sure you have a Commercial License to use this library.
*/

/** @type {typeof eco.artd.Firebase} */
export const Firebase=Module['50152546451']

/** Allows to embed the object code directly into other packages. */