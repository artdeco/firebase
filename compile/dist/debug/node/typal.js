import '@artdeco/firebase.h'
import Module from './node' /* compiler fn:../precompile/index mod renameReport:../../../module/server/server-modules.txt packageName:@artdeco/firebase */

/** @export {../../../api/license.js} */

/** @export {../../../module/server/server-api.js} */

/** Allows to embed the object code directly into other packages. */
