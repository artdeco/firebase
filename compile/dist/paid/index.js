require('@type.engineering/type-engineer/types/typedefs')
require('./types/typology')
require('./types/typedefs')

/**
@license
@LICENSE @artdeco/firebase (c) by Art Deco™ 2024.
Please make sure you have a Commercial License to use this library.
*/

/**
 *     Various firebase utils and runtime to download JS files from HTTP hosting
 * for execution in functions (helps to eliminate reploy every so often).
 * @extends {eco.artd.Firebase}
 */
class Firebase extends (class {/* lazy-loaded */}) {}
/** @extends {eco.artd.FirebaseAspectsInstaller} */
class FirebaseAspectsInstaller extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `eco.artd.IFirebaseAspects` interface.
 * @extends {eco.artd.AbstractFirebaseAspects}
 */
class AbstractFirebaseAspects extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `eco.artd.IHyperFirebase` interface.
 * @extends {eco.artd.AbstractHyperFirebase}
 */
class AbstractHyperFirebase extends (class {/* lazy-loaded */}) {}
/** @extends {eco.artd.HyperFirebase} */
class HyperFirebase extends (class {/* lazy-loaded */}) {}
/**
 * Produces a configured hyper class.
 * @param {!eco.artd.FirebaseFactory.Config} [config] The config.
 * - `[aspectsInstallers]` _!Array?_
 * - `[subjects]` _!Array&lt;IFirebase&gt;?_
 * - `[hypers]` _!Array?_
 * - `[aspects]` _!Array&lt;!IFirebaseAspects&gt;&vert;!IFirebaseAspects?_
 * @return {typeof eco.artd.HyperFirebase}
 */
function FirebaseFactory(config) {
  // lazy-loaded(config)
}
/**
 * Logs about method executions.
 * @extends {eco.artd.FirebaseLoggingAspects}
 */
class FirebaseLoggingAspects extends (class {/* lazy-loaded */}) {}
/** @extends {eco.artd.FirebaseProfilingAspects} */
class FirebaseProfilingAspects extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `eco.artd.IFirebaseProfilingAide` interface.
 * @extends {eco.artd.AbstractFirebaseProfilingAide}
 */
class AbstractFirebaseProfilingAide extends (class {/* lazy-loaded */}) {}
/**
 * Records the results of the profiling.
 * @extends {eco.artd.FirebaseProfilingAide}
 */
class FirebaseProfilingAide extends (class {/* lazy-loaded */}) {}
/** @extends {eco.artd.FirebaseConditioningAspects} */
class FirebaseConditioningAspects extends (class {/* lazy-loaded */}) {}

module.exports.Firebase = Firebase
module.exports.FirebaseAspectsInstaller = FirebaseAspectsInstaller
module.exports.AbstractFirebaseAspects = AbstractFirebaseAspects
module.exports.AbstractHyperFirebase = AbstractHyperFirebase
module.exports.HyperFirebase = HyperFirebase
module.exports.FirebaseFactory = FirebaseFactory
module.exports.FirebaseLoggingAspects = FirebaseLoggingAspects
module.exports.FirebaseProfilingAspects = FirebaseProfilingAspects
module.exports.AbstractFirebaseProfilingAide = AbstractFirebaseProfilingAide
module.exports.FirebaseProfilingAide = FirebaseProfilingAide
module.exports.FirebaseConditioningAspects = FirebaseConditioningAspects

Object.defineProperties(module.exports, {
 'Firebase': {get: () => require('./compile')[50152546451]},
 [50152546451]: {get: () => module.exports['Firebase']},
 'FirebaseAspectsInstaller': {get: () => require('./compile')[50152546452]},
 [50152546452]: {get: () => module.exports['FirebaseAspectsInstaller']},
 'AbstractFirebaseAspects': {get: () => require('./compile')[50152546453]},
 [50152546453]: {get: () => module.exports['AbstractFirebaseAspects']},
 'AbstractHyperFirebase': {get: () => require('./compile')[50152546455]},
 [50152546455]: {get: () => module.exports['AbstractHyperFirebase']},
 'HyperFirebase': {get: () => require('./compile')[50152546456]},
 [50152546456]: {get: () => module.exports['HyperFirebase']},
 'FirebaseFactory': {get: () => require('./compile')[50152546457]},
 [50152546457]: {get: () => module.exports['FirebaseFactory']},
 'FirebaseLoggingAspects': {get: () => require('./compile')[50152546458]},
 [50152546458]: {get: () => module.exports['FirebaseLoggingAspects']},
 'FirebaseProfilingAspects': {get: () => require('./compile')[50152546459]},
 [50152546459]: {get: () => module.exports['FirebaseProfilingAspects']},
 'AbstractFirebaseProfilingAide': {get: () => require('./compile')[501525464510]},
 [501525464510]: {get: () => module.exports['AbstractFirebaseProfilingAide']},
 'FirebaseProfilingAide': {get: () => require('./compile')[501525464511]},
 [501525464511]: {get: () => module.exports['FirebaseProfilingAide']},
 'FirebaseConditioningAspects': {get: () => require('./compile')[501525464512]},
 [501525464512]: {get: () => module.exports['FirebaseConditioningAspects']},
})