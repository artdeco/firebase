import * as serverApi from './compile' /* compiler renameReport:../../module/server/server-modules.txt */
require('@type.engineering/type-engineer/types/typedefs')
require('./types/typology')
require('./types/typedefs')

/** @export {../../api/license.js} */

/** @export {../../module/server/server-api.js} */
