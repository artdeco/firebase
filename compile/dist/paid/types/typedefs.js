/** @nocompile */
eval(`var http={}
var eco={}
eco.artd={}
eco.artd.IFirebase={}
eco.artd.IFirebaseAspectsInstaller={}
eco.artd.IFirebaseJoinpointModel={}
eco.artd.IFirebaseAspects={}
eco.artd.IHyperFirebase={}
eco.artd.FirebaseFactory={}
eco.artd.firebase={}`)

/** */
var __$te_plain={}

/**
 * @typedef {Object} eco.artd.IFirebase.Initialese A record with object's initial values, dependencies and other configuration.
 * @prop {string} [symbol] A private symbol.
 * @prop {{ info: function() }} [logger] The logger.
 */

/** @typedef {function(new: eco.artd.Firebase)} eco.artd.AbstractFirebase.constructor */
/** @typedef {typeof eco.artd.Firebase} eco.artd.Firebase.typeof */
/**
 * An abstract class of `eco.artd.IFirebase` interface.
 * @constructor eco.artd.AbstractFirebase
 */
eco.artd.AbstractFirebase = class extends /** @type {eco.artd.AbstractFirebase.constructor&eco.artd.Firebase.typeof} */ (class {}) { }
eco.artd.AbstractFirebase.prototype.constructor = eco.artd.AbstractFirebase
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
eco.artd.AbstractFirebase.class = /** @type {typeof eco.artd.AbstractFirebase} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...!eco.artd.IFirebase|typeof eco.artd.Firebase} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof eco.artd.Firebase}
 * @nosideeffects
 */
eco.artd.AbstractFirebase.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof eco.artd.AbstractFirebase}
 */
eco.artd.AbstractFirebase.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof eco.artd.Firebase}
 */
eco.artd.AbstractFirebase.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...!eco.artd.IFirebase|typeof eco.artd.Firebase} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof eco.artd.Firebase}
 */
eco.artd.AbstractFirebase.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...!eco.artd.IFirebase|typeof eco.artd.Firebase} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof eco.artd.Firebase}
 */
eco.artd.AbstractFirebase.__trait = function(...Implementations) {}

/**
 * A hyper slice includes all combined methods of an interface.
 * @interface eco.artd.IFirebaseJoinpointModelHyperslice
 */
eco.artd.IFirebaseJoinpointModelHyperslice = class {
  constructor() {
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeRun=/** @type {!eco.artd.IFirebaseJoinpointModel._beforeRun|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._beforeRun>} */ (void 0)
    /**
     * After the method.
     */
    this.afterRun=/** @type {!eco.artd.IFirebaseJoinpointModel._afterRun|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterRun>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterRunThrows=/** @type {!eco.artd.IFirebaseJoinpointModel._afterRunThrows|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterRunThrows>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterRunReturns=/** @type {!eco.artd.IFirebaseJoinpointModel._afterRunReturns|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterRunReturns>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterRunCancels=/** @type {!eco.artd.IFirebaseJoinpointModel._afterRunCancels|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterRunCancels>} */ (void 0)
    /**
     * Immediately after the async method returned its promise.
     */
    this.immediatelyAfterRun=/** @type {!eco.artd.IFirebaseJoinpointModel._immediatelyAfterRun|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._immediatelyAfterRun>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeGetId=/** @type {!eco.artd.IFirebaseJoinpointModel._beforeGetId|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._beforeGetId>} */ (void 0)
    /**
     * After the method.
     */
    this.afterGetId=/** @type {!eco.artd.IFirebaseJoinpointModel._afterGetId|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterGetId>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterGetIdThrows=/** @type {!eco.artd.IFirebaseJoinpointModel._afterGetIdThrows|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterGetIdThrows>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterGetIdReturns=/** @type {!eco.artd.IFirebaseJoinpointModel._afterGetIdReturns|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterGetIdReturns>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterGetIdCancels=/** @type {!eco.artd.IFirebaseJoinpointModel._afterGetIdCancels|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterGetIdCancels>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeGetLocalId=/** @type {!eco.artd.IFirebaseJoinpointModel._beforeGetLocalId|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._beforeGetLocalId>} */ (void 0)
    /**
     * After the method.
     */
    this.afterGetLocalId=/** @type {!eco.artd.IFirebaseJoinpointModel._afterGetLocalId|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterGetLocalId>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterGetLocalIdThrows=/** @type {!eco.artd.IFirebaseJoinpointModel._afterGetLocalIdThrows|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterGetLocalIdThrows>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterGetLocalIdReturns=/** @type {!eco.artd.IFirebaseJoinpointModel._afterGetLocalIdReturns|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterGetLocalIdReturns>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterGetLocalIdCancels=/** @type {!eco.artd.IFirebaseJoinpointModel._afterGetLocalIdCancels|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterGetLocalIdCancels>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeLoadRequire=/** @type {!eco.artd.IFirebaseJoinpointModel._beforeLoadRequire|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._beforeLoadRequire>} */ (void 0)
    /**
     * After the method.
     */
    this.afterLoadRequire=/** @type {!eco.artd.IFirebaseJoinpointModel._afterLoadRequire|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterLoadRequire>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterLoadRequireThrows=/** @type {!eco.artd.IFirebaseJoinpointModel._afterLoadRequireThrows|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterLoadRequireThrows>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterLoadRequireReturns=/** @type {!eco.artd.IFirebaseJoinpointModel._afterLoadRequireReturns|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterLoadRequireReturns>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterLoadRequireCancels=/** @type {!eco.artd.IFirebaseJoinpointModel._afterLoadRequireCancels|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterLoadRequireCancels>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeValidateMethod=/** @type {!eco.artd.IFirebaseJoinpointModel._beforeValidateMethod|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._beforeValidateMethod>} */ (void 0)
    /**
     * After the method.
     */
    this.afterValidateMethod=/** @type {!eco.artd.IFirebaseJoinpointModel._afterValidateMethod|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterValidateMethod>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterValidateMethodThrows=/** @type {!eco.artd.IFirebaseJoinpointModel._afterValidateMethodThrows|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterValidateMethodThrows>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterValidateMethodReturns=/** @type {!eco.artd.IFirebaseJoinpointModel._afterValidateMethodReturns|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterValidateMethodReturns>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterValidateMethodCancels=/** @type {!eco.artd.IFirebaseJoinpointModel._afterValidateMethodCancels|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterValidateMethodCancels>} */ (void 0)
  }
}
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.constructor = eco.artd.IFirebaseJoinpointModelHyperslice

/**
 * A concrete class of _IFirebaseJoinpointModelHyperslice_ instances.
 * @constructor eco.artd.FirebaseJoinpointModelHyperslice
 * @implements {eco.artd.IFirebaseJoinpointModelHyperslice} A hyper slice includes all combined methods of an interface.
 */
eco.artd.FirebaseJoinpointModelHyperslice = class extends eco.artd.IFirebaseJoinpointModelHyperslice { }
eco.artd.FirebaseJoinpointModelHyperslice.prototype.constructor = eco.artd.FirebaseJoinpointModelHyperslice

/**
 * A hyper-slice includes all combined methods of an interface.
 * @interface eco.artd.IFirebaseJoinpointModelBindingHyperslice
 * @template THIS
 */
eco.artd.IFirebaseJoinpointModelBindingHyperslice = class {
  constructor() {
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeRun=/** @type {!eco.artd.IFirebaseJoinpointModel.__beforeRun<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__beforeRun<THIS>>} */ (void 0)
    /**
     * After the method.
     */
    this.afterRun=/** @type {!eco.artd.IFirebaseJoinpointModel.__afterRun<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterRun<THIS>>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterRunThrows=/** @type {!eco.artd.IFirebaseJoinpointModel.__afterRunThrows<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterRunThrows<THIS>>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterRunReturns=/** @type {!eco.artd.IFirebaseJoinpointModel.__afterRunReturns<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterRunReturns<THIS>>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterRunCancels=/** @type {!eco.artd.IFirebaseJoinpointModel.__afterRunCancels<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterRunCancels<THIS>>} */ (void 0)
    /**
     * Immediately after the async method returned its promise.
     */
    this.immediatelyAfterRun=/** @type {!eco.artd.IFirebaseJoinpointModel.__immediatelyAfterRun<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__immediatelyAfterRun<THIS>>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeGetId=/** @type {!eco.artd.IFirebaseJoinpointModel.__beforeGetId<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__beforeGetId<THIS>>} */ (void 0)
    /**
     * After the method.
     */
    this.afterGetId=/** @type {!eco.artd.IFirebaseJoinpointModel.__afterGetId<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterGetId<THIS>>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterGetIdThrows=/** @type {!eco.artd.IFirebaseJoinpointModel.__afterGetIdThrows<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterGetIdThrows<THIS>>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterGetIdReturns=/** @type {!eco.artd.IFirebaseJoinpointModel.__afterGetIdReturns<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterGetIdReturns<THIS>>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterGetIdCancels=/** @type {!eco.artd.IFirebaseJoinpointModel.__afterGetIdCancels<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterGetIdCancels<THIS>>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeGetLocalId=/** @type {!eco.artd.IFirebaseJoinpointModel.__beforeGetLocalId<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__beforeGetLocalId<THIS>>} */ (void 0)
    /**
     * After the method.
     */
    this.afterGetLocalId=/** @type {!eco.artd.IFirebaseJoinpointModel.__afterGetLocalId<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterGetLocalId<THIS>>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterGetLocalIdThrows=/** @type {!eco.artd.IFirebaseJoinpointModel.__afterGetLocalIdThrows<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterGetLocalIdThrows<THIS>>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterGetLocalIdReturns=/** @type {!eco.artd.IFirebaseJoinpointModel.__afterGetLocalIdReturns<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterGetLocalIdReturns<THIS>>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterGetLocalIdCancels=/** @type {!eco.artd.IFirebaseJoinpointModel.__afterGetLocalIdCancels<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterGetLocalIdCancels<THIS>>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeLoadRequire=/** @type {!eco.artd.IFirebaseJoinpointModel.__beforeLoadRequire<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__beforeLoadRequire<THIS>>} */ (void 0)
    /**
     * After the method.
     */
    this.afterLoadRequire=/** @type {!eco.artd.IFirebaseJoinpointModel.__afterLoadRequire<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterLoadRequire<THIS>>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterLoadRequireThrows=/** @type {!eco.artd.IFirebaseJoinpointModel.__afterLoadRequireThrows<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterLoadRequireThrows<THIS>>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterLoadRequireReturns=/** @type {!eco.artd.IFirebaseJoinpointModel.__afterLoadRequireReturns<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterLoadRequireReturns<THIS>>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterLoadRequireCancels=/** @type {!eco.artd.IFirebaseJoinpointModel.__afterLoadRequireCancels<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterLoadRequireCancels<THIS>>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeValidateMethod=/** @type {!eco.artd.IFirebaseJoinpointModel.__beforeValidateMethod<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__beforeValidateMethod<THIS>>} */ (void 0)
    /**
     * After the method.
     */
    this.afterValidateMethod=/** @type {!eco.artd.IFirebaseJoinpointModel.__afterValidateMethod<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterValidateMethod<THIS>>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterValidateMethodThrows=/** @type {!eco.artd.IFirebaseJoinpointModel.__afterValidateMethodThrows<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterValidateMethodThrows<THIS>>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterValidateMethodReturns=/** @type {!eco.artd.IFirebaseJoinpointModel.__afterValidateMethodReturns<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterValidateMethodReturns<THIS>>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterValidateMethodCancels=/** @type {!eco.artd.IFirebaseJoinpointModel.__afterValidateMethodCancels<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterValidateMethodCancels<THIS>>} */ (void 0)
  }
}
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.constructor = eco.artd.IFirebaseJoinpointModelBindingHyperslice

/**
 * A concrete class of _IFirebaseJoinpointModelBindingHyperslice_ instances.
 * @constructor eco.artd.FirebaseJoinpointModelBindingHyperslice
 * @implements {eco.artd.IFirebaseJoinpointModelBindingHyperslice<THIS>} A hyper-slice includes all combined methods of an interface.
 * @template THIS
 * @extends {eco.artd.IFirebaseJoinpointModelBindingHyperslice<THIS>}
 */
eco.artd.FirebaseJoinpointModelBindingHyperslice = class extends eco.artd.IFirebaseJoinpointModelBindingHyperslice { }
eco.artd.FirebaseJoinpointModelBindingHyperslice.prototype.constructor = eco.artd.FirebaseJoinpointModelBindingHyperslice

/**
 * An interface that enumerates the joinpoints of `IFirebase`'s methods.
 * @interface eco.artd.IFirebaseJoinpointModel
 */
eco.artd.IFirebaseJoinpointModel = class { }
/** @type {eco.artd.IFirebaseJoinpointModel.beforeRun} */
eco.artd.IFirebaseJoinpointModel.prototype.beforeRun = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.afterRun} */
eco.artd.IFirebaseJoinpointModel.prototype.afterRun = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.afterRunThrows} */
eco.artd.IFirebaseJoinpointModel.prototype.afterRunThrows = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.afterRunReturns} */
eco.artd.IFirebaseJoinpointModel.prototype.afterRunReturns = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.afterRunCancels} */
eco.artd.IFirebaseJoinpointModel.prototype.afterRunCancels = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.immediatelyAfterRun} */
eco.artd.IFirebaseJoinpointModel.prototype.immediatelyAfterRun = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.beforeGetId} */
eco.artd.IFirebaseJoinpointModel.prototype.beforeGetId = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.afterGetId} */
eco.artd.IFirebaseJoinpointModel.prototype.afterGetId = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.afterGetIdThrows} */
eco.artd.IFirebaseJoinpointModel.prototype.afterGetIdThrows = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.afterGetIdReturns} */
eco.artd.IFirebaseJoinpointModel.prototype.afterGetIdReturns = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.afterGetIdCancels} */
eco.artd.IFirebaseJoinpointModel.prototype.afterGetIdCancels = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.beforeGetLocalId} */
eco.artd.IFirebaseJoinpointModel.prototype.beforeGetLocalId = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.afterGetLocalId} */
eco.artd.IFirebaseJoinpointModel.prototype.afterGetLocalId = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.afterGetLocalIdThrows} */
eco.artd.IFirebaseJoinpointModel.prototype.afterGetLocalIdThrows = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.afterGetLocalIdReturns} */
eco.artd.IFirebaseJoinpointModel.prototype.afterGetLocalIdReturns = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.afterGetLocalIdCancels} */
eco.artd.IFirebaseJoinpointModel.prototype.afterGetLocalIdCancels = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.beforeLoadRequire} */
eco.artd.IFirebaseJoinpointModel.prototype.beforeLoadRequire = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.afterLoadRequire} */
eco.artd.IFirebaseJoinpointModel.prototype.afterLoadRequire = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.afterLoadRequireThrows} */
eco.artd.IFirebaseJoinpointModel.prototype.afterLoadRequireThrows = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.afterLoadRequireReturns} */
eco.artd.IFirebaseJoinpointModel.prototype.afterLoadRequireReturns = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.afterLoadRequireCancels} */
eco.artd.IFirebaseJoinpointModel.prototype.afterLoadRequireCancels = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.beforeValidateMethod} */
eco.artd.IFirebaseJoinpointModel.prototype.beforeValidateMethod = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.afterValidateMethod} */
eco.artd.IFirebaseJoinpointModel.prototype.afterValidateMethod = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.afterValidateMethodThrows} */
eco.artd.IFirebaseJoinpointModel.prototype.afterValidateMethodThrows = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.afterValidateMethodReturns} */
eco.artd.IFirebaseJoinpointModel.prototype.afterValidateMethodReturns = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.afterValidateMethodCancels} */
eco.artd.IFirebaseJoinpointModel.prototype.afterValidateMethodCancels = function() {}

/**
 * A concrete class of _IFirebaseJoinpointModel_ instances.
 * @constructor eco.artd.FirebaseJoinpointModel
 * @implements {eco.artd.IFirebaseJoinpointModel} An interface that enumerates the joinpoints of `IFirebase`'s methods.
 */
eco.artd.FirebaseJoinpointModel = class extends eco.artd.IFirebaseJoinpointModel { }
eco.artd.FirebaseJoinpointModel.prototype.constructor = eco.artd.FirebaseJoinpointModel

/** @typedef {eco.artd.IFirebaseJoinpointModel} */
eco.artd.RecordIFirebaseJoinpointModel

/** @typedef {eco.artd.IFirebaseJoinpointModel} eco.artd.BoundIFirebaseJoinpointModel */

/** @typedef {eco.artd.FirebaseJoinpointModel} eco.artd.BoundFirebaseJoinpointModel */

/** @typedef {typeof __$te_plain} eco.artd.IFirebaseAspectsInstaller.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: eco.artd.FirebaseAspectsInstaller)} eco.artd.AbstractFirebaseAspectsInstaller.constructor */
/** @typedef {typeof eco.artd.FirebaseAspectsInstaller} eco.artd.FirebaseAspectsInstaller.typeof */
/**
 * An abstract class of `eco.artd.IFirebaseAspectsInstaller` interface.
 * @constructor eco.artd.AbstractFirebaseAspectsInstaller
 */
eco.artd.AbstractFirebaseAspectsInstaller = class extends /** @type {eco.artd.AbstractFirebaseAspectsInstaller.constructor&eco.artd.FirebaseAspectsInstaller.typeof} */ (class {}) { }
eco.artd.AbstractFirebaseAspectsInstaller.prototype.constructor = eco.artd.AbstractFirebaseAspectsInstaller
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
eco.artd.AbstractFirebaseAspectsInstaller.class = /** @type {typeof eco.artd.AbstractFirebaseAspectsInstaller} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...!eco.artd.IFirebaseAspectsInstaller|typeof eco.artd.FirebaseAspectsInstaller} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof eco.artd.FirebaseAspectsInstaller}
 * @nosideeffects
 */
eco.artd.AbstractFirebaseAspectsInstaller.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof eco.artd.AbstractFirebaseAspectsInstaller}
 */
eco.artd.AbstractFirebaseAspectsInstaller.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof eco.artd.FirebaseAspectsInstaller}
 */
eco.artd.AbstractFirebaseAspectsInstaller.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...!eco.artd.IFirebaseAspectsInstaller|typeof eco.artd.FirebaseAspectsInstaller} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof eco.artd.FirebaseAspectsInstaller}
 */
eco.artd.AbstractFirebaseAspectsInstaller.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...!eco.artd.IFirebaseAspectsInstaller|typeof eco.artd.FirebaseAspectsInstaller} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof eco.artd.FirebaseAspectsInstaller}
 */
eco.artd.AbstractFirebaseAspectsInstaller.__trait = function(...Implementations) {}

/** @typedef {new (...args: !eco.artd.IFirebaseAspectsInstaller.Initialese[]) => eco.artd.IFirebaseAspectsInstaller} eco.artd.FirebaseAspectsInstallerConstructor */

/** @typedef {function(new: engineering.type.IEngineer)} eco.artd.IFirebaseAspectsInstaller.constructor */
/** @typedef {typeof engineering.type.IEngineer} engineering.type.IEngineer.typeof */
/** @interface eco.artd.IFirebaseAspectsInstaller */
eco.artd.IFirebaseAspectsInstaller = class extends /** @type {eco.artd.IFirebaseAspectsInstaller.constructor&engineering.type.IEngineer.typeof} */ (class {}) {
  constructor() {
    super(...init)
    this.beforeRun=/** @type {number} */ (void 0)
    this.afterRun=/** @type {number} */ (void 0)
    this.afterRunThrows=/** @type {number} */ (void 0)
    this.afterRunReturns=/** @type {number} */ (void 0)
    this.afterRunCancels=/** @type {number} */ (void 0)
    this.immediateAfterRun=/** @type {number} */ (void 0)
    this.beforeGetId=/** @type {number} */ (void 0)
    this.afterGetId=/** @type {number} */ (void 0)
    this.afterGetIdThrows=/** @type {number} */ (void 0)
    this.afterGetIdReturns=/** @type {number} */ (void 0)
    this.afterGetIdCancels=/** @type {number} */ (void 0)
    this.beforeGetLocalId=/** @type {number} */ (void 0)
    this.afterGetLocalId=/** @type {number} */ (void 0)
    this.afterGetLocalIdThrows=/** @type {number} */ (void 0)
    this.afterGetLocalIdReturns=/** @type {number} */ (void 0)
    this.afterGetLocalIdCancels=/** @type {number} */ (void 0)
    this.beforeLoadRequire=/** @type {number} */ (void 0)
    this.afterLoadRequire=/** @type {number} */ (void 0)
    this.afterLoadRequireThrows=/** @type {number} */ (void 0)
    this.afterLoadRequireReturns=/** @type {number} */ (void 0)
    this.afterLoadRequireCancels=/** @type {number} */ (void 0)
    this.beforeValidateMethod=/** @type {number} */ (void 0)
    this.afterValidateMethod=/** @type {number} */ (void 0)
    this.afterValidateMethodThrows=/** @type {number} */ (void 0)
    this.afterValidateMethodReturns=/** @type {number} */ (void 0)
    this.afterValidateMethodCancels=/** @type {number} */ (void 0)
  }
  /**
   * A joinpointer.
   * @return {?}
   */
  run() { }
  /**
   * A joinpointer.
   * @return {void}
   */
  getId() { }
  /**
   * A joinpointer.
   * @return {void}
   */
  getLocalId() { }
  /**
   * A joinpointer.
   * @return {?}
   */
  loadRequire() { }
  /**
   * A joinpointer.
   * @return {?}
   */
  validateMethod() { }
}
/**
 * Create a new *IFirebaseAspectsInstaller* instance and automatically initialise it with options.
 * @param {...!eco.artd.IFirebaseAspectsInstaller.Initialese} init The initialisation options.
 */
eco.artd.IFirebaseAspectsInstaller.prototype.constructor = function(...init) {}

/** @typedef {function(new: eco.artd.IFirebaseAspectsInstaller&engineering.type.IInitialiser<!eco.artd.IFirebaseAspectsInstaller.Initialese>)} eco.artd.FirebaseAspectsInstaller.constructor */
/** @typedef {typeof eco.artd.IFirebaseAspectsInstaller} eco.artd.IFirebaseAspectsInstaller.typeof */
/** @typedef {typeof engineering.type.IInitialiser} engineering.type.IInitialiser.typeof */
/**
 * A concrete class of _IFirebaseAspectsInstaller_ instances.
 * @constructor eco.artd.FirebaseAspectsInstaller
 * @implements {eco.artd.IFirebaseAspectsInstaller} ‎
 * @implements {engineering.type.IInitialiser<!eco.artd.IFirebaseAspectsInstaller.Initialese>} ‎
 */
eco.artd.FirebaseAspectsInstaller = class extends /** @type {eco.artd.FirebaseAspectsInstaller.constructor&eco.artd.IFirebaseAspectsInstaller.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IFirebaseAspectsInstaller* instance and automatically initialise it with options.
   * @param {...!eco.artd.IFirebaseAspectsInstaller.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IFirebaseAspectsInstaller* instance and automatically initialise it with options.
 * @param {...!eco.artd.IFirebaseAspectsInstaller.Initialese} init The initialisation options.
 */
eco.artd.FirebaseAspectsInstaller.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof eco.artd.FirebaseAspectsInstaller}
 */
eco.artd.FirebaseAspectsInstaller.__extend = function(...Extensions) {}

/**
 * @typedef {Object} eco.artd.IFirebase.RunNArgs
 * @prop {!eco.artd.firebase.Config} conf The config to run the method against.
 */

/**
 * @typedef {Object} eco.artd.IFirebaseJoinpointModel.RunPointcutData The general pointcut data.
 * @prop {symbol} ticket A symbol unique to the particular execution.
 * @prop {eco.artd.IFirebase.RunNArgs} args The arguments passed to the method converted to a lambda-n record.
 * @prop {!Function} proc Allows to work with the process.
 */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.BeforeRunPointcutData
 * @prop {(arg0: !Object<string, *>) => void} cond Conditions the object to the process.
 * @prop {(args: eco.artd.IFirebase.RunNArgs) => void} setArgs Allows to set or modify arguments passed to the method and futher aspects.
 * @prop {(reason?: string) => void} cancel Allows to stop the `run` method from being executed.
 * @prop {(value: !Promise<string>) => void} sub Cancels a call to `run` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.BeforeRunPointcutData&eco.artd.IFirebaseJoinpointModel.RunPointcutData} eco.artd.IFirebaseJoinpointModel.BeforeRunPointcutData The pointcut data passed to _before_ aspects. */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.AfterRunPointcutData
 * @prop {string} res The return of the method after it's successfully run.
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.AfterRunPointcutData&eco.artd.IFirebaseJoinpointModel.RunPointcutData} eco.artd.IFirebaseJoinpointModel.AfterRunPointcutData The pointcut data passed to _after_ aspects. */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.AfterThrowsRunPointcutData
 * @prop {!Error} err The error with which the method failed.
 * @prop {() => void} hide Hides the error in the `run` (use only at the top-level of program flow).
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.AfterThrowsRunPointcutData&eco.artd.IFirebaseJoinpointModel.RunPointcutData} eco.artd.IFirebaseJoinpointModel.AfterThrowsRunPointcutData The pointcut data passed to _afterThrows_ aspects. */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.AfterReturnsRunPointcutData
 * @prop {string} res The return of the method after it's successfully run.
 * @prop {(value: !Promise<string>|string) => ?} sub Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.AfterReturnsRunPointcutData&eco.artd.IFirebaseJoinpointModel.RunPointcutData} eco.artd.IFirebaseJoinpointModel.AfterReturnsRunPointcutData The pointcut data passed to _afterReturns_ aspects. */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.AfterCancelsRunPointcutData
 * @prop {!Set<string>} reasons The list of reasons why the method was cancelled.
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.AfterCancelsRunPointcutData&eco.artd.IFirebaseJoinpointModel.RunPointcutData} eco.artd.IFirebaseJoinpointModel.AfterCancelsRunPointcutData The pointcut data passed to _afterCancels_ aspects. */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterRunPointcutData
 * @prop {!Promise<string>} promise The promise with the return of the method after it's successfully run.
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterRunPointcutData&eco.artd.IFirebaseJoinpointModel.RunPointcutData} eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterRunPointcutData The pointcut data passed to _immediatelyAfter_ aspects. */

/**
 * @typedef {Object} eco.artd.IFirebaseJoinpointModel.GetIdPointcutData The general pointcut data.
 * @prop {symbol} ticket A symbol unique to the particular execution.
 * @prop {!Function} proc Allows to work with the process.
 */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.BeforeGetIdPointcutData
 * @prop {(arg0: !Object<string, *>) => void} cond Conditions the object to the process.
 * @prop {(reason?: string) => void} cancel Allows to stop the `getId` method from being executed.
 * @prop {(value: number) => void} sub Cancels a call to `getId` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.BeforeGetIdPointcutData&eco.artd.IFirebaseJoinpointModel.GetIdPointcutData} eco.artd.IFirebaseJoinpointModel.BeforeGetIdPointcutData The pointcut data passed to _before_ aspects. */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.AfterGetIdPointcutData
 * @prop {number} res The return of the method after it's successfully run.
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.AfterGetIdPointcutData&eco.artd.IFirebaseJoinpointModel.GetIdPointcutData} eco.artd.IFirebaseJoinpointModel.AfterGetIdPointcutData The pointcut data passed to _after_ aspects. */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.AfterThrowsGetIdPointcutData
 * @prop {!Error} err The error with which the method failed.
 * @prop {() => void} hide Hides the error in the `getId` (use only at the top-level of program flow).
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.AfterThrowsGetIdPointcutData&eco.artd.IFirebaseJoinpointModel.GetIdPointcutData} eco.artd.IFirebaseJoinpointModel.AfterThrowsGetIdPointcutData The pointcut data passed to _afterThrows_ aspects. */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.AfterReturnsGetIdPointcutData
 * @prop {number} res The return of the method after it's successfully run.
 * @prop {(value: number) => ?} sub Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.AfterReturnsGetIdPointcutData&eco.artd.IFirebaseJoinpointModel.GetIdPointcutData} eco.artd.IFirebaseJoinpointModel.AfterReturnsGetIdPointcutData The pointcut data passed to _afterReturns_ aspects. */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.AfterCancelsGetIdPointcutData
 * @prop {!Set<string>} reasons The list of reasons why the method was cancelled.
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.AfterCancelsGetIdPointcutData&eco.artd.IFirebaseJoinpointModel.GetIdPointcutData} eco.artd.IFirebaseJoinpointModel.AfterCancelsGetIdPointcutData The pointcut data passed to _afterCancels_ aspects. */

/**
 * @typedef {Object} eco.artd.IFirebaseJoinpointModel.GetLocalIdPointcutData The general pointcut data.
 * @prop {symbol} ticket A symbol unique to the particular execution.
 * @prop {!Function} proc Allows to work with the process.
 */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.BeforeGetLocalIdPointcutData
 * @prop {(arg0: !Object<string, *>) => void} cond Conditions the object to the process.
 * @prop {(reason?: string) => void} cancel Allows to stop the `getLocalId` method from being executed.
 * @prop {(value: number) => void} sub Cancels a call to `getLocalId` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.BeforeGetLocalIdPointcutData&eco.artd.IFirebaseJoinpointModel.GetLocalIdPointcutData} eco.artd.IFirebaseJoinpointModel.BeforeGetLocalIdPointcutData The pointcut data passed to _before_ aspects. */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.AfterGetLocalIdPointcutData
 * @prop {number} res The return of the method after it's successfully run.
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.AfterGetLocalIdPointcutData&eco.artd.IFirebaseJoinpointModel.GetLocalIdPointcutData} eco.artd.IFirebaseJoinpointModel.AfterGetLocalIdPointcutData The pointcut data passed to _after_ aspects. */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.AfterThrowsGetLocalIdPointcutData
 * @prop {!Error} err The error with which the method failed.
 * @prop {() => void} hide Hides the error in the `getLocalId` (use only at the top-level of program flow).
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.AfterThrowsGetLocalIdPointcutData&eco.artd.IFirebaseJoinpointModel.GetLocalIdPointcutData} eco.artd.IFirebaseJoinpointModel.AfterThrowsGetLocalIdPointcutData The pointcut data passed to _afterThrows_ aspects. */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.AfterReturnsGetLocalIdPointcutData
 * @prop {number} res The return of the method after it's successfully run.
 * @prop {(value: number) => ?} sub Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.AfterReturnsGetLocalIdPointcutData&eco.artd.IFirebaseJoinpointModel.GetLocalIdPointcutData} eco.artd.IFirebaseJoinpointModel.AfterReturnsGetLocalIdPointcutData The pointcut data passed to _afterReturns_ aspects. */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.AfterCancelsGetLocalIdPointcutData
 * @prop {!Set<string>} reasons The list of reasons why the method was cancelled.
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.AfterCancelsGetLocalIdPointcutData&eco.artd.IFirebaseJoinpointModel.GetLocalIdPointcutData} eco.artd.IFirebaseJoinpointModel.AfterCancelsGetLocalIdPointcutData The pointcut data passed to _afterCancels_ aspects. */

/**
 * @typedef {Object} eco.artd.IFirebase.LoadRequireNArgs
 * @prop {string} path </prop>,<prop name="dir" type="string">
 */

/**
 * @typedef {Object} eco.artd.IFirebaseJoinpointModel.LoadRequirePointcutData The general pointcut data.
 * @prop {symbol} ticket A symbol unique to the particular execution.
 * @prop {eco.artd.IFirebase.LoadRequireNArgs} args The arguments passed to the method converted to a lambda-n record.
 * @prop {!Function} proc Allows to work with the process.
 */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.BeforeLoadRequirePointcutData
 * @prop {(arg0: !Object<string, *>) => void} cond Conditions the object to the process.
 * @prop {(args: eco.artd.IFirebase.LoadRequireNArgs) => void} setArgs Allows to set or modify arguments passed to the method and futher aspects.
 * @prop {(reason?: string) => void} cancel Allows to stop the `loadRequire` method from being executed.
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.BeforeLoadRequirePointcutData&eco.artd.IFirebaseJoinpointModel.LoadRequirePointcutData} eco.artd.IFirebaseJoinpointModel.BeforeLoadRequirePointcutData The pointcut data passed to _before_ aspects. */

/** @typedef {eco.artd.IFirebaseJoinpointModel.LoadRequirePointcutData} eco.artd.IFirebaseJoinpointModel.AfterLoadRequirePointcutData The pointcut data passed to _after_ aspects. */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.AfterThrowsLoadRequirePointcutData
 * @prop {!Error} err The error with which the method failed.
 * @prop {() => void} hide Hides the error in the `loadRequire` (use only at the top-level of program flow).
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.AfterThrowsLoadRequirePointcutData&eco.artd.IFirebaseJoinpointModel.LoadRequirePointcutData} eco.artd.IFirebaseJoinpointModel.AfterThrowsLoadRequirePointcutData The pointcut data passed to _afterThrows_ aspects. */

/** @typedef {eco.artd.IFirebaseJoinpointModel.LoadRequirePointcutData} eco.artd.IFirebaseJoinpointModel.AfterReturnsLoadRequirePointcutData The pointcut data passed to _afterReturns_ aspects. */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.AfterCancelsLoadRequirePointcutData
 * @prop {!Set<string>} reasons The list of reasons why the method was cancelled.
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.AfterCancelsLoadRequirePointcutData&eco.artd.IFirebaseJoinpointModel.LoadRequirePointcutData} eco.artd.IFirebaseJoinpointModel.AfterCancelsLoadRequirePointcutData The pointcut data passed to _afterCancels_ aspects. */

/**
 * @typedef {Object} eco.artd.IFirebase.ValidateMethodNArgs
 * @prop {http.IncomingMessage} req </prop>,<prop name="res" type="http.ServerResponse">
 * @prop {Map<string, string>} methodsMap
 */

/**
 * @typedef {Object} eco.artd.IFirebaseJoinpointModel.ValidateMethodPointcutData The general pointcut data.
 * @prop {symbol} ticket A symbol unique to the particular execution.
 * @prop {eco.artd.IFirebase.ValidateMethodNArgs} args The arguments passed to the method converted to a lambda-n record.
 * @prop {!Function} proc Allows to work with the process.
 */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.BeforeValidateMethodPointcutData
 * @prop {(arg0: !Object<string, *>) => void} cond Conditions the object to the process.
 * @prop {(args: eco.artd.IFirebase.ValidateMethodNArgs) => void} setArgs Allows to set or modify arguments passed to the method and futher aspects.
 * @prop {(reason?: string) => void} cancel Allows to stop the `validateMethod` method from being executed.
 * @prop {(value: string) => void} sub Cancels a call to `validateMethod` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.BeforeValidateMethodPointcutData&eco.artd.IFirebaseJoinpointModel.ValidateMethodPointcutData} eco.artd.IFirebaseJoinpointModel.BeforeValidateMethodPointcutData The pointcut data passed to _before_ aspects. */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.AfterValidateMethodPointcutData
 * @prop {string} res The return of the method after it's successfully run.
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.AfterValidateMethodPointcutData&eco.artd.IFirebaseJoinpointModel.ValidateMethodPointcutData} eco.artd.IFirebaseJoinpointModel.AfterValidateMethodPointcutData The pointcut data passed to _after_ aspects. */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.AfterThrowsValidateMethodPointcutData
 * @prop {!Error} err The error with which the method failed.
 * @prop {() => void} hide Hides the error in the `validateMethod` (use only at the top-level of program flow).
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.AfterThrowsValidateMethodPointcutData&eco.artd.IFirebaseJoinpointModel.ValidateMethodPointcutData} eco.artd.IFirebaseJoinpointModel.AfterThrowsValidateMethodPointcutData The pointcut data passed to _afterThrows_ aspects. */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.AfterReturnsValidateMethodPointcutData
 * @prop {string} res The return of the method after it's successfully run.
 * @prop {(value: string) => ?} sub Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.AfterReturnsValidateMethodPointcutData&eco.artd.IFirebaseJoinpointModel.ValidateMethodPointcutData} eco.artd.IFirebaseJoinpointModel.AfterReturnsValidateMethodPointcutData The pointcut data passed to _afterReturns_ aspects. */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.AfterCancelsValidateMethodPointcutData
 * @prop {!Set<string>} reasons The list of reasons why the method was cancelled.
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.AfterCancelsValidateMethodPointcutData&eco.artd.IFirebaseJoinpointModel.ValidateMethodPointcutData} eco.artd.IFirebaseJoinpointModel.AfterCancelsValidateMethodPointcutData The pointcut data passed to _afterCancels_ aspects. */

/** @typedef {new (...args: !eco.artd.IFirebase.Initialese[]) => eco.artd.IFirebase} eco.artd.FirebaseConstructor */

/** @typedef {function(new: eco.artd.BFirebaseAspectsCaster<THIS>&eco.artd.IFirebaseJoinpointModelBindingHyperslice<THIS>)} eco.artd.BFirebaseAspects.constructor */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModelBindingHyperslice} eco.artd.IFirebaseJoinpointModelBindingHyperslice.typeof */
/**
 * The aspects of the *IFirebase* that bind to an instance.
 * @interface eco.artd.BFirebaseAspects
 * @template THIS
 */
eco.artd.BFirebaseAspects = class extends /** @type {eco.artd.BFirebaseAspects.constructor&eco.artd.IFirebaseJoinpointModelBindingHyperslice.typeof} */ (class {}) { }
eco.artd.BFirebaseAspects.prototype.constructor = eco.artd.BFirebaseAspects

/** @typedef {typeof __$te_plain} eco.artd.IFirebaseAspects.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: eco.artd.FirebaseAspects)} eco.artd.AbstractFirebaseAspects.constructor */
/** @typedef {typeof eco.artd.FirebaseAspects} eco.artd.FirebaseAspects.typeof */
/**
 * An abstract class of `eco.artd.IFirebaseAspects` interface.
 * @constructor eco.artd.AbstractFirebaseAspects
 */
eco.artd.AbstractFirebaseAspects = class extends /** @type {eco.artd.AbstractFirebaseAspects.constructor&eco.artd.FirebaseAspects.typeof} */ (class {}) { }
eco.artd.AbstractFirebaseAspects.prototype.constructor = eco.artd.AbstractFirebaseAspects
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
eco.artd.AbstractFirebaseAspects.class = /** @type {typeof eco.artd.AbstractFirebaseAspects} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!eco.artd.IFirebaseAspects|typeof eco.artd.FirebaseAspects)|(!eco.artd.BFirebaseAspects|typeof eco.artd.BFirebaseAspects)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof eco.artd.FirebaseAspects}
 * @nosideeffects
 */
eco.artd.AbstractFirebaseAspects.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof eco.artd.AbstractFirebaseAspects}
 */
eco.artd.AbstractFirebaseAspects.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof eco.artd.FirebaseAspects}
 */
eco.artd.AbstractFirebaseAspects.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!eco.artd.IFirebaseAspects|typeof eco.artd.FirebaseAspects)|(!eco.artd.BFirebaseAspects|typeof eco.artd.BFirebaseAspects)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof eco.artd.FirebaseAspects}
 */
eco.artd.AbstractFirebaseAspects.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!eco.artd.IFirebaseAspects|typeof eco.artd.FirebaseAspects)|(!eco.artd.BFirebaseAspects|typeof eco.artd.BFirebaseAspects)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof eco.artd.FirebaseAspects}
 */
eco.artd.AbstractFirebaseAspects.__trait = function(...Implementations) {}

/** @typedef {new (...args: !eco.artd.IFirebaseAspects.Initialese[]) => eco.artd.IFirebaseAspects} eco.artd.FirebaseAspectsConstructor */

/** @typedef {function(new: engineering.type.IEngineer&eco.artd.IFirebaseAspectsCaster&eco.artd.BFirebaseAspects<!eco.artd.IFirebaseAspects>)} eco.artd.IFirebaseAspects.constructor */
/** @typedef {typeof eco.artd.BFirebaseAspects} eco.artd.BFirebaseAspects.typeof */
/**
 * The aspects of the *IFirebase*.
 * @interface eco.artd.IFirebaseAspects
 */
eco.artd.IFirebaseAspects = class extends /** @type {eco.artd.IFirebaseAspects.constructor&engineering.type.IEngineer.typeof&eco.artd.BFirebaseAspects.typeof} */ (class {}) {
}
/**
 * Create a new *IFirebaseAspects* instance and automatically initialise it with options.
 * @param {...!eco.artd.IFirebaseAspects.Initialese} init The initialisation options.
 */
eco.artd.IFirebaseAspects.prototype.constructor = function(...init) {}

/** @typedef {function(new: eco.artd.IFirebaseAspects&engineering.type.IInitialiser<!eco.artd.IFirebaseAspects.Initialese>)} eco.artd.FirebaseAspects.constructor */
/** @typedef {typeof eco.artd.IFirebaseAspects} eco.artd.IFirebaseAspects.typeof */
/**
 * A concrete class of _IFirebaseAspects_ instances.
 * @constructor eco.artd.FirebaseAspects
 * @implements {eco.artd.IFirebaseAspects} The aspects of the *IFirebase*.
 * @implements {engineering.type.IInitialiser<!eco.artd.IFirebaseAspects.Initialese>} ‎
 */
eco.artd.FirebaseAspects = class extends /** @type {eco.artd.FirebaseAspects.constructor&eco.artd.IFirebaseAspects.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IFirebaseAspects* instance and automatically initialise it with options.
   * @param {...!eco.artd.IFirebaseAspects.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IFirebaseAspects* instance and automatically initialise it with options.
 * @param {...!eco.artd.IFirebaseAspects.Initialese} init The initialisation options.
 */
eco.artd.FirebaseAspects.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof eco.artd.FirebaseAspects}
 */
eco.artd.FirebaseAspects.__extend = function(...Extensions) {}

/**
 * Contains getters to cast the _BFirebaseAspects_ interface.
 * @interface eco.artd.BFirebaseAspectsCaster
 * @template THIS
 */
eco.artd.BFirebaseAspectsCaster = class { }
/**
 * Cast the _BFirebaseAspects_ instance into the _BoundIFirebase_ type.
 * @type {!eco.artd.BoundIFirebase}
 */
eco.artd.BFirebaseAspectsCaster.prototype.asIFirebase

/**
 * Contains getters to cast the _IFirebaseAspects_ interface.
 * @interface eco.artd.IFirebaseAspectsCaster
 */
eco.artd.IFirebaseAspectsCaster = class { }
/**
 * Cast the _IFirebaseAspects_ instance into the _BoundIFirebase_ type.
 * @type {!eco.artd.BoundIFirebase}
 */
eco.artd.IFirebaseAspectsCaster.prototype.asIFirebase

/** @typedef {eco.artd.IFirebase.Initialese} eco.artd.IHyperFirebase.Initialese */

/** @typedef {function(new: eco.artd.HyperFirebase)} eco.artd.AbstractHyperFirebase.constructor */
/** @typedef {typeof eco.artd.HyperFirebase} eco.artd.HyperFirebase.typeof */
/**
 * An abstract class of `eco.artd.IHyperFirebase` interface.
 * @constructor eco.artd.AbstractHyperFirebase
 */
eco.artd.AbstractHyperFirebase = class extends /** @type {eco.artd.AbstractHyperFirebase.constructor&eco.artd.HyperFirebase.typeof} */ (class {}) { }
eco.artd.AbstractHyperFirebase.prototype.constructor = eco.artd.AbstractHyperFirebase
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
eco.artd.AbstractHyperFirebase.class = /** @type {typeof eco.artd.AbstractHyperFirebase} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!eco.artd.IHyperFirebase|typeof eco.artd.HyperFirebase)|(!eco.artd.IFirebase|typeof eco.artd.Firebase)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof eco.artd.HyperFirebase}
 * @nosideeffects
 */
eco.artd.AbstractHyperFirebase.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof eco.artd.AbstractHyperFirebase}
 */
eco.artd.AbstractHyperFirebase.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof eco.artd.HyperFirebase}
 */
eco.artd.AbstractHyperFirebase.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!eco.artd.IHyperFirebase|typeof eco.artd.HyperFirebase)|(!eco.artd.IFirebase|typeof eco.artd.Firebase)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof eco.artd.HyperFirebase}
 */
eco.artd.AbstractHyperFirebase.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!eco.artd.IHyperFirebase|typeof eco.artd.HyperFirebase)|(!eco.artd.IFirebase|typeof eco.artd.Firebase)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof eco.artd.HyperFirebase}
 */
eco.artd.AbstractHyperFirebase.__trait = function(...Implementations) {}
/**
 * Consult aides for their advice on methods to be implemented, so that all implementation methods provided on subsequent `.implements` calls will be advised according to this consultation.
 * @param {...!eco.artd.IFirebaseAspects|!Array<!eco.artd.IFirebaseAspects>|function(new: eco.artd.IFirebaseAspects)|!Function|!Array<!Function>|void|null} aides The list of aides that advise the IFirebase to implement aspects.
 * @return {typeof eco.artd.AbstractHyperFirebase}
 * @nosideeffects
 */
eco.artd.AbstractHyperFirebase.consults = function(...aides) {}
/**
 * Creates a new abstract class which extends passed hyper-classes by installing
 * their aspects and implementations.
 * @param {...!Function|!Array<!Function>|void|null} hypers The list of hyper classes.
 * @return {typeof eco.artd.AbstractHyperFirebase}
 * @nosideeffects
 */
eco.artd.AbstractHyperFirebase.extends = function(...hypers) {}
/**
 * Adds the aspects installers in order for advices to work.
 * @param {...!Function|!Array<!Function>|void|null} aspectsInstallers The list of aspects installers.
 * @return {typeof eco.artd.AbstractHyperFirebase}
 * @nosideeffects
 */
eco.artd.AbstractHyperFirebase.installs = function(...aspectsInstallers) {}

/** @typedef {new (...args: !eco.artd.IHyperFirebase.Initialese[]) => eco.artd.IHyperFirebase} eco.artd.HyperFirebaseConstructor */

/** @typedef {function(new: engineering.type.IEngineer&eco.artd.IHyperFirebaseCaster&eco.artd.IFirebase)} eco.artd.IHyperFirebase.constructor */
/** @typedef {typeof eco.artd.IFirebase} eco.artd.IFirebase.typeof */
/** @interface eco.artd.IHyperFirebase */
eco.artd.IHyperFirebase = class extends /** @type {eco.artd.IHyperFirebase.constructor&engineering.type.IEngineer.typeof&eco.artd.IFirebase.typeof} */ (class {}) {
}
/**
 * Create a new *IHyperFirebase* instance and automatically initialise it with options.
 * @param {...!eco.artd.IHyperFirebase.Initialese} init The initialisation options.
 */
eco.artd.IHyperFirebase.prototype.constructor = function(...init) {}

/** @typedef {function(new: eco.artd.IHyperFirebase&engineering.type.IInitialiser<!eco.artd.IHyperFirebase.Initialese>)} eco.artd.HyperFirebase.constructor */
/** @typedef {typeof eco.artd.IHyperFirebase} eco.artd.IHyperFirebase.typeof */
/**
 * A concrete class of _IHyperFirebase_ instances.
 * @constructor eco.artd.HyperFirebase
 * @implements {eco.artd.IHyperFirebase} ‎
 * @implements {engineering.type.IInitialiser<!eco.artd.IHyperFirebase.Initialese>} ‎
 */
eco.artd.HyperFirebase = class extends /** @type {eco.artd.HyperFirebase.constructor&eco.artd.IHyperFirebase.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IHyperFirebase* instance and automatically initialise it with options.
   * @param {...!eco.artd.IHyperFirebase.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IHyperFirebase* instance and automatically initialise it with options.
 * @param {...!eco.artd.IHyperFirebase.Initialese} init The initialisation options.
 */
eco.artd.HyperFirebase.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof eco.artd.HyperFirebase}
 */
eco.artd.HyperFirebase.__extend = function(...Extensions) {}

/** @typedef {eco.artd.IHyperFirebase} */
eco.artd.RecordIHyperFirebase

/** @typedef {eco.artd.IHyperFirebase} eco.artd.BoundIHyperFirebase */

/** @typedef {eco.artd.HyperFirebase} eco.artd.BoundHyperFirebase */

/**
 * Contains getters to cast the _IHyperFirebase_ interface.
 * @interface eco.artd.IHyperFirebaseCaster
 */
eco.artd.IHyperFirebaseCaster = class { }
/**
 * Cast the _IHyperFirebase_ instance into the _BoundIHyperFirebase_ type.
 * @type {!eco.artd.BoundIHyperFirebase}
 */
eco.artd.IHyperFirebaseCaster.prototype.asIHyperFirebase
/**
 * Access the _HyperFirebase_ prototype.
 * @type {!eco.artd.BoundHyperFirebase}
 */
eco.artd.IHyperFirebaseCaster.prototype.superHyperFirebase

/** @typedef {function(new: eco.artd.IFirebaseFields&engineering.type.IEngineer&eco.artd.IFirebaseCaster)} eco.artd.IFirebase.constructor */
/**
 * Various firebase utils and runtime to download JS files from HTTP hosting for execution in functions (helps to eliminate reploy every so often).
 * @interface eco.artd.IFirebase
 */
eco.artd.IFirebase = class extends /** @type {eco.artd.IFirebase.constructor&engineering.type.IEngineer.typeof} */ (class {}) {
}
/**
 * Create a new *IFirebase* instance and automatically initialise it with options.
 * @param {...!eco.artd.IFirebase.Initialese} init The initialisation options.
 */
eco.artd.IFirebase.prototype.constructor = function(...init) {}
/** @type {eco.artd.IFirebase.run} */
eco.artd.IFirebase.prototype.run = function() {}
/** @type {eco.artd.IFirebase.getId} */
eco.artd.IFirebase.prototype.getId = function() {}
/** @type {eco.artd.IFirebase.getLocalId} */
eco.artd.IFirebase.prototype.getLocalId = function() {}
/** @type {eco.artd.IFirebase.loadRequire} */
eco.artd.IFirebase.prototype.loadRequire = function() {}
/** @type {eco.artd.IFirebase.validateMethod} */
eco.artd.IFirebase.prototype.validateMethod = function() {}

/** @typedef {function(new: eco.artd.IFirebase&engineering.type.IInitialiser<!eco.artd.IFirebase.Initialese>)} eco.artd.Firebase.constructor */
/**
 * A concrete class of _IFirebase_ instances.
 * @constructor eco.artd.Firebase
 * @implements {eco.artd.IFirebase} Various firebase utils and runtime to download JS files from HTTP hosting for execution in functions (helps to eliminate reploy every so often).
 * @implements {engineering.type.IInitialiser<!eco.artd.IFirebase.Initialese>} ‎
 */
eco.artd.Firebase = class extends /** @type {eco.artd.Firebase.constructor&eco.artd.IFirebase.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IFirebase* instance and automatically initialise it with options.
   * @param {...!eco.artd.IFirebase.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IFirebase* instance and automatically initialise it with options.
 * @param {...!eco.artd.IFirebase.Initialese} init The initialisation options.
 */
eco.artd.Firebase.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof eco.artd.Firebase}
 */
eco.artd.Firebase.__extend = function(...Extensions) {}

/**
 * Fields of the IFirebase.
 * @interface eco.artd.IFirebaseFields
 */
eco.artd.IFirebaseFields = class { }
/**
 * An example property. Default `ok`.
 */
eco.artd.IFirebaseFields.prototype.example = /** @type {string} */ (void 0)
/**
 * A private symbol. Default empty string.
 */
eco.artd.IFirebaseFields.prototype.symbol = /** @type {string} */ (void 0)
/**
 * The logger. Default `null`.
 */
eco.artd.IFirebaseFields.prototype.logger = /** @type {{ info: function() }} */ (void 0)

/** @typedef {eco.artd.IFirebase} */
eco.artd.RecordIFirebase

/** @typedef {eco.artd.IFirebase} eco.artd.BoundIFirebase */

/** @typedef {eco.artd.Firebase} eco.artd.BoundFirebase */

/**
 * Contains getters to cast the _IFirebase_ interface.
 * @interface eco.artd.IFirebaseCaster
 */
eco.artd.IFirebaseCaster = class { }
/**
 * Cast the _IFirebase_ instance into the _BoundIFirebase_ type.
 * @type {!eco.artd.BoundIFirebase}
 */
eco.artd.IFirebaseCaster.prototype.asIFirebase
/**
 * Access the _Firebase_ prototype.
 * @type {!eco.artd.BoundFirebase}
 */
eco.artd.IFirebaseCaster.prototype.superFirebase

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.BeforeRunPointcutData) => void} eco.artd.IFirebaseJoinpointModel.__beforeRun
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__beforeRun<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._beforeRun */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.beforeRun} */
/**
 * Before the method executes. Allows to cancel the method. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.BeforeRunPointcutData} [data] Metadata passed to the pointcuts of _run_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: IFirebase.RunNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `run` method from being executed.
 * - `sub` _(value: !Promise&lt;string&gt;) =&gt; void_ Cancels a call to `run` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.RunPointcutData*
 * - `args` _IFirebase.RunNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.RunPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.RunPointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.beforeRun = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.AfterRunPointcutData) => void} eco.artd.IFirebaseJoinpointModel.__afterRun
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__afterRun<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._afterRun */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.afterRun} */
/**
 * After the method. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterRunPointcutData} [data] Metadata passed to the pointcuts of _run_ at the `after` joinpoint.
 * - `res` _string_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.RunPointcutData*
 * - `args` _IFirebase.RunNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.RunPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.RunPointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.afterRun = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.AfterThrowsRunPointcutData) => void} eco.artd.IFirebaseJoinpointModel.__afterRunThrows
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__afterRunThrows<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._afterRunThrows */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.afterRunThrows} */
/**
 * After the method throw. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterThrowsRunPointcutData} [data] Metadata passed to the pointcuts of _run_ at the `afterThrows` joinpoint.
 * - `err` _!Error_ The error with which the method failed.
 * - `hide` _() =&gt; void_ Hides the error in the `run` (use only at the top-level of program flow).
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.RunPointcutData*
 * - `args` _IFirebase.RunNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.RunPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.RunPointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.afterRunThrows = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.AfterReturnsRunPointcutData) => void} eco.artd.IFirebaseJoinpointModel.__afterRunReturns
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__afterRunReturns<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._afterRunReturns */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.afterRunReturns} */
/**
 * After the method returned. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterReturnsRunPointcutData} [data] Metadata passed to the pointcuts of _run_ at the `afterReturns` joinpoint.
 * - `res` _string_ The return of the method after it's successfully run.
 * - `sub` _(value: !Promise&lt;string&gt;&vert;string) =&gt; ?_ Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.RunPointcutData*
 * - `args` _IFirebase.RunNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.RunPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.RunPointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.afterRunReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.AfterCancelsRunPointcutData) => void} eco.artd.IFirebaseJoinpointModel.__afterRunCancels
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__afterRunCancels<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._afterRunCancels */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.afterRunCancels} */
/**
 * After the method cancels. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterCancelsRunPointcutData} [data] Metadata passed to the pointcuts of _run_ at the `afterCancels` joinpoint.
 * - `reasons` _!Set&lt;string&gt;_ The list of reasons why the method was cancelled.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.RunPointcutData*
 * - `args` _IFirebase.RunNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.RunPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.RunPointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.afterRunCancels = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterRunPointcutData) => void} eco.artd.IFirebaseJoinpointModel.__immediatelyAfterRun
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__immediatelyAfterRun<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._immediatelyAfterRun */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.immediatelyAfterRun} */
/**
 * Immediately after the async method returned its promise. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterRunPointcutData} [data] Metadata passed to the pointcuts of _run_ at the `immediatelyAfter` joinpoint.
 * - `promise` _!Promise&lt;string&gt;_ The promise with the return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.RunPointcutData*
 * - `args` _IFirebase.RunNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.RunPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.RunPointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.immediatelyAfterRun = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.BeforeGetIdPointcutData) => void} eco.artd.IFirebaseJoinpointModel.__beforeGetId
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__beforeGetId<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._beforeGetId */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.beforeGetId} */
/**
 * Before the method executes. Allows to cancel the method. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.BeforeGetIdPointcutData} [data] Metadata passed to the pointcuts of _getId_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `getId` method from being executed.
 * - `sub` _(value: number) =&gt; void_ Cancels a call to `getId` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.GetIdPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.GetIdPointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.beforeGetId = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.AfterGetIdPointcutData) => void} eco.artd.IFirebaseJoinpointModel.__afterGetId
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__afterGetId<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._afterGetId */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.afterGetId} */
/**
 * After the method. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterGetIdPointcutData} [data] Metadata passed to the pointcuts of _getId_ at the `after` joinpoint.
 * - `res` _number_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.GetIdPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.GetIdPointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.afterGetId = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.AfterThrowsGetIdPointcutData) => void} eco.artd.IFirebaseJoinpointModel.__afterGetIdThrows
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__afterGetIdThrows<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._afterGetIdThrows */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.afterGetIdThrows} */
/**
 * After the method throw. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterThrowsGetIdPointcutData} [data] Metadata passed to the pointcuts of _getId_ at the `afterThrows` joinpoint.
 * - `err` _!Error_ The error with which the method failed.
 * - `hide` _() =&gt; void_ Hides the error in the `getId` (use only at the top-level of program flow).
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.GetIdPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.GetIdPointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.afterGetIdThrows = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.AfterReturnsGetIdPointcutData) => void} eco.artd.IFirebaseJoinpointModel.__afterGetIdReturns
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__afterGetIdReturns<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._afterGetIdReturns */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.afterGetIdReturns} */
/**
 * After the method returned. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterReturnsGetIdPointcutData} [data] Metadata passed to the pointcuts of _getId_ at the `afterReturns` joinpoint.
 * - `res` _number_ The return of the method after it's successfully run.
 * - `sub` _(value: number) =&gt; ?_ Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.GetIdPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.GetIdPointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.afterGetIdReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.AfterCancelsGetIdPointcutData) => void} eco.artd.IFirebaseJoinpointModel.__afterGetIdCancels
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__afterGetIdCancels<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._afterGetIdCancels */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.afterGetIdCancels} */
/**
 * After the method cancels. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterCancelsGetIdPointcutData} [data] Metadata passed to the pointcuts of _getId_ at the `afterCancels` joinpoint.
 * - `reasons` _!Set&lt;string&gt;_ The list of reasons why the method was cancelled.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.GetIdPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.GetIdPointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.afterGetIdCancels = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.BeforeGetLocalIdPointcutData) => void} eco.artd.IFirebaseJoinpointModel.__beforeGetLocalId
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__beforeGetLocalId<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._beforeGetLocalId */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.beforeGetLocalId} */
/**
 * Before the method executes. Allows to cancel the method. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.BeforeGetLocalIdPointcutData} [data] Metadata passed to the pointcuts of _getLocalId_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `getLocalId` method from being executed.
 * - `sub` _(value: number) =&gt; void_ Cancels a call to `getLocalId` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.GetLocalIdPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.GetLocalIdPointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.beforeGetLocalId = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.AfterGetLocalIdPointcutData) => void} eco.artd.IFirebaseJoinpointModel.__afterGetLocalId
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__afterGetLocalId<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._afterGetLocalId */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.afterGetLocalId} */
/**
 * After the method. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterGetLocalIdPointcutData} [data] Metadata passed to the pointcuts of _getLocalId_ at the `after` joinpoint.
 * - `res` _number_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.GetLocalIdPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.GetLocalIdPointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.afterGetLocalId = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.AfterThrowsGetLocalIdPointcutData) => void} eco.artd.IFirebaseJoinpointModel.__afterGetLocalIdThrows
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__afterGetLocalIdThrows<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._afterGetLocalIdThrows */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.afterGetLocalIdThrows} */
/**
 * After the method throw. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterThrowsGetLocalIdPointcutData} [data] Metadata passed to the pointcuts of _getLocalId_ at the `afterThrows` joinpoint.
 * - `err` _!Error_ The error with which the method failed.
 * - `hide` _() =&gt; void_ Hides the error in the `getLocalId` (use only at the top-level of program flow).
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.GetLocalIdPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.GetLocalIdPointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.afterGetLocalIdThrows = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.AfterReturnsGetLocalIdPointcutData) => void} eco.artd.IFirebaseJoinpointModel.__afterGetLocalIdReturns
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__afterGetLocalIdReturns<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._afterGetLocalIdReturns */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.afterGetLocalIdReturns} */
/**
 * After the method returned. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterReturnsGetLocalIdPointcutData} [data] Metadata passed to the pointcuts of _getLocalId_ at the `afterReturns` joinpoint.
 * - `res` _number_ The return of the method after it's successfully run.
 * - `sub` _(value: number) =&gt; ?_ Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.GetLocalIdPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.GetLocalIdPointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.afterGetLocalIdReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.AfterCancelsGetLocalIdPointcutData) => void} eco.artd.IFirebaseJoinpointModel.__afterGetLocalIdCancels
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__afterGetLocalIdCancels<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._afterGetLocalIdCancels */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.afterGetLocalIdCancels} */
/**
 * After the method cancels. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterCancelsGetLocalIdPointcutData} [data] Metadata passed to the pointcuts of _getLocalId_ at the `afterCancels` joinpoint.
 * - `reasons` _!Set&lt;string&gt;_ The list of reasons why the method was cancelled.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.GetLocalIdPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.GetLocalIdPointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.afterGetLocalIdCancels = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.BeforeLoadRequirePointcutData) => void} eco.artd.IFirebaseJoinpointModel.__beforeLoadRequire
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__beforeLoadRequire<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._beforeLoadRequire */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.beforeLoadRequire} */
/**
 * Before the method executes. Allows to cancel the method. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.BeforeLoadRequirePointcutData} [data] Metadata passed to the pointcuts of _loadRequire_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: IFirebase.LoadRequireNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `loadRequire` method from being executed.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.LoadRequirePointcutData*
 * - `args` _IFirebase.LoadRequireNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.LoadRequirePointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.LoadRequirePointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.beforeLoadRequire = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.AfterLoadRequirePointcutData) => void} eco.artd.IFirebaseJoinpointModel.__afterLoadRequire
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__afterLoadRequire<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._afterLoadRequire */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.afterLoadRequire} */
/**
 * After the method. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterLoadRequirePointcutData} [data] Metadata passed to the pointcuts of _loadRequire_ at the `after` joinpoint.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.LoadRequirePointcutData*
 * - `args` _IFirebase.LoadRequireNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.LoadRequirePointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.LoadRequirePointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.afterLoadRequire = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.AfterThrowsLoadRequirePointcutData) => void} eco.artd.IFirebaseJoinpointModel.__afterLoadRequireThrows
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__afterLoadRequireThrows<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._afterLoadRequireThrows */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.afterLoadRequireThrows} */
/**
 * After the method throw. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterThrowsLoadRequirePointcutData} [data] Metadata passed to the pointcuts of _loadRequire_ at the `afterThrows` joinpoint.
 * - `err` _!Error_ The error with which the method failed.
 * - `hide` _() =&gt; void_ Hides the error in the `loadRequire` (use only at the top-level of program flow).
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.LoadRequirePointcutData*
 * - `args` _IFirebase.LoadRequireNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.LoadRequirePointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.LoadRequirePointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.afterLoadRequireThrows = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.AfterReturnsLoadRequirePointcutData) => void} eco.artd.IFirebaseJoinpointModel.__afterLoadRequireReturns
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__afterLoadRequireReturns<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._afterLoadRequireReturns */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.afterLoadRequireReturns} */
/**
 * After the method returned. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterReturnsLoadRequirePointcutData} [data] Metadata passed to the pointcuts of _loadRequire_ at the `afterReturns` joinpoint.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.LoadRequirePointcutData*
 * - `args` _IFirebase.LoadRequireNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.LoadRequirePointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.LoadRequirePointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.afterLoadRequireReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.AfterCancelsLoadRequirePointcutData) => void} eco.artd.IFirebaseJoinpointModel.__afterLoadRequireCancels
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__afterLoadRequireCancels<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._afterLoadRequireCancels */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.afterLoadRequireCancels} */
/**
 * After the method cancels. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterCancelsLoadRequirePointcutData} [data] Metadata passed to the pointcuts of _loadRequire_ at the `afterCancels` joinpoint.
 * - `reasons` _!Set&lt;string&gt;_ The list of reasons why the method was cancelled.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.LoadRequirePointcutData*
 * - `args` _IFirebase.LoadRequireNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.LoadRequirePointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.LoadRequirePointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.afterLoadRequireCancels = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.BeforeValidateMethodPointcutData) => void} eco.artd.IFirebaseJoinpointModel.__beforeValidateMethod
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__beforeValidateMethod<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._beforeValidateMethod */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.beforeValidateMethod} */
/**
 * Before the method executes. Allows to cancel the method. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.BeforeValidateMethodPointcutData} [data] Metadata passed to the pointcuts of _validateMethod_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: IFirebase.ValidateMethodNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `validateMethod` method from being executed.
 * - `sub` _(value: string) =&gt; void_ Cancels a call to `validateMethod` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.ValidateMethodPointcutData*
 * - `args` _IFirebase.ValidateMethodNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.ValidateMethodPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.ValidateMethodPointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.beforeValidateMethod = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.AfterValidateMethodPointcutData) => void} eco.artd.IFirebaseJoinpointModel.__afterValidateMethod
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__afterValidateMethod<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._afterValidateMethod */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.afterValidateMethod} */
/**
 * After the method. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterValidateMethodPointcutData} [data] Metadata passed to the pointcuts of _validateMethod_ at the `after` joinpoint.
 * - `res` _string_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.ValidateMethodPointcutData*
 * - `args` _IFirebase.ValidateMethodNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.ValidateMethodPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.ValidateMethodPointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.afterValidateMethod = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.AfterThrowsValidateMethodPointcutData) => void} eco.artd.IFirebaseJoinpointModel.__afterValidateMethodThrows
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__afterValidateMethodThrows<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._afterValidateMethodThrows */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.afterValidateMethodThrows} */
/**
 * After the method throw. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterThrowsValidateMethodPointcutData} [data] Metadata passed to the pointcuts of _validateMethod_ at the `afterThrows` joinpoint.
 * - `err` _!Error_ The error with which the method failed.
 * - `hide` _() =&gt; void_ Hides the error in the `validateMethod` (use only at the top-level of program flow).
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.ValidateMethodPointcutData*
 * - `args` _IFirebase.ValidateMethodNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.ValidateMethodPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.ValidateMethodPointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.afterValidateMethodThrows = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.AfterReturnsValidateMethodPointcutData) => void} eco.artd.IFirebaseJoinpointModel.__afterValidateMethodReturns
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__afterValidateMethodReturns<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._afterValidateMethodReturns */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.afterValidateMethodReturns} */
/**
 * After the method returned. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterReturnsValidateMethodPointcutData} [data] Metadata passed to the pointcuts of _validateMethod_ at the `afterReturns` joinpoint.
 * - `res` _string_ The return of the method after it's successfully run.
 * - `sub` _(value: string) =&gt; ?_ Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.ValidateMethodPointcutData*
 * - `args` _IFirebase.ValidateMethodNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.ValidateMethodPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.ValidateMethodPointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.afterValidateMethodReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.AfterCancelsValidateMethodPointcutData) => void} eco.artd.IFirebaseJoinpointModel.__afterValidateMethodCancels
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__afterValidateMethodCancels<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._afterValidateMethodCancels */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.afterValidateMethodCancels} */
/**
 * After the method cancels. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterCancelsValidateMethodPointcutData} [data] Metadata passed to the pointcuts of _validateMethod_ at the `afterCancels` joinpoint.
 * - `reasons` _!Set&lt;string&gt;_ The list of reasons why the method was cancelled.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.ValidateMethodPointcutData*
 * - `args` _IFirebase.ValidateMethodNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.ValidateMethodPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.ValidateMethodPointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.afterValidateMethodCancels = function(data) {}

/** @typedef {typeof eco.artd.FirebaseFactory} */
/**
 * Produces a configured hyper class.
 * @param {!eco.artd.FirebaseFactory.Config} [config] The config.
 * - `[aspectsInstallers]` _!Array?_
 * - `[subjects]` _!Array&lt;IFirebase&gt;?_
 * - `[hypers]` _!Array?_
 * - `[aspects]` _!Array&lt;!IFirebaseAspects&gt;&vert;!IFirebaseAspects?_
 * @return {typeof eco.artd.HyperFirebase}
 */
eco.artd.FirebaseFactory = function(config) {}

/**
 * @typedef {Object} eco.artd.FirebaseFactory.Config The config.
 * @prop {!Array} [aspectsInstallers]
 * @prop {!Array<eco.artd.IFirebase>} [subjects]
 * @prop {!Array} [hypers]
 * @prop {!Array<!eco.artd.IFirebaseAspects>|!eco.artd.IFirebaseAspects} [aspects]
 */

/**
 * @typedef {(this: THIS, conf: !eco.artd.firebase.Config) => !Promise<string>} eco.artd.IFirebase.__run
 * @template THIS
 */
/** @typedef {eco.artd.IFirebase.__run<!eco.artd.IFirebase>} eco.artd.IFirebase._run */
/** @typedef {typeof eco.artd.IFirebase.run} */
/**
 * Executes the main method via the class.
 * @param {!eco.artd.firebase.Config} conf The config to run the method against.
 * - `[shouldRun=true]` _boolean?_ A boolean option. Default `true`.
 * - `[text]` _string?_ A text to return.
 * @return {!Promise<string>} The result of transforms.
 * @example
 * ```js
 * import {Firebase} from '＠artdeco/firebase'
 *
 * let firebase = new Firebase()
 * let res = await firebase.run({
 * ‎ shouldRun: true,
 * ‎ text: 'hello-world',
 * })
 * console.log(res) // ＠artdeco/firebase called with hello-world
 * ```
 */
eco.artd.IFirebase.run = function(conf) {}

/**
 * @typedef {(this: THIS) => number} eco.artd.IFirebase.__getId
 * @template THIS
 */
/** @typedef {eco.artd.IFirebase.__getId<!eco.artd.IFirebase>} eco.artd.IFirebase._getId */
/** @typedef {typeof eco.artd.IFirebase.getId} */
/**
 * Returns the ID which decreases with time that helps debugging.
 * @return {number}
 */
eco.artd.IFirebase.getId = function() {}

/**
 * @typedef {(this: THIS) => number} eco.artd.IFirebase.__getLocalId
 * @template THIS
 */
/** @typedef {eco.artd.IFirebase.__getLocalId<!eco.artd.IFirebase>} eco.artd.IFirebase._getLocalId */
/** @typedef {typeof eco.artd.IFirebase.getLocalId} */
/**
 * Returns ID for local systems where hrtime is precise.
 * @return {number}
 */
eco.artd.IFirebase.getLocalId = function() {}

/**
 * @typedef {(this: THIS, path: string, dir: string) => ?} eco.artd.IFirebase.__loadRequire
 * @template THIS
 */
/** @typedef {eco.artd.IFirebase.__loadRequire<!eco.artd.IFirebase>} eco.artd.IFirebase._loadRequire */
/** @typedef {typeof eco.artd.IFirebase.loadRequire} */
/**
 * Loads the module using `require`. Requires the deployment of cloud
 * functions. Works synchronously.
 * @param {string} path
 * @param {string} dir
 * @return {?}
 */
eco.artd.IFirebase.loadRequire = function(path, dir) {}

/**
 * @typedef {(this: THIS, req: http.IncomingMessage, res: http.ServerResponse, methodsMap: Map<string, string>) => string} eco.artd.IFirebase.__validateMethod
 * @template THIS
 */
/** @typedef {eco.artd.IFirebase.__validateMethod<!eco.artd.IFirebase>} eco.artd.IFirebase._validateMethod */
/** @typedef {typeof eco.artd.IFirebase.validateMethod} */
/**
 *       Checks that the method is present in the map. Otherwise rejects request.
 * The method that cane as `e3c51` will be checked for handler in the methods
 * map; if not found, the response will be 404.
 * @param {http.IncomingMessage} req
 * @param {http.ServerResponse} res
 * @param {Map<string, string>} methodsMap
 * @return {string}
 */
eco.artd.IFirebase.validateMethod = function(req, res, methodsMap) {}

/** @typedef {import('http').IncomingMessage} */
http.IncomingMessage

/** @typedef {import('http').ServerResponse} */
http.ServerResponse

/**
 * @typedef {Object} eco.artd.firebase.Config Additional options for the program.
 * @prop {boolean} [shouldRun=true] A boolean option. Default `true`.
 * @prop {string} [text] A text to return.
 */