/** @nocompile */
import {registerTypology} from '@type.engineering/type-engineer'

registerTypology({
 'eco.artd.IFirebase': {
  'id': 16817934091,
  'symbols': {
   'symbol': 1,
   'logger': 2
  },
  'methods': {
   'run': 1,
   'getId': 2,
   'getLocalId': 3,
   'loadRequire': 4,
   'validateMethod': 5
  }
 },
 'eco.artd.IFirebaseJoinpointModelHyperslice': {
  'id': 16817934092,
  'symbols': {},
  'methods': {}
 },
 'eco.artd.IFirebaseJoinpointModelBindingHyperslice': {
  'id': 16817934093,
  'symbols': {},
  'methods': {}
 },
 'eco.artd.IFirebaseJoinpointModel': {
  'id': 16817934094,
  'symbols': {},
  'methods': {
   'beforeRun': 1,
   'afterRun': 2,
   'afterRunThrows': 3,
   'afterRunReturns': 4,
   'afterRunCancels': 5,
   'immediatelyAfterRun': 6,
   'beforeGetId': 7,
   'afterGetId': 8,
   'afterGetIdThrows': 9,
   'afterGetIdReturns': 10,
   'afterGetIdCancels': 11,
   'beforeGetLocalId': 12,
   'afterGetLocalId': 13,
   'afterGetLocalIdThrows': 14,
   'afterGetLocalIdReturns': 15,
   'afterGetLocalIdCancels': 16,
   'beforeLoadRequire': 17,
   'afterLoadRequire': 18,
   'afterLoadRequireThrows': 19,
   'afterLoadRequireReturns': 20,
   'afterLoadRequireCancels': 21,
   'beforeValidateMethod': 22,
   'afterValidateMethod': 23,
   'afterValidateMethodThrows': 24,
   'afterValidateMethodReturns': 25,
   'afterValidateMethodCancels': 26
  }
 },
 'eco.artd.IFirebaseAspectsInstaller': {
  'id': 16817934095,
  'symbols': {},
  'methods': {
   'run': 1,
   'getId': 2,
   'getLocalId': 3,
   'loadRequire': 4,
   'validateMethod': 5
  }
 },
 'eco.artd.BFirebaseAspects': {
  'id': 16817934096,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'eco.artd.IFirebaseAspects': {
  'id': 16817934097,
  'symbols': {},
  'methods': {}
 },
 'eco.artd.IHyperFirebase': {
  'id': 16817934098,
  'symbols': {},
  'methods': {}
 }
})