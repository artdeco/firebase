import '@type.engineering/type-engineer' // deps must be loaded
import getModule from '../precompile/browser-upd'
import './polyfills'

const Module=getModule({},self['DEPACK_REQUIRE'])

export default Module