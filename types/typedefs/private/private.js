/** @nocompile */

/** @typedef {(this: eco.artd.IFirebase, conf: !eco.artd.firebase.Config) => !Promise<string>} eco.artd.IFirebase._run */
