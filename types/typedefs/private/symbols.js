/** @nocompile */
eval(`var http={}
var eco={}
eco.artd={}
eco.artd.AbstractFirebase={}
eco.artd.IFirebase={}`)

/* @typal-start {types/design/IFirebase.xml} filter:Symbolism 50e8bbc1fe335ed01681ebb85ea90d26 */
/** @typedef {!eco.artd.IFirebase.getSymbols} eco.artd.AbstractFirebase.getSymbols Returns the protected symbols. */

/** @typedef {!eco.artd.IFirebase.setSymbols} eco.artd.AbstractFirebase.setSymbols Sets the protected symbols. */

/** @typedef {!eco.artd.IFirebase.Symbols} eco.artd.AbstractFirebase.Symbols The list of protected symbols used by the class. */

/**
 * @typedef {Object} eco.artd.IFirebase.SymbolsIn
 * @prop {string} [_firestorePackage]
 * @prop {string} [_rpcDir]
 * @prop {http.ServerResponse} [_res]
 * @prop {http.IncomingMessage} [_req]
 * @prop {string} [_gCloudProject] The name of the firebase project.
 * @prop {string} [_functionTarget] Added on profuction functions to indicate their name.
 * @prop {string} [_functionsEmulator] Set to _True_ when using local debugging.
 * @prop {string} [_db] The database pointer from firebase package.
 * @prop {?{ info: function(...*) }} [_logger] The logger.
 */

/**
 * @typedef {Object} eco.artd.IFirebase.Symbols
 * @prop {symbol} _firestorePackage
 * @prop {symbol} _rpcDir
 * @prop {symbol} _res
 * @prop {symbol} _req
 * @prop {symbol} _gCloudProject
 * @prop {symbol} _functionTarget
 * @prop {symbol} _functionsEmulator
 * @prop {symbol} _db
 * @prop {symbol} _logger
 */

/**
 * @typedef {Object} eco.artd.IFirebase.SymbolsOut
 * @prop {string} _firestorePackage
 * @prop {string} _rpcDir
 * @prop {http.ServerResponse} _res
 * @prop {http.IncomingMessage} _req
 * @prop {string} _gCloudProject The name of the firebase project.
 * @prop {string} _functionTarget Added on profuction functions to indicate their name.
 * @prop {string} _functionsEmulator Set to _True_ when using local debugging.
 * @prop {string} _db The database pointer from firebase package.
 * @prop {?{ info: function(...*) }} _logger The logger.
 */

/** @typedef {typeof eco.artd.IFirebase.setSymbols} */
/**
 * Sets the _IFirebase_ symbols on the instance.
 * @param {!eco.artd.IFirebase} instance The _IFirebase_ instance on which to set the symbols.
 * @param {!eco.artd.IFirebase.SymbolsIn} symbols The symbols to set on the instance.
 * - `[_firestorePackage]` _string?_
 * - `[_rpcDir]` _string?_
 * - `[_res]` _ServerResponse?_
 * - `[_req]` _IncomingMessage?_
 * - `[_gCloudProject]` _string?_ The name of the firebase project.
 * - `[_functionTarget]` _string?_ Added on profuction functions to indicate their name.
 * - `[_functionsEmulator]` _string?_ Set to _True_ when using local debugging.
 * - `[_db]` _string?_ The database pointer from firebase package.
 * - `[_logger]` _?{ info: function(...&#42;) }?_ The logger.
 * @return {void}
 */
eco.artd.IFirebase.setSymbols = function(instance, symbols) {}

/** @typedef {typeof eco.artd.IFirebase.getSymbols} */
/**
 * Gets the _IFirebase_ symbols currently set on the instance.
 * @param {!eco.artd.IFirebase} instance The _IFirebase_ instance from which to read the symbols.
 * @return {!eco.artd.IFirebase.SymbolsOut} The symbols currently set on the instance.
 */
eco.artd.IFirebase.getSymbols = function(instance) {}

// nss:eco.artd.IFirebase
/* @typal-end */
/** @typedef {import('http').IncomingMessage} */
http.IncomingMessage

/** @typedef {import('http').ServerResponse} */
http.ServerResponse