/** @define {string} */
var ECO_ARTD_FIREBASE_DEMO_VERSION=''
/** @define {boolean} */
var ECO_ARTD_FIREBASE_COMPILE_SHARED=false
/** @define {string} */
var ECO_ARTD_FIREBASE_PACKAGE_NAME='@artdeco/firebase'
/** @define {boolean} */
var ECO_ARTD_FIREBASE_BROWSER=false