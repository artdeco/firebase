/**
 * @fileoverview
 * @externs
 */

eco.artd.FirebaseFactory={}
eco.artd.firebase={}
/** @const */
var $$eco={}
$$eco.artd={}
$$eco.artd.IFirebaseJoinpointModel={}
$$eco.artd.IFirebaseProfilingAide={}
$$eco.artd.IFirebase={}
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebase.Initialese no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/** @record */
eco.artd.IFirebase.Initialese = function() {}
/** @type {string|undefined} */
eco.artd.IFirebase.Initialese.prototype.firestorePackage
/** @type {string|undefined} */
eco.artd.IFirebase.Initialese.prototype.rpcDir
/** @type {http.ServerResponse|undefined} */
eco.artd.IFirebase.Initialese.prototype.res
/** @type {http.IncomingMessage|undefined} */
eco.artd.IFirebase.Initialese.prototype.req
/** @type {string|undefined} */
eco.artd.IFirebase.Initialese.prototype.gCloudProject
/** @type {string|undefined} */
eco.artd.IFirebase.Initialese.prototype.functionTarget
/** @type {string|undefined} */
eco.artd.IFirebase.Initialese.prototype.functionsEmulator
/** @type {string|undefined} */
eco.artd.IFirebase.Initialese.prototype.db
/** @type {(?{ info: function(...*) })|undefined} */
eco.artd.IFirebase.Initialese.prototype.logger

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseFields no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/** @interface */
eco.artd.IFirebaseFields
/** @type {string} */
eco.artd.IFirebaseFields.prototype.firestorePackage
/** @type {string} */
eco.artd.IFirebaseFields.prototype.rpcDir
/** @type {http.ServerResponse} */
eco.artd.IFirebaseFields.prototype.res
/** @type {http.IncomingMessage} */
eco.artd.IFirebaseFields.prototype.req
/** @type {string} */
eco.artd.IFirebaseFields.prototype.gCloudProject
/** @type {string} */
eco.artd.IFirebaseFields.prototype.functionTarget
/** @type {string} */
eco.artd.IFirebaseFields.prototype.functionsEmulator
/** @type {string} */
eco.artd.IFirebaseFields.prototype.db
/** @type {string} */
eco.artd.IFirebaseFields.prototype.host
/** @type {boolean} */
eco.artd.IFirebaseFields.prototype.isLocal
/** @type {?{ info: function(...*) }} */
eco.artd.IFirebaseFields.prototype.logger

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseCaster no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/** @interface */
eco.artd.IFirebaseCaster
/** @type {!eco.artd.BoundIFirebase} */
eco.artd.IFirebaseCaster.prototype.asIFirebase
/** @type {!eco.artd.BoundFirebase} */
eco.artd.IFirebaseCaster.prototype.superFirebase

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebase no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @interface
 * @extends {eco.artd.IFirebaseFields}
 * @extends {engineering.type.IEngineer}
 * @extends {eco.artd.IFirebaseCaster}
 */
eco.artd.IFirebase = function() {}
/** @param {...!eco.artd.IFirebase.Initialese} init */
eco.artd.IFirebase.prototype.constructor = function(...init) {}
/**
 * @param {http.IncomingMessage} request
 * @param {http.ServerResponse} response
 * @param {(!Function|number)} cb
 * @param {!eco.artd.IFirebase.function.Meta} [meta]
 * @return {!Promise<void>}
 */
eco.artd.IFirebase.prototype.function = function(request, response, cb, meta) {}
/**
 * @param {string} body
 * @return {eco.artd.IFirebase.evalBodyWithProfileTime.Return}
 */
eco.artd.IFirebase.prototype.evalBodyWithProfileTime = function(body) {}
/**
 * @param {number} cid
 * @return {!Promise<?eco.artd.IFirebase.PageData>}
 */
eco.artd.IFirebase.prototype.getPageDataFromFirestore = function(cid) {}
/**
 * @param {number} id
 * @return {!Promise}
 */
eco.artd.IFirebase.prototype.downloadAndEvalPageData = function(id) {}
/**
 * @param {string} body
 * @return {function(!http.IncomingMessage, !http.ServerResponse): (void|Promise<void>)}
 */
eco.artd.IFirebase.prototype.evalBody = function(body) {}
/**
 * @param {!Date} dateTime
 * @param {eco.artd.IFirebase.Times} times
 * @param {eco.artd.IFirebase.Methods} methods
 * @param {{ error: Error, res: null, req: null }} invRes
 * @return {!Promise<void>}
 */
eco.artd.IFirebase.prototype.logRpcInvocation = function(dateTime, times, methods, invRes) {}
/**
 * @param {string} file
 * @return {!Promise<eco.artd.IFirebase.fetchFunctionsFile.Return>}
 */
eco.artd.IFirebase.prototype.fetchFunctionsFile = function(file) {}
/**
 * @param {!Array<string>} deps
 * @param {string} methodName
 * @param {number} id
 * @return {!Promise}
 */
eco.artd.IFirebase.prototype.downloadDeps = function(deps, methodName, id) {}
/**
 * @param {number} cid
 * @param {string} methodName
 * @return {!Promise<string>}
 */
eco.artd.IFirebase.prototype.downloadMethod = function(cid, methodName) {}
/**
 * @param {number} cid
 * @param {string} methodId
 * @return {!Promise<eco.artd.IFirebase.downloadMethodDirectly.Return>}
 */
eco.artd.IFirebase.prototype.downloadMethodDirectly = function(cid, methodId) {}
/** @return {number} */
eco.artd.IFirebase.prototype.getId = function() {}
/**
 * @param {string} body
 * @return {!Promise<eco.artd.IFirebase.patchDeps.Return>}
 */
eco.artd.IFirebase.prototype.patchDeps = function(body) {}
/**
 * @param {number} cid
 * @return {!Promise<string>}
 */
eco.artd.IFirebase.prototype.downloadPageData = function(cid) {}
/**
 * @param {number} cid
 * @return {!eco.artd.IFirebase.PageData}
 */
eco.artd.IFirebase.prototype.loadRequirePageData = function(cid) {}
/**
 * @param {string} path
 * @param {string} dir
 * @return {?}
 */
eco.artd.IFirebase.prototype.loadRequire = function(path, dir) {}
/**
 * @param {!Error} error
 * @param {string} url
 * @return {void}
 */
eco.artd.IFirebase.prototype.updateErrorStack = function(error, url) {}
/**
 * @param {Map<string, string>} [methodsMap]
 * @return {string}
 */
eco.artd.IFirebase.prototype.validateMethod = function(methodsMap) {}
/**
 * @param {!Object<string, eco.artd.IFirebase.PageData>} pageData
 * @param {number} id
 * @return {!Promise<void>}
 */
eco.artd.IFirebase.prototype.validateLoadAndInvoke = function(pageData, id) {}
/**
 * @param {number} cid
 * @return {!Promise<void>}
 */
eco.artd.IFirebase.prototype.validateLoadAndInvokeById = function(cid) {}
/**
 * @param {number} id
 * @return {function()}
 */
eco.artd.IFirebase.prototype.makeInvoker = function(id) {}
/**
 * @param {number} cid
 * @param {string} mid
 * @return {?}
 */
eco.artd.IFirebase.prototype.loadAndInvokeById = function(cid, mid) {}
/**
 * @param {string} methodId
 * @param {!Object<string, eco.artd.IFirebase.PageData>} pageData
 * @param {number} id
 * @return {!Promise<void>}
 */
eco.artd.IFirebase.prototype.loadAndInvoke = function(methodId, pageData, id) {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.Firebase no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @constructor
 * @param {...!eco.artd.IFirebase.Initialese} init
 * @implements {eco.artd.IFirebase}
 * @implements {engineering.type.IInitialiser<!eco.artd.IFirebase.Initialese>}
 */
eco.artd.Firebase = function(...init) {}
/** @param {...!eco.artd.IFirebase.Initialese} init */
eco.artd.Firebase.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof eco.artd.Firebase}
 */
eco.artd.Firebase.__extend = function(...Extensions) {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.AbstractFirebase no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @constructor
 * @extends {eco.artd.Firebase}
 */
eco.artd.AbstractFirebase = function() {}
/**
 * @param {...(!eco.artd.IFirebase|typeof eco.artd.Firebase)} Implementations
 * @return {typeof eco.artd.Firebase}
 * @nosideeffects
 */
eco.artd.AbstractFirebase.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof eco.artd.AbstractFirebase}
 */
eco.artd.AbstractFirebase.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof eco.artd.Firebase}
 */
eco.artd.AbstractFirebase.__extend = function(...Extensions) {}
/**
 * @param {...(!eco.artd.IFirebase|typeof eco.artd.Firebase)} Implementations
 * @return {typeof eco.artd.Firebase}
 */
eco.artd.AbstractFirebase.continues = function(...Implementations) {}
/**
 * @param {...(!eco.artd.IFirebase|typeof eco.artd.Firebase)} Implementations
 * @return {typeof eco.artd.Firebase}
 */
eco.artd.AbstractFirebase.__trait = function(...Implementations) {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModelHyperslice no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/** @interface */
eco.artd.IFirebaseJoinpointModelHyperslice = function() {}
/** @type {(!eco.artd.IFirebaseJoinpointModel._beforeFunction|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._beforeFunction>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.beforeFunction
/** @type {(!eco.artd.IFirebaseJoinpointModel._afterFunction|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterFunction>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.afterFunction
/** @type {(!eco.artd.IFirebaseJoinpointModel._afterFunctionThrows|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterFunctionThrows>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.afterFunctionThrows
/** @type {(!eco.artd.IFirebaseJoinpointModel._afterFunctionReturns|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterFunctionReturns>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.afterFunctionReturns
/** @type {(!eco.artd.IFirebaseJoinpointModel._afterFunctionCancels|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterFunctionCancels>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.afterFunctionCancels
/** @type {(!eco.artd.IFirebaseJoinpointModel._immediatelyAfterFunction|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._immediatelyAfterFunction>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.immediatelyAfterFunction
/** @type {(!eco.artd.IFirebaseJoinpointModel._beforeEvalBodyWithProfileTime|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._beforeEvalBodyWithProfileTime>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.beforeEvalBodyWithProfileTime
/** @type {(!eco.artd.IFirebaseJoinpointModel._afterEvalBodyWithProfileTime|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterEvalBodyWithProfileTime>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.afterEvalBodyWithProfileTime
/** @type {(!eco.artd.IFirebaseJoinpointModel._afterEvalBodyWithProfileTimeThrows|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterEvalBodyWithProfileTimeThrows>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.afterEvalBodyWithProfileTimeThrows
/** @type {(!eco.artd.IFirebaseJoinpointModel._afterEvalBodyWithProfileTimeReturns|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterEvalBodyWithProfileTimeReturns>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.afterEvalBodyWithProfileTimeReturns
/** @type {(!eco.artd.IFirebaseJoinpointModel._afterEvalBodyWithProfileTimeCancels|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterEvalBodyWithProfileTimeCancels>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.afterEvalBodyWithProfileTimeCancels
/** @type {(!eco.artd.IFirebaseJoinpointModel._beforeGetPageDataFromFirestore|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._beforeGetPageDataFromFirestore>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.beforeGetPageDataFromFirestore
/** @type {(!eco.artd.IFirebaseJoinpointModel._afterGetPageDataFromFirestore|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterGetPageDataFromFirestore>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.afterGetPageDataFromFirestore
/** @type {(!eco.artd.IFirebaseJoinpointModel._afterGetPageDataFromFirestoreThrows|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterGetPageDataFromFirestoreThrows>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.afterGetPageDataFromFirestoreThrows
/** @type {(!eco.artd.IFirebaseJoinpointModel._afterGetPageDataFromFirestoreReturns|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterGetPageDataFromFirestoreReturns>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.afterGetPageDataFromFirestoreReturns
/** @type {(!eco.artd.IFirebaseJoinpointModel._afterGetPageDataFromFirestoreCancels|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterGetPageDataFromFirestoreCancels>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.afterGetPageDataFromFirestoreCancels
/** @type {(!eco.artd.IFirebaseJoinpointModel._immediatelyAfterGetPageDataFromFirestore|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._immediatelyAfterGetPageDataFromFirestore>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.immediatelyAfterGetPageDataFromFirestore
/** @type {(!eco.artd.IFirebaseJoinpointModel._beforeDownloadAndEvalPageData|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._beforeDownloadAndEvalPageData>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.beforeDownloadAndEvalPageData
/** @type {(!eco.artd.IFirebaseJoinpointModel._afterDownloadAndEvalPageData|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterDownloadAndEvalPageData>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.afterDownloadAndEvalPageData
/** @type {(!eco.artd.IFirebaseJoinpointModel._afterDownloadAndEvalPageDataThrows|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterDownloadAndEvalPageDataThrows>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.afterDownloadAndEvalPageDataThrows
/** @type {(!eco.artd.IFirebaseJoinpointModel._afterDownloadAndEvalPageDataReturns|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterDownloadAndEvalPageDataReturns>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.afterDownloadAndEvalPageDataReturns
/** @type {(!eco.artd.IFirebaseJoinpointModel._afterDownloadAndEvalPageDataCancels|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterDownloadAndEvalPageDataCancels>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.afterDownloadAndEvalPageDataCancels
/** @type {(!eco.artd.IFirebaseJoinpointModel._immediatelyAfterDownloadAndEvalPageData|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._immediatelyAfterDownloadAndEvalPageData>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.immediatelyAfterDownloadAndEvalPageData
/** @type {(!eco.artd.IFirebaseJoinpointModel._beforeEvalBody|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._beforeEvalBody>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.beforeEvalBody
/** @type {(!eco.artd.IFirebaseJoinpointModel._afterEvalBody|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterEvalBody>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.afterEvalBody
/** @type {(!eco.artd.IFirebaseJoinpointModel._afterEvalBodyThrows|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterEvalBodyThrows>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.afterEvalBodyThrows
/** @type {(!eco.artd.IFirebaseJoinpointModel._afterEvalBodyReturns|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterEvalBodyReturns>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.afterEvalBodyReturns
/** @type {(!eco.artd.IFirebaseJoinpointModel._afterEvalBodyCancels|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterEvalBodyCancels>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.afterEvalBodyCancels
/** @type {(!eco.artd.IFirebaseJoinpointModel._beforeLogRpcInvocation|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._beforeLogRpcInvocation>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.beforeLogRpcInvocation
/** @type {(!eco.artd.IFirebaseJoinpointModel._afterLogRpcInvocation|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterLogRpcInvocation>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.afterLogRpcInvocation
/** @type {(!eco.artd.IFirebaseJoinpointModel._afterLogRpcInvocationThrows|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterLogRpcInvocationThrows>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.afterLogRpcInvocationThrows
/** @type {(!eco.artd.IFirebaseJoinpointModel._afterLogRpcInvocationReturns|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterLogRpcInvocationReturns>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.afterLogRpcInvocationReturns
/** @type {(!eco.artd.IFirebaseJoinpointModel._afterLogRpcInvocationCancels|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterLogRpcInvocationCancels>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.afterLogRpcInvocationCancels
/** @type {(!eco.artd.IFirebaseJoinpointModel._immediatelyAfterLogRpcInvocation|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._immediatelyAfterLogRpcInvocation>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.immediatelyAfterLogRpcInvocation
/** @type {(!eco.artd.IFirebaseJoinpointModel._beforeFetchFunctionsFile|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._beforeFetchFunctionsFile>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.beforeFetchFunctionsFile
/** @type {(!eco.artd.IFirebaseJoinpointModel._afterFetchFunctionsFile|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterFetchFunctionsFile>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.afterFetchFunctionsFile
/** @type {(!eco.artd.IFirebaseJoinpointModel._afterFetchFunctionsFileThrows|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterFetchFunctionsFileThrows>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.afterFetchFunctionsFileThrows
/** @type {(!eco.artd.IFirebaseJoinpointModel._afterFetchFunctionsFileReturns|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterFetchFunctionsFileReturns>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.afterFetchFunctionsFileReturns
/** @type {(!eco.artd.IFirebaseJoinpointModel._afterFetchFunctionsFileCancels|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterFetchFunctionsFileCancels>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.afterFetchFunctionsFileCancels
/** @type {(!eco.artd.IFirebaseJoinpointModel._immediatelyAfterFetchFunctionsFile|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._immediatelyAfterFetchFunctionsFile>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.immediatelyAfterFetchFunctionsFile
/** @type {(!eco.artd.IFirebaseJoinpointModel._beforeDownloadDeps|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._beforeDownloadDeps>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.beforeDownloadDeps
/** @type {(!eco.artd.IFirebaseJoinpointModel._afterDownloadDeps|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterDownloadDeps>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.afterDownloadDeps
/** @type {(!eco.artd.IFirebaseJoinpointModel._afterDownloadDepsThrows|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterDownloadDepsThrows>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.afterDownloadDepsThrows
/** @type {(!eco.artd.IFirebaseJoinpointModel._afterDownloadDepsReturns|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterDownloadDepsReturns>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.afterDownloadDepsReturns
/** @type {(!eco.artd.IFirebaseJoinpointModel._afterDownloadDepsCancels|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterDownloadDepsCancels>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.afterDownloadDepsCancels
/** @type {(!eco.artd.IFirebaseJoinpointModel._immediatelyAfterDownloadDeps|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._immediatelyAfterDownloadDeps>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.immediatelyAfterDownloadDeps
/** @type {(!eco.artd.IFirebaseJoinpointModel._beforeDownloadMethod|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._beforeDownloadMethod>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.beforeDownloadMethod
/** @type {(!eco.artd.IFirebaseJoinpointModel._afterDownloadMethod|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterDownloadMethod>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.afterDownloadMethod
/** @type {(!eco.artd.IFirebaseJoinpointModel._afterDownloadMethodThrows|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterDownloadMethodThrows>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.afterDownloadMethodThrows
/** @type {(!eco.artd.IFirebaseJoinpointModel._afterDownloadMethodReturns|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterDownloadMethodReturns>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.afterDownloadMethodReturns
/** @type {(!eco.artd.IFirebaseJoinpointModel._afterDownloadMethodCancels|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterDownloadMethodCancels>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.afterDownloadMethodCancels
/** @type {(!eco.artd.IFirebaseJoinpointModel._immediatelyAfterDownloadMethod|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._immediatelyAfterDownloadMethod>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.immediatelyAfterDownloadMethod
/** @type {(!eco.artd.IFirebaseJoinpointModel._beforeDownloadMethodDirectly|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._beforeDownloadMethodDirectly>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.beforeDownloadMethodDirectly
/** @type {(!eco.artd.IFirebaseJoinpointModel._afterDownloadMethodDirectly|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterDownloadMethodDirectly>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.afterDownloadMethodDirectly
/** @type {(!eco.artd.IFirebaseJoinpointModel._afterDownloadMethodDirectlyThrows|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterDownloadMethodDirectlyThrows>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.afterDownloadMethodDirectlyThrows
/** @type {(!eco.artd.IFirebaseJoinpointModel._afterDownloadMethodDirectlyReturns|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterDownloadMethodDirectlyReturns>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.afterDownloadMethodDirectlyReturns
/** @type {(!eco.artd.IFirebaseJoinpointModel._afterDownloadMethodDirectlyCancels|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterDownloadMethodDirectlyCancels>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.afterDownloadMethodDirectlyCancels
/** @type {(!eco.artd.IFirebaseJoinpointModel._immediatelyAfterDownloadMethodDirectly|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._immediatelyAfterDownloadMethodDirectly>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.immediatelyAfterDownloadMethodDirectly
/** @type {(!eco.artd.IFirebaseJoinpointModel._beforeGetId|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._beforeGetId>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.beforeGetId
/** @type {(!eco.artd.IFirebaseJoinpointModel._afterGetId|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterGetId>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.afterGetId
/** @type {(!eco.artd.IFirebaseJoinpointModel._afterGetIdThrows|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterGetIdThrows>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.afterGetIdThrows
/** @type {(!eco.artd.IFirebaseJoinpointModel._afterGetIdReturns|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterGetIdReturns>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.afterGetIdReturns
/** @type {(!eco.artd.IFirebaseJoinpointModel._afterGetIdCancels|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterGetIdCancels>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.afterGetIdCancels
/** @type {(!eco.artd.IFirebaseJoinpointModel._beforePatchDeps|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._beforePatchDeps>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.beforePatchDeps
/** @type {(!eco.artd.IFirebaseJoinpointModel._afterPatchDeps|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterPatchDeps>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.afterPatchDeps
/** @type {(!eco.artd.IFirebaseJoinpointModel._afterPatchDepsThrows|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterPatchDepsThrows>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.afterPatchDepsThrows
/** @type {(!eco.artd.IFirebaseJoinpointModel._afterPatchDepsReturns|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterPatchDepsReturns>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.afterPatchDepsReturns
/** @type {(!eco.artd.IFirebaseJoinpointModel._afterPatchDepsCancels|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterPatchDepsCancels>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.afterPatchDepsCancels
/** @type {(!eco.artd.IFirebaseJoinpointModel._immediatelyAfterPatchDeps|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._immediatelyAfterPatchDeps>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.immediatelyAfterPatchDeps
/** @type {(!eco.artd.IFirebaseJoinpointModel._beforeDownloadPageData|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._beforeDownloadPageData>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.beforeDownloadPageData
/** @type {(!eco.artd.IFirebaseJoinpointModel._afterDownloadPageData|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterDownloadPageData>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.afterDownloadPageData
/** @type {(!eco.artd.IFirebaseJoinpointModel._afterDownloadPageDataThrows|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterDownloadPageDataThrows>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.afterDownloadPageDataThrows
/** @type {(!eco.artd.IFirebaseJoinpointModel._afterDownloadPageDataReturns|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterDownloadPageDataReturns>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.afterDownloadPageDataReturns
/** @type {(!eco.artd.IFirebaseJoinpointModel._afterDownloadPageDataCancels|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterDownloadPageDataCancels>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.afterDownloadPageDataCancels
/** @type {(!eco.artd.IFirebaseJoinpointModel._immediatelyAfterDownloadPageData|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._immediatelyAfterDownloadPageData>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.immediatelyAfterDownloadPageData
/** @type {(!eco.artd.IFirebaseJoinpointModel._beforeLoadRequirePageData|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._beforeLoadRequirePageData>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.beforeLoadRequirePageData
/** @type {(!eco.artd.IFirebaseJoinpointModel._afterLoadRequirePageData|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterLoadRequirePageData>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.afterLoadRequirePageData
/** @type {(!eco.artd.IFirebaseJoinpointModel._afterLoadRequirePageDataThrows|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterLoadRequirePageDataThrows>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.afterLoadRequirePageDataThrows
/** @type {(!eco.artd.IFirebaseJoinpointModel._afterLoadRequirePageDataReturns|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterLoadRequirePageDataReturns>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.afterLoadRequirePageDataReturns
/** @type {(!eco.artd.IFirebaseJoinpointModel._afterLoadRequirePageDataCancels|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterLoadRequirePageDataCancels>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.afterLoadRequirePageDataCancels
/** @type {(!eco.artd.IFirebaseJoinpointModel._beforeLoadRequire|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._beforeLoadRequire>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.beforeLoadRequire
/** @type {(!eco.artd.IFirebaseJoinpointModel._afterLoadRequire|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterLoadRequire>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.afterLoadRequire
/** @type {(!eco.artd.IFirebaseJoinpointModel._afterLoadRequireThrows|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterLoadRequireThrows>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.afterLoadRequireThrows
/** @type {(!eco.artd.IFirebaseJoinpointModel._afterLoadRequireReturns|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterLoadRequireReturns>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.afterLoadRequireReturns
/** @type {(!eco.artd.IFirebaseJoinpointModel._afterLoadRequireCancels|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterLoadRequireCancels>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.afterLoadRequireCancels
/** @type {(!eco.artd.IFirebaseJoinpointModel._beforeUpdateErrorStack|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._beforeUpdateErrorStack>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.beforeUpdateErrorStack
/** @type {(!eco.artd.IFirebaseJoinpointModel._afterUpdateErrorStack|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterUpdateErrorStack>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.afterUpdateErrorStack
/** @type {(!eco.artd.IFirebaseJoinpointModel._afterUpdateErrorStackThrows|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterUpdateErrorStackThrows>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.afterUpdateErrorStackThrows
/** @type {(!eco.artd.IFirebaseJoinpointModel._afterUpdateErrorStackReturns|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterUpdateErrorStackReturns>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.afterUpdateErrorStackReturns
/** @type {(!eco.artd.IFirebaseJoinpointModel._afterUpdateErrorStackCancels|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterUpdateErrorStackCancels>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.afterUpdateErrorStackCancels
/** @type {(!eco.artd.IFirebaseJoinpointModel._beforeValidateMethod|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._beforeValidateMethod>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.beforeValidateMethod
/** @type {(!eco.artd.IFirebaseJoinpointModel._afterValidateMethod|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterValidateMethod>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.afterValidateMethod
/** @type {(!eco.artd.IFirebaseJoinpointModel._afterValidateMethodThrows|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterValidateMethodThrows>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.afterValidateMethodThrows
/** @type {(!eco.artd.IFirebaseJoinpointModel._afterValidateMethodReturns|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterValidateMethodReturns>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.afterValidateMethodReturns
/** @type {(!eco.artd.IFirebaseJoinpointModel._afterValidateMethodCancels|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterValidateMethodCancels>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.afterValidateMethodCancels
/** @type {(!eco.artd.IFirebaseJoinpointModel._beforeValidateLoadAndInvoke|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._beforeValidateLoadAndInvoke>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.beforeValidateLoadAndInvoke
/** @type {(!eco.artd.IFirebaseJoinpointModel._afterValidateLoadAndInvoke|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterValidateLoadAndInvoke>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.afterValidateLoadAndInvoke
/** @type {(!eco.artd.IFirebaseJoinpointModel._afterValidateLoadAndInvokeThrows|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterValidateLoadAndInvokeThrows>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.afterValidateLoadAndInvokeThrows
/** @type {(!eco.artd.IFirebaseJoinpointModel._afterValidateLoadAndInvokeReturns|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterValidateLoadAndInvokeReturns>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.afterValidateLoadAndInvokeReturns
/** @type {(!eco.artd.IFirebaseJoinpointModel._afterValidateLoadAndInvokeCancels|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterValidateLoadAndInvokeCancels>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.afterValidateLoadAndInvokeCancels
/** @type {(!eco.artd.IFirebaseJoinpointModel._immediatelyAfterValidateLoadAndInvoke|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._immediatelyAfterValidateLoadAndInvoke>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.immediatelyAfterValidateLoadAndInvoke
/** @type {(!eco.artd.IFirebaseJoinpointModel._beforeValidateLoadAndInvokeById|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._beforeValidateLoadAndInvokeById>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.beforeValidateLoadAndInvokeById
/** @type {(!eco.artd.IFirebaseJoinpointModel._afterValidateLoadAndInvokeById|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterValidateLoadAndInvokeById>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.afterValidateLoadAndInvokeById
/** @type {(!eco.artd.IFirebaseJoinpointModel._afterValidateLoadAndInvokeByIdThrows|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterValidateLoadAndInvokeByIdThrows>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.afterValidateLoadAndInvokeByIdThrows
/** @type {(!eco.artd.IFirebaseJoinpointModel._afterValidateLoadAndInvokeByIdReturns|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterValidateLoadAndInvokeByIdReturns>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.afterValidateLoadAndInvokeByIdReturns
/** @type {(!eco.artd.IFirebaseJoinpointModel._afterValidateLoadAndInvokeByIdCancels|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterValidateLoadAndInvokeByIdCancels>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.afterValidateLoadAndInvokeByIdCancels
/** @type {(!eco.artd.IFirebaseJoinpointModel._immediatelyAfterValidateLoadAndInvokeById|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._immediatelyAfterValidateLoadAndInvokeById>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.immediatelyAfterValidateLoadAndInvokeById
/** @type {(!eco.artd.IFirebaseJoinpointModel._beforeMakeInvoker|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._beforeMakeInvoker>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.beforeMakeInvoker
/** @type {(!eco.artd.IFirebaseJoinpointModel._afterMakeInvoker|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterMakeInvoker>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.afterMakeInvoker
/** @type {(!eco.artd.IFirebaseJoinpointModel._afterMakeInvokerThrows|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterMakeInvokerThrows>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.afterMakeInvokerThrows
/** @type {(!eco.artd.IFirebaseJoinpointModel._afterMakeInvokerReturns|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterMakeInvokerReturns>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.afterMakeInvokerReturns
/** @type {(!eco.artd.IFirebaseJoinpointModel._afterMakeInvokerCancels|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterMakeInvokerCancels>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.afterMakeInvokerCancels
/** @type {(!eco.artd.IFirebaseJoinpointModel._beforeLoadAndInvokeById|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._beforeLoadAndInvokeById>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.beforeLoadAndInvokeById
/** @type {(!eco.artd.IFirebaseJoinpointModel._afterLoadAndInvokeById|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterLoadAndInvokeById>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.afterLoadAndInvokeById
/** @type {(!eco.artd.IFirebaseJoinpointModel._afterLoadAndInvokeByIdThrows|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterLoadAndInvokeByIdThrows>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.afterLoadAndInvokeByIdThrows
/** @type {(!eco.artd.IFirebaseJoinpointModel._afterLoadAndInvokeByIdReturns|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterLoadAndInvokeByIdReturns>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.afterLoadAndInvokeByIdReturns
/** @type {(!eco.artd.IFirebaseJoinpointModel._afterLoadAndInvokeByIdCancels|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterLoadAndInvokeByIdCancels>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.afterLoadAndInvokeByIdCancels
/** @type {(!eco.artd.IFirebaseJoinpointModel._beforeLoadAndInvoke|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._beforeLoadAndInvoke>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.beforeLoadAndInvoke
/** @type {(!eco.artd.IFirebaseJoinpointModel._afterLoadAndInvoke|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterLoadAndInvoke>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.afterLoadAndInvoke
/** @type {(!eco.artd.IFirebaseJoinpointModel._afterLoadAndInvokeThrows|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterLoadAndInvokeThrows>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.afterLoadAndInvokeThrows
/** @type {(!eco.artd.IFirebaseJoinpointModel._afterLoadAndInvokeReturns|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterLoadAndInvokeReturns>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.afterLoadAndInvokeReturns
/** @type {(!eco.artd.IFirebaseJoinpointModel._afterLoadAndInvokeCancels|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterLoadAndInvokeCancels>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.afterLoadAndInvokeCancels
/** @type {(!eco.artd.IFirebaseJoinpointModel._immediatelyAfterLoadAndInvoke|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._immediatelyAfterLoadAndInvoke>)} */
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.immediatelyAfterLoadAndInvoke

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.FirebaseJoinpointModelHyperslice no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @constructor
 * @implements {eco.artd.IFirebaseJoinpointModelHyperslice}
 */
eco.artd.FirebaseJoinpointModelHyperslice = function() {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModelBindingHyperslice no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @interface
 * @template THIS
 */
eco.artd.IFirebaseJoinpointModelBindingHyperslice = function() {}
/** @type {(!eco.artd.IFirebaseJoinpointModel.__beforeFunction<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__beforeFunction<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.beforeFunction
/** @type {(!eco.artd.IFirebaseJoinpointModel.__afterFunction<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterFunction<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.afterFunction
/** @type {(!eco.artd.IFirebaseJoinpointModel.__afterFunctionThrows<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterFunctionThrows<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.afterFunctionThrows
/** @type {(!eco.artd.IFirebaseJoinpointModel.__afterFunctionReturns<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterFunctionReturns<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.afterFunctionReturns
/** @type {(!eco.artd.IFirebaseJoinpointModel.__afterFunctionCancels<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterFunctionCancels<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.afterFunctionCancels
/** @type {(!eco.artd.IFirebaseJoinpointModel.__immediatelyAfterFunction<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__immediatelyAfterFunction<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.immediatelyAfterFunction
/** @type {(!eco.artd.IFirebaseJoinpointModel.__beforeEvalBodyWithProfileTime<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__beforeEvalBodyWithProfileTime<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.beforeEvalBodyWithProfileTime
/** @type {(!eco.artd.IFirebaseJoinpointModel.__afterEvalBodyWithProfileTime<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterEvalBodyWithProfileTime<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.afterEvalBodyWithProfileTime
/** @type {(!eco.artd.IFirebaseJoinpointModel.__afterEvalBodyWithProfileTimeThrows<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterEvalBodyWithProfileTimeThrows<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.afterEvalBodyWithProfileTimeThrows
/** @type {(!eco.artd.IFirebaseJoinpointModel.__afterEvalBodyWithProfileTimeReturns<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterEvalBodyWithProfileTimeReturns<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.afterEvalBodyWithProfileTimeReturns
/** @type {(!eco.artd.IFirebaseJoinpointModel.__afterEvalBodyWithProfileTimeCancels<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterEvalBodyWithProfileTimeCancels<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.afterEvalBodyWithProfileTimeCancels
/** @type {(!eco.artd.IFirebaseJoinpointModel.__beforeGetPageDataFromFirestore<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__beforeGetPageDataFromFirestore<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.beforeGetPageDataFromFirestore
/** @type {(!eco.artd.IFirebaseJoinpointModel.__afterGetPageDataFromFirestore<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterGetPageDataFromFirestore<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.afterGetPageDataFromFirestore
/** @type {(!eco.artd.IFirebaseJoinpointModel.__afterGetPageDataFromFirestoreThrows<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterGetPageDataFromFirestoreThrows<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.afterGetPageDataFromFirestoreThrows
/** @type {(!eco.artd.IFirebaseJoinpointModel.__afterGetPageDataFromFirestoreReturns<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterGetPageDataFromFirestoreReturns<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.afterGetPageDataFromFirestoreReturns
/** @type {(!eco.artd.IFirebaseJoinpointModel.__afterGetPageDataFromFirestoreCancels<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterGetPageDataFromFirestoreCancels<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.afterGetPageDataFromFirestoreCancels
/** @type {(!eco.artd.IFirebaseJoinpointModel.__immediatelyAfterGetPageDataFromFirestore<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__immediatelyAfterGetPageDataFromFirestore<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.immediatelyAfterGetPageDataFromFirestore
/** @type {(!eco.artd.IFirebaseJoinpointModel.__beforeDownloadAndEvalPageData<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__beforeDownloadAndEvalPageData<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.beforeDownloadAndEvalPageData
/** @type {(!eco.artd.IFirebaseJoinpointModel.__afterDownloadAndEvalPageData<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterDownloadAndEvalPageData<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.afterDownloadAndEvalPageData
/** @type {(!eco.artd.IFirebaseJoinpointModel.__afterDownloadAndEvalPageDataThrows<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterDownloadAndEvalPageDataThrows<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.afterDownloadAndEvalPageDataThrows
/** @type {(!eco.artd.IFirebaseJoinpointModel.__afterDownloadAndEvalPageDataReturns<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterDownloadAndEvalPageDataReturns<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.afterDownloadAndEvalPageDataReturns
/** @type {(!eco.artd.IFirebaseJoinpointModel.__afterDownloadAndEvalPageDataCancels<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterDownloadAndEvalPageDataCancels<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.afterDownloadAndEvalPageDataCancels
/** @type {(!eco.artd.IFirebaseJoinpointModel.__immediatelyAfterDownloadAndEvalPageData<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__immediatelyAfterDownloadAndEvalPageData<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.immediatelyAfterDownloadAndEvalPageData
/** @type {(!eco.artd.IFirebaseJoinpointModel.__beforeEvalBody<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__beforeEvalBody<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.beforeEvalBody
/** @type {(!eco.artd.IFirebaseJoinpointModel.__afterEvalBody<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterEvalBody<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.afterEvalBody
/** @type {(!eco.artd.IFirebaseJoinpointModel.__afterEvalBodyThrows<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterEvalBodyThrows<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.afterEvalBodyThrows
/** @type {(!eco.artd.IFirebaseJoinpointModel.__afterEvalBodyReturns<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterEvalBodyReturns<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.afterEvalBodyReturns
/** @type {(!eco.artd.IFirebaseJoinpointModel.__afterEvalBodyCancels<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterEvalBodyCancels<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.afterEvalBodyCancels
/** @type {(!eco.artd.IFirebaseJoinpointModel.__beforeLogRpcInvocation<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__beforeLogRpcInvocation<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.beforeLogRpcInvocation
/** @type {(!eco.artd.IFirebaseJoinpointModel.__afterLogRpcInvocation<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterLogRpcInvocation<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.afterLogRpcInvocation
/** @type {(!eco.artd.IFirebaseJoinpointModel.__afterLogRpcInvocationThrows<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterLogRpcInvocationThrows<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.afterLogRpcInvocationThrows
/** @type {(!eco.artd.IFirebaseJoinpointModel.__afterLogRpcInvocationReturns<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterLogRpcInvocationReturns<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.afterLogRpcInvocationReturns
/** @type {(!eco.artd.IFirebaseJoinpointModel.__afterLogRpcInvocationCancels<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterLogRpcInvocationCancels<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.afterLogRpcInvocationCancels
/** @type {(!eco.artd.IFirebaseJoinpointModel.__immediatelyAfterLogRpcInvocation<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__immediatelyAfterLogRpcInvocation<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.immediatelyAfterLogRpcInvocation
/** @type {(!eco.artd.IFirebaseJoinpointModel.__beforeFetchFunctionsFile<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__beforeFetchFunctionsFile<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.beforeFetchFunctionsFile
/** @type {(!eco.artd.IFirebaseJoinpointModel.__afterFetchFunctionsFile<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterFetchFunctionsFile<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.afterFetchFunctionsFile
/** @type {(!eco.artd.IFirebaseJoinpointModel.__afterFetchFunctionsFileThrows<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterFetchFunctionsFileThrows<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.afterFetchFunctionsFileThrows
/** @type {(!eco.artd.IFirebaseJoinpointModel.__afterFetchFunctionsFileReturns<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterFetchFunctionsFileReturns<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.afterFetchFunctionsFileReturns
/** @type {(!eco.artd.IFirebaseJoinpointModel.__afterFetchFunctionsFileCancels<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterFetchFunctionsFileCancels<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.afterFetchFunctionsFileCancels
/** @type {(!eco.artd.IFirebaseJoinpointModel.__immediatelyAfterFetchFunctionsFile<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__immediatelyAfterFetchFunctionsFile<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.immediatelyAfterFetchFunctionsFile
/** @type {(!eco.artd.IFirebaseJoinpointModel.__beforeDownloadDeps<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__beforeDownloadDeps<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.beforeDownloadDeps
/** @type {(!eco.artd.IFirebaseJoinpointModel.__afterDownloadDeps<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterDownloadDeps<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.afterDownloadDeps
/** @type {(!eco.artd.IFirebaseJoinpointModel.__afterDownloadDepsThrows<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterDownloadDepsThrows<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.afterDownloadDepsThrows
/** @type {(!eco.artd.IFirebaseJoinpointModel.__afterDownloadDepsReturns<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterDownloadDepsReturns<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.afterDownloadDepsReturns
/** @type {(!eco.artd.IFirebaseJoinpointModel.__afterDownloadDepsCancels<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterDownloadDepsCancels<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.afterDownloadDepsCancels
/** @type {(!eco.artd.IFirebaseJoinpointModel.__immediatelyAfterDownloadDeps<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__immediatelyAfterDownloadDeps<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.immediatelyAfterDownloadDeps
/** @type {(!eco.artd.IFirebaseJoinpointModel.__beforeDownloadMethod<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__beforeDownloadMethod<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.beforeDownloadMethod
/** @type {(!eco.artd.IFirebaseJoinpointModel.__afterDownloadMethod<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterDownloadMethod<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.afterDownloadMethod
/** @type {(!eco.artd.IFirebaseJoinpointModel.__afterDownloadMethodThrows<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterDownloadMethodThrows<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.afterDownloadMethodThrows
/** @type {(!eco.artd.IFirebaseJoinpointModel.__afterDownloadMethodReturns<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterDownloadMethodReturns<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.afterDownloadMethodReturns
/** @type {(!eco.artd.IFirebaseJoinpointModel.__afterDownloadMethodCancels<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterDownloadMethodCancels<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.afterDownloadMethodCancels
/** @type {(!eco.artd.IFirebaseJoinpointModel.__immediatelyAfterDownloadMethod<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__immediatelyAfterDownloadMethod<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.immediatelyAfterDownloadMethod
/** @type {(!eco.artd.IFirebaseJoinpointModel.__beforeDownloadMethodDirectly<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__beforeDownloadMethodDirectly<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.beforeDownloadMethodDirectly
/** @type {(!eco.artd.IFirebaseJoinpointModel.__afterDownloadMethodDirectly<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterDownloadMethodDirectly<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.afterDownloadMethodDirectly
/** @type {(!eco.artd.IFirebaseJoinpointModel.__afterDownloadMethodDirectlyThrows<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterDownloadMethodDirectlyThrows<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.afterDownloadMethodDirectlyThrows
/** @type {(!eco.artd.IFirebaseJoinpointModel.__afterDownloadMethodDirectlyReturns<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterDownloadMethodDirectlyReturns<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.afterDownloadMethodDirectlyReturns
/** @type {(!eco.artd.IFirebaseJoinpointModel.__afterDownloadMethodDirectlyCancels<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterDownloadMethodDirectlyCancels<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.afterDownloadMethodDirectlyCancels
/** @type {(!eco.artd.IFirebaseJoinpointModel.__immediatelyAfterDownloadMethodDirectly<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__immediatelyAfterDownloadMethodDirectly<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.immediatelyAfterDownloadMethodDirectly
/** @type {(!eco.artd.IFirebaseJoinpointModel.__beforeGetId<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__beforeGetId<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.beforeGetId
/** @type {(!eco.artd.IFirebaseJoinpointModel.__afterGetId<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterGetId<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.afterGetId
/** @type {(!eco.artd.IFirebaseJoinpointModel.__afterGetIdThrows<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterGetIdThrows<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.afterGetIdThrows
/** @type {(!eco.artd.IFirebaseJoinpointModel.__afterGetIdReturns<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterGetIdReturns<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.afterGetIdReturns
/** @type {(!eco.artd.IFirebaseJoinpointModel.__afterGetIdCancels<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterGetIdCancels<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.afterGetIdCancels
/** @type {(!eco.artd.IFirebaseJoinpointModel.__beforePatchDeps<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__beforePatchDeps<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.beforePatchDeps
/** @type {(!eco.artd.IFirebaseJoinpointModel.__afterPatchDeps<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterPatchDeps<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.afterPatchDeps
/** @type {(!eco.artd.IFirebaseJoinpointModel.__afterPatchDepsThrows<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterPatchDepsThrows<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.afterPatchDepsThrows
/** @type {(!eco.artd.IFirebaseJoinpointModel.__afterPatchDepsReturns<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterPatchDepsReturns<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.afterPatchDepsReturns
/** @type {(!eco.artd.IFirebaseJoinpointModel.__afterPatchDepsCancels<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterPatchDepsCancels<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.afterPatchDepsCancels
/** @type {(!eco.artd.IFirebaseJoinpointModel.__immediatelyAfterPatchDeps<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__immediatelyAfterPatchDeps<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.immediatelyAfterPatchDeps
/** @type {(!eco.artd.IFirebaseJoinpointModel.__beforeDownloadPageData<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__beforeDownloadPageData<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.beforeDownloadPageData
/** @type {(!eco.artd.IFirebaseJoinpointModel.__afterDownloadPageData<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterDownloadPageData<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.afterDownloadPageData
/** @type {(!eco.artd.IFirebaseJoinpointModel.__afterDownloadPageDataThrows<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterDownloadPageDataThrows<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.afterDownloadPageDataThrows
/** @type {(!eco.artd.IFirebaseJoinpointModel.__afterDownloadPageDataReturns<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterDownloadPageDataReturns<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.afterDownloadPageDataReturns
/** @type {(!eco.artd.IFirebaseJoinpointModel.__afterDownloadPageDataCancels<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterDownloadPageDataCancels<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.afterDownloadPageDataCancels
/** @type {(!eco.artd.IFirebaseJoinpointModel.__immediatelyAfterDownloadPageData<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__immediatelyAfterDownloadPageData<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.immediatelyAfterDownloadPageData
/** @type {(!eco.artd.IFirebaseJoinpointModel.__beforeLoadRequirePageData<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__beforeLoadRequirePageData<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.beforeLoadRequirePageData
/** @type {(!eco.artd.IFirebaseJoinpointModel.__afterLoadRequirePageData<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterLoadRequirePageData<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.afterLoadRequirePageData
/** @type {(!eco.artd.IFirebaseJoinpointModel.__afterLoadRequirePageDataThrows<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterLoadRequirePageDataThrows<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.afterLoadRequirePageDataThrows
/** @type {(!eco.artd.IFirebaseJoinpointModel.__afterLoadRequirePageDataReturns<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterLoadRequirePageDataReturns<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.afterLoadRequirePageDataReturns
/** @type {(!eco.artd.IFirebaseJoinpointModel.__afterLoadRequirePageDataCancels<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterLoadRequirePageDataCancels<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.afterLoadRequirePageDataCancels
/** @type {(!eco.artd.IFirebaseJoinpointModel.__beforeLoadRequire<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__beforeLoadRequire<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.beforeLoadRequire
/** @type {(!eco.artd.IFirebaseJoinpointModel.__afterLoadRequire<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterLoadRequire<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.afterLoadRequire
/** @type {(!eco.artd.IFirebaseJoinpointModel.__afterLoadRequireThrows<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterLoadRequireThrows<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.afterLoadRequireThrows
/** @type {(!eco.artd.IFirebaseJoinpointModel.__afterLoadRequireReturns<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterLoadRequireReturns<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.afterLoadRequireReturns
/** @type {(!eco.artd.IFirebaseJoinpointModel.__afterLoadRequireCancels<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterLoadRequireCancels<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.afterLoadRequireCancels
/** @type {(!eco.artd.IFirebaseJoinpointModel.__beforeUpdateErrorStack<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__beforeUpdateErrorStack<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.beforeUpdateErrorStack
/** @type {(!eco.artd.IFirebaseJoinpointModel.__afterUpdateErrorStack<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterUpdateErrorStack<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.afterUpdateErrorStack
/** @type {(!eco.artd.IFirebaseJoinpointModel.__afterUpdateErrorStackThrows<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterUpdateErrorStackThrows<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.afterUpdateErrorStackThrows
/** @type {(!eco.artd.IFirebaseJoinpointModel.__afterUpdateErrorStackReturns<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterUpdateErrorStackReturns<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.afterUpdateErrorStackReturns
/** @type {(!eco.artd.IFirebaseJoinpointModel.__afterUpdateErrorStackCancels<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterUpdateErrorStackCancels<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.afterUpdateErrorStackCancels
/** @type {(!eco.artd.IFirebaseJoinpointModel.__beforeValidateMethod<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__beforeValidateMethod<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.beforeValidateMethod
/** @type {(!eco.artd.IFirebaseJoinpointModel.__afterValidateMethod<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterValidateMethod<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.afterValidateMethod
/** @type {(!eco.artd.IFirebaseJoinpointModel.__afterValidateMethodThrows<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterValidateMethodThrows<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.afterValidateMethodThrows
/** @type {(!eco.artd.IFirebaseJoinpointModel.__afterValidateMethodReturns<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterValidateMethodReturns<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.afterValidateMethodReturns
/** @type {(!eco.artd.IFirebaseJoinpointModel.__afterValidateMethodCancels<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterValidateMethodCancels<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.afterValidateMethodCancels
/** @type {(!eco.artd.IFirebaseJoinpointModel.__beforeValidateLoadAndInvoke<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__beforeValidateLoadAndInvoke<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.beforeValidateLoadAndInvoke
/** @type {(!eco.artd.IFirebaseJoinpointModel.__afterValidateLoadAndInvoke<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterValidateLoadAndInvoke<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.afterValidateLoadAndInvoke
/** @type {(!eco.artd.IFirebaseJoinpointModel.__afterValidateLoadAndInvokeThrows<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterValidateLoadAndInvokeThrows<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.afterValidateLoadAndInvokeThrows
/** @type {(!eco.artd.IFirebaseJoinpointModel.__afterValidateLoadAndInvokeReturns<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterValidateLoadAndInvokeReturns<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.afterValidateLoadAndInvokeReturns
/** @type {(!eco.artd.IFirebaseJoinpointModel.__afterValidateLoadAndInvokeCancels<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterValidateLoadAndInvokeCancels<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.afterValidateLoadAndInvokeCancels
/** @type {(!eco.artd.IFirebaseJoinpointModel.__immediatelyAfterValidateLoadAndInvoke<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__immediatelyAfterValidateLoadAndInvoke<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.immediatelyAfterValidateLoadAndInvoke
/** @type {(!eco.artd.IFirebaseJoinpointModel.__beforeValidateLoadAndInvokeById<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__beforeValidateLoadAndInvokeById<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.beforeValidateLoadAndInvokeById
/** @type {(!eco.artd.IFirebaseJoinpointModel.__afterValidateLoadAndInvokeById<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterValidateLoadAndInvokeById<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.afterValidateLoadAndInvokeById
/** @type {(!eco.artd.IFirebaseJoinpointModel.__afterValidateLoadAndInvokeByIdThrows<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterValidateLoadAndInvokeByIdThrows<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.afterValidateLoadAndInvokeByIdThrows
/** @type {(!eco.artd.IFirebaseJoinpointModel.__afterValidateLoadAndInvokeByIdReturns<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterValidateLoadAndInvokeByIdReturns<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.afterValidateLoadAndInvokeByIdReturns
/** @type {(!eco.artd.IFirebaseJoinpointModel.__afterValidateLoadAndInvokeByIdCancels<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterValidateLoadAndInvokeByIdCancels<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.afterValidateLoadAndInvokeByIdCancels
/** @type {(!eco.artd.IFirebaseJoinpointModel.__immediatelyAfterValidateLoadAndInvokeById<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__immediatelyAfterValidateLoadAndInvokeById<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.immediatelyAfterValidateLoadAndInvokeById
/** @type {(!eco.artd.IFirebaseJoinpointModel.__beforeMakeInvoker<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__beforeMakeInvoker<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.beforeMakeInvoker
/** @type {(!eco.artd.IFirebaseJoinpointModel.__afterMakeInvoker<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterMakeInvoker<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.afterMakeInvoker
/** @type {(!eco.artd.IFirebaseJoinpointModel.__afterMakeInvokerThrows<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterMakeInvokerThrows<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.afterMakeInvokerThrows
/** @type {(!eco.artd.IFirebaseJoinpointModel.__afterMakeInvokerReturns<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterMakeInvokerReturns<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.afterMakeInvokerReturns
/** @type {(!eco.artd.IFirebaseJoinpointModel.__afterMakeInvokerCancels<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterMakeInvokerCancels<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.afterMakeInvokerCancels
/** @type {(!eco.artd.IFirebaseJoinpointModel.__beforeLoadAndInvokeById<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__beforeLoadAndInvokeById<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.beforeLoadAndInvokeById
/** @type {(!eco.artd.IFirebaseJoinpointModel.__afterLoadAndInvokeById<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterLoadAndInvokeById<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.afterLoadAndInvokeById
/** @type {(!eco.artd.IFirebaseJoinpointModel.__afterLoadAndInvokeByIdThrows<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterLoadAndInvokeByIdThrows<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.afterLoadAndInvokeByIdThrows
/** @type {(!eco.artd.IFirebaseJoinpointModel.__afterLoadAndInvokeByIdReturns<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterLoadAndInvokeByIdReturns<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.afterLoadAndInvokeByIdReturns
/** @type {(!eco.artd.IFirebaseJoinpointModel.__afterLoadAndInvokeByIdCancels<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterLoadAndInvokeByIdCancels<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.afterLoadAndInvokeByIdCancels
/** @type {(!eco.artd.IFirebaseJoinpointModel.__beforeLoadAndInvoke<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__beforeLoadAndInvoke<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.beforeLoadAndInvoke
/** @type {(!eco.artd.IFirebaseJoinpointModel.__afterLoadAndInvoke<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterLoadAndInvoke<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.afterLoadAndInvoke
/** @type {(!eco.artd.IFirebaseJoinpointModel.__afterLoadAndInvokeThrows<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterLoadAndInvokeThrows<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.afterLoadAndInvokeThrows
/** @type {(!eco.artd.IFirebaseJoinpointModel.__afterLoadAndInvokeReturns<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterLoadAndInvokeReturns<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.afterLoadAndInvokeReturns
/** @type {(!eco.artd.IFirebaseJoinpointModel.__afterLoadAndInvokeCancels<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterLoadAndInvokeCancels<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.afterLoadAndInvokeCancels
/** @type {(!eco.artd.IFirebaseJoinpointModel.__immediatelyAfterLoadAndInvoke<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__immediatelyAfterLoadAndInvoke<THIS>>)} */
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.immediatelyAfterLoadAndInvoke

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.FirebaseJoinpointModelBindingHyperslice no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @constructor
 * @implements {eco.artd.IFirebaseJoinpointModelBindingHyperslice<THIS>}
 * @template THIS
 */
eco.artd.FirebaseJoinpointModelBindingHyperslice = function() {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/** @interface */
eco.artd.IFirebaseJoinpointModel = function() {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.BeforeFunctionPointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.beforeFunction = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterFunctionPointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.afterFunction = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterThrowsFunctionPointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.afterFunctionThrows = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterReturnsFunctionPointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.afterFunctionReturns = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterCancelsFunctionPointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.afterFunctionCancels = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterFunctionPointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.immediatelyAfterFunction = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.BeforeEvalBodyWithProfileTimePointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.beforeEvalBodyWithProfileTime = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterEvalBodyWithProfileTimePointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.afterEvalBodyWithProfileTime = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterThrowsEvalBodyWithProfileTimePointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.afterEvalBodyWithProfileTimeThrows = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterReturnsEvalBodyWithProfileTimePointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.afterEvalBodyWithProfileTimeReturns = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterCancelsEvalBodyWithProfileTimePointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.afterEvalBodyWithProfileTimeCancels = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.BeforeGetPageDataFromFirestorePointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.beforeGetPageDataFromFirestore = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterGetPageDataFromFirestorePointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.afterGetPageDataFromFirestore = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterThrowsGetPageDataFromFirestorePointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.afterGetPageDataFromFirestoreThrows = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterReturnsGetPageDataFromFirestorePointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.afterGetPageDataFromFirestoreReturns = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterCancelsGetPageDataFromFirestorePointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.afterGetPageDataFromFirestoreCancels = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterGetPageDataFromFirestorePointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.immediatelyAfterGetPageDataFromFirestore = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.BeforeDownloadAndEvalPageDataPointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.beforeDownloadAndEvalPageData = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterDownloadAndEvalPageDataPointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.afterDownloadAndEvalPageData = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterThrowsDownloadAndEvalPageDataPointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.afterDownloadAndEvalPageDataThrows = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterReturnsDownloadAndEvalPageDataPointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.afterDownloadAndEvalPageDataReturns = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterCancelsDownloadAndEvalPageDataPointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.afterDownloadAndEvalPageDataCancels = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterDownloadAndEvalPageDataPointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.immediatelyAfterDownloadAndEvalPageData = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.BeforeEvalBodyPointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.beforeEvalBody = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterEvalBodyPointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.afterEvalBody = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterThrowsEvalBodyPointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.afterEvalBodyThrows = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterReturnsEvalBodyPointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.afterEvalBodyReturns = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterCancelsEvalBodyPointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.afterEvalBodyCancels = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.BeforeLogRpcInvocationPointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.beforeLogRpcInvocation = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterLogRpcInvocationPointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.afterLogRpcInvocation = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterThrowsLogRpcInvocationPointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.afterLogRpcInvocationThrows = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterReturnsLogRpcInvocationPointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.afterLogRpcInvocationReturns = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterCancelsLogRpcInvocationPointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.afterLogRpcInvocationCancels = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterLogRpcInvocationPointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.immediatelyAfterLogRpcInvocation = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.BeforeFetchFunctionsFilePointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.beforeFetchFunctionsFile = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterFetchFunctionsFilePointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.afterFetchFunctionsFile = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterThrowsFetchFunctionsFilePointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.afterFetchFunctionsFileThrows = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterReturnsFetchFunctionsFilePointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.afterFetchFunctionsFileReturns = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterCancelsFetchFunctionsFilePointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.afterFetchFunctionsFileCancels = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterFetchFunctionsFilePointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.immediatelyAfterFetchFunctionsFile = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.BeforeDownloadDepsPointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.beforeDownloadDeps = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterDownloadDepsPointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.afterDownloadDeps = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterThrowsDownloadDepsPointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.afterDownloadDepsThrows = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterReturnsDownloadDepsPointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.afterDownloadDepsReturns = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterCancelsDownloadDepsPointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.afterDownloadDepsCancels = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterDownloadDepsPointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.immediatelyAfterDownloadDeps = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.BeforeDownloadMethodPointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.beforeDownloadMethod = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterDownloadMethodPointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.afterDownloadMethod = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterThrowsDownloadMethodPointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.afterDownloadMethodThrows = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterReturnsDownloadMethodPointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.afterDownloadMethodReturns = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterCancelsDownloadMethodPointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.afterDownloadMethodCancels = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterDownloadMethodPointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.immediatelyAfterDownloadMethod = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.BeforeDownloadMethodDirectlyPointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.beforeDownloadMethodDirectly = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterDownloadMethodDirectlyPointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.afterDownloadMethodDirectly = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterThrowsDownloadMethodDirectlyPointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.afterDownloadMethodDirectlyThrows = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterReturnsDownloadMethodDirectlyPointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.afterDownloadMethodDirectlyReturns = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterCancelsDownloadMethodDirectlyPointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.afterDownloadMethodDirectlyCancels = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterDownloadMethodDirectlyPointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.immediatelyAfterDownloadMethodDirectly = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.BeforeGetIdPointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.beforeGetId = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterGetIdPointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.afterGetId = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterThrowsGetIdPointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.afterGetIdThrows = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterReturnsGetIdPointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.afterGetIdReturns = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterCancelsGetIdPointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.afterGetIdCancels = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.BeforePatchDepsPointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.beforePatchDeps = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterPatchDepsPointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.afterPatchDeps = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterThrowsPatchDepsPointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.afterPatchDepsThrows = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterReturnsPatchDepsPointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.afterPatchDepsReturns = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterCancelsPatchDepsPointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.afterPatchDepsCancels = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterPatchDepsPointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.immediatelyAfterPatchDeps = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.BeforeDownloadPageDataPointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.beforeDownloadPageData = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterDownloadPageDataPointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.afterDownloadPageData = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterThrowsDownloadPageDataPointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.afterDownloadPageDataThrows = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterReturnsDownloadPageDataPointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.afterDownloadPageDataReturns = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterCancelsDownloadPageDataPointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.afterDownloadPageDataCancels = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterDownloadPageDataPointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.immediatelyAfterDownloadPageData = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.BeforeLoadRequirePageDataPointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.beforeLoadRequirePageData = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterLoadRequirePageDataPointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.afterLoadRequirePageData = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterThrowsLoadRequirePageDataPointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.afterLoadRequirePageDataThrows = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterReturnsLoadRequirePageDataPointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.afterLoadRequirePageDataReturns = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterCancelsLoadRequirePageDataPointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.afterLoadRequirePageDataCancels = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.BeforeLoadRequirePointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.beforeLoadRequire = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterLoadRequirePointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.afterLoadRequire = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterThrowsLoadRequirePointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.afterLoadRequireThrows = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterReturnsLoadRequirePointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.afterLoadRequireReturns = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterCancelsLoadRequirePointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.afterLoadRequireCancels = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.BeforeUpdateErrorStackPointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.beforeUpdateErrorStack = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterUpdateErrorStackPointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.afterUpdateErrorStack = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterThrowsUpdateErrorStackPointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.afterUpdateErrorStackThrows = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterReturnsUpdateErrorStackPointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.afterUpdateErrorStackReturns = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterCancelsUpdateErrorStackPointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.afterUpdateErrorStackCancels = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.BeforeValidateMethodPointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.beforeValidateMethod = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterValidateMethodPointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.afterValidateMethod = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterThrowsValidateMethodPointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.afterValidateMethodThrows = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterReturnsValidateMethodPointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.afterValidateMethodReturns = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterCancelsValidateMethodPointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.afterValidateMethodCancels = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.BeforeValidateLoadAndInvokePointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.beforeValidateLoadAndInvoke = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterValidateLoadAndInvokePointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.afterValidateLoadAndInvoke = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterThrowsValidateLoadAndInvokePointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.afterValidateLoadAndInvokeThrows = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterReturnsValidateLoadAndInvokePointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.afterValidateLoadAndInvokeReturns = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterCancelsValidateLoadAndInvokePointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.afterValidateLoadAndInvokeCancels = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterValidateLoadAndInvokePointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.immediatelyAfterValidateLoadAndInvoke = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.BeforeValidateLoadAndInvokeByIdPointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.beforeValidateLoadAndInvokeById = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterValidateLoadAndInvokeByIdPointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.afterValidateLoadAndInvokeById = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterThrowsValidateLoadAndInvokeByIdPointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.afterValidateLoadAndInvokeByIdThrows = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterReturnsValidateLoadAndInvokeByIdPointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.afterValidateLoadAndInvokeByIdReturns = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterCancelsValidateLoadAndInvokeByIdPointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.afterValidateLoadAndInvokeByIdCancels = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterValidateLoadAndInvokeByIdPointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.immediatelyAfterValidateLoadAndInvokeById = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.BeforeMakeInvokerPointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.beforeMakeInvoker = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterMakeInvokerPointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.afterMakeInvoker = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterThrowsMakeInvokerPointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.afterMakeInvokerThrows = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterReturnsMakeInvokerPointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.afterMakeInvokerReturns = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterCancelsMakeInvokerPointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.afterMakeInvokerCancels = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.BeforeLoadAndInvokeByIdPointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.beforeLoadAndInvokeById = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterLoadAndInvokeByIdPointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.afterLoadAndInvokeById = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterThrowsLoadAndInvokeByIdPointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.afterLoadAndInvokeByIdThrows = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterReturnsLoadAndInvokeByIdPointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.afterLoadAndInvokeByIdReturns = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterCancelsLoadAndInvokeByIdPointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.afterLoadAndInvokeByIdCancels = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.BeforeLoadAndInvokePointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.beforeLoadAndInvoke = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterLoadAndInvokePointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.afterLoadAndInvoke = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterThrowsLoadAndInvokePointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.afterLoadAndInvokeThrows = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterReturnsLoadAndInvokePointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.afterLoadAndInvokeReturns = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterCancelsLoadAndInvokePointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.afterLoadAndInvokeCancels = function(data) {}
/**
 * @param {!eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterLoadAndInvokePointcutData} [data]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.prototype.immediatelyAfterLoadAndInvoke = function(data) {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.FirebaseJoinpointModel no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @constructor
 * @implements {eco.artd.IFirebaseJoinpointModel}
 */
eco.artd.FirebaseJoinpointModel = function() {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.RecordIFirebaseJoinpointModel no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/** @typedef {{ beforeFunction: eco.artd.IFirebaseJoinpointModel.beforeFunction, afterFunction: eco.artd.IFirebaseJoinpointModel.afterFunction, afterFunctionThrows: eco.artd.IFirebaseJoinpointModel.afterFunctionThrows, afterFunctionReturns: eco.artd.IFirebaseJoinpointModel.afterFunctionReturns, afterFunctionCancels: eco.artd.IFirebaseJoinpointModel.afterFunctionCancels, immediatelyAfterFunction: eco.artd.IFirebaseJoinpointModel.immediatelyAfterFunction, beforeEvalBodyWithProfileTime: eco.artd.IFirebaseJoinpointModel.beforeEvalBodyWithProfileTime, afterEvalBodyWithProfileTime: eco.artd.IFirebaseJoinpointModel.afterEvalBodyWithProfileTime, afterEvalBodyWithProfileTimeThrows: eco.artd.IFirebaseJoinpointModel.afterEvalBodyWithProfileTimeThrows, afterEvalBodyWithProfileTimeReturns: eco.artd.IFirebaseJoinpointModel.afterEvalBodyWithProfileTimeReturns, afterEvalBodyWithProfileTimeCancels: eco.artd.IFirebaseJoinpointModel.afterEvalBodyWithProfileTimeCancels, beforeGetPageDataFromFirestore: eco.artd.IFirebaseJoinpointModel.beforeGetPageDataFromFirestore, afterGetPageDataFromFirestore: eco.artd.IFirebaseJoinpointModel.afterGetPageDataFromFirestore, afterGetPageDataFromFirestoreThrows: eco.artd.IFirebaseJoinpointModel.afterGetPageDataFromFirestoreThrows, afterGetPageDataFromFirestoreReturns: eco.artd.IFirebaseJoinpointModel.afterGetPageDataFromFirestoreReturns, afterGetPageDataFromFirestoreCancels: eco.artd.IFirebaseJoinpointModel.afterGetPageDataFromFirestoreCancels, immediatelyAfterGetPageDataFromFirestore: eco.artd.IFirebaseJoinpointModel.immediatelyAfterGetPageDataFromFirestore, beforeDownloadAndEvalPageData: eco.artd.IFirebaseJoinpointModel.beforeDownloadAndEvalPageData, afterDownloadAndEvalPageData: eco.artd.IFirebaseJoinpointModel.afterDownloadAndEvalPageData, afterDownloadAndEvalPageDataThrows: eco.artd.IFirebaseJoinpointModel.afterDownloadAndEvalPageDataThrows, afterDownloadAndEvalPageDataReturns: eco.artd.IFirebaseJoinpointModel.afterDownloadAndEvalPageDataReturns, afterDownloadAndEvalPageDataCancels: eco.artd.IFirebaseJoinpointModel.afterDownloadAndEvalPageDataCancels, immediatelyAfterDownloadAndEvalPageData: eco.artd.IFirebaseJoinpointModel.immediatelyAfterDownloadAndEvalPageData, beforeEvalBody: eco.artd.IFirebaseJoinpointModel.beforeEvalBody, afterEvalBody: eco.artd.IFirebaseJoinpointModel.afterEvalBody, afterEvalBodyThrows: eco.artd.IFirebaseJoinpointModel.afterEvalBodyThrows, afterEvalBodyReturns: eco.artd.IFirebaseJoinpointModel.afterEvalBodyReturns, afterEvalBodyCancels: eco.artd.IFirebaseJoinpointModel.afterEvalBodyCancels, beforeLogRpcInvocation: eco.artd.IFirebaseJoinpointModel.beforeLogRpcInvocation, afterLogRpcInvocation: eco.artd.IFirebaseJoinpointModel.afterLogRpcInvocation, afterLogRpcInvocationThrows: eco.artd.IFirebaseJoinpointModel.afterLogRpcInvocationThrows, afterLogRpcInvocationReturns: eco.artd.IFirebaseJoinpointModel.afterLogRpcInvocationReturns, afterLogRpcInvocationCancels: eco.artd.IFirebaseJoinpointModel.afterLogRpcInvocationCancels, immediatelyAfterLogRpcInvocation: eco.artd.IFirebaseJoinpointModel.immediatelyAfterLogRpcInvocation, beforeFetchFunctionsFile: eco.artd.IFirebaseJoinpointModel.beforeFetchFunctionsFile, afterFetchFunctionsFile: eco.artd.IFirebaseJoinpointModel.afterFetchFunctionsFile, afterFetchFunctionsFileThrows: eco.artd.IFirebaseJoinpointModel.afterFetchFunctionsFileThrows, afterFetchFunctionsFileReturns: eco.artd.IFirebaseJoinpointModel.afterFetchFunctionsFileReturns, afterFetchFunctionsFileCancels: eco.artd.IFirebaseJoinpointModel.afterFetchFunctionsFileCancels, immediatelyAfterFetchFunctionsFile: eco.artd.IFirebaseJoinpointModel.immediatelyAfterFetchFunctionsFile, beforeDownloadDeps: eco.artd.IFirebaseJoinpointModel.beforeDownloadDeps, afterDownloadDeps: eco.artd.IFirebaseJoinpointModel.afterDownloadDeps, afterDownloadDepsThrows: eco.artd.IFirebaseJoinpointModel.afterDownloadDepsThrows, afterDownloadDepsReturns: eco.artd.IFirebaseJoinpointModel.afterDownloadDepsReturns, afterDownloadDepsCancels: eco.artd.IFirebaseJoinpointModel.afterDownloadDepsCancels, immediatelyAfterDownloadDeps: eco.artd.IFirebaseJoinpointModel.immediatelyAfterDownloadDeps, beforeDownloadMethod: eco.artd.IFirebaseJoinpointModel.beforeDownloadMethod, afterDownloadMethod: eco.artd.IFirebaseJoinpointModel.afterDownloadMethod, afterDownloadMethodThrows: eco.artd.IFirebaseJoinpointModel.afterDownloadMethodThrows, afterDownloadMethodReturns: eco.artd.IFirebaseJoinpointModel.afterDownloadMethodReturns, afterDownloadMethodCancels: eco.artd.IFirebaseJoinpointModel.afterDownloadMethodCancels, immediatelyAfterDownloadMethod: eco.artd.IFirebaseJoinpointModel.immediatelyAfterDownloadMethod, beforeDownloadMethodDirectly: eco.artd.IFirebaseJoinpointModel.beforeDownloadMethodDirectly, afterDownloadMethodDirectly: eco.artd.IFirebaseJoinpointModel.afterDownloadMethodDirectly, afterDownloadMethodDirectlyThrows: eco.artd.IFirebaseJoinpointModel.afterDownloadMethodDirectlyThrows, afterDownloadMethodDirectlyReturns: eco.artd.IFirebaseJoinpointModel.afterDownloadMethodDirectlyReturns, afterDownloadMethodDirectlyCancels: eco.artd.IFirebaseJoinpointModel.afterDownloadMethodDirectlyCancels, immediatelyAfterDownloadMethodDirectly: eco.artd.IFirebaseJoinpointModel.immediatelyAfterDownloadMethodDirectly, beforeGetId: eco.artd.IFirebaseJoinpointModel.beforeGetId, afterGetId: eco.artd.IFirebaseJoinpointModel.afterGetId, afterGetIdThrows: eco.artd.IFirebaseJoinpointModel.afterGetIdThrows, afterGetIdReturns: eco.artd.IFirebaseJoinpointModel.afterGetIdReturns, afterGetIdCancels: eco.artd.IFirebaseJoinpointModel.afterGetIdCancels, beforePatchDeps: eco.artd.IFirebaseJoinpointModel.beforePatchDeps, afterPatchDeps: eco.artd.IFirebaseJoinpointModel.afterPatchDeps, afterPatchDepsThrows: eco.artd.IFirebaseJoinpointModel.afterPatchDepsThrows, afterPatchDepsReturns: eco.artd.IFirebaseJoinpointModel.afterPatchDepsReturns, afterPatchDepsCancels: eco.artd.IFirebaseJoinpointModel.afterPatchDepsCancels, immediatelyAfterPatchDeps: eco.artd.IFirebaseJoinpointModel.immediatelyAfterPatchDeps, beforeDownloadPageData: eco.artd.IFirebaseJoinpointModel.beforeDownloadPageData, afterDownloadPageData: eco.artd.IFirebaseJoinpointModel.afterDownloadPageData, afterDownloadPageDataThrows: eco.artd.IFirebaseJoinpointModel.afterDownloadPageDataThrows, afterDownloadPageDataReturns: eco.artd.IFirebaseJoinpointModel.afterDownloadPageDataReturns, afterDownloadPageDataCancels: eco.artd.IFirebaseJoinpointModel.afterDownloadPageDataCancels, immediatelyAfterDownloadPageData: eco.artd.IFirebaseJoinpointModel.immediatelyAfterDownloadPageData, beforeLoadRequirePageData: eco.artd.IFirebaseJoinpointModel.beforeLoadRequirePageData, afterLoadRequirePageData: eco.artd.IFirebaseJoinpointModel.afterLoadRequirePageData, afterLoadRequirePageDataThrows: eco.artd.IFirebaseJoinpointModel.afterLoadRequirePageDataThrows, afterLoadRequirePageDataReturns: eco.artd.IFirebaseJoinpointModel.afterLoadRequirePageDataReturns, afterLoadRequirePageDataCancels: eco.artd.IFirebaseJoinpointModel.afterLoadRequirePageDataCancels, beforeLoadRequire: eco.artd.IFirebaseJoinpointModel.beforeLoadRequire, afterLoadRequire: eco.artd.IFirebaseJoinpointModel.afterLoadRequire, afterLoadRequireThrows: eco.artd.IFirebaseJoinpointModel.afterLoadRequireThrows, afterLoadRequireReturns: eco.artd.IFirebaseJoinpointModel.afterLoadRequireReturns, afterLoadRequireCancels: eco.artd.IFirebaseJoinpointModel.afterLoadRequireCancels, beforeUpdateErrorStack: eco.artd.IFirebaseJoinpointModel.beforeUpdateErrorStack, afterUpdateErrorStack: eco.artd.IFirebaseJoinpointModel.afterUpdateErrorStack, afterUpdateErrorStackThrows: eco.artd.IFirebaseJoinpointModel.afterUpdateErrorStackThrows, afterUpdateErrorStackReturns: eco.artd.IFirebaseJoinpointModel.afterUpdateErrorStackReturns, afterUpdateErrorStackCancels: eco.artd.IFirebaseJoinpointModel.afterUpdateErrorStackCancels, beforeValidateMethod: eco.artd.IFirebaseJoinpointModel.beforeValidateMethod, afterValidateMethod: eco.artd.IFirebaseJoinpointModel.afterValidateMethod, afterValidateMethodThrows: eco.artd.IFirebaseJoinpointModel.afterValidateMethodThrows, afterValidateMethodReturns: eco.artd.IFirebaseJoinpointModel.afterValidateMethodReturns, afterValidateMethodCancels: eco.artd.IFirebaseJoinpointModel.afterValidateMethodCancels, beforeValidateLoadAndInvoke: eco.artd.IFirebaseJoinpointModel.beforeValidateLoadAndInvoke, afterValidateLoadAndInvoke: eco.artd.IFirebaseJoinpointModel.afterValidateLoadAndInvoke, afterValidateLoadAndInvokeThrows: eco.artd.IFirebaseJoinpointModel.afterValidateLoadAndInvokeThrows, afterValidateLoadAndInvokeReturns: eco.artd.IFirebaseJoinpointModel.afterValidateLoadAndInvokeReturns, afterValidateLoadAndInvokeCancels: eco.artd.IFirebaseJoinpointModel.afterValidateLoadAndInvokeCancels, immediatelyAfterValidateLoadAndInvoke: eco.artd.IFirebaseJoinpointModel.immediatelyAfterValidateLoadAndInvoke, beforeValidateLoadAndInvokeById: eco.artd.IFirebaseJoinpointModel.beforeValidateLoadAndInvokeById, afterValidateLoadAndInvokeById: eco.artd.IFirebaseJoinpointModel.afterValidateLoadAndInvokeById, afterValidateLoadAndInvokeByIdThrows: eco.artd.IFirebaseJoinpointModel.afterValidateLoadAndInvokeByIdThrows, afterValidateLoadAndInvokeByIdReturns: eco.artd.IFirebaseJoinpointModel.afterValidateLoadAndInvokeByIdReturns, afterValidateLoadAndInvokeByIdCancels: eco.artd.IFirebaseJoinpointModel.afterValidateLoadAndInvokeByIdCancels, immediatelyAfterValidateLoadAndInvokeById: eco.artd.IFirebaseJoinpointModel.immediatelyAfterValidateLoadAndInvokeById, beforeMakeInvoker: eco.artd.IFirebaseJoinpointModel.beforeMakeInvoker, afterMakeInvoker: eco.artd.IFirebaseJoinpointModel.afterMakeInvoker, afterMakeInvokerThrows: eco.artd.IFirebaseJoinpointModel.afterMakeInvokerThrows, afterMakeInvokerReturns: eco.artd.IFirebaseJoinpointModel.afterMakeInvokerReturns, afterMakeInvokerCancels: eco.artd.IFirebaseJoinpointModel.afterMakeInvokerCancels, beforeLoadAndInvokeById: eco.artd.IFirebaseJoinpointModel.beforeLoadAndInvokeById, afterLoadAndInvokeById: eco.artd.IFirebaseJoinpointModel.afterLoadAndInvokeById, afterLoadAndInvokeByIdThrows: eco.artd.IFirebaseJoinpointModel.afterLoadAndInvokeByIdThrows, afterLoadAndInvokeByIdReturns: eco.artd.IFirebaseJoinpointModel.afterLoadAndInvokeByIdReturns, afterLoadAndInvokeByIdCancels: eco.artd.IFirebaseJoinpointModel.afterLoadAndInvokeByIdCancels, beforeLoadAndInvoke: eco.artd.IFirebaseJoinpointModel.beforeLoadAndInvoke, afterLoadAndInvoke: eco.artd.IFirebaseJoinpointModel.afterLoadAndInvoke, afterLoadAndInvokeThrows: eco.artd.IFirebaseJoinpointModel.afterLoadAndInvokeThrows, afterLoadAndInvokeReturns: eco.artd.IFirebaseJoinpointModel.afterLoadAndInvokeReturns, afterLoadAndInvokeCancels: eco.artd.IFirebaseJoinpointModel.afterLoadAndInvokeCancels, immediatelyAfterLoadAndInvoke: eco.artd.IFirebaseJoinpointModel.immediatelyAfterLoadAndInvoke }} */
eco.artd.RecordIFirebaseJoinpointModel

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.BoundIFirebaseJoinpointModel no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.RecordIFirebaseJoinpointModel}
 */
eco.artd.BoundIFirebaseJoinpointModel = function() {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.BoundFirebaseJoinpointModel no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.BoundIFirebaseJoinpointModel}
 * @extends {engineering.type.BoundIInitialiser}
 */
eco.artd.BoundFirebaseJoinpointModel = function() {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.beforeFunction no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.BeforeFunctionPointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__beforeFunction = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.BeforeFunctionPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.beforeFunction
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.BeforeFunctionPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._beforeFunction
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__beforeFunction} */
eco.artd.IFirebaseJoinpointModel.__beforeFunction

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.afterFunction no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterFunctionPointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__afterFunction = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.AfterFunctionPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.afterFunction
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.AfterFunctionPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._afterFunction
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__afterFunction} */
eco.artd.IFirebaseJoinpointModel.__afterFunction

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.afterFunctionThrows no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterThrowsFunctionPointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__afterFunctionThrows = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.AfterThrowsFunctionPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.afterFunctionThrows
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.AfterThrowsFunctionPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._afterFunctionThrows
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__afterFunctionThrows} */
eco.artd.IFirebaseJoinpointModel.__afterFunctionThrows

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.afterFunctionReturns no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterReturnsFunctionPointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__afterFunctionReturns = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.AfterReturnsFunctionPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.afterFunctionReturns
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.AfterReturnsFunctionPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._afterFunctionReturns
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__afterFunctionReturns} */
eco.artd.IFirebaseJoinpointModel.__afterFunctionReturns

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.afterFunctionCancels no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterCancelsFunctionPointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__afterFunctionCancels = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.AfterCancelsFunctionPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.afterFunctionCancels
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.AfterCancelsFunctionPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._afterFunctionCancels
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__afterFunctionCancels} */
eco.artd.IFirebaseJoinpointModel.__afterFunctionCancels

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.immediatelyAfterFunction no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterFunctionPointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__immediatelyAfterFunction = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterFunctionPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.immediatelyAfterFunction
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterFunctionPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._immediatelyAfterFunction
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__immediatelyAfterFunction} */
eco.artd.IFirebaseJoinpointModel.__immediatelyAfterFunction

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.beforeEvalBodyWithProfileTime no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.BeforeEvalBodyWithProfileTimePointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__beforeEvalBodyWithProfileTime = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.BeforeEvalBodyWithProfileTimePointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.beforeEvalBodyWithProfileTime
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.BeforeEvalBodyWithProfileTimePointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._beforeEvalBodyWithProfileTime
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__beforeEvalBodyWithProfileTime} */
eco.artd.IFirebaseJoinpointModel.__beforeEvalBodyWithProfileTime

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.afterEvalBodyWithProfileTime no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterEvalBodyWithProfileTimePointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__afterEvalBodyWithProfileTime = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.AfterEvalBodyWithProfileTimePointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.afterEvalBodyWithProfileTime
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.AfterEvalBodyWithProfileTimePointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._afterEvalBodyWithProfileTime
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__afterEvalBodyWithProfileTime} */
eco.artd.IFirebaseJoinpointModel.__afterEvalBodyWithProfileTime

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.afterEvalBodyWithProfileTimeThrows no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterThrowsEvalBodyWithProfileTimePointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__afterEvalBodyWithProfileTimeThrows = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.AfterThrowsEvalBodyWithProfileTimePointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.afterEvalBodyWithProfileTimeThrows
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.AfterThrowsEvalBodyWithProfileTimePointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._afterEvalBodyWithProfileTimeThrows
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__afterEvalBodyWithProfileTimeThrows} */
eco.artd.IFirebaseJoinpointModel.__afterEvalBodyWithProfileTimeThrows

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.afterEvalBodyWithProfileTimeReturns no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterReturnsEvalBodyWithProfileTimePointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__afterEvalBodyWithProfileTimeReturns = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.AfterReturnsEvalBodyWithProfileTimePointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.afterEvalBodyWithProfileTimeReturns
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.AfterReturnsEvalBodyWithProfileTimePointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._afterEvalBodyWithProfileTimeReturns
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__afterEvalBodyWithProfileTimeReturns} */
eco.artd.IFirebaseJoinpointModel.__afterEvalBodyWithProfileTimeReturns

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.afterEvalBodyWithProfileTimeCancels no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterCancelsEvalBodyWithProfileTimePointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__afterEvalBodyWithProfileTimeCancels = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.AfterCancelsEvalBodyWithProfileTimePointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.afterEvalBodyWithProfileTimeCancels
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.AfterCancelsEvalBodyWithProfileTimePointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._afterEvalBodyWithProfileTimeCancels
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__afterEvalBodyWithProfileTimeCancels} */
eco.artd.IFirebaseJoinpointModel.__afterEvalBodyWithProfileTimeCancels

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.beforeGetPageDataFromFirestore no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.BeforeGetPageDataFromFirestorePointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__beforeGetPageDataFromFirestore = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.BeforeGetPageDataFromFirestorePointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.beforeGetPageDataFromFirestore
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.BeforeGetPageDataFromFirestorePointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._beforeGetPageDataFromFirestore
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__beforeGetPageDataFromFirestore} */
eco.artd.IFirebaseJoinpointModel.__beforeGetPageDataFromFirestore

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.afterGetPageDataFromFirestore no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterGetPageDataFromFirestorePointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__afterGetPageDataFromFirestore = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.AfterGetPageDataFromFirestorePointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.afterGetPageDataFromFirestore
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.AfterGetPageDataFromFirestorePointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._afterGetPageDataFromFirestore
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__afterGetPageDataFromFirestore} */
eco.artd.IFirebaseJoinpointModel.__afterGetPageDataFromFirestore

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.afterGetPageDataFromFirestoreThrows no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterThrowsGetPageDataFromFirestorePointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__afterGetPageDataFromFirestoreThrows = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.AfterThrowsGetPageDataFromFirestorePointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.afterGetPageDataFromFirestoreThrows
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.AfterThrowsGetPageDataFromFirestorePointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._afterGetPageDataFromFirestoreThrows
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__afterGetPageDataFromFirestoreThrows} */
eco.artd.IFirebaseJoinpointModel.__afterGetPageDataFromFirestoreThrows

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.afterGetPageDataFromFirestoreReturns no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterReturnsGetPageDataFromFirestorePointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__afterGetPageDataFromFirestoreReturns = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.AfterReturnsGetPageDataFromFirestorePointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.afterGetPageDataFromFirestoreReturns
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.AfterReturnsGetPageDataFromFirestorePointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._afterGetPageDataFromFirestoreReturns
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__afterGetPageDataFromFirestoreReturns} */
eco.artd.IFirebaseJoinpointModel.__afterGetPageDataFromFirestoreReturns

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.afterGetPageDataFromFirestoreCancels no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterCancelsGetPageDataFromFirestorePointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__afterGetPageDataFromFirestoreCancels = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.AfterCancelsGetPageDataFromFirestorePointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.afterGetPageDataFromFirestoreCancels
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.AfterCancelsGetPageDataFromFirestorePointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._afterGetPageDataFromFirestoreCancels
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__afterGetPageDataFromFirestoreCancels} */
eco.artd.IFirebaseJoinpointModel.__afterGetPageDataFromFirestoreCancels

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.immediatelyAfterGetPageDataFromFirestore no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterGetPageDataFromFirestorePointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__immediatelyAfterGetPageDataFromFirestore = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterGetPageDataFromFirestorePointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.immediatelyAfterGetPageDataFromFirestore
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterGetPageDataFromFirestorePointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._immediatelyAfterGetPageDataFromFirestore
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__immediatelyAfterGetPageDataFromFirestore} */
eco.artd.IFirebaseJoinpointModel.__immediatelyAfterGetPageDataFromFirestore

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.beforeDownloadAndEvalPageData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.BeforeDownloadAndEvalPageDataPointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__beforeDownloadAndEvalPageData = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.BeforeDownloadAndEvalPageDataPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.beforeDownloadAndEvalPageData
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.BeforeDownloadAndEvalPageDataPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._beforeDownloadAndEvalPageData
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__beforeDownloadAndEvalPageData} */
eco.artd.IFirebaseJoinpointModel.__beforeDownloadAndEvalPageData

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.afterDownloadAndEvalPageData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterDownloadAndEvalPageDataPointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__afterDownloadAndEvalPageData = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.AfterDownloadAndEvalPageDataPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.afterDownloadAndEvalPageData
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.AfterDownloadAndEvalPageDataPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._afterDownloadAndEvalPageData
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__afterDownloadAndEvalPageData} */
eco.artd.IFirebaseJoinpointModel.__afterDownloadAndEvalPageData

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.afterDownloadAndEvalPageDataThrows no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterThrowsDownloadAndEvalPageDataPointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__afterDownloadAndEvalPageDataThrows = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.AfterThrowsDownloadAndEvalPageDataPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.afterDownloadAndEvalPageDataThrows
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.AfterThrowsDownloadAndEvalPageDataPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._afterDownloadAndEvalPageDataThrows
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__afterDownloadAndEvalPageDataThrows} */
eco.artd.IFirebaseJoinpointModel.__afterDownloadAndEvalPageDataThrows

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.afterDownloadAndEvalPageDataReturns no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterReturnsDownloadAndEvalPageDataPointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__afterDownloadAndEvalPageDataReturns = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.AfterReturnsDownloadAndEvalPageDataPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.afterDownloadAndEvalPageDataReturns
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.AfterReturnsDownloadAndEvalPageDataPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._afterDownloadAndEvalPageDataReturns
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__afterDownloadAndEvalPageDataReturns} */
eco.artd.IFirebaseJoinpointModel.__afterDownloadAndEvalPageDataReturns

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.afterDownloadAndEvalPageDataCancels no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterCancelsDownloadAndEvalPageDataPointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__afterDownloadAndEvalPageDataCancels = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.AfterCancelsDownloadAndEvalPageDataPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.afterDownloadAndEvalPageDataCancels
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.AfterCancelsDownloadAndEvalPageDataPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._afterDownloadAndEvalPageDataCancels
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__afterDownloadAndEvalPageDataCancels} */
eco.artd.IFirebaseJoinpointModel.__afterDownloadAndEvalPageDataCancels

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.immediatelyAfterDownloadAndEvalPageData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterDownloadAndEvalPageDataPointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__immediatelyAfterDownloadAndEvalPageData = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterDownloadAndEvalPageDataPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.immediatelyAfterDownloadAndEvalPageData
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterDownloadAndEvalPageDataPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._immediatelyAfterDownloadAndEvalPageData
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__immediatelyAfterDownloadAndEvalPageData} */
eco.artd.IFirebaseJoinpointModel.__immediatelyAfterDownloadAndEvalPageData

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.beforeEvalBody no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.BeforeEvalBodyPointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__beforeEvalBody = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.BeforeEvalBodyPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.beforeEvalBody
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.BeforeEvalBodyPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._beforeEvalBody
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__beforeEvalBody} */
eco.artd.IFirebaseJoinpointModel.__beforeEvalBody

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.afterEvalBody no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterEvalBodyPointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__afterEvalBody = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.AfterEvalBodyPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.afterEvalBody
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.AfterEvalBodyPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._afterEvalBody
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__afterEvalBody} */
eco.artd.IFirebaseJoinpointModel.__afterEvalBody

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.afterEvalBodyThrows no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterThrowsEvalBodyPointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__afterEvalBodyThrows = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.AfterThrowsEvalBodyPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.afterEvalBodyThrows
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.AfterThrowsEvalBodyPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._afterEvalBodyThrows
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__afterEvalBodyThrows} */
eco.artd.IFirebaseJoinpointModel.__afterEvalBodyThrows

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.afterEvalBodyReturns no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterReturnsEvalBodyPointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__afterEvalBodyReturns = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.AfterReturnsEvalBodyPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.afterEvalBodyReturns
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.AfterReturnsEvalBodyPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._afterEvalBodyReturns
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__afterEvalBodyReturns} */
eco.artd.IFirebaseJoinpointModel.__afterEvalBodyReturns

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.afterEvalBodyCancels no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterCancelsEvalBodyPointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__afterEvalBodyCancels = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.AfterCancelsEvalBodyPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.afterEvalBodyCancels
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.AfterCancelsEvalBodyPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._afterEvalBodyCancels
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__afterEvalBodyCancels} */
eco.artd.IFirebaseJoinpointModel.__afterEvalBodyCancels

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.beforeLogRpcInvocation no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.BeforeLogRpcInvocationPointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__beforeLogRpcInvocation = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.BeforeLogRpcInvocationPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.beforeLogRpcInvocation
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.BeforeLogRpcInvocationPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._beforeLogRpcInvocation
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__beforeLogRpcInvocation} */
eco.artd.IFirebaseJoinpointModel.__beforeLogRpcInvocation

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.afterLogRpcInvocation no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterLogRpcInvocationPointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__afterLogRpcInvocation = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.AfterLogRpcInvocationPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.afterLogRpcInvocation
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.AfterLogRpcInvocationPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._afterLogRpcInvocation
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__afterLogRpcInvocation} */
eco.artd.IFirebaseJoinpointModel.__afterLogRpcInvocation

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.afterLogRpcInvocationThrows no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterThrowsLogRpcInvocationPointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__afterLogRpcInvocationThrows = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.AfterThrowsLogRpcInvocationPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.afterLogRpcInvocationThrows
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.AfterThrowsLogRpcInvocationPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._afterLogRpcInvocationThrows
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__afterLogRpcInvocationThrows} */
eco.artd.IFirebaseJoinpointModel.__afterLogRpcInvocationThrows

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.afterLogRpcInvocationReturns no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterReturnsLogRpcInvocationPointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__afterLogRpcInvocationReturns = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.AfterReturnsLogRpcInvocationPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.afterLogRpcInvocationReturns
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.AfterReturnsLogRpcInvocationPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._afterLogRpcInvocationReturns
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__afterLogRpcInvocationReturns} */
eco.artd.IFirebaseJoinpointModel.__afterLogRpcInvocationReturns

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.afterLogRpcInvocationCancels no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterCancelsLogRpcInvocationPointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__afterLogRpcInvocationCancels = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.AfterCancelsLogRpcInvocationPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.afterLogRpcInvocationCancels
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.AfterCancelsLogRpcInvocationPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._afterLogRpcInvocationCancels
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__afterLogRpcInvocationCancels} */
eco.artd.IFirebaseJoinpointModel.__afterLogRpcInvocationCancels

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.immediatelyAfterLogRpcInvocation no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterLogRpcInvocationPointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__immediatelyAfterLogRpcInvocation = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterLogRpcInvocationPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.immediatelyAfterLogRpcInvocation
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterLogRpcInvocationPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._immediatelyAfterLogRpcInvocation
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__immediatelyAfterLogRpcInvocation} */
eco.artd.IFirebaseJoinpointModel.__immediatelyAfterLogRpcInvocation

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.beforeFetchFunctionsFile no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.BeforeFetchFunctionsFilePointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__beforeFetchFunctionsFile = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.BeforeFetchFunctionsFilePointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.beforeFetchFunctionsFile
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.BeforeFetchFunctionsFilePointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._beforeFetchFunctionsFile
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__beforeFetchFunctionsFile} */
eco.artd.IFirebaseJoinpointModel.__beforeFetchFunctionsFile

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.afterFetchFunctionsFile no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterFetchFunctionsFilePointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__afterFetchFunctionsFile = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.AfterFetchFunctionsFilePointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.afterFetchFunctionsFile
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.AfterFetchFunctionsFilePointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._afterFetchFunctionsFile
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__afterFetchFunctionsFile} */
eco.artd.IFirebaseJoinpointModel.__afterFetchFunctionsFile

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.afterFetchFunctionsFileThrows no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterThrowsFetchFunctionsFilePointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__afterFetchFunctionsFileThrows = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.AfterThrowsFetchFunctionsFilePointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.afterFetchFunctionsFileThrows
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.AfterThrowsFetchFunctionsFilePointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._afterFetchFunctionsFileThrows
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__afterFetchFunctionsFileThrows} */
eco.artd.IFirebaseJoinpointModel.__afterFetchFunctionsFileThrows

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.afterFetchFunctionsFileReturns no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterReturnsFetchFunctionsFilePointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__afterFetchFunctionsFileReturns = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.AfterReturnsFetchFunctionsFilePointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.afterFetchFunctionsFileReturns
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.AfterReturnsFetchFunctionsFilePointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._afterFetchFunctionsFileReturns
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__afterFetchFunctionsFileReturns} */
eco.artd.IFirebaseJoinpointModel.__afterFetchFunctionsFileReturns

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.afterFetchFunctionsFileCancels no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterCancelsFetchFunctionsFilePointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__afterFetchFunctionsFileCancels = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.AfterCancelsFetchFunctionsFilePointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.afterFetchFunctionsFileCancels
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.AfterCancelsFetchFunctionsFilePointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._afterFetchFunctionsFileCancels
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__afterFetchFunctionsFileCancels} */
eco.artd.IFirebaseJoinpointModel.__afterFetchFunctionsFileCancels

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.immediatelyAfterFetchFunctionsFile no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterFetchFunctionsFilePointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__immediatelyAfterFetchFunctionsFile = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterFetchFunctionsFilePointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.immediatelyAfterFetchFunctionsFile
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterFetchFunctionsFilePointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._immediatelyAfterFetchFunctionsFile
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__immediatelyAfterFetchFunctionsFile} */
eco.artd.IFirebaseJoinpointModel.__immediatelyAfterFetchFunctionsFile

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.beforeDownloadDeps no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.BeforeDownloadDepsPointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__beforeDownloadDeps = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.BeforeDownloadDepsPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.beforeDownloadDeps
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.BeforeDownloadDepsPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._beforeDownloadDeps
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__beforeDownloadDeps} */
eco.artd.IFirebaseJoinpointModel.__beforeDownloadDeps

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.afterDownloadDeps no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterDownloadDepsPointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__afterDownloadDeps = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.AfterDownloadDepsPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.afterDownloadDeps
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.AfterDownloadDepsPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._afterDownloadDeps
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__afterDownloadDeps} */
eco.artd.IFirebaseJoinpointModel.__afterDownloadDeps

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.afterDownloadDepsThrows no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterThrowsDownloadDepsPointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__afterDownloadDepsThrows = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.AfterThrowsDownloadDepsPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.afterDownloadDepsThrows
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.AfterThrowsDownloadDepsPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._afterDownloadDepsThrows
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__afterDownloadDepsThrows} */
eco.artd.IFirebaseJoinpointModel.__afterDownloadDepsThrows

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.afterDownloadDepsReturns no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterReturnsDownloadDepsPointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__afterDownloadDepsReturns = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.AfterReturnsDownloadDepsPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.afterDownloadDepsReturns
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.AfterReturnsDownloadDepsPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._afterDownloadDepsReturns
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__afterDownloadDepsReturns} */
eco.artd.IFirebaseJoinpointModel.__afterDownloadDepsReturns

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.afterDownloadDepsCancels no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterCancelsDownloadDepsPointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__afterDownloadDepsCancels = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.AfterCancelsDownloadDepsPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.afterDownloadDepsCancels
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.AfterCancelsDownloadDepsPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._afterDownloadDepsCancels
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__afterDownloadDepsCancels} */
eco.artd.IFirebaseJoinpointModel.__afterDownloadDepsCancels

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.immediatelyAfterDownloadDeps no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterDownloadDepsPointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__immediatelyAfterDownloadDeps = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterDownloadDepsPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.immediatelyAfterDownloadDeps
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterDownloadDepsPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._immediatelyAfterDownloadDeps
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__immediatelyAfterDownloadDeps} */
eco.artd.IFirebaseJoinpointModel.__immediatelyAfterDownloadDeps

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.beforeDownloadMethod no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.BeforeDownloadMethodPointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__beforeDownloadMethod = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.BeforeDownloadMethodPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.beforeDownloadMethod
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.BeforeDownloadMethodPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._beforeDownloadMethod
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__beforeDownloadMethod} */
eco.artd.IFirebaseJoinpointModel.__beforeDownloadMethod

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.afterDownloadMethod no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterDownloadMethodPointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__afterDownloadMethod = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.AfterDownloadMethodPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.afterDownloadMethod
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.AfterDownloadMethodPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._afterDownloadMethod
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__afterDownloadMethod} */
eco.artd.IFirebaseJoinpointModel.__afterDownloadMethod

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.afterDownloadMethodThrows no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterThrowsDownloadMethodPointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__afterDownloadMethodThrows = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.AfterThrowsDownloadMethodPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.afterDownloadMethodThrows
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.AfterThrowsDownloadMethodPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._afterDownloadMethodThrows
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__afterDownloadMethodThrows} */
eco.artd.IFirebaseJoinpointModel.__afterDownloadMethodThrows

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.afterDownloadMethodReturns no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterReturnsDownloadMethodPointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__afterDownloadMethodReturns = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.AfterReturnsDownloadMethodPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.afterDownloadMethodReturns
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.AfterReturnsDownloadMethodPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._afterDownloadMethodReturns
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__afterDownloadMethodReturns} */
eco.artd.IFirebaseJoinpointModel.__afterDownloadMethodReturns

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.afterDownloadMethodCancels no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterCancelsDownloadMethodPointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__afterDownloadMethodCancels = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.AfterCancelsDownloadMethodPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.afterDownloadMethodCancels
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.AfterCancelsDownloadMethodPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._afterDownloadMethodCancels
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__afterDownloadMethodCancels} */
eco.artd.IFirebaseJoinpointModel.__afterDownloadMethodCancels

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.immediatelyAfterDownloadMethod no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterDownloadMethodPointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__immediatelyAfterDownloadMethod = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterDownloadMethodPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.immediatelyAfterDownloadMethod
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterDownloadMethodPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._immediatelyAfterDownloadMethod
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__immediatelyAfterDownloadMethod} */
eco.artd.IFirebaseJoinpointModel.__immediatelyAfterDownloadMethod

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.beforeDownloadMethodDirectly no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.BeforeDownloadMethodDirectlyPointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__beforeDownloadMethodDirectly = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.BeforeDownloadMethodDirectlyPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.beforeDownloadMethodDirectly
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.BeforeDownloadMethodDirectlyPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._beforeDownloadMethodDirectly
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__beforeDownloadMethodDirectly} */
eco.artd.IFirebaseJoinpointModel.__beforeDownloadMethodDirectly

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.afterDownloadMethodDirectly no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterDownloadMethodDirectlyPointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__afterDownloadMethodDirectly = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.AfterDownloadMethodDirectlyPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.afterDownloadMethodDirectly
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.AfterDownloadMethodDirectlyPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._afterDownloadMethodDirectly
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__afterDownloadMethodDirectly} */
eco.artd.IFirebaseJoinpointModel.__afterDownloadMethodDirectly

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.afterDownloadMethodDirectlyThrows no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterThrowsDownloadMethodDirectlyPointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__afterDownloadMethodDirectlyThrows = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.AfterThrowsDownloadMethodDirectlyPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.afterDownloadMethodDirectlyThrows
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.AfterThrowsDownloadMethodDirectlyPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._afterDownloadMethodDirectlyThrows
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__afterDownloadMethodDirectlyThrows} */
eco.artd.IFirebaseJoinpointModel.__afterDownloadMethodDirectlyThrows

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.afterDownloadMethodDirectlyReturns no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterReturnsDownloadMethodDirectlyPointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__afterDownloadMethodDirectlyReturns = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.AfterReturnsDownloadMethodDirectlyPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.afterDownloadMethodDirectlyReturns
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.AfterReturnsDownloadMethodDirectlyPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._afterDownloadMethodDirectlyReturns
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__afterDownloadMethodDirectlyReturns} */
eco.artd.IFirebaseJoinpointModel.__afterDownloadMethodDirectlyReturns

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.afterDownloadMethodDirectlyCancels no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterCancelsDownloadMethodDirectlyPointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__afterDownloadMethodDirectlyCancels = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.AfterCancelsDownloadMethodDirectlyPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.afterDownloadMethodDirectlyCancels
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.AfterCancelsDownloadMethodDirectlyPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._afterDownloadMethodDirectlyCancels
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__afterDownloadMethodDirectlyCancels} */
eco.artd.IFirebaseJoinpointModel.__afterDownloadMethodDirectlyCancels

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.immediatelyAfterDownloadMethodDirectly no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterDownloadMethodDirectlyPointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__immediatelyAfterDownloadMethodDirectly = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterDownloadMethodDirectlyPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.immediatelyAfterDownloadMethodDirectly
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterDownloadMethodDirectlyPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._immediatelyAfterDownloadMethodDirectly
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__immediatelyAfterDownloadMethodDirectly} */
eco.artd.IFirebaseJoinpointModel.__immediatelyAfterDownloadMethodDirectly

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.beforeGetId no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.BeforeGetIdPointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__beforeGetId = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.BeforeGetIdPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.beforeGetId
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.BeforeGetIdPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._beforeGetId
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__beforeGetId} */
eco.artd.IFirebaseJoinpointModel.__beforeGetId

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.afterGetId no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterGetIdPointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__afterGetId = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.AfterGetIdPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.afterGetId
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.AfterGetIdPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._afterGetId
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__afterGetId} */
eco.artd.IFirebaseJoinpointModel.__afterGetId

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.afterGetIdThrows no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterThrowsGetIdPointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__afterGetIdThrows = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.AfterThrowsGetIdPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.afterGetIdThrows
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.AfterThrowsGetIdPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._afterGetIdThrows
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__afterGetIdThrows} */
eco.artd.IFirebaseJoinpointModel.__afterGetIdThrows

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.afterGetIdReturns no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterReturnsGetIdPointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__afterGetIdReturns = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.AfterReturnsGetIdPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.afterGetIdReturns
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.AfterReturnsGetIdPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._afterGetIdReturns
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__afterGetIdReturns} */
eco.artd.IFirebaseJoinpointModel.__afterGetIdReturns

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.afterGetIdCancels no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterCancelsGetIdPointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__afterGetIdCancels = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.AfterCancelsGetIdPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.afterGetIdCancels
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.AfterCancelsGetIdPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._afterGetIdCancels
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__afterGetIdCancels} */
eco.artd.IFirebaseJoinpointModel.__afterGetIdCancels

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.beforePatchDeps no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.BeforePatchDepsPointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__beforePatchDeps = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.BeforePatchDepsPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.beforePatchDeps
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.BeforePatchDepsPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._beforePatchDeps
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__beforePatchDeps} */
eco.artd.IFirebaseJoinpointModel.__beforePatchDeps

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.afterPatchDeps no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterPatchDepsPointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__afterPatchDeps = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.AfterPatchDepsPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.afterPatchDeps
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.AfterPatchDepsPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._afterPatchDeps
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__afterPatchDeps} */
eco.artd.IFirebaseJoinpointModel.__afterPatchDeps

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.afterPatchDepsThrows no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterThrowsPatchDepsPointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__afterPatchDepsThrows = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.AfterThrowsPatchDepsPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.afterPatchDepsThrows
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.AfterThrowsPatchDepsPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._afterPatchDepsThrows
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__afterPatchDepsThrows} */
eco.artd.IFirebaseJoinpointModel.__afterPatchDepsThrows

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.afterPatchDepsReturns no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterReturnsPatchDepsPointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__afterPatchDepsReturns = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.AfterReturnsPatchDepsPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.afterPatchDepsReturns
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.AfterReturnsPatchDepsPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._afterPatchDepsReturns
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__afterPatchDepsReturns} */
eco.artd.IFirebaseJoinpointModel.__afterPatchDepsReturns

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.afterPatchDepsCancels no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterCancelsPatchDepsPointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__afterPatchDepsCancels = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.AfterCancelsPatchDepsPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.afterPatchDepsCancels
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.AfterCancelsPatchDepsPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._afterPatchDepsCancels
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__afterPatchDepsCancels} */
eco.artd.IFirebaseJoinpointModel.__afterPatchDepsCancels

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.immediatelyAfterPatchDeps no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterPatchDepsPointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__immediatelyAfterPatchDeps = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterPatchDepsPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.immediatelyAfterPatchDeps
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterPatchDepsPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._immediatelyAfterPatchDeps
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__immediatelyAfterPatchDeps} */
eco.artd.IFirebaseJoinpointModel.__immediatelyAfterPatchDeps

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.beforeDownloadPageData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.BeforeDownloadPageDataPointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__beforeDownloadPageData = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.BeforeDownloadPageDataPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.beforeDownloadPageData
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.BeforeDownloadPageDataPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._beforeDownloadPageData
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__beforeDownloadPageData} */
eco.artd.IFirebaseJoinpointModel.__beforeDownloadPageData

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.afterDownloadPageData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterDownloadPageDataPointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__afterDownloadPageData = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.AfterDownloadPageDataPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.afterDownloadPageData
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.AfterDownloadPageDataPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._afterDownloadPageData
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__afterDownloadPageData} */
eco.artd.IFirebaseJoinpointModel.__afterDownloadPageData

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.afterDownloadPageDataThrows no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterThrowsDownloadPageDataPointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__afterDownloadPageDataThrows = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.AfterThrowsDownloadPageDataPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.afterDownloadPageDataThrows
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.AfterThrowsDownloadPageDataPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._afterDownloadPageDataThrows
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__afterDownloadPageDataThrows} */
eco.artd.IFirebaseJoinpointModel.__afterDownloadPageDataThrows

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.afterDownloadPageDataReturns no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterReturnsDownloadPageDataPointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__afterDownloadPageDataReturns = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.AfterReturnsDownloadPageDataPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.afterDownloadPageDataReturns
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.AfterReturnsDownloadPageDataPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._afterDownloadPageDataReturns
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__afterDownloadPageDataReturns} */
eco.artd.IFirebaseJoinpointModel.__afterDownloadPageDataReturns

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.afterDownloadPageDataCancels no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterCancelsDownloadPageDataPointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__afterDownloadPageDataCancels = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.AfterCancelsDownloadPageDataPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.afterDownloadPageDataCancels
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.AfterCancelsDownloadPageDataPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._afterDownloadPageDataCancels
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__afterDownloadPageDataCancels} */
eco.artd.IFirebaseJoinpointModel.__afterDownloadPageDataCancels

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.immediatelyAfterDownloadPageData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterDownloadPageDataPointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__immediatelyAfterDownloadPageData = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterDownloadPageDataPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.immediatelyAfterDownloadPageData
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterDownloadPageDataPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._immediatelyAfterDownloadPageData
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__immediatelyAfterDownloadPageData} */
eco.artd.IFirebaseJoinpointModel.__immediatelyAfterDownloadPageData

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.beforeLoadRequirePageData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.BeforeLoadRequirePageDataPointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__beforeLoadRequirePageData = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.BeforeLoadRequirePageDataPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.beforeLoadRequirePageData
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.BeforeLoadRequirePageDataPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._beforeLoadRequirePageData
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__beforeLoadRequirePageData} */
eco.artd.IFirebaseJoinpointModel.__beforeLoadRequirePageData

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.afterLoadRequirePageData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterLoadRequirePageDataPointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__afterLoadRequirePageData = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.AfterLoadRequirePageDataPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.afterLoadRequirePageData
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.AfterLoadRequirePageDataPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._afterLoadRequirePageData
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__afterLoadRequirePageData} */
eco.artd.IFirebaseJoinpointModel.__afterLoadRequirePageData

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.afterLoadRequirePageDataThrows no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterThrowsLoadRequirePageDataPointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__afterLoadRequirePageDataThrows = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.AfterThrowsLoadRequirePageDataPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.afterLoadRequirePageDataThrows
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.AfterThrowsLoadRequirePageDataPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._afterLoadRequirePageDataThrows
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__afterLoadRequirePageDataThrows} */
eco.artd.IFirebaseJoinpointModel.__afterLoadRequirePageDataThrows

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.afterLoadRequirePageDataReturns no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterReturnsLoadRequirePageDataPointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__afterLoadRequirePageDataReturns = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.AfterReturnsLoadRequirePageDataPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.afterLoadRequirePageDataReturns
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.AfterReturnsLoadRequirePageDataPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._afterLoadRequirePageDataReturns
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__afterLoadRequirePageDataReturns} */
eco.artd.IFirebaseJoinpointModel.__afterLoadRequirePageDataReturns

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.afterLoadRequirePageDataCancels no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterCancelsLoadRequirePageDataPointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__afterLoadRequirePageDataCancels = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.AfterCancelsLoadRequirePageDataPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.afterLoadRequirePageDataCancels
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.AfterCancelsLoadRequirePageDataPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._afterLoadRequirePageDataCancels
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__afterLoadRequirePageDataCancels} */
eco.artd.IFirebaseJoinpointModel.__afterLoadRequirePageDataCancels

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.beforeLoadRequire no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.BeforeLoadRequirePointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__beforeLoadRequire = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.BeforeLoadRequirePointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.beforeLoadRequire
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.BeforeLoadRequirePointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._beforeLoadRequire
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__beforeLoadRequire} */
eco.artd.IFirebaseJoinpointModel.__beforeLoadRequire

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.afterLoadRequire no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterLoadRequirePointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__afterLoadRequire = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.AfterLoadRequirePointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.afterLoadRequire
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.AfterLoadRequirePointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._afterLoadRequire
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__afterLoadRequire} */
eco.artd.IFirebaseJoinpointModel.__afterLoadRequire

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.afterLoadRequireThrows no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterThrowsLoadRequirePointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__afterLoadRequireThrows = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.AfterThrowsLoadRequirePointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.afterLoadRequireThrows
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.AfterThrowsLoadRequirePointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._afterLoadRequireThrows
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__afterLoadRequireThrows} */
eco.artd.IFirebaseJoinpointModel.__afterLoadRequireThrows

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.afterLoadRequireReturns no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterReturnsLoadRequirePointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__afterLoadRequireReturns = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.AfterReturnsLoadRequirePointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.afterLoadRequireReturns
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.AfterReturnsLoadRequirePointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._afterLoadRequireReturns
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__afterLoadRequireReturns} */
eco.artd.IFirebaseJoinpointModel.__afterLoadRequireReturns

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.afterLoadRequireCancels no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterCancelsLoadRequirePointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__afterLoadRequireCancels = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.AfterCancelsLoadRequirePointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.afterLoadRequireCancels
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.AfterCancelsLoadRequirePointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._afterLoadRequireCancels
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__afterLoadRequireCancels} */
eco.artd.IFirebaseJoinpointModel.__afterLoadRequireCancels

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.beforeUpdateErrorStack no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.BeforeUpdateErrorStackPointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__beforeUpdateErrorStack = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.BeforeUpdateErrorStackPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.beforeUpdateErrorStack
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.BeforeUpdateErrorStackPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._beforeUpdateErrorStack
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__beforeUpdateErrorStack} */
eco.artd.IFirebaseJoinpointModel.__beforeUpdateErrorStack

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.afterUpdateErrorStack no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterUpdateErrorStackPointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__afterUpdateErrorStack = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.AfterUpdateErrorStackPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.afterUpdateErrorStack
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.AfterUpdateErrorStackPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._afterUpdateErrorStack
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__afterUpdateErrorStack} */
eco.artd.IFirebaseJoinpointModel.__afterUpdateErrorStack

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.afterUpdateErrorStackThrows no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterThrowsUpdateErrorStackPointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__afterUpdateErrorStackThrows = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.AfterThrowsUpdateErrorStackPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.afterUpdateErrorStackThrows
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.AfterThrowsUpdateErrorStackPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._afterUpdateErrorStackThrows
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__afterUpdateErrorStackThrows} */
eco.artd.IFirebaseJoinpointModel.__afterUpdateErrorStackThrows

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.afterUpdateErrorStackReturns no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterReturnsUpdateErrorStackPointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__afterUpdateErrorStackReturns = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.AfterReturnsUpdateErrorStackPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.afterUpdateErrorStackReturns
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.AfterReturnsUpdateErrorStackPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._afterUpdateErrorStackReturns
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__afterUpdateErrorStackReturns} */
eco.artd.IFirebaseJoinpointModel.__afterUpdateErrorStackReturns

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.afterUpdateErrorStackCancels no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterCancelsUpdateErrorStackPointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__afterUpdateErrorStackCancels = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.AfterCancelsUpdateErrorStackPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.afterUpdateErrorStackCancels
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.AfterCancelsUpdateErrorStackPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._afterUpdateErrorStackCancels
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__afterUpdateErrorStackCancels} */
eco.artd.IFirebaseJoinpointModel.__afterUpdateErrorStackCancels

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.beforeValidateMethod no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.BeforeValidateMethodPointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__beforeValidateMethod = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.BeforeValidateMethodPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.beforeValidateMethod
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.BeforeValidateMethodPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._beforeValidateMethod
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__beforeValidateMethod} */
eco.artd.IFirebaseJoinpointModel.__beforeValidateMethod

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.afterValidateMethod no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterValidateMethodPointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__afterValidateMethod = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.AfterValidateMethodPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.afterValidateMethod
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.AfterValidateMethodPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._afterValidateMethod
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__afterValidateMethod} */
eco.artd.IFirebaseJoinpointModel.__afterValidateMethod

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.afterValidateMethodThrows no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterThrowsValidateMethodPointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__afterValidateMethodThrows = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.AfterThrowsValidateMethodPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.afterValidateMethodThrows
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.AfterThrowsValidateMethodPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._afterValidateMethodThrows
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__afterValidateMethodThrows} */
eco.artd.IFirebaseJoinpointModel.__afterValidateMethodThrows

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.afterValidateMethodReturns no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterReturnsValidateMethodPointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__afterValidateMethodReturns = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.AfterReturnsValidateMethodPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.afterValidateMethodReturns
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.AfterReturnsValidateMethodPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._afterValidateMethodReturns
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__afterValidateMethodReturns} */
eco.artd.IFirebaseJoinpointModel.__afterValidateMethodReturns

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.afterValidateMethodCancels no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterCancelsValidateMethodPointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__afterValidateMethodCancels = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.AfterCancelsValidateMethodPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.afterValidateMethodCancels
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.AfterCancelsValidateMethodPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._afterValidateMethodCancels
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__afterValidateMethodCancels} */
eco.artd.IFirebaseJoinpointModel.__afterValidateMethodCancels

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.beforeValidateLoadAndInvoke no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.BeforeValidateLoadAndInvokePointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__beforeValidateLoadAndInvoke = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.BeforeValidateLoadAndInvokePointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.beforeValidateLoadAndInvoke
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.BeforeValidateLoadAndInvokePointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._beforeValidateLoadAndInvoke
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__beforeValidateLoadAndInvoke} */
eco.artd.IFirebaseJoinpointModel.__beforeValidateLoadAndInvoke

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.afterValidateLoadAndInvoke no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterValidateLoadAndInvokePointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__afterValidateLoadAndInvoke = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.AfterValidateLoadAndInvokePointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.afterValidateLoadAndInvoke
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.AfterValidateLoadAndInvokePointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._afterValidateLoadAndInvoke
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__afterValidateLoadAndInvoke} */
eco.artd.IFirebaseJoinpointModel.__afterValidateLoadAndInvoke

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.afterValidateLoadAndInvokeThrows no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterThrowsValidateLoadAndInvokePointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__afterValidateLoadAndInvokeThrows = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.AfterThrowsValidateLoadAndInvokePointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.afterValidateLoadAndInvokeThrows
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.AfterThrowsValidateLoadAndInvokePointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._afterValidateLoadAndInvokeThrows
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__afterValidateLoadAndInvokeThrows} */
eco.artd.IFirebaseJoinpointModel.__afterValidateLoadAndInvokeThrows

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.afterValidateLoadAndInvokeReturns no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterReturnsValidateLoadAndInvokePointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__afterValidateLoadAndInvokeReturns = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.AfterReturnsValidateLoadAndInvokePointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.afterValidateLoadAndInvokeReturns
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.AfterReturnsValidateLoadAndInvokePointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._afterValidateLoadAndInvokeReturns
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__afterValidateLoadAndInvokeReturns} */
eco.artd.IFirebaseJoinpointModel.__afterValidateLoadAndInvokeReturns

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.afterValidateLoadAndInvokeCancels no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterCancelsValidateLoadAndInvokePointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__afterValidateLoadAndInvokeCancels = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.AfterCancelsValidateLoadAndInvokePointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.afterValidateLoadAndInvokeCancels
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.AfterCancelsValidateLoadAndInvokePointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._afterValidateLoadAndInvokeCancels
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__afterValidateLoadAndInvokeCancels} */
eco.artd.IFirebaseJoinpointModel.__afterValidateLoadAndInvokeCancels

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.immediatelyAfterValidateLoadAndInvoke no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterValidateLoadAndInvokePointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__immediatelyAfterValidateLoadAndInvoke = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterValidateLoadAndInvokePointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.immediatelyAfterValidateLoadAndInvoke
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterValidateLoadAndInvokePointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._immediatelyAfterValidateLoadAndInvoke
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__immediatelyAfterValidateLoadAndInvoke} */
eco.artd.IFirebaseJoinpointModel.__immediatelyAfterValidateLoadAndInvoke

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.beforeValidateLoadAndInvokeById no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.BeforeValidateLoadAndInvokeByIdPointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__beforeValidateLoadAndInvokeById = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.BeforeValidateLoadAndInvokeByIdPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.beforeValidateLoadAndInvokeById
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.BeforeValidateLoadAndInvokeByIdPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._beforeValidateLoadAndInvokeById
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__beforeValidateLoadAndInvokeById} */
eco.artd.IFirebaseJoinpointModel.__beforeValidateLoadAndInvokeById

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.afterValidateLoadAndInvokeById no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterValidateLoadAndInvokeByIdPointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__afterValidateLoadAndInvokeById = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.AfterValidateLoadAndInvokeByIdPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.afterValidateLoadAndInvokeById
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.AfterValidateLoadAndInvokeByIdPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._afterValidateLoadAndInvokeById
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__afterValidateLoadAndInvokeById} */
eco.artd.IFirebaseJoinpointModel.__afterValidateLoadAndInvokeById

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.afterValidateLoadAndInvokeByIdThrows no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterThrowsValidateLoadAndInvokeByIdPointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__afterValidateLoadAndInvokeByIdThrows = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.AfterThrowsValidateLoadAndInvokeByIdPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.afterValidateLoadAndInvokeByIdThrows
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.AfterThrowsValidateLoadAndInvokeByIdPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._afterValidateLoadAndInvokeByIdThrows
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__afterValidateLoadAndInvokeByIdThrows} */
eco.artd.IFirebaseJoinpointModel.__afterValidateLoadAndInvokeByIdThrows

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.afterValidateLoadAndInvokeByIdReturns no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterReturnsValidateLoadAndInvokeByIdPointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__afterValidateLoadAndInvokeByIdReturns = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.AfterReturnsValidateLoadAndInvokeByIdPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.afterValidateLoadAndInvokeByIdReturns
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.AfterReturnsValidateLoadAndInvokeByIdPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._afterValidateLoadAndInvokeByIdReturns
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__afterValidateLoadAndInvokeByIdReturns} */
eco.artd.IFirebaseJoinpointModel.__afterValidateLoadAndInvokeByIdReturns

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.afterValidateLoadAndInvokeByIdCancels no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterCancelsValidateLoadAndInvokeByIdPointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__afterValidateLoadAndInvokeByIdCancels = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.AfterCancelsValidateLoadAndInvokeByIdPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.afterValidateLoadAndInvokeByIdCancels
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.AfterCancelsValidateLoadAndInvokeByIdPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._afterValidateLoadAndInvokeByIdCancels
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__afterValidateLoadAndInvokeByIdCancels} */
eco.artd.IFirebaseJoinpointModel.__afterValidateLoadAndInvokeByIdCancels

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.immediatelyAfterValidateLoadAndInvokeById no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterValidateLoadAndInvokeByIdPointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__immediatelyAfterValidateLoadAndInvokeById = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterValidateLoadAndInvokeByIdPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.immediatelyAfterValidateLoadAndInvokeById
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterValidateLoadAndInvokeByIdPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._immediatelyAfterValidateLoadAndInvokeById
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__immediatelyAfterValidateLoadAndInvokeById} */
eco.artd.IFirebaseJoinpointModel.__immediatelyAfterValidateLoadAndInvokeById

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.beforeMakeInvoker no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.BeforeMakeInvokerPointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__beforeMakeInvoker = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.BeforeMakeInvokerPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.beforeMakeInvoker
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.BeforeMakeInvokerPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._beforeMakeInvoker
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__beforeMakeInvoker} */
eco.artd.IFirebaseJoinpointModel.__beforeMakeInvoker

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.afterMakeInvoker no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterMakeInvokerPointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__afterMakeInvoker = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.AfterMakeInvokerPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.afterMakeInvoker
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.AfterMakeInvokerPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._afterMakeInvoker
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__afterMakeInvoker} */
eco.artd.IFirebaseJoinpointModel.__afterMakeInvoker

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.afterMakeInvokerThrows no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterThrowsMakeInvokerPointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__afterMakeInvokerThrows = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.AfterThrowsMakeInvokerPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.afterMakeInvokerThrows
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.AfterThrowsMakeInvokerPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._afterMakeInvokerThrows
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__afterMakeInvokerThrows} */
eco.artd.IFirebaseJoinpointModel.__afterMakeInvokerThrows

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.afterMakeInvokerReturns no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterReturnsMakeInvokerPointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__afterMakeInvokerReturns = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.AfterReturnsMakeInvokerPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.afterMakeInvokerReturns
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.AfterReturnsMakeInvokerPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._afterMakeInvokerReturns
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__afterMakeInvokerReturns} */
eco.artd.IFirebaseJoinpointModel.__afterMakeInvokerReturns

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.afterMakeInvokerCancels no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterCancelsMakeInvokerPointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__afterMakeInvokerCancels = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.AfterCancelsMakeInvokerPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.afterMakeInvokerCancels
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.AfterCancelsMakeInvokerPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._afterMakeInvokerCancels
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__afterMakeInvokerCancels} */
eco.artd.IFirebaseJoinpointModel.__afterMakeInvokerCancels

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.beforeLoadAndInvokeById no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.BeforeLoadAndInvokeByIdPointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__beforeLoadAndInvokeById = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.BeforeLoadAndInvokeByIdPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.beforeLoadAndInvokeById
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.BeforeLoadAndInvokeByIdPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._beforeLoadAndInvokeById
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__beforeLoadAndInvokeById} */
eco.artd.IFirebaseJoinpointModel.__beforeLoadAndInvokeById

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.afterLoadAndInvokeById no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterLoadAndInvokeByIdPointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__afterLoadAndInvokeById = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.AfterLoadAndInvokeByIdPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.afterLoadAndInvokeById
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.AfterLoadAndInvokeByIdPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._afterLoadAndInvokeById
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__afterLoadAndInvokeById} */
eco.artd.IFirebaseJoinpointModel.__afterLoadAndInvokeById

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.afterLoadAndInvokeByIdThrows no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterThrowsLoadAndInvokeByIdPointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__afterLoadAndInvokeByIdThrows = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.AfterThrowsLoadAndInvokeByIdPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.afterLoadAndInvokeByIdThrows
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.AfterThrowsLoadAndInvokeByIdPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._afterLoadAndInvokeByIdThrows
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__afterLoadAndInvokeByIdThrows} */
eco.artd.IFirebaseJoinpointModel.__afterLoadAndInvokeByIdThrows

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.afterLoadAndInvokeByIdReturns no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterReturnsLoadAndInvokeByIdPointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__afterLoadAndInvokeByIdReturns = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.AfterReturnsLoadAndInvokeByIdPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.afterLoadAndInvokeByIdReturns
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.AfterReturnsLoadAndInvokeByIdPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._afterLoadAndInvokeByIdReturns
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__afterLoadAndInvokeByIdReturns} */
eco.artd.IFirebaseJoinpointModel.__afterLoadAndInvokeByIdReturns

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.afterLoadAndInvokeByIdCancels no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterCancelsLoadAndInvokeByIdPointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__afterLoadAndInvokeByIdCancels = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.AfterCancelsLoadAndInvokeByIdPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.afterLoadAndInvokeByIdCancels
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.AfterCancelsLoadAndInvokeByIdPointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._afterLoadAndInvokeByIdCancels
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__afterLoadAndInvokeByIdCancels} */
eco.artd.IFirebaseJoinpointModel.__afterLoadAndInvokeByIdCancels

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.beforeLoadAndInvoke no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.BeforeLoadAndInvokePointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__beforeLoadAndInvoke = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.BeforeLoadAndInvokePointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.beforeLoadAndInvoke
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.BeforeLoadAndInvokePointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._beforeLoadAndInvoke
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__beforeLoadAndInvoke} */
eco.artd.IFirebaseJoinpointModel.__beforeLoadAndInvoke

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.afterLoadAndInvoke no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterLoadAndInvokePointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__afterLoadAndInvoke = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.AfterLoadAndInvokePointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.afterLoadAndInvoke
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.AfterLoadAndInvokePointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._afterLoadAndInvoke
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__afterLoadAndInvoke} */
eco.artd.IFirebaseJoinpointModel.__afterLoadAndInvoke

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.afterLoadAndInvokeThrows no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterThrowsLoadAndInvokePointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__afterLoadAndInvokeThrows = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.AfterThrowsLoadAndInvokePointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.afterLoadAndInvokeThrows
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.AfterThrowsLoadAndInvokePointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._afterLoadAndInvokeThrows
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__afterLoadAndInvokeThrows} */
eco.artd.IFirebaseJoinpointModel.__afterLoadAndInvokeThrows

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.afterLoadAndInvokeReturns no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterReturnsLoadAndInvokePointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__afterLoadAndInvokeReturns = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.AfterReturnsLoadAndInvokePointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.afterLoadAndInvokeReturns
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.AfterReturnsLoadAndInvokePointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._afterLoadAndInvokeReturns
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__afterLoadAndInvokeReturns} */
eco.artd.IFirebaseJoinpointModel.__afterLoadAndInvokeReturns

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.afterLoadAndInvokeCancels no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterCancelsLoadAndInvokePointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__afterLoadAndInvokeCancels = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.AfterCancelsLoadAndInvokePointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.afterLoadAndInvokeCancels
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.AfterCancelsLoadAndInvokePointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._afterLoadAndInvokeCancels
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__afterLoadAndInvokeCancels} */
eco.artd.IFirebaseJoinpointModel.__afterLoadAndInvokeCancels

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.immediatelyAfterLoadAndInvoke no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterLoadAndInvokePointcutData} [data]
 * @return {void}
 */
$$eco.artd.IFirebaseJoinpointModel.__immediatelyAfterLoadAndInvoke = function(data) {}
/** @typedef {function(!eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterLoadAndInvokePointcutData=): void} */
eco.artd.IFirebaseJoinpointModel.immediatelyAfterLoadAndInvoke
/** @typedef {function(this: eco.artd.IFirebaseJoinpointModel, !eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterLoadAndInvokePointcutData=): void} */
eco.artd.IFirebaseJoinpointModel._immediatelyAfterLoadAndInvoke
/** @typedef {typeof $$eco.artd.IFirebaseJoinpointModel.__immediatelyAfterLoadAndInvoke} */
eco.artd.IFirebaseJoinpointModel.__immediatelyAfterLoadAndInvoke

// nss:eco.artd.IFirebaseJoinpointModel,$$eco.artd.IFirebaseJoinpointModel,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseAspectsInstaller.Initialese no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/** @record */
eco.artd.IFirebaseAspectsInstaller.Initialese = function() {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseAspectsInstaller no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 */
eco.artd.IFirebaseAspectsInstaller = function() {}
/** @param {...!eco.artd.IFirebaseAspectsInstaller.Initialese} init */
eco.artd.IFirebaseAspectsInstaller.prototype.constructor = function(...init) {}
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.beforeFunction
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.afterFunction
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.afterFunctionThrows
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.afterFunctionReturns
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.afterFunctionCancels
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.immediateAfterFunction
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.beforeEvalBodyWithProfileTime
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.afterEvalBodyWithProfileTime
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.afterEvalBodyWithProfileTimeThrows
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.afterEvalBodyWithProfileTimeReturns
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.afterEvalBodyWithProfileTimeCancels
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.beforeGetPageDataFromFirestore
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.afterGetPageDataFromFirestore
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.afterGetPageDataFromFirestoreThrows
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.afterGetPageDataFromFirestoreReturns
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.afterGetPageDataFromFirestoreCancels
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.immediateAfterGetPageDataFromFirestore
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.beforeDownloadAndEvalPageData
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.afterDownloadAndEvalPageData
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.afterDownloadAndEvalPageDataThrows
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.afterDownloadAndEvalPageDataReturns
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.afterDownloadAndEvalPageDataCancels
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.immediateAfterDownloadAndEvalPageData
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.beforeEvalBody
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.afterEvalBody
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.afterEvalBodyThrows
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.afterEvalBodyReturns
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.afterEvalBodyCancels
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.beforeLogRpcInvocation
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.afterLogRpcInvocation
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.afterLogRpcInvocationThrows
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.afterLogRpcInvocationReturns
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.afterLogRpcInvocationCancels
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.immediateAfterLogRpcInvocation
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.beforeFetchFunctionsFile
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.afterFetchFunctionsFile
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.afterFetchFunctionsFileThrows
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.afterFetchFunctionsFileReturns
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.afterFetchFunctionsFileCancels
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.immediateAfterFetchFunctionsFile
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.beforeDownloadDeps
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.afterDownloadDeps
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.afterDownloadDepsThrows
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.afterDownloadDepsReturns
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.afterDownloadDepsCancels
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.immediateAfterDownloadDeps
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.beforeDownloadMethod
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.afterDownloadMethod
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.afterDownloadMethodThrows
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.afterDownloadMethodReturns
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.afterDownloadMethodCancels
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.immediateAfterDownloadMethod
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.beforeDownloadMethodDirectly
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.afterDownloadMethodDirectly
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.afterDownloadMethodDirectlyThrows
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.afterDownloadMethodDirectlyReturns
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.afterDownloadMethodDirectlyCancels
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.immediateAfterDownloadMethodDirectly
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.beforeGetId
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.afterGetId
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.afterGetIdThrows
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.afterGetIdReturns
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.afterGetIdCancels
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.beforePatchDeps
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.afterPatchDeps
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.afterPatchDepsThrows
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.afterPatchDepsReturns
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.afterPatchDepsCancels
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.immediateAfterPatchDeps
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.beforeDownloadPageData
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.afterDownloadPageData
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.afterDownloadPageDataThrows
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.afterDownloadPageDataReturns
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.afterDownloadPageDataCancels
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.immediateAfterDownloadPageData
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.beforeLoadRequirePageData
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.afterLoadRequirePageData
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.afterLoadRequirePageDataThrows
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.afterLoadRequirePageDataReturns
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.afterLoadRequirePageDataCancels
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.beforeLoadRequire
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.afterLoadRequire
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.afterLoadRequireThrows
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.afterLoadRequireReturns
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.afterLoadRequireCancels
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.beforeUpdateErrorStack
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.afterUpdateErrorStack
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.afterUpdateErrorStackThrows
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.afterUpdateErrorStackReturns
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.afterUpdateErrorStackCancels
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.beforeValidateMethod
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.afterValidateMethod
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.afterValidateMethodThrows
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.afterValidateMethodReturns
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.afterValidateMethodCancels
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.beforeValidateLoadAndInvoke
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.afterValidateLoadAndInvoke
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.afterValidateLoadAndInvokeThrows
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.afterValidateLoadAndInvokeReturns
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.afterValidateLoadAndInvokeCancels
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.immediateAfterValidateLoadAndInvoke
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.beforeValidateLoadAndInvokeById
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.afterValidateLoadAndInvokeById
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.afterValidateLoadAndInvokeByIdThrows
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.afterValidateLoadAndInvokeByIdReturns
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.afterValidateLoadAndInvokeByIdCancels
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.immediateAfterValidateLoadAndInvokeById
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.beforeMakeInvoker
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.afterMakeInvoker
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.afterMakeInvokerThrows
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.afterMakeInvokerReturns
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.afterMakeInvokerCancels
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.beforeLoadAndInvokeById
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.afterLoadAndInvokeById
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.afterLoadAndInvokeByIdThrows
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.afterLoadAndInvokeByIdReturns
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.afterLoadAndInvokeByIdCancels
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.beforeLoadAndInvoke
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.afterLoadAndInvoke
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.afterLoadAndInvokeThrows
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.afterLoadAndInvokeReturns
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.afterLoadAndInvokeCancels
/** @type {number} */
eco.artd.IFirebaseAspectsInstaller.prototype.immediateAfterLoadAndInvoke
/** @return {?} */
eco.artd.IFirebaseAspectsInstaller.prototype.function = function() {}
/** @return {?} */
eco.artd.IFirebaseAspectsInstaller.prototype.evalBodyWithProfileTime = function() {}
/** @return {?} */
eco.artd.IFirebaseAspectsInstaller.prototype.getPageDataFromFirestore = function() {}
/** @return {?} */
eco.artd.IFirebaseAspectsInstaller.prototype.downloadAndEvalPageData = function() {}
/** @return {?} */
eco.artd.IFirebaseAspectsInstaller.prototype.evalBody = function() {}
/** @return {?} */
eco.artd.IFirebaseAspectsInstaller.prototype.logRpcInvocation = function() {}
/** @return {?} */
eco.artd.IFirebaseAspectsInstaller.prototype.fetchFunctionsFile = function() {}
/** @return {?} */
eco.artd.IFirebaseAspectsInstaller.prototype.downloadDeps = function() {}
/** @return {?} */
eco.artd.IFirebaseAspectsInstaller.prototype.downloadMethod = function() {}
/** @return {?} */
eco.artd.IFirebaseAspectsInstaller.prototype.downloadMethodDirectly = function() {}
/** @return {void} */
eco.artd.IFirebaseAspectsInstaller.prototype.getId = function() {}
/** @return {?} */
eco.artd.IFirebaseAspectsInstaller.prototype.patchDeps = function() {}
/** @return {?} */
eco.artd.IFirebaseAspectsInstaller.prototype.downloadPageData = function() {}
/** @return {?} */
eco.artd.IFirebaseAspectsInstaller.prototype.loadRequirePageData = function() {}
/** @return {?} */
eco.artd.IFirebaseAspectsInstaller.prototype.loadRequire = function() {}
/** @return {?} */
eco.artd.IFirebaseAspectsInstaller.prototype.updateErrorStack = function() {}
/** @return {?} */
eco.artd.IFirebaseAspectsInstaller.prototype.validateMethod = function() {}
/** @return {?} */
eco.artd.IFirebaseAspectsInstaller.prototype.validateLoadAndInvoke = function() {}
/** @return {?} */
eco.artd.IFirebaseAspectsInstaller.prototype.validateLoadAndInvokeById = function() {}
/** @return {?} */
eco.artd.IFirebaseAspectsInstaller.prototype.makeInvoker = function() {}
/** @return {?} */
eco.artd.IFirebaseAspectsInstaller.prototype.loadAndInvokeById = function() {}
/** @return {?} */
eco.artd.IFirebaseAspectsInstaller.prototype.loadAndInvoke = function() {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.FirebaseAspectsInstaller no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @constructor
 * @param {...!eco.artd.IFirebaseAspectsInstaller.Initialese} init
 * @implements {eco.artd.IFirebaseAspectsInstaller}
 * @implements {engineering.type.IInitialiser<!eco.artd.IFirebaseAspectsInstaller.Initialese>}
 */
eco.artd.FirebaseAspectsInstaller = function(...init) {}
/** @param {...!eco.artd.IFirebaseAspectsInstaller.Initialese} init */
eco.artd.FirebaseAspectsInstaller.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof eco.artd.FirebaseAspectsInstaller}
 */
eco.artd.FirebaseAspectsInstaller.__extend = function(...Extensions) {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.AbstractFirebaseAspectsInstaller no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @constructor
 * @extends {eco.artd.FirebaseAspectsInstaller}
 */
eco.artd.AbstractFirebaseAspectsInstaller = function() {}
/**
 * @param {...(!eco.artd.IFirebaseAspectsInstaller|typeof eco.artd.FirebaseAspectsInstaller)} Implementations
 * @return {typeof eco.artd.FirebaseAspectsInstaller}
 * @nosideeffects
 */
eco.artd.AbstractFirebaseAspectsInstaller.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof eco.artd.AbstractFirebaseAspectsInstaller}
 */
eco.artd.AbstractFirebaseAspectsInstaller.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof eco.artd.FirebaseAspectsInstaller}
 */
eco.artd.AbstractFirebaseAspectsInstaller.__extend = function(...Extensions) {}
/**
 * @param {...(!eco.artd.IFirebaseAspectsInstaller|typeof eco.artd.FirebaseAspectsInstaller)} Implementations
 * @return {typeof eco.artd.FirebaseAspectsInstaller}
 */
eco.artd.AbstractFirebaseAspectsInstaller.continues = function(...Implementations) {}
/**
 * @param {...(!eco.artd.IFirebaseAspectsInstaller|typeof eco.artd.FirebaseAspectsInstaller)} Implementations
 * @return {typeof eco.artd.FirebaseAspectsInstaller}
 */
eco.artd.AbstractFirebaseAspectsInstaller.__trait = function(...Implementations) {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.FirebaseAspectsInstallerConstructor no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/** @typedef {function(new: eco.artd.IFirebaseAspectsInstaller, ...!eco.artd.IFirebaseAspectsInstaller.Initialese)} */
eco.artd.FirebaseAspectsInstallerConstructor

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebase.FunctionNArgs no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/** @typedef {{ request: http.IncomingMessage, response: http.ServerResponse, cb: (!Function|number), meta: !eco.artd.IFirebase.function.Meta }} */
eco.artd.IFirebase.FunctionNArgs

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.FunctionPointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/** @typedef {{ ticket: symbol, args: eco.artd.IFirebase.FunctionNArgs, proc: !Function }} */
eco.artd.IFirebaseJoinpointModel.FunctionPointcutData

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.BeforeFunctionPointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.FunctionPointcutData}
 */
eco.artd.IFirebaseJoinpointModel.BeforeFunctionPointcutData = function() {}
/** @type {!function(!Object<string, *>): void} */
eco.artd.IFirebaseJoinpointModel.BeforeFunctionPointcutData.prototype.cond
/**
 * @param {eco.artd.IFirebase.FunctionNArgs} args
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.BeforeFunctionPointcutData.prototype.setArgs = function(args) {}
/**
 * @param {string} [reason]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.BeforeFunctionPointcutData.prototype.cancel = function(reason) {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.AfterFunctionPointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.FunctionPointcutData}
 */
eco.artd.IFirebaseJoinpointModel.AfterFunctionPointcutData = function() {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.AfterThrowsFunctionPointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.FunctionPointcutData}
 */
eco.artd.IFirebaseJoinpointModel.AfterThrowsFunctionPointcutData = function() {}
/** @type {!Error} */
eco.artd.IFirebaseJoinpointModel.AfterThrowsFunctionPointcutData.prototype.err
/** @return {void} */
eco.artd.IFirebaseJoinpointModel.AfterThrowsFunctionPointcutData.prototype.hide = function() {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.AfterReturnsFunctionPointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.FunctionPointcutData}
 */
eco.artd.IFirebaseJoinpointModel.AfterReturnsFunctionPointcutData = function() {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.AfterCancelsFunctionPointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.FunctionPointcutData}
 */
eco.artd.IFirebaseJoinpointModel.AfterCancelsFunctionPointcutData = function() {}
/** @type {!Set<string>} */
eco.artd.IFirebaseJoinpointModel.AfterCancelsFunctionPointcutData.prototype.reasons

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterFunctionPointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.FunctionPointcutData}
 */
eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterFunctionPointcutData = function() {}
/** @type {!Promise<void>} */
eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterFunctionPointcutData.prototype.promise

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebase.EvalBodyWithProfileTimeNArgs no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/** @typedef {{ body: string }} */
eco.artd.IFirebase.EvalBodyWithProfileTimeNArgs

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.EvalBodyWithProfileTimePointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/** @typedef {{ ticket: symbol, args: eco.artd.IFirebase.EvalBodyWithProfileTimeNArgs, proc: !Function }} */
eco.artd.IFirebaseJoinpointModel.EvalBodyWithProfileTimePointcutData

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.BeforeEvalBodyWithProfileTimePointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.EvalBodyWithProfileTimePointcutData}
 */
eco.artd.IFirebaseJoinpointModel.BeforeEvalBodyWithProfileTimePointcutData = function() {}
/** @type {!function(!Object<string, *>): void} */
eco.artd.IFirebaseJoinpointModel.BeforeEvalBodyWithProfileTimePointcutData.prototype.cond
/**
 * @param {eco.artd.IFirebase.EvalBodyWithProfileTimeNArgs} args
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.BeforeEvalBodyWithProfileTimePointcutData.prototype.setArgs = function(args) {}
/**
 * @param {string} [reason]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.BeforeEvalBodyWithProfileTimePointcutData.prototype.cancel = function(reason) {}
/**
 * @param {eco.artd.IFirebase.evalBodyWithProfileTime.Return} value
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.BeforeEvalBodyWithProfileTimePointcutData.prototype.sub = function(value) {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.AfterEvalBodyWithProfileTimePointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.EvalBodyWithProfileTimePointcutData}
 */
eco.artd.IFirebaseJoinpointModel.AfterEvalBodyWithProfileTimePointcutData = function() {}
/** @type {eco.artd.IFirebase.evalBodyWithProfileTime.Return} */
eco.artd.IFirebaseJoinpointModel.AfterEvalBodyWithProfileTimePointcutData.prototype.res

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.AfterThrowsEvalBodyWithProfileTimePointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.EvalBodyWithProfileTimePointcutData}
 */
eco.artd.IFirebaseJoinpointModel.AfterThrowsEvalBodyWithProfileTimePointcutData = function() {}
/** @type {!Error} */
eco.artd.IFirebaseJoinpointModel.AfterThrowsEvalBodyWithProfileTimePointcutData.prototype.err
/** @return {void} */
eco.artd.IFirebaseJoinpointModel.AfterThrowsEvalBodyWithProfileTimePointcutData.prototype.hide = function() {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.AfterReturnsEvalBodyWithProfileTimePointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.EvalBodyWithProfileTimePointcutData}
 */
eco.artd.IFirebaseJoinpointModel.AfterReturnsEvalBodyWithProfileTimePointcutData = function() {}
/** @type {eco.artd.IFirebase.evalBodyWithProfileTime.Return} */
eco.artd.IFirebaseJoinpointModel.AfterReturnsEvalBodyWithProfileTimePointcutData.prototype.res
/**
 * @param {eco.artd.IFirebase.evalBodyWithProfileTime.Return} value
 * @return {?}
 */
eco.artd.IFirebaseJoinpointModel.AfterReturnsEvalBodyWithProfileTimePointcutData.prototype.sub = function(value) {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.AfterCancelsEvalBodyWithProfileTimePointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.EvalBodyWithProfileTimePointcutData}
 */
eco.artd.IFirebaseJoinpointModel.AfterCancelsEvalBodyWithProfileTimePointcutData = function() {}
/** @type {!Set<string>} */
eco.artd.IFirebaseJoinpointModel.AfterCancelsEvalBodyWithProfileTimePointcutData.prototype.reasons

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebase.GetPageDataFromFirestoreNArgs no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/** @typedef {{ cid: number }} */
eco.artd.IFirebase.GetPageDataFromFirestoreNArgs

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.GetPageDataFromFirestorePointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/** @typedef {{ ticket: symbol, args: eco.artd.IFirebase.GetPageDataFromFirestoreNArgs, proc: !Function }} */
eco.artd.IFirebaseJoinpointModel.GetPageDataFromFirestorePointcutData

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.BeforeGetPageDataFromFirestorePointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.GetPageDataFromFirestorePointcutData}
 */
eco.artd.IFirebaseJoinpointModel.BeforeGetPageDataFromFirestorePointcutData = function() {}
/** @type {!function(!Object<string, *>): void} */
eco.artd.IFirebaseJoinpointModel.BeforeGetPageDataFromFirestorePointcutData.prototype.cond
/**
 * @param {eco.artd.IFirebase.GetPageDataFromFirestoreNArgs} args
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.BeforeGetPageDataFromFirestorePointcutData.prototype.setArgs = function(args) {}
/**
 * @param {string} [reason]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.BeforeGetPageDataFromFirestorePointcutData.prototype.cancel = function(reason) {}
/**
 * @param {!Promise<?eco.artd.IFirebase.PageData>} value
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.BeforeGetPageDataFromFirestorePointcutData.prototype.sub = function(value) {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.AfterGetPageDataFromFirestorePointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.GetPageDataFromFirestorePointcutData}
 */
eco.artd.IFirebaseJoinpointModel.AfterGetPageDataFromFirestorePointcutData = function() {}
/** @type {?eco.artd.IFirebase.PageData} */
eco.artd.IFirebaseJoinpointModel.AfterGetPageDataFromFirestorePointcutData.prototype.res

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.AfterThrowsGetPageDataFromFirestorePointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.GetPageDataFromFirestorePointcutData}
 */
eco.artd.IFirebaseJoinpointModel.AfterThrowsGetPageDataFromFirestorePointcutData = function() {}
/** @type {!Error} */
eco.artd.IFirebaseJoinpointModel.AfterThrowsGetPageDataFromFirestorePointcutData.prototype.err
/** @return {void} */
eco.artd.IFirebaseJoinpointModel.AfterThrowsGetPageDataFromFirestorePointcutData.prototype.hide = function() {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.AfterReturnsGetPageDataFromFirestorePointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.GetPageDataFromFirestorePointcutData}
 */
eco.artd.IFirebaseJoinpointModel.AfterReturnsGetPageDataFromFirestorePointcutData = function() {}
/** @type {?eco.artd.IFirebase.PageData} */
eco.artd.IFirebaseJoinpointModel.AfterReturnsGetPageDataFromFirestorePointcutData.prototype.res
/**
 * @param {(!Promise<?eco.artd.IFirebase.PageData>|?eco.artd.IFirebase.PageData)} value
 * @return {?}
 */
eco.artd.IFirebaseJoinpointModel.AfterReturnsGetPageDataFromFirestorePointcutData.prototype.sub = function(value) {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.AfterCancelsGetPageDataFromFirestorePointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.GetPageDataFromFirestorePointcutData}
 */
eco.artd.IFirebaseJoinpointModel.AfterCancelsGetPageDataFromFirestorePointcutData = function() {}
/** @type {!Set<string>} */
eco.artd.IFirebaseJoinpointModel.AfterCancelsGetPageDataFromFirestorePointcutData.prototype.reasons

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterGetPageDataFromFirestorePointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.GetPageDataFromFirestorePointcutData}
 */
eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterGetPageDataFromFirestorePointcutData = function() {}
/** @type {!Promise<?eco.artd.IFirebase.PageData>} */
eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterGetPageDataFromFirestorePointcutData.prototype.promise

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebase.DownloadAndEvalPageDataNArgs no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/** @typedef {{ id: number }} */
eco.artd.IFirebase.DownloadAndEvalPageDataNArgs

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.DownloadAndEvalPageDataPointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/** @typedef {{ ticket: symbol, args: eco.artd.IFirebase.DownloadAndEvalPageDataNArgs, proc: !Function }} */
eco.artd.IFirebaseJoinpointModel.DownloadAndEvalPageDataPointcutData

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.BeforeDownloadAndEvalPageDataPointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.DownloadAndEvalPageDataPointcutData}
 */
eco.artd.IFirebaseJoinpointModel.BeforeDownloadAndEvalPageDataPointcutData = function() {}
/** @type {!function(!Object<string, *>): void} */
eco.artd.IFirebaseJoinpointModel.BeforeDownloadAndEvalPageDataPointcutData.prototype.cond
/**
 * @param {eco.artd.IFirebase.DownloadAndEvalPageDataNArgs} args
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.BeforeDownloadAndEvalPageDataPointcutData.prototype.setArgs = function(args) {}
/**
 * @param {string} [reason]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.BeforeDownloadAndEvalPageDataPointcutData.prototype.cancel = function(reason) {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.AfterDownloadAndEvalPageDataPointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.DownloadAndEvalPageDataPointcutData}
 */
eco.artd.IFirebaseJoinpointModel.AfterDownloadAndEvalPageDataPointcutData = function() {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.AfterThrowsDownloadAndEvalPageDataPointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.DownloadAndEvalPageDataPointcutData}
 */
eco.artd.IFirebaseJoinpointModel.AfterThrowsDownloadAndEvalPageDataPointcutData = function() {}
/** @type {!Error} */
eco.artd.IFirebaseJoinpointModel.AfterThrowsDownloadAndEvalPageDataPointcutData.prototype.err
/** @return {void} */
eco.artd.IFirebaseJoinpointModel.AfterThrowsDownloadAndEvalPageDataPointcutData.prototype.hide = function() {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.AfterReturnsDownloadAndEvalPageDataPointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.DownloadAndEvalPageDataPointcutData}
 */
eco.artd.IFirebaseJoinpointModel.AfterReturnsDownloadAndEvalPageDataPointcutData = function() {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.AfterCancelsDownloadAndEvalPageDataPointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.DownloadAndEvalPageDataPointcutData}
 */
eco.artd.IFirebaseJoinpointModel.AfterCancelsDownloadAndEvalPageDataPointcutData = function() {}
/** @type {!Set<string>} */
eco.artd.IFirebaseJoinpointModel.AfterCancelsDownloadAndEvalPageDataPointcutData.prototype.reasons

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterDownloadAndEvalPageDataPointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.DownloadAndEvalPageDataPointcutData}
 */
eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterDownloadAndEvalPageDataPointcutData = function() {}
/** @type {!Promise<void>} */
eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterDownloadAndEvalPageDataPointcutData.prototype.promise

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebase.EvalBodyNArgs no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/** @typedef {{ body: string }} */
eco.artd.IFirebase.EvalBodyNArgs

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.EvalBodyPointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/** @typedef {{ ticket: symbol, args: eco.artd.IFirebase.EvalBodyNArgs, proc: !Function }} */
eco.artd.IFirebaseJoinpointModel.EvalBodyPointcutData

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.BeforeEvalBodyPointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.EvalBodyPointcutData}
 */
eco.artd.IFirebaseJoinpointModel.BeforeEvalBodyPointcutData = function() {}
/** @type {!function(!Object<string, *>): void} */
eco.artd.IFirebaseJoinpointModel.BeforeEvalBodyPointcutData.prototype.cond
/**
 * @param {eco.artd.IFirebase.EvalBodyNArgs} args
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.BeforeEvalBodyPointcutData.prototype.setArgs = function(args) {}
/**
 * @param {string} [reason]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.BeforeEvalBodyPointcutData.prototype.cancel = function(reason) {}
/**
 * @param {function(!http.IncomingMessage, !http.ServerResponse): (void|Promise<void>)} value
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.BeforeEvalBodyPointcutData.prototype.sub = function(value) {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.AfterEvalBodyPointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.EvalBodyPointcutData}
 */
eco.artd.IFirebaseJoinpointModel.AfterEvalBodyPointcutData = function() {}
/** @type {function(!http.IncomingMessage, !http.ServerResponse): (void|Promise<void>)} */
eco.artd.IFirebaseJoinpointModel.AfterEvalBodyPointcutData.prototype.res

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.AfterThrowsEvalBodyPointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.EvalBodyPointcutData}
 */
eco.artd.IFirebaseJoinpointModel.AfterThrowsEvalBodyPointcutData = function() {}
/** @type {!Error} */
eco.artd.IFirebaseJoinpointModel.AfterThrowsEvalBodyPointcutData.prototype.err
/** @return {void} */
eco.artd.IFirebaseJoinpointModel.AfterThrowsEvalBodyPointcutData.prototype.hide = function() {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.AfterReturnsEvalBodyPointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.EvalBodyPointcutData}
 */
eco.artd.IFirebaseJoinpointModel.AfterReturnsEvalBodyPointcutData = function() {}
/** @type {function(!http.IncomingMessage, !http.ServerResponse): (void|Promise<void>)} */
eco.artd.IFirebaseJoinpointModel.AfterReturnsEvalBodyPointcutData.prototype.res
/**
 * @param {function(!http.IncomingMessage, !http.ServerResponse): (void|Promise<void>)} value
 * @return {?}
 */
eco.artd.IFirebaseJoinpointModel.AfterReturnsEvalBodyPointcutData.prototype.sub = function(value) {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.AfterCancelsEvalBodyPointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.EvalBodyPointcutData}
 */
eco.artd.IFirebaseJoinpointModel.AfterCancelsEvalBodyPointcutData = function() {}
/** @type {!Set<string>} */
eco.artd.IFirebaseJoinpointModel.AfterCancelsEvalBodyPointcutData.prototype.reasons

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebase.LogRpcInvocationNArgs no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/** @typedef {{ dateTime: !Date, times: eco.artd.IFirebase.Times, methods: eco.artd.IFirebase.Methods, invRes: { error: Error, res: null, req: null } }} */
eco.artd.IFirebase.LogRpcInvocationNArgs

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.LogRpcInvocationPointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/** @typedef {{ ticket: symbol, args: eco.artd.IFirebase.LogRpcInvocationNArgs, proc: !Function }} */
eco.artd.IFirebaseJoinpointModel.LogRpcInvocationPointcutData

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.BeforeLogRpcInvocationPointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.LogRpcInvocationPointcutData}
 */
eco.artd.IFirebaseJoinpointModel.BeforeLogRpcInvocationPointcutData = function() {}
/** @type {!function(!Object<string, *>): void} */
eco.artd.IFirebaseJoinpointModel.BeforeLogRpcInvocationPointcutData.prototype.cond
/**
 * @param {eco.artd.IFirebase.LogRpcInvocationNArgs} args
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.BeforeLogRpcInvocationPointcutData.prototype.setArgs = function(args) {}
/**
 * @param {string} [reason]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.BeforeLogRpcInvocationPointcutData.prototype.cancel = function(reason) {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.AfterLogRpcInvocationPointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.LogRpcInvocationPointcutData}
 */
eco.artd.IFirebaseJoinpointModel.AfterLogRpcInvocationPointcutData = function() {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.AfterThrowsLogRpcInvocationPointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.LogRpcInvocationPointcutData}
 */
eco.artd.IFirebaseJoinpointModel.AfterThrowsLogRpcInvocationPointcutData = function() {}
/** @type {!Error} */
eco.artd.IFirebaseJoinpointModel.AfterThrowsLogRpcInvocationPointcutData.prototype.err
/** @return {void} */
eco.artd.IFirebaseJoinpointModel.AfterThrowsLogRpcInvocationPointcutData.prototype.hide = function() {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.AfterReturnsLogRpcInvocationPointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.LogRpcInvocationPointcutData}
 */
eco.artd.IFirebaseJoinpointModel.AfterReturnsLogRpcInvocationPointcutData = function() {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.AfterCancelsLogRpcInvocationPointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.LogRpcInvocationPointcutData}
 */
eco.artd.IFirebaseJoinpointModel.AfterCancelsLogRpcInvocationPointcutData = function() {}
/** @type {!Set<string>} */
eco.artd.IFirebaseJoinpointModel.AfterCancelsLogRpcInvocationPointcutData.prototype.reasons

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterLogRpcInvocationPointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.LogRpcInvocationPointcutData}
 */
eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterLogRpcInvocationPointcutData = function() {}
/** @type {!Promise<void>} */
eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterLogRpcInvocationPointcutData.prototype.promise

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebase.FetchFunctionsFileNArgs no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/** @typedef {{ file: string }} */
eco.artd.IFirebase.FetchFunctionsFileNArgs

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.FetchFunctionsFilePointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/** @typedef {{ ticket: symbol, args: eco.artd.IFirebase.FetchFunctionsFileNArgs, proc: !Function }} */
eco.artd.IFirebaseJoinpointModel.FetchFunctionsFilePointcutData

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.BeforeFetchFunctionsFilePointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.FetchFunctionsFilePointcutData}
 */
eco.artd.IFirebaseJoinpointModel.BeforeFetchFunctionsFilePointcutData = function() {}
/** @type {!function(!Object<string, *>): void} */
eco.artd.IFirebaseJoinpointModel.BeforeFetchFunctionsFilePointcutData.prototype.cond
/**
 * @param {eco.artd.IFirebase.FetchFunctionsFileNArgs} args
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.BeforeFetchFunctionsFilePointcutData.prototype.setArgs = function(args) {}
/**
 * @param {string} [reason]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.BeforeFetchFunctionsFilePointcutData.prototype.cancel = function(reason) {}
/**
 * @param {!Promise<eco.artd.IFirebase.fetchFunctionsFile.Return>} value
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.BeforeFetchFunctionsFilePointcutData.prototype.sub = function(value) {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.AfterFetchFunctionsFilePointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.FetchFunctionsFilePointcutData}
 */
eco.artd.IFirebaseJoinpointModel.AfterFetchFunctionsFilePointcutData = function() {}
/** @type {eco.artd.IFirebase.fetchFunctionsFile.Return} */
eco.artd.IFirebaseJoinpointModel.AfterFetchFunctionsFilePointcutData.prototype.res

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.AfterThrowsFetchFunctionsFilePointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.FetchFunctionsFilePointcutData}
 */
eco.artd.IFirebaseJoinpointModel.AfterThrowsFetchFunctionsFilePointcutData = function() {}
/** @type {!Error} */
eco.artd.IFirebaseJoinpointModel.AfterThrowsFetchFunctionsFilePointcutData.prototype.err
/** @return {void} */
eco.artd.IFirebaseJoinpointModel.AfterThrowsFetchFunctionsFilePointcutData.prototype.hide = function() {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.AfterReturnsFetchFunctionsFilePointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.FetchFunctionsFilePointcutData}
 */
eco.artd.IFirebaseJoinpointModel.AfterReturnsFetchFunctionsFilePointcutData = function() {}
/** @type {eco.artd.IFirebase.fetchFunctionsFile.Return} */
eco.artd.IFirebaseJoinpointModel.AfterReturnsFetchFunctionsFilePointcutData.prototype.res
/**
 * @param {(!Promise<eco.artd.IFirebase.fetchFunctionsFile.Return>|eco.artd.IFirebase.fetchFunctionsFile.Return)} value
 * @return {?}
 */
eco.artd.IFirebaseJoinpointModel.AfterReturnsFetchFunctionsFilePointcutData.prototype.sub = function(value) {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.AfterCancelsFetchFunctionsFilePointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.FetchFunctionsFilePointcutData}
 */
eco.artd.IFirebaseJoinpointModel.AfterCancelsFetchFunctionsFilePointcutData = function() {}
/** @type {!Set<string>} */
eco.artd.IFirebaseJoinpointModel.AfterCancelsFetchFunctionsFilePointcutData.prototype.reasons

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterFetchFunctionsFilePointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.FetchFunctionsFilePointcutData}
 */
eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterFetchFunctionsFilePointcutData = function() {}
/** @type {!Promise<eco.artd.IFirebase.fetchFunctionsFile.Return>} */
eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterFetchFunctionsFilePointcutData.prototype.promise

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebase.DownloadDepsNArgs no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/** @typedef {{ deps: !Array<string>, id: number }} */
eco.artd.IFirebase.DownloadDepsNArgs

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.DownloadDepsPointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/** @typedef {{ ticket: symbol, args: eco.artd.IFirebase.DownloadDepsNArgs, proc: !Function }} */
eco.artd.IFirebaseJoinpointModel.DownloadDepsPointcutData

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.BeforeDownloadDepsPointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.DownloadDepsPointcutData}
 */
eco.artd.IFirebaseJoinpointModel.BeforeDownloadDepsPointcutData = function() {}
/** @type {!function(!Object<string, *>): void} */
eco.artd.IFirebaseJoinpointModel.BeforeDownloadDepsPointcutData.prototype.cond
/**
 * @param {eco.artd.IFirebase.DownloadDepsNArgs} args
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.BeforeDownloadDepsPointcutData.prototype.setArgs = function(args) {}
/**
 * @param {string} [reason]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.BeforeDownloadDepsPointcutData.prototype.cancel = function(reason) {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.AfterDownloadDepsPointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.DownloadDepsPointcutData}
 */
eco.artd.IFirebaseJoinpointModel.AfterDownloadDepsPointcutData = function() {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.AfterThrowsDownloadDepsPointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.DownloadDepsPointcutData}
 */
eco.artd.IFirebaseJoinpointModel.AfterThrowsDownloadDepsPointcutData = function() {}
/** @type {!Error} */
eco.artd.IFirebaseJoinpointModel.AfterThrowsDownloadDepsPointcutData.prototype.err
/** @return {void} */
eco.artd.IFirebaseJoinpointModel.AfterThrowsDownloadDepsPointcutData.prototype.hide = function() {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.AfterReturnsDownloadDepsPointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.DownloadDepsPointcutData}
 */
eco.artd.IFirebaseJoinpointModel.AfterReturnsDownloadDepsPointcutData = function() {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.AfterCancelsDownloadDepsPointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.DownloadDepsPointcutData}
 */
eco.artd.IFirebaseJoinpointModel.AfterCancelsDownloadDepsPointcutData = function() {}
/** @type {!Set<string>} */
eco.artd.IFirebaseJoinpointModel.AfterCancelsDownloadDepsPointcutData.prototype.reasons

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterDownloadDepsPointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.DownloadDepsPointcutData}
 */
eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterDownloadDepsPointcutData = function() {}
/** @type {!Promise<void>} */
eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterDownloadDepsPointcutData.prototype.promise

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebase.DownloadMethodNArgs no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/** @typedef {{ cid: number, methodName: string }} */
eco.artd.IFirebase.DownloadMethodNArgs

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.DownloadMethodPointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/** @typedef {{ ticket: symbol, args: eco.artd.IFirebase.DownloadMethodNArgs, proc: !Function }} */
eco.artd.IFirebaseJoinpointModel.DownloadMethodPointcutData

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.BeforeDownloadMethodPointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.DownloadMethodPointcutData}
 */
eco.artd.IFirebaseJoinpointModel.BeforeDownloadMethodPointcutData = function() {}
/** @type {!function(!Object<string, *>): void} */
eco.artd.IFirebaseJoinpointModel.BeforeDownloadMethodPointcutData.prototype.cond
/**
 * @param {eco.artd.IFirebase.DownloadMethodNArgs} args
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.BeforeDownloadMethodPointcutData.prototype.setArgs = function(args) {}
/**
 * @param {string} [reason]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.BeforeDownloadMethodPointcutData.prototype.cancel = function(reason) {}
/**
 * @param {!Promise<string>} value
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.BeforeDownloadMethodPointcutData.prototype.sub = function(value) {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.AfterDownloadMethodPointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.DownloadMethodPointcutData}
 */
eco.artd.IFirebaseJoinpointModel.AfterDownloadMethodPointcutData = function() {}
/** @type {string} */
eco.artd.IFirebaseJoinpointModel.AfterDownloadMethodPointcutData.prototype.res

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.AfterThrowsDownloadMethodPointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.DownloadMethodPointcutData}
 */
eco.artd.IFirebaseJoinpointModel.AfterThrowsDownloadMethodPointcutData = function() {}
/** @type {!Error} */
eco.artd.IFirebaseJoinpointModel.AfterThrowsDownloadMethodPointcutData.prototype.err
/** @return {void} */
eco.artd.IFirebaseJoinpointModel.AfterThrowsDownloadMethodPointcutData.prototype.hide = function() {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.AfterReturnsDownloadMethodPointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.DownloadMethodPointcutData}
 */
eco.artd.IFirebaseJoinpointModel.AfterReturnsDownloadMethodPointcutData = function() {}
/** @type {string} */
eco.artd.IFirebaseJoinpointModel.AfterReturnsDownloadMethodPointcutData.prototype.res
/**
 * @param {(!Promise<string>|string)} value
 * @return {?}
 */
eco.artd.IFirebaseJoinpointModel.AfterReturnsDownloadMethodPointcutData.prototype.sub = function(value) {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.AfterCancelsDownloadMethodPointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.DownloadMethodPointcutData}
 */
eco.artd.IFirebaseJoinpointModel.AfterCancelsDownloadMethodPointcutData = function() {}
/** @type {!Set<string>} */
eco.artd.IFirebaseJoinpointModel.AfterCancelsDownloadMethodPointcutData.prototype.reasons

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterDownloadMethodPointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.DownloadMethodPointcutData}
 */
eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterDownloadMethodPointcutData = function() {}
/** @type {!Promise<string>} */
eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterDownloadMethodPointcutData.prototype.promise

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebase.DownloadMethodDirectlyNArgs no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/** @typedef {{ cid: number, methodId: string }} */
eco.artd.IFirebase.DownloadMethodDirectlyNArgs

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.DownloadMethodDirectlyPointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/** @typedef {{ ticket: symbol, args: eco.artd.IFirebase.DownloadMethodDirectlyNArgs, proc: !Function }} */
eco.artd.IFirebaseJoinpointModel.DownloadMethodDirectlyPointcutData

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.BeforeDownloadMethodDirectlyPointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.DownloadMethodDirectlyPointcutData}
 */
eco.artd.IFirebaseJoinpointModel.BeforeDownloadMethodDirectlyPointcutData = function() {}
/** @type {!function(!Object<string, *>): void} */
eco.artd.IFirebaseJoinpointModel.BeforeDownloadMethodDirectlyPointcutData.prototype.cond
/**
 * @param {eco.artd.IFirebase.DownloadMethodDirectlyNArgs} args
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.BeforeDownloadMethodDirectlyPointcutData.prototype.setArgs = function(args) {}
/**
 * @param {string} [reason]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.BeforeDownloadMethodDirectlyPointcutData.prototype.cancel = function(reason) {}
/**
 * @param {!Promise<eco.artd.IFirebase.downloadMethodDirectly.Return>} value
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.BeforeDownloadMethodDirectlyPointcutData.prototype.sub = function(value) {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.AfterDownloadMethodDirectlyPointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.DownloadMethodDirectlyPointcutData}
 */
eco.artd.IFirebaseJoinpointModel.AfterDownloadMethodDirectlyPointcutData = function() {}
/** @type {eco.artd.IFirebase.downloadMethodDirectly.Return} */
eco.artd.IFirebaseJoinpointModel.AfterDownloadMethodDirectlyPointcutData.prototype.res

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.AfterThrowsDownloadMethodDirectlyPointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.DownloadMethodDirectlyPointcutData}
 */
eco.artd.IFirebaseJoinpointModel.AfterThrowsDownloadMethodDirectlyPointcutData = function() {}
/** @type {!Error} */
eco.artd.IFirebaseJoinpointModel.AfterThrowsDownloadMethodDirectlyPointcutData.prototype.err
/** @return {void} */
eco.artd.IFirebaseJoinpointModel.AfterThrowsDownloadMethodDirectlyPointcutData.prototype.hide = function() {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.AfterReturnsDownloadMethodDirectlyPointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.DownloadMethodDirectlyPointcutData}
 */
eco.artd.IFirebaseJoinpointModel.AfterReturnsDownloadMethodDirectlyPointcutData = function() {}
/** @type {eco.artd.IFirebase.downloadMethodDirectly.Return} */
eco.artd.IFirebaseJoinpointModel.AfterReturnsDownloadMethodDirectlyPointcutData.prototype.res
/**
 * @param {(!Promise<eco.artd.IFirebase.downloadMethodDirectly.Return>|eco.artd.IFirebase.downloadMethodDirectly.Return)} value
 * @return {?}
 */
eco.artd.IFirebaseJoinpointModel.AfterReturnsDownloadMethodDirectlyPointcutData.prototype.sub = function(value) {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.AfterCancelsDownloadMethodDirectlyPointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.DownloadMethodDirectlyPointcutData}
 */
eco.artd.IFirebaseJoinpointModel.AfterCancelsDownloadMethodDirectlyPointcutData = function() {}
/** @type {!Set<string>} */
eco.artd.IFirebaseJoinpointModel.AfterCancelsDownloadMethodDirectlyPointcutData.prototype.reasons

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterDownloadMethodDirectlyPointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.DownloadMethodDirectlyPointcutData}
 */
eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterDownloadMethodDirectlyPointcutData = function() {}
/** @type {!Promise<eco.artd.IFirebase.downloadMethodDirectly.Return>} */
eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterDownloadMethodDirectlyPointcutData.prototype.promise

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.GetIdPointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/** @typedef {{ ticket: symbol, proc: !Function }} */
eco.artd.IFirebaseJoinpointModel.GetIdPointcutData

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.BeforeGetIdPointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.GetIdPointcutData}
 */
eco.artd.IFirebaseJoinpointModel.BeforeGetIdPointcutData = function() {}
/** @type {!function(!Object<string, *>): void} */
eco.artd.IFirebaseJoinpointModel.BeforeGetIdPointcutData.prototype.cond
/**
 * @param {string} [reason]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.BeforeGetIdPointcutData.prototype.cancel = function(reason) {}
/**
 * @param {number} value
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.BeforeGetIdPointcutData.prototype.sub = function(value) {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.AfterGetIdPointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.GetIdPointcutData}
 */
eco.artd.IFirebaseJoinpointModel.AfterGetIdPointcutData = function() {}
/** @type {number} */
eco.artd.IFirebaseJoinpointModel.AfterGetIdPointcutData.prototype.res

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.AfterThrowsGetIdPointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.GetIdPointcutData}
 */
eco.artd.IFirebaseJoinpointModel.AfterThrowsGetIdPointcutData = function() {}
/** @type {!Error} */
eco.artd.IFirebaseJoinpointModel.AfterThrowsGetIdPointcutData.prototype.err
/** @return {void} */
eco.artd.IFirebaseJoinpointModel.AfterThrowsGetIdPointcutData.prototype.hide = function() {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.AfterReturnsGetIdPointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.GetIdPointcutData}
 */
eco.artd.IFirebaseJoinpointModel.AfterReturnsGetIdPointcutData = function() {}
/** @type {number} */
eco.artd.IFirebaseJoinpointModel.AfterReturnsGetIdPointcutData.prototype.res
/**
 * @param {number} value
 * @return {?}
 */
eco.artd.IFirebaseJoinpointModel.AfterReturnsGetIdPointcutData.prototype.sub = function(value) {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.AfterCancelsGetIdPointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.GetIdPointcutData}
 */
eco.artd.IFirebaseJoinpointModel.AfterCancelsGetIdPointcutData = function() {}
/** @type {!Set<string>} */
eco.artd.IFirebaseJoinpointModel.AfterCancelsGetIdPointcutData.prototype.reasons

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebase.PatchDepsNArgs no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/** @typedef {{ body: string }} */
eco.artd.IFirebase.PatchDepsNArgs

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.PatchDepsPointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/** @typedef {{ ticket: symbol, args: eco.artd.IFirebase.PatchDepsNArgs, proc: !Function }} */
eco.artd.IFirebaseJoinpointModel.PatchDepsPointcutData

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.BeforePatchDepsPointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.PatchDepsPointcutData}
 */
eco.artd.IFirebaseJoinpointModel.BeforePatchDepsPointcutData = function() {}
/** @type {!function(!Object<string, *>): void} */
eco.artd.IFirebaseJoinpointModel.BeforePatchDepsPointcutData.prototype.cond
/**
 * @param {eco.artd.IFirebase.PatchDepsNArgs} args
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.BeforePatchDepsPointcutData.prototype.setArgs = function(args) {}
/**
 * @param {string} [reason]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.BeforePatchDepsPointcutData.prototype.cancel = function(reason) {}
/**
 * @param {!Promise<eco.artd.IFirebase.patchDeps.Return>} value
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.BeforePatchDepsPointcutData.prototype.sub = function(value) {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.AfterPatchDepsPointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.PatchDepsPointcutData}
 */
eco.artd.IFirebaseJoinpointModel.AfterPatchDepsPointcutData = function() {}
/** @type {eco.artd.IFirebase.patchDeps.Return} */
eco.artd.IFirebaseJoinpointModel.AfterPatchDepsPointcutData.prototype.res

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.AfterThrowsPatchDepsPointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.PatchDepsPointcutData}
 */
eco.artd.IFirebaseJoinpointModel.AfterThrowsPatchDepsPointcutData = function() {}
/** @type {!Error} */
eco.artd.IFirebaseJoinpointModel.AfterThrowsPatchDepsPointcutData.prototype.err
/** @return {void} */
eco.artd.IFirebaseJoinpointModel.AfterThrowsPatchDepsPointcutData.prototype.hide = function() {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.AfterReturnsPatchDepsPointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.PatchDepsPointcutData}
 */
eco.artd.IFirebaseJoinpointModel.AfterReturnsPatchDepsPointcutData = function() {}
/** @type {eco.artd.IFirebase.patchDeps.Return} */
eco.artd.IFirebaseJoinpointModel.AfterReturnsPatchDepsPointcutData.prototype.res
/**
 * @param {(!Promise<eco.artd.IFirebase.patchDeps.Return>|eco.artd.IFirebase.patchDeps.Return)} value
 * @return {?}
 */
eco.artd.IFirebaseJoinpointModel.AfterReturnsPatchDepsPointcutData.prototype.sub = function(value) {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.AfterCancelsPatchDepsPointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.PatchDepsPointcutData}
 */
eco.artd.IFirebaseJoinpointModel.AfterCancelsPatchDepsPointcutData = function() {}
/** @type {!Set<string>} */
eco.artd.IFirebaseJoinpointModel.AfterCancelsPatchDepsPointcutData.prototype.reasons

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterPatchDepsPointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.PatchDepsPointcutData}
 */
eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterPatchDepsPointcutData = function() {}
/** @type {!Promise<eco.artd.IFirebase.patchDeps.Return>} */
eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterPatchDepsPointcutData.prototype.promise

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebase.DownloadPageDataNArgs no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/** @typedef {{ cid: number }} */
eco.artd.IFirebase.DownloadPageDataNArgs

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.DownloadPageDataPointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/** @typedef {{ ticket: symbol, args: eco.artd.IFirebase.DownloadPageDataNArgs, proc: !Function }} */
eco.artd.IFirebaseJoinpointModel.DownloadPageDataPointcutData

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.BeforeDownloadPageDataPointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.DownloadPageDataPointcutData}
 */
eco.artd.IFirebaseJoinpointModel.BeforeDownloadPageDataPointcutData = function() {}
/** @type {!function(!Object<string, *>): void} */
eco.artd.IFirebaseJoinpointModel.BeforeDownloadPageDataPointcutData.prototype.cond
/**
 * @param {eco.artd.IFirebase.DownloadPageDataNArgs} args
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.BeforeDownloadPageDataPointcutData.prototype.setArgs = function(args) {}
/**
 * @param {string} [reason]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.BeforeDownloadPageDataPointcutData.prototype.cancel = function(reason) {}
/**
 * @param {!Promise<string>} value
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.BeforeDownloadPageDataPointcutData.prototype.sub = function(value) {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.AfterDownloadPageDataPointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.DownloadPageDataPointcutData}
 */
eco.artd.IFirebaseJoinpointModel.AfterDownloadPageDataPointcutData = function() {}
/** @type {string} */
eco.artd.IFirebaseJoinpointModel.AfterDownloadPageDataPointcutData.prototype.res

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.AfterThrowsDownloadPageDataPointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.DownloadPageDataPointcutData}
 */
eco.artd.IFirebaseJoinpointModel.AfterThrowsDownloadPageDataPointcutData = function() {}
/** @type {!Error} */
eco.artd.IFirebaseJoinpointModel.AfterThrowsDownloadPageDataPointcutData.prototype.err
/** @return {void} */
eco.artd.IFirebaseJoinpointModel.AfterThrowsDownloadPageDataPointcutData.prototype.hide = function() {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.AfterReturnsDownloadPageDataPointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.DownloadPageDataPointcutData}
 */
eco.artd.IFirebaseJoinpointModel.AfterReturnsDownloadPageDataPointcutData = function() {}
/** @type {string} */
eco.artd.IFirebaseJoinpointModel.AfterReturnsDownloadPageDataPointcutData.prototype.res
/**
 * @param {(!Promise<string>|string)} value
 * @return {?}
 */
eco.artd.IFirebaseJoinpointModel.AfterReturnsDownloadPageDataPointcutData.prototype.sub = function(value) {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.AfterCancelsDownloadPageDataPointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.DownloadPageDataPointcutData}
 */
eco.artd.IFirebaseJoinpointModel.AfterCancelsDownloadPageDataPointcutData = function() {}
/** @type {!Set<string>} */
eco.artd.IFirebaseJoinpointModel.AfterCancelsDownloadPageDataPointcutData.prototype.reasons

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterDownloadPageDataPointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.DownloadPageDataPointcutData}
 */
eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterDownloadPageDataPointcutData = function() {}
/** @type {!Promise<string>} */
eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterDownloadPageDataPointcutData.prototype.promise

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebase.LoadRequirePageDataNArgs no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/** @typedef {{ cid: number }} */
eco.artd.IFirebase.LoadRequirePageDataNArgs

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.LoadRequirePageDataPointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/** @typedef {{ ticket: symbol, args: eco.artd.IFirebase.LoadRequirePageDataNArgs, proc: !Function }} */
eco.artd.IFirebaseJoinpointModel.LoadRequirePageDataPointcutData

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.BeforeLoadRequirePageDataPointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.LoadRequirePageDataPointcutData}
 */
eco.artd.IFirebaseJoinpointModel.BeforeLoadRequirePageDataPointcutData = function() {}
/** @type {!function(!Object<string, *>): void} */
eco.artd.IFirebaseJoinpointModel.BeforeLoadRequirePageDataPointcutData.prototype.cond
/**
 * @param {eco.artd.IFirebase.LoadRequirePageDataNArgs} args
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.BeforeLoadRequirePageDataPointcutData.prototype.setArgs = function(args) {}
/**
 * @param {string} [reason]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.BeforeLoadRequirePageDataPointcutData.prototype.cancel = function(reason) {}
/**
 * @param {!eco.artd.IFirebase.PageData} value
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.BeforeLoadRequirePageDataPointcutData.prototype.sub = function(value) {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.AfterLoadRequirePageDataPointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.LoadRequirePageDataPointcutData}
 */
eco.artd.IFirebaseJoinpointModel.AfterLoadRequirePageDataPointcutData = function() {}
/** @type {!eco.artd.IFirebase.PageData} */
eco.artd.IFirebaseJoinpointModel.AfterLoadRequirePageDataPointcutData.prototype.res

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.AfterThrowsLoadRequirePageDataPointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.LoadRequirePageDataPointcutData}
 */
eco.artd.IFirebaseJoinpointModel.AfterThrowsLoadRequirePageDataPointcutData = function() {}
/** @type {!Error} */
eco.artd.IFirebaseJoinpointModel.AfterThrowsLoadRequirePageDataPointcutData.prototype.err
/** @return {void} */
eco.artd.IFirebaseJoinpointModel.AfterThrowsLoadRequirePageDataPointcutData.prototype.hide = function() {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.AfterReturnsLoadRequirePageDataPointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.LoadRequirePageDataPointcutData}
 */
eco.artd.IFirebaseJoinpointModel.AfterReturnsLoadRequirePageDataPointcutData = function() {}
/** @type {!eco.artd.IFirebase.PageData} */
eco.artd.IFirebaseJoinpointModel.AfterReturnsLoadRequirePageDataPointcutData.prototype.res
/**
 * @param {!eco.artd.IFirebase.PageData} value
 * @return {?}
 */
eco.artd.IFirebaseJoinpointModel.AfterReturnsLoadRequirePageDataPointcutData.prototype.sub = function(value) {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.AfterCancelsLoadRequirePageDataPointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.LoadRequirePageDataPointcutData}
 */
eco.artd.IFirebaseJoinpointModel.AfterCancelsLoadRequirePageDataPointcutData = function() {}
/** @type {!Set<string>} */
eco.artd.IFirebaseJoinpointModel.AfterCancelsLoadRequirePageDataPointcutData.prototype.reasons

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebase.LoadRequireNArgs no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/** @typedef {{ path: string }} */
eco.artd.IFirebase.LoadRequireNArgs

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.LoadRequirePointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/** @typedef {{ ticket: symbol, args: eco.artd.IFirebase.LoadRequireNArgs, proc: !Function }} */
eco.artd.IFirebaseJoinpointModel.LoadRequirePointcutData

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.BeforeLoadRequirePointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.LoadRequirePointcutData}
 */
eco.artd.IFirebaseJoinpointModel.BeforeLoadRequirePointcutData = function() {}
/** @type {!function(!Object<string, *>): void} */
eco.artd.IFirebaseJoinpointModel.BeforeLoadRequirePointcutData.prototype.cond
/**
 * @param {eco.artd.IFirebase.LoadRequireNArgs} args
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.BeforeLoadRequirePointcutData.prototype.setArgs = function(args) {}
/**
 * @param {string} [reason]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.BeforeLoadRequirePointcutData.prototype.cancel = function(reason) {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.AfterLoadRequirePointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.LoadRequirePointcutData}
 */
eco.artd.IFirebaseJoinpointModel.AfterLoadRequirePointcutData = function() {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.AfterThrowsLoadRequirePointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.LoadRequirePointcutData}
 */
eco.artd.IFirebaseJoinpointModel.AfterThrowsLoadRequirePointcutData = function() {}
/** @type {!Error} */
eco.artd.IFirebaseJoinpointModel.AfterThrowsLoadRequirePointcutData.prototype.err
/** @return {void} */
eco.artd.IFirebaseJoinpointModel.AfterThrowsLoadRequirePointcutData.prototype.hide = function() {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.AfterReturnsLoadRequirePointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.LoadRequirePointcutData}
 */
eco.artd.IFirebaseJoinpointModel.AfterReturnsLoadRequirePointcutData = function() {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.AfterCancelsLoadRequirePointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.LoadRequirePointcutData}
 */
eco.artd.IFirebaseJoinpointModel.AfterCancelsLoadRequirePointcutData = function() {}
/** @type {!Set<string>} */
eco.artd.IFirebaseJoinpointModel.AfterCancelsLoadRequirePointcutData.prototype.reasons

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebase.UpdateErrorStackNArgs no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/** @typedef {{ error: !Error, url: string }} */
eco.artd.IFirebase.UpdateErrorStackNArgs

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.UpdateErrorStackPointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/** @typedef {{ ticket: symbol, args: eco.artd.IFirebase.UpdateErrorStackNArgs, proc: !Function }} */
eco.artd.IFirebaseJoinpointModel.UpdateErrorStackPointcutData

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.BeforeUpdateErrorStackPointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.UpdateErrorStackPointcutData}
 */
eco.artd.IFirebaseJoinpointModel.BeforeUpdateErrorStackPointcutData = function() {}
/** @type {!function(!Object<string, *>): void} */
eco.artd.IFirebaseJoinpointModel.BeforeUpdateErrorStackPointcutData.prototype.cond
/**
 * @param {eco.artd.IFirebase.UpdateErrorStackNArgs} args
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.BeforeUpdateErrorStackPointcutData.prototype.setArgs = function(args) {}
/**
 * @param {string} [reason]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.BeforeUpdateErrorStackPointcutData.prototype.cancel = function(reason) {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.AfterUpdateErrorStackPointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.UpdateErrorStackPointcutData}
 */
eco.artd.IFirebaseJoinpointModel.AfterUpdateErrorStackPointcutData = function() {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.AfterThrowsUpdateErrorStackPointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.UpdateErrorStackPointcutData}
 */
eco.artd.IFirebaseJoinpointModel.AfterThrowsUpdateErrorStackPointcutData = function() {}
/** @type {!Error} */
eco.artd.IFirebaseJoinpointModel.AfterThrowsUpdateErrorStackPointcutData.prototype.err
/** @return {void} */
eco.artd.IFirebaseJoinpointModel.AfterThrowsUpdateErrorStackPointcutData.prototype.hide = function() {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.AfterReturnsUpdateErrorStackPointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.UpdateErrorStackPointcutData}
 */
eco.artd.IFirebaseJoinpointModel.AfterReturnsUpdateErrorStackPointcutData = function() {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.AfterCancelsUpdateErrorStackPointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.UpdateErrorStackPointcutData}
 */
eco.artd.IFirebaseJoinpointModel.AfterCancelsUpdateErrorStackPointcutData = function() {}
/** @type {!Set<string>} */
eco.artd.IFirebaseJoinpointModel.AfterCancelsUpdateErrorStackPointcutData.prototype.reasons

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebase.ValidateMethodNArgs no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/** @typedef {{ methodsMap: Map<string, string> }} */
eco.artd.IFirebase.ValidateMethodNArgs

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.ValidateMethodPointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/** @typedef {{ ticket: symbol, args: eco.artd.IFirebase.ValidateMethodNArgs, proc: !Function }} */
eco.artd.IFirebaseJoinpointModel.ValidateMethodPointcutData

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.BeforeValidateMethodPointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.ValidateMethodPointcutData}
 */
eco.artd.IFirebaseJoinpointModel.BeforeValidateMethodPointcutData = function() {}
/** @type {!function(!Object<string, *>): void} */
eco.artd.IFirebaseJoinpointModel.BeforeValidateMethodPointcutData.prototype.cond
/**
 * @param {eco.artd.IFirebase.ValidateMethodNArgs} args
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.BeforeValidateMethodPointcutData.prototype.setArgs = function(args) {}
/**
 * @param {string} [reason]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.BeforeValidateMethodPointcutData.prototype.cancel = function(reason) {}
/**
 * @param {string} value
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.BeforeValidateMethodPointcutData.prototype.sub = function(value) {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.AfterValidateMethodPointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.ValidateMethodPointcutData}
 */
eco.artd.IFirebaseJoinpointModel.AfterValidateMethodPointcutData = function() {}
/** @type {string} */
eco.artd.IFirebaseJoinpointModel.AfterValidateMethodPointcutData.prototype.res

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.AfterThrowsValidateMethodPointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.ValidateMethodPointcutData}
 */
eco.artd.IFirebaseJoinpointModel.AfterThrowsValidateMethodPointcutData = function() {}
/** @type {!Error} */
eco.artd.IFirebaseJoinpointModel.AfterThrowsValidateMethodPointcutData.prototype.err
/** @return {void} */
eco.artd.IFirebaseJoinpointModel.AfterThrowsValidateMethodPointcutData.prototype.hide = function() {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.AfterReturnsValidateMethodPointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.ValidateMethodPointcutData}
 */
eco.artd.IFirebaseJoinpointModel.AfterReturnsValidateMethodPointcutData = function() {}
/** @type {string} */
eco.artd.IFirebaseJoinpointModel.AfterReturnsValidateMethodPointcutData.prototype.res
/**
 * @param {string} value
 * @return {?}
 */
eco.artd.IFirebaseJoinpointModel.AfterReturnsValidateMethodPointcutData.prototype.sub = function(value) {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.AfterCancelsValidateMethodPointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.ValidateMethodPointcutData}
 */
eco.artd.IFirebaseJoinpointModel.AfterCancelsValidateMethodPointcutData = function() {}
/** @type {!Set<string>} */
eco.artd.IFirebaseJoinpointModel.AfterCancelsValidateMethodPointcutData.prototype.reasons

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebase.ValidateLoadAndInvokeNArgs no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/** @typedef {{ pageData: !Object<string, eco.artd.IFirebase.PageData> }} */
eco.artd.IFirebase.ValidateLoadAndInvokeNArgs

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.ValidateLoadAndInvokePointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/** @typedef {{ ticket: symbol, args: eco.artd.IFirebase.ValidateLoadAndInvokeNArgs, proc: !Function }} */
eco.artd.IFirebaseJoinpointModel.ValidateLoadAndInvokePointcutData

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.BeforeValidateLoadAndInvokePointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.ValidateLoadAndInvokePointcutData}
 */
eco.artd.IFirebaseJoinpointModel.BeforeValidateLoadAndInvokePointcutData = function() {}
/** @type {!function(!Object<string, *>): void} */
eco.artd.IFirebaseJoinpointModel.BeforeValidateLoadAndInvokePointcutData.prototype.cond
/**
 * @param {eco.artd.IFirebase.ValidateLoadAndInvokeNArgs} args
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.BeforeValidateLoadAndInvokePointcutData.prototype.setArgs = function(args) {}
/**
 * @param {string} [reason]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.BeforeValidateLoadAndInvokePointcutData.prototype.cancel = function(reason) {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.AfterValidateLoadAndInvokePointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.ValidateLoadAndInvokePointcutData}
 */
eco.artd.IFirebaseJoinpointModel.AfterValidateLoadAndInvokePointcutData = function() {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.AfterThrowsValidateLoadAndInvokePointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.ValidateLoadAndInvokePointcutData}
 */
eco.artd.IFirebaseJoinpointModel.AfterThrowsValidateLoadAndInvokePointcutData = function() {}
/** @type {!Error} */
eco.artd.IFirebaseJoinpointModel.AfterThrowsValidateLoadAndInvokePointcutData.prototype.err
/** @return {void} */
eco.artd.IFirebaseJoinpointModel.AfterThrowsValidateLoadAndInvokePointcutData.prototype.hide = function() {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.AfterReturnsValidateLoadAndInvokePointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.ValidateLoadAndInvokePointcutData}
 */
eco.artd.IFirebaseJoinpointModel.AfterReturnsValidateLoadAndInvokePointcutData = function() {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.AfterCancelsValidateLoadAndInvokePointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.ValidateLoadAndInvokePointcutData}
 */
eco.artd.IFirebaseJoinpointModel.AfterCancelsValidateLoadAndInvokePointcutData = function() {}
/** @type {!Set<string>} */
eco.artd.IFirebaseJoinpointModel.AfterCancelsValidateLoadAndInvokePointcutData.prototype.reasons

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterValidateLoadAndInvokePointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.ValidateLoadAndInvokePointcutData}
 */
eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterValidateLoadAndInvokePointcutData = function() {}
/** @type {!Promise<void>} */
eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterValidateLoadAndInvokePointcutData.prototype.promise

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebase.ValidateLoadAndInvokeByIdNArgs no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/** @typedef {{ cid: number }} */
eco.artd.IFirebase.ValidateLoadAndInvokeByIdNArgs

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.ValidateLoadAndInvokeByIdPointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/** @typedef {{ ticket: symbol, args: eco.artd.IFirebase.ValidateLoadAndInvokeByIdNArgs, proc: !Function }} */
eco.artd.IFirebaseJoinpointModel.ValidateLoadAndInvokeByIdPointcutData

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.BeforeValidateLoadAndInvokeByIdPointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.ValidateLoadAndInvokeByIdPointcutData}
 */
eco.artd.IFirebaseJoinpointModel.BeforeValidateLoadAndInvokeByIdPointcutData = function() {}
/** @type {!function(!Object<string, *>): void} */
eco.artd.IFirebaseJoinpointModel.BeforeValidateLoadAndInvokeByIdPointcutData.prototype.cond
/**
 * @param {eco.artd.IFirebase.ValidateLoadAndInvokeByIdNArgs} args
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.BeforeValidateLoadAndInvokeByIdPointcutData.prototype.setArgs = function(args) {}
/**
 * @param {string} [reason]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.BeforeValidateLoadAndInvokeByIdPointcutData.prototype.cancel = function(reason) {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.AfterValidateLoadAndInvokeByIdPointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.ValidateLoadAndInvokeByIdPointcutData}
 */
eco.artd.IFirebaseJoinpointModel.AfterValidateLoadAndInvokeByIdPointcutData = function() {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.AfterThrowsValidateLoadAndInvokeByIdPointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.ValidateLoadAndInvokeByIdPointcutData}
 */
eco.artd.IFirebaseJoinpointModel.AfterThrowsValidateLoadAndInvokeByIdPointcutData = function() {}
/** @type {!Error} */
eco.artd.IFirebaseJoinpointModel.AfterThrowsValidateLoadAndInvokeByIdPointcutData.prototype.err
/** @return {void} */
eco.artd.IFirebaseJoinpointModel.AfterThrowsValidateLoadAndInvokeByIdPointcutData.prototype.hide = function() {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.AfterReturnsValidateLoadAndInvokeByIdPointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.ValidateLoadAndInvokeByIdPointcutData}
 */
eco.artd.IFirebaseJoinpointModel.AfterReturnsValidateLoadAndInvokeByIdPointcutData = function() {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.AfterCancelsValidateLoadAndInvokeByIdPointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.ValidateLoadAndInvokeByIdPointcutData}
 */
eco.artd.IFirebaseJoinpointModel.AfterCancelsValidateLoadAndInvokeByIdPointcutData = function() {}
/** @type {!Set<string>} */
eco.artd.IFirebaseJoinpointModel.AfterCancelsValidateLoadAndInvokeByIdPointcutData.prototype.reasons

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterValidateLoadAndInvokeByIdPointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.ValidateLoadAndInvokeByIdPointcutData}
 */
eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterValidateLoadAndInvokeByIdPointcutData = function() {}
/** @type {!Promise<void>} */
eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterValidateLoadAndInvokeByIdPointcutData.prototype.promise

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebase.MakeInvokerNArgs no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/** @typedef {{ id: number }} */
eco.artd.IFirebase.MakeInvokerNArgs

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.MakeInvokerPointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/** @typedef {{ ticket: symbol, args: eco.artd.IFirebase.MakeInvokerNArgs, proc: !Function }} */
eco.artd.IFirebaseJoinpointModel.MakeInvokerPointcutData

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.BeforeMakeInvokerPointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.MakeInvokerPointcutData}
 */
eco.artd.IFirebaseJoinpointModel.BeforeMakeInvokerPointcutData = function() {}
/** @type {!function(!Object<string, *>): void} */
eco.artd.IFirebaseJoinpointModel.BeforeMakeInvokerPointcutData.prototype.cond
/**
 * @param {eco.artd.IFirebase.MakeInvokerNArgs} args
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.BeforeMakeInvokerPointcutData.prototype.setArgs = function(args) {}
/**
 * @param {string} [reason]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.BeforeMakeInvokerPointcutData.prototype.cancel = function(reason) {}
/**
 * @param {function()} value
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.BeforeMakeInvokerPointcutData.prototype.sub = function(value) {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.AfterMakeInvokerPointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.MakeInvokerPointcutData}
 */
eco.artd.IFirebaseJoinpointModel.AfterMakeInvokerPointcutData = function() {}
/** @type {function()} */
eco.artd.IFirebaseJoinpointModel.AfterMakeInvokerPointcutData.prototype.res

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.AfterThrowsMakeInvokerPointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.MakeInvokerPointcutData}
 */
eco.artd.IFirebaseJoinpointModel.AfterThrowsMakeInvokerPointcutData = function() {}
/** @type {!Error} */
eco.artd.IFirebaseJoinpointModel.AfterThrowsMakeInvokerPointcutData.prototype.err
/** @return {void} */
eco.artd.IFirebaseJoinpointModel.AfterThrowsMakeInvokerPointcutData.prototype.hide = function() {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.AfterReturnsMakeInvokerPointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.MakeInvokerPointcutData}
 */
eco.artd.IFirebaseJoinpointModel.AfterReturnsMakeInvokerPointcutData = function() {}
/** @type {function()} */
eco.artd.IFirebaseJoinpointModel.AfterReturnsMakeInvokerPointcutData.prototype.res
/**
 * @param {function()} value
 * @return {?}
 */
eco.artd.IFirebaseJoinpointModel.AfterReturnsMakeInvokerPointcutData.prototype.sub = function(value) {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.AfterCancelsMakeInvokerPointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.MakeInvokerPointcutData}
 */
eco.artd.IFirebaseJoinpointModel.AfterCancelsMakeInvokerPointcutData = function() {}
/** @type {!Set<string>} */
eco.artd.IFirebaseJoinpointModel.AfterCancelsMakeInvokerPointcutData.prototype.reasons

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebase.LoadAndInvokeByIdNArgs no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/** @typedef {{ cid: number }} */
eco.artd.IFirebase.LoadAndInvokeByIdNArgs

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.LoadAndInvokeByIdPointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/** @typedef {{ ticket: symbol, args: eco.artd.IFirebase.LoadAndInvokeByIdNArgs, proc: !Function }} */
eco.artd.IFirebaseJoinpointModel.LoadAndInvokeByIdPointcutData

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.BeforeLoadAndInvokeByIdPointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.LoadAndInvokeByIdPointcutData}
 */
eco.artd.IFirebaseJoinpointModel.BeforeLoadAndInvokeByIdPointcutData = function() {}
/** @type {!function(!Object<string, *>): void} */
eco.artd.IFirebaseJoinpointModel.BeforeLoadAndInvokeByIdPointcutData.prototype.cond
/**
 * @param {eco.artd.IFirebase.LoadAndInvokeByIdNArgs} args
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.BeforeLoadAndInvokeByIdPointcutData.prototype.setArgs = function(args) {}
/**
 * @param {string} [reason]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.BeforeLoadAndInvokeByIdPointcutData.prototype.cancel = function(reason) {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.AfterLoadAndInvokeByIdPointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.LoadAndInvokeByIdPointcutData}
 */
eco.artd.IFirebaseJoinpointModel.AfterLoadAndInvokeByIdPointcutData = function() {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.AfterThrowsLoadAndInvokeByIdPointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.LoadAndInvokeByIdPointcutData}
 */
eco.artd.IFirebaseJoinpointModel.AfterThrowsLoadAndInvokeByIdPointcutData = function() {}
/** @type {!Error} */
eco.artd.IFirebaseJoinpointModel.AfterThrowsLoadAndInvokeByIdPointcutData.prototype.err
/** @return {void} */
eco.artd.IFirebaseJoinpointModel.AfterThrowsLoadAndInvokeByIdPointcutData.prototype.hide = function() {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.AfterReturnsLoadAndInvokeByIdPointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.LoadAndInvokeByIdPointcutData}
 */
eco.artd.IFirebaseJoinpointModel.AfterReturnsLoadAndInvokeByIdPointcutData = function() {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.AfterCancelsLoadAndInvokeByIdPointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.LoadAndInvokeByIdPointcutData}
 */
eco.artd.IFirebaseJoinpointModel.AfterCancelsLoadAndInvokeByIdPointcutData = function() {}
/** @type {!Set<string>} */
eco.artd.IFirebaseJoinpointModel.AfterCancelsLoadAndInvokeByIdPointcutData.prototype.reasons

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebase.LoadAndInvokeNArgs no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/** @typedef {{ methodId: string, id: number }} */
eco.artd.IFirebase.LoadAndInvokeNArgs

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.LoadAndInvokePointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/** @typedef {{ ticket: symbol, args: eco.artd.IFirebase.LoadAndInvokeNArgs, proc: !Function }} */
eco.artd.IFirebaseJoinpointModel.LoadAndInvokePointcutData

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.BeforeLoadAndInvokePointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.LoadAndInvokePointcutData}
 */
eco.artd.IFirebaseJoinpointModel.BeforeLoadAndInvokePointcutData = function() {}
/** @type {!function(!Object<string, *>): void} */
eco.artd.IFirebaseJoinpointModel.BeforeLoadAndInvokePointcutData.prototype.cond
/**
 * @param {eco.artd.IFirebase.LoadAndInvokeNArgs} args
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.BeforeLoadAndInvokePointcutData.prototype.setArgs = function(args) {}
/**
 * @param {string} [reason]
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.BeforeLoadAndInvokePointcutData.prototype.cancel = function(reason) {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.AfterLoadAndInvokePointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.LoadAndInvokePointcutData}
 */
eco.artd.IFirebaseJoinpointModel.AfterLoadAndInvokePointcutData = function() {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.AfterThrowsLoadAndInvokePointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.LoadAndInvokePointcutData}
 */
eco.artd.IFirebaseJoinpointModel.AfterThrowsLoadAndInvokePointcutData = function() {}
/** @type {!Error} */
eco.artd.IFirebaseJoinpointModel.AfterThrowsLoadAndInvokePointcutData.prototype.err
/** @return {void} */
eco.artd.IFirebaseJoinpointModel.AfterThrowsLoadAndInvokePointcutData.prototype.hide = function() {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.AfterReturnsLoadAndInvokePointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.LoadAndInvokePointcutData}
 */
eco.artd.IFirebaseJoinpointModel.AfterReturnsLoadAndInvokePointcutData = function() {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.AfterCancelsLoadAndInvokePointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.LoadAndInvokePointcutData}
 */
eco.artd.IFirebaseJoinpointModel.AfterCancelsLoadAndInvokePointcutData = function() {}
/** @type {!Set<string>} */
eco.artd.IFirebaseJoinpointModel.AfterCancelsLoadAndInvokePointcutData.prototype.reasons

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterLoadAndInvokePointcutData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseJoinpointModel.LoadAndInvokePointcutData}
 */
eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterLoadAndInvokePointcutData = function() {}
/** @type {!Promise<void>} */
eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterLoadAndInvokePointcutData.prototype.promise

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.FirebaseConstructor no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/** @typedef {function(new: eco.artd.IFirebase, ...!eco.artd.IFirebase.Initialese)} */
eco.artd.FirebaseConstructor

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.BFirebaseAspectsCaster no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @interface
 * @template THIS
 */
eco.artd.BFirebaseAspectsCaster
/** @type {!eco.artd.BoundIFirebase} */
eco.artd.BFirebaseAspectsCaster.prototype.asIFirebase

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.BFirebaseAspects no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @interface
 * @extends {eco.artd.BFirebaseAspectsCaster<THIS>}
 * @extends {eco.artd.IFirebaseJoinpointModelBindingHyperslice<THIS>}
 * @template THIS
 */
eco.artd.BFirebaseAspects = function() {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseAspects.Initialese no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/** @record */
eco.artd.IFirebaseAspects.Initialese = function() {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseAspectsCaster no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/** @interface */
eco.artd.IFirebaseAspectsCaster
/** @type {!eco.artd.BoundIFirebase} */
eco.artd.IFirebaseAspectsCaster.prototype.asIFirebase

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseAspects no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {eco.artd.IFirebaseAspectsCaster}
 * @extends {eco.artd.BFirebaseAspects<!eco.artd.IFirebaseAspects>}
 */
eco.artd.IFirebaseAspects = function() {}
/** @param {...!eco.artd.IFirebaseAspects.Initialese} init */
eco.artd.IFirebaseAspects.prototype.constructor = function(...init) {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.FirebaseAspects no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @constructor
 * @param {...!eco.artd.IFirebaseAspects.Initialese} init
 * @implements {eco.artd.IFirebaseAspects}
 * @implements {engineering.type.IInitialiser<!eco.artd.IFirebaseAspects.Initialese>}
 */
eco.artd.FirebaseAspects = function(...init) {}
/** @param {...!eco.artd.IFirebaseAspects.Initialese} init */
eco.artd.FirebaseAspects.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof eco.artd.FirebaseAspects}
 */
eco.artd.FirebaseAspects.__extend = function(...Extensions) {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.AbstractFirebaseAspects no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @constructor
 * @extends {eco.artd.FirebaseAspects}
 */
eco.artd.AbstractFirebaseAspects = function() {}
/**
 * @param {...((!eco.artd.IFirebaseAspects|typeof eco.artd.FirebaseAspects)|(!eco.artd.BFirebaseAspects|typeof eco.artd.BFirebaseAspects))} Implementations
 * @return {typeof eco.artd.FirebaseAspects}
 * @nosideeffects
 */
eco.artd.AbstractFirebaseAspects.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof eco.artd.AbstractFirebaseAspects}
 */
eco.artd.AbstractFirebaseAspects.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof eco.artd.FirebaseAspects}
 */
eco.artd.AbstractFirebaseAspects.__extend = function(...Extensions) {}
/**
 * @param {...((!eco.artd.IFirebaseAspects|typeof eco.artd.FirebaseAspects)|(!eco.artd.BFirebaseAspects|typeof eco.artd.BFirebaseAspects))} Implementations
 * @return {typeof eco.artd.FirebaseAspects}
 */
eco.artd.AbstractFirebaseAspects.continues = function(...Implementations) {}
/**
 * @param {...((!eco.artd.IFirebaseAspects|typeof eco.artd.FirebaseAspects)|(!eco.artd.BFirebaseAspects|typeof eco.artd.BFirebaseAspects))} Implementations
 * @return {typeof eco.artd.FirebaseAspects}
 */
eco.artd.AbstractFirebaseAspects.__trait = function(...Implementations) {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.FirebaseAspectsConstructor no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/** @typedef {function(new: eco.artd.IFirebaseAspects, ...!eco.artd.IFirebaseAspects.Initialese)} */
eco.artd.FirebaseAspectsConstructor

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseProfilingAide.Initialese no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {engineering.type.IProfilingAide.Initialese}
 */
eco.artd.IFirebaseProfilingAide.Initialese = function() {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseProfilingAspects.Initialese no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseProfilingAide.Initialese}
 */
eco.artd.IFirebaseProfilingAspects.Initialese = function() {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseProfilingAspectsCaster no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/** @interface */
eco.artd.IFirebaseProfilingAspectsCaster
/** @type {!eco.artd.BoundIFirebase} */
eco.artd.IFirebaseProfilingAspectsCaster.prototype.asIFirebase

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseProfilingAideCaster no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/** @interface */
eco.artd.IFirebaseProfilingAideCaster
/** @type {!eco.artd.BoundIFirebaseProfilingAide} */
eco.artd.IFirebaseProfilingAideCaster.prototype.asIFirebaseProfilingAide
/** @type {!eco.artd.BoundIFirebase} */
eco.artd.IFirebaseProfilingAideCaster.prototype.asIFirebase
/** @type {!eco.artd.BoundFirebaseProfilingAide} */
eco.artd.IFirebaseProfilingAideCaster.prototype.superFirebaseProfilingAide

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseProfilingAide no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {eco.artd.IFirebaseProfilingAideCaster}
 * @extends {engineering.type.IProfilingAide}
 */
eco.artd.IFirebaseProfilingAide = function() {}
/** @param {...!eco.artd.IFirebaseProfilingAide.Initialese} init */
eco.artd.IFirebaseProfilingAide.prototype.constructor = function(...init) {}
/**
 * @param {number} time
 * @param {eco.artd.IFirebase.GetPageDataFromFirestoreNArgs} args
 * @param {!Function} proc
 * @param {?eco.artd.IFirebase.PageData} res
 * @return {(undefined|!Promise<undefined>)}
 */
eco.artd.IFirebaseProfilingAide.prototype.reportGetPageDataFromFirestoreProfile = function(time, args, proc, res) {}
/**
 * @param {number} time
 * @param {eco.artd.IFirebase.DownloadAndEvalPageDataNArgs} args
 * @param {!Function} proc
 * @param {*} res
 * @return {(undefined|!Promise<undefined>)}
 */
eco.artd.IFirebaseProfilingAide.prototype.reportDownloadAndEvalPageDataProfile = function(time, args, proc, res) {}
/**
 * @param {number} time
 * @param {eco.artd.IFirebase.EvalBodyNArgs} args
 * @param {!Function} proc
 * @param {function(!http.IncomingMessage, !http.ServerResponse): (void|Promise<void>)} res
 * @return {(undefined|!Promise<undefined>)}
 */
eco.artd.IFirebaseProfilingAide.prototype.reportEvalBodyProfile = function(time, args, proc, res) {}
/**
 * @param {number} time
 * @param {eco.artd.IFirebase.FetchFunctionsFileNArgs} args
 * @param {!Function} proc
 * @param {eco.artd.IFirebase.fetchFunctionsFile.Return} res
 * @return {(undefined|!Promise<undefined>)}
 */
eco.artd.IFirebaseProfilingAide.prototype.reportFetchFunctionsFileProfile = function(time, args, proc, res) {}
/**
 * @param {number} time
 * @param {eco.artd.IFirebase.DownloadDepsNArgs} args
 * @param {!Function} proc
 * @param {*} res
 * @return {(undefined|!Promise<undefined>)}
 */
eco.artd.IFirebaseProfilingAide.prototype.reportDownloadDepsProfile = function(time, args, proc, res) {}
/**
 * @param {number} time
 * @param {eco.artd.IFirebase.DownloadMethodNArgs} args
 * @param {!Function} proc
 * @param {string} res
 * @return {(undefined|!Promise<undefined>)}
 */
eco.artd.IFirebaseProfilingAide.prototype.reportDownloadMethodProfile = function(time, args, proc, res) {}
/**
 * @param {number} time
 * @param {eco.artd.IFirebase.DownloadMethodDirectlyNArgs} args
 * @param {!Function} proc
 * @param {eco.artd.IFirebase.downloadMethodDirectly.Return} res
 * @return {(undefined|!Promise<undefined>)}
 */
eco.artd.IFirebaseProfilingAide.prototype.reportDownloadMethodDirectlyProfile = function(time, args, proc, res) {}
/**
 * @param {number} time
 * @param {eco.artd.IFirebase.DownloadPageDataNArgs} args
 * @param {!Function} proc
 * @param {string} res
 * @return {(undefined|!Promise<undefined>)}
 */
eco.artd.IFirebaseProfilingAide.prototype.reportDownloadPageDataProfile = function(time, args, proc, res) {}
/**
 * @param {number} time
 * @param {eco.artd.IFirebase.LoadRequirePageDataNArgs} args
 * @param {!Function} proc
 * @param {!eco.artd.IFirebase.PageData} res
 * @return {(undefined|!Promise<undefined>)}
 */
eco.artd.IFirebaseProfilingAide.prototype.reportLoadRequirePageDataProfile = function(time, args, proc, res) {}
/**
 * @param {number} time
 * @param {eco.artd.IFirebase.LoadRequireNArgs} args
 * @param {!Function} proc
 * @param {*} res
 * @return {(undefined|!Promise<undefined>)}
 */
eco.artd.IFirebaseProfilingAide.prototype.reportLoadRequireProfile = function(time, args, proc, res) {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseProfilingAspects no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {eco.artd.IFirebaseProfilingAspectsCaster}
 * @extends {eco.artd.BFirebaseAspects<!eco.artd.IFirebaseProfilingAspects>}
 * @extends {eco.artd.IFirebaseProfilingAide}
 */
eco.artd.IFirebaseProfilingAspects = function() {}
/** @param {...!eco.artd.IFirebaseProfilingAspects.Initialese} init */
eco.artd.IFirebaseProfilingAspects.prototype.constructor = function(...init) {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.FirebaseProfilingAspects no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @constructor
 * @param {...!eco.artd.IFirebaseProfilingAspects.Initialese} init
 * @implements {eco.artd.IFirebaseProfilingAspects}
 * @implements {engineering.type.IInitialiser<!eco.artd.IFirebaseProfilingAspects.Initialese>}
 */
eco.artd.FirebaseProfilingAspects = function(...init) {}
/** @param {...!eco.artd.IFirebaseProfilingAspects.Initialese} init */
eco.artd.FirebaseProfilingAspects.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof eco.artd.FirebaseProfilingAspects}
 */
eco.artd.FirebaseProfilingAspects.__extend = function(...Extensions) {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.AbstractFirebaseProfilingAspects no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @constructor
 * @extends {eco.artd.FirebaseProfilingAspects}
 */
eco.artd.AbstractFirebaseProfilingAspects = function() {}
/**
 * @param {...((!eco.artd.IFirebaseProfilingAspects|typeof eco.artd.FirebaseProfilingAspects)|(!eco.artd.BFirebaseAspects|typeof eco.artd.BFirebaseAspects)|(!eco.artd.IFirebaseProfilingAide|typeof eco.artd.FirebaseProfilingAide))} Implementations
 * @return {typeof eco.artd.FirebaseProfilingAspects}
 * @nosideeffects
 */
eco.artd.AbstractFirebaseProfilingAspects.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof eco.artd.AbstractFirebaseProfilingAspects}
 */
eco.artd.AbstractFirebaseProfilingAspects.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof eco.artd.FirebaseProfilingAspects}
 */
eco.artd.AbstractFirebaseProfilingAspects.__extend = function(...Extensions) {}
/**
 * @param {...((!eco.artd.IFirebaseProfilingAspects|typeof eco.artd.FirebaseProfilingAspects)|(!eco.artd.BFirebaseAspects|typeof eco.artd.BFirebaseAspects)|(!eco.artd.IFirebaseProfilingAide|typeof eco.artd.FirebaseProfilingAide))} Implementations
 * @return {typeof eco.artd.FirebaseProfilingAspects}
 */
eco.artd.AbstractFirebaseProfilingAspects.continues = function(...Implementations) {}
/**
 * @param {...((!eco.artd.IFirebaseProfilingAspects|typeof eco.artd.FirebaseProfilingAspects)|(!eco.artd.BFirebaseAspects|typeof eco.artd.BFirebaseAspects)|(!eco.artd.IFirebaseProfilingAide|typeof eco.artd.FirebaseProfilingAide))} Implementations
 * @return {typeof eco.artd.FirebaseProfilingAspects}
 */
eco.artd.AbstractFirebaseProfilingAspects.__trait = function(...Implementations) {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.FirebaseProfilingAspectsConstructor no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/** @typedef {function(new: eco.artd.IFirebaseProfilingAspects, ...!eco.artd.IFirebaseProfilingAspects.Initialese)} */
eco.artd.FirebaseProfilingAspectsConstructor

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.FirebaseProfilingAide no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @constructor
 * @param {...!eco.artd.IFirebaseProfilingAide.Initialese} init
 * @implements {eco.artd.IFirebaseProfilingAide}
 * @implements {engineering.type.IInitialiser<!eco.artd.IFirebaseProfilingAide.Initialese>}
 */
eco.artd.FirebaseProfilingAide = function(...init) {}
/** @param {...!eco.artd.IFirebaseProfilingAide.Initialese} init */
eco.artd.FirebaseProfilingAide.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof eco.artd.FirebaseProfilingAide}
 */
eco.artd.FirebaseProfilingAide.__extend = function(...Extensions) {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.AbstractFirebaseProfilingAide no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @constructor
 * @extends {eco.artd.FirebaseProfilingAide}
 */
eco.artd.AbstractFirebaseProfilingAide = function() {}
/**
 * @param {...((!eco.artd.IFirebaseProfilingAide|typeof eco.artd.FirebaseProfilingAide)|(!engineering.type.IProfilingAide|typeof engineering.type.ProfilingAide)|!eco.artd.IFirebaseProfilingAideHyperslice)} Implementations
 * @return {typeof eco.artd.FirebaseProfilingAide}
 * @nosideeffects
 */
eco.artd.AbstractFirebaseProfilingAide.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof eco.artd.AbstractFirebaseProfilingAide}
 */
eco.artd.AbstractFirebaseProfilingAide.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof eco.artd.FirebaseProfilingAide}
 */
eco.artd.AbstractFirebaseProfilingAide.__extend = function(...Extensions) {}
/**
 * @param {...((!eco.artd.IFirebaseProfilingAide|typeof eco.artd.FirebaseProfilingAide)|(!engineering.type.IProfilingAide|typeof engineering.type.ProfilingAide)|!eco.artd.IFirebaseProfilingAideHyperslice)} Implementations
 * @return {typeof eco.artd.FirebaseProfilingAide}
 */
eco.artd.AbstractFirebaseProfilingAide.continues = function(...Implementations) {}
/**
 * @param {...((!eco.artd.IFirebaseProfilingAide|typeof eco.artd.FirebaseProfilingAide)|(!engineering.type.IProfilingAide|typeof engineering.type.ProfilingAide)|!eco.artd.IFirebaseProfilingAideHyperslice)} Implementations
 * @return {typeof eco.artd.FirebaseProfilingAide}
 */
eco.artd.AbstractFirebaseProfilingAide.__trait = function(...Implementations) {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.FirebaseProfilingAideConstructor no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/** @typedef {function(new: eco.artd.IFirebaseProfilingAide, ...!eco.artd.IFirebaseProfilingAide.Initialese)} */
eco.artd.FirebaseProfilingAideConstructor

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseProfilingAideHyperslice no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/** @interface */
eco.artd.IFirebaseProfilingAideHyperslice = function() {}
/** @type {(!eco.artd.IFirebaseProfilingAide._reportGetPageDataFromFirestoreProfile|!engineering.type.RecursiveArray<!eco.artd.IFirebaseProfilingAide._reportGetPageDataFromFirestoreProfile>)} */
eco.artd.IFirebaseProfilingAideHyperslice.prototype.reportGetPageDataFromFirestoreProfile
/** @type {(!eco.artd.IFirebaseProfilingAide._reportDownloadAndEvalPageDataProfile|!engineering.type.RecursiveArray<!eco.artd.IFirebaseProfilingAide._reportDownloadAndEvalPageDataProfile>)} */
eco.artd.IFirebaseProfilingAideHyperslice.prototype.reportDownloadAndEvalPageDataProfile
/** @type {(!eco.artd.IFirebaseProfilingAide._reportEvalBodyProfile|!engineering.type.RecursiveArray<!eco.artd.IFirebaseProfilingAide._reportEvalBodyProfile>)} */
eco.artd.IFirebaseProfilingAideHyperslice.prototype.reportEvalBodyProfile
/** @type {(!eco.artd.IFirebaseProfilingAide._reportFetchFunctionsFileProfile|!engineering.type.RecursiveArray<!eco.artd.IFirebaseProfilingAide._reportFetchFunctionsFileProfile>)} */
eco.artd.IFirebaseProfilingAideHyperslice.prototype.reportFetchFunctionsFileProfile
/** @type {(!eco.artd.IFirebaseProfilingAide._reportDownloadDepsProfile|!engineering.type.RecursiveArray<!eco.artd.IFirebaseProfilingAide._reportDownloadDepsProfile>)} */
eco.artd.IFirebaseProfilingAideHyperslice.prototype.reportDownloadDepsProfile
/** @type {(!eco.artd.IFirebaseProfilingAide._reportDownloadMethodProfile|!engineering.type.RecursiveArray<!eco.artd.IFirebaseProfilingAide._reportDownloadMethodProfile>)} */
eco.artd.IFirebaseProfilingAideHyperslice.prototype.reportDownloadMethodProfile
/** @type {(!eco.artd.IFirebaseProfilingAide._reportDownloadMethodDirectlyProfile|!engineering.type.RecursiveArray<!eco.artd.IFirebaseProfilingAide._reportDownloadMethodDirectlyProfile>)} */
eco.artd.IFirebaseProfilingAideHyperslice.prototype.reportDownloadMethodDirectlyProfile
/** @type {(!eco.artd.IFirebaseProfilingAide._reportDownloadPageDataProfile|!engineering.type.RecursiveArray<!eco.artd.IFirebaseProfilingAide._reportDownloadPageDataProfile>)} */
eco.artd.IFirebaseProfilingAideHyperslice.prototype.reportDownloadPageDataProfile
/** @type {(!eco.artd.IFirebaseProfilingAide._reportLoadRequirePageDataProfile|!engineering.type.RecursiveArray<!eco.artd.IFirebaseProfilingAide._reportLoadRequirePageDataProfile>)} */
eco.artd.IFirebaseProfilingAideHyperslice.prototype.reportLoadRequirePageDataProfile
/** @type {(!eco.artd.IFirebaseProfilingAide._reportLoadRequireProfile|!engineering.type.RecursiveArray<!eco.artd.IFirebaseProfilingAide._reportLoadRequireProfile>)} */
eco.artd.IFirebaseProfilingAideHyperslice.prototype.reportLoadRequireProfile

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.FirebaseProfilingAideHyperslice no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @constructor
 * @implements {eco.artd.IFirebaseProfilingAideHyperslice}
 */
eco.artd.FirebaseProfilingAideHyperslice = function() {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseProfilingAideBindingHyperslice no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @interface
 * @template THIS
 */
eco.artd.IFirebaseProfilingAideBindingHyperslice = function() {}
/** @type {(!eco.artd.IFirebaseProfilingAide.__reportGetPageDataFromFirestoreProfile<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseProfilingAide.__reportGetPageDataFromFirestoreProfile<THIS>>)} */
eco.artd.IFirebaseProfilingAideBindingHyperslice.prototype.reportGetPageDataFromFirestoreProfile
/** @type {(!eco.artd.IFirebaseProfilingAide.__reportDownloadAndEvalPageDataProfile<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseProfilingAide.__reportDownloadAndEvalPageDataProfile<THIS>>)} */
eco.artd.IFirebaseProfilingAideBindingHyperslice.prototype.reportDownloadAndEvalPageDataProfile
/** @type {(!eco.artd.IFirebaseProfilingAide.__reportEvalBodyProfile<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseProfilingAide.__reportEvalBodyProfile<THIS>>)} */
eco.artd.IFirebaseProfilingAideBindingHyperslice.prototype.reportEvalBodyProfile
/** @type {(!eco.artd.IFirebaseProfilingAide.__reportFetchFunctionsFileProfile<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseProfilingAide.__reportFetchFunctionsFileProfile<THIS>>)} */
eco.artd.IFirebaseProfilingAideBindingHyperslice.prototype.reportFetchFunctionsFileProfile
/** @type {(!eco.artd.IFirebaseProfilingAide.__reportDownloadDepsProfile<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseProfilingAide.__reportDownloadDepsProfile<THIS>>)} */
eco.artd.IFirebaseProfilingAideBindingHyperslice.prototype.reportDownloadDepsProfile
/** @type {(!eco.artd.IFirebaseProfilingAide.__reportDownloadMethodProfile<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseProfilingAide.__reportDownloadMethodProfile<THIS>>)} */
eco.artd.IFirebaseProfilingAideBindingHyperslice.prototype.reportDownloadMethodProfile
/** @type {(!eco.artd.IFirebaseProfilingAide.__reportDownloadMethodDirectlyProfile<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseProfilingAide.__reportDownloadMethodDirectlyProfile<THIS>>)} */
eco.artd.IFirebaseProfilingAideBindingHyperslice.prototype.reportDownloadMethodDirectlyProfile
/** @type {(!eco.artd.IFirebaseProfilingAide.__reportDownloadPageDataProfile<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseProfilingAide.__reportDownloadPageDataProfile<THIS>>)} */
eco.artd.IFirebaseProfilingAideBindingHyperslice.prototype.reportDownloadPageDataProfile
/** @type {(!eco.artd.IFirebaseProfilingAide.__reportLoadRequirePageDataProfile<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseProfilingAide.__reportLoadRequirePageDataProfile<THIS>>)} */
eco.artd.IFirebaseProfilingAideBindingHyperslice.prototype.reportLoadRequirePageDataProfile
/** @type {(!eco.artd.IFirebaseProfilingAide.__reportLoadRequireProfile<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseProfilingAide.__reportLoadRequireProfile<THIS>>)} */
eco.artd.IFirebaseProfilingAideBindingHyperslice.prototype.reportLoadRequireProfile

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.FirebaseProfilingAideBindingHyperslice no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @constructor
 * @implements {eco.artd.IFirebaseProfilingAideBindingHyperslice<THIS>}
 * @template THIS
 */
eco.artd.FirebaseProfilingAideBindingHyperslice = function() {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.RecordIFirebaseProfilingAide no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/** @typedef {{ reportGetPageDataFromFirestoreProfile: eco.artd.IFirebaseProfilingAide.reportGetPageDataFromFirestoreProfile, reportDownloadAndEvalPageDataProfile: eco.artd.IFirebaseProfilingAide.reportDownloadAndEvalPageDataProfile, reportEvalBodyProfile: eco.artd.IFirebaseProfilingAide.reportEvalBodyProfile, reportFetchFunctionsFileProfile: eco.artd.IFirebaseProfilingAide.reportFetchFunctionsFileProfile, reportDownloadDepsProfile: eco.artd.IFirebaseProfilingAide.reportDownloadDepsProfile, reportDownloadMethodProfile: eco.artd.IFirebaseProfilingAide.reportDownloadMethodProfile, reportDownloadMethodDirectlyProfile: eco.artd.IFirebaseProfilingAide.reportDownloadMethodDirectlyProfile, reportDownloadPageDataProfile: eco.artd.IFirebaseProfilingAide.reportDownloadPageDataProfile, reportLoadRequirePageDataProfile: eco.artd.IFirebaseProfilingAide.reportLoadRequirePageDataProfile, reportLoadRequireProfile: eco.artd.IFirebaseProfilingAide.reportLoadRequireProfile }} */
eco.artd.RecordIFirebaseProfilingAide

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.BoundIFirebaseProfilingAide no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.RecordIFirebaseProfilingAide}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {eco.artd.IFirebaseProfilingAideCaster}
 * @extends {engineering.type.BoundIProfilingAide}
 */
eco.artd.BoundIFirebaseProfilingAide = function() {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.BoundFirebaseProfilingAide no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.BoundIFirebaseProfilingAide}
 * @extends {engineering.type.BoundIInitialiser}
 */
eco.artd.BoundFirebaseProfilingAide = function() {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseProfilingAide.reportGetPageDataFromFirestoreProfile no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {number} time
 * @param {eco.artd.IFirebase.GetPageDataFromFirestoreNArgs} args
 * @param {!Function} proc
 * @param {?eco.artd.IFirebase.PageData} res
 * @return {(undefined|!Promise<undefined>)}
 */
$$eco.artd.IFirebaseProfilingAide.__reportGetPageDataFromFirestoreProfile = function(time, args, proc, res) {}
/** @typedef {function(number, eco.artd.IFirebase.GetPageDataFromFirestoreNArgs, !Function, ?eco.artd.IFirebase.PageData): (undefined|!Promise<undefined>)} */
eco.artd.IFirebaseProfilingAide.reportGetPageDataFromFirestoreProfile
/** @typedef {function(this: eco.artd.IFirebaseProfilingAide, number, eco.artd.IFirebase.GetPageDataFromFirestoreNArgs, !Function, ?eco.artd.IFirebase.PageData): (undefined|!Promise<undefined>)} */
eco.artd.IFirebaseProfilingAide._reportGetPageDataFromFirestoreProfile
/** @typedef {typeof $$eco.artd.IFirebaseProfilingAide.__reportGetPageDataFromFirestoreProfile} */
eco.artd.IFirebaseProfilingAide.__reportGetPageDataFromFirestoreProfile

// nss:eco.artd.IFirebaseProfilingAide,$$eco.artd.IFirebaseProfilingAide,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseProfilingAide.reportDownloadAndEvalPageDataProfile no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {number} time
 * @param {eco.artd.IFirebase.DownloadAndEvalPageDataNArgs} args
 * @param {!Function} proc
 * @param {*} res
 * @return {(undefined|!Promise<undefined>)}
 */
$$eco.artd.IFirebaseProfilingAide.__reportDownloadAndEvalPageDataProfile = function(time, args, proc, res) {}
/** @typedef {function(number, eco.artd.IFirebase.DownloadAndEvalPageDataNArgs, !Function, *): (undefined|!Promise<undefined>)} */
eco.artd.IFirebaseProfilingAide.reportDownloadAndEvalPageDataProfile
/** @typedef {function(this: eco.artd.IFirebaseProfilingAide, number, eco.artd.IFirebase.DownloadAndEvalPageDataNArgs, !Function, *): (undefined|!Promise<undefined>)} */
eco.artd.IFirebaseProfilingAide._reportDownloadAndEvalPageDataProfile
/** @typedef {typeof $$eco.artd.IFirebaseProfilingAide.__reportDownloadAndEvalPageDataProfile} */
eco.artd.IFirebaseProfilingAide.__reportDownloadAndEvalPageDataProfile

// nss:eco.artd.IFirebaseProfilingAide,$$eco.artd.IFirebaseProfilingAide,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseProfilingAide.reportEvalBodyProfile no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {number} time
 * @param {eco.artd.IFirebase.EvalBodyNArgs} args
 * @param {!Function} proc
 * @param {function(!http.IncomingMessage, !http.ServerResponse): (void|Promise<void>)} res
 * @return {(undefined|!Promise<undefined>)}
 */
$$eco.artd.IFirebaseProfilingAide.__reportEvalBodyProfile = function(time, args, proc, res) {}
/** @typedef {function(number, eco.artd.IFirebase.EvalBodyNArgs, !Function, function(!http.IncomingMessage, !http.ServerResponse): (void|Promise<void>)): (undefined|!Promise<undefined>)} */
eco.artd.IFirebaseProfilingAide.reportEvalBodyProfile
/** @typedef {function(this: eco.artd.IFirebaseProfilingAide, number, eco.artd.IFirebase.EvalBodyNArgs, !Function, function(!http.IncomingMessage, !http.ServerResponse): (void|Promise<void>)): (undefined|!Promise<undefined>)} */
eco.artd.IFirebaseProfilingAide._reportEvalBodyProfile
/** @typedef {typeof $$eco.artd.IFirebaseProfilingAide.__reportEvalBodyProfile} */
eco.artd.IFirebaseProfilingAide.__reportEvalBodyProfile

// nss:eco.artd.IFirebaseProfilingAide,$$eco.artd.IFirebaseProfilingAide,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseProfilingAide.reportFetchFunctionsFileProfile no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {number} time
 * @param {eco.artd.IFirebase.FetchFunctionsFileNArgs} args
 * @param {!Function} proc
 * @param {eco.artd.IFirebase.fetchFunctionsFile.Return} res
 * @return {(undefined|!Promise<undefined>)}
 */
$$eco.artd.IFirebaseProfilingAide.__reportFetchFunctionsFileProfile = function(time, args, proc, res) {}
/** @typedef {function(number, eco.artd.IFirebase.FetchFunctionsFileNArgs, !Function, eco.artd.IFirebase.fetchFunctionsFile.Return): (undefined|!Promise<undefined>)} */
eco.artd.IFirebaseProfilingAide.reportFetchFunctionsFileProfile
/** @typedef {function(this: eco.artd.IFirebaseProfilingAide, number, eco.artd.IFirebase.FetchFunctionsFileNArgs, !Function, eco.artd.IFirebase.fetchFunctionsFile.Return): (undefined|!Promise<undefined>)} */
eco.artd.IFirebaseProfilingAide._reportFetchFunctionsFileProfile
/** @typedef {typeof $$eco.artd.IFirebaseProfilingAide.__reportFetchFunctionsFileProfile} */
eco.artd.IFirebaseProfilingAide.__reportFetchFunctionsFileProfile

// nss:eco.artd.IFirebaseProfilingAide,$$eco.artd.IFirebaseProfilingAide,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseProfilingAide.reportDownloadDepsProfile no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {number} time
 * @param {eco.artd.IFirebase.DownloadDepsNArgs} args
 * @param {!Function} proc
 * @param {*} res
 * @return {(undefined|!Promise<undefined>)}
 */
$$eco.artd.IFirebaseProfilingAide.__reportDownloadDepsProfile = function(time, args, proc, res) {}
/** @typedef {function(number, eco.artd.IFirebase.DownloadDepsNArgs, !Function, *): (undefined|!Promise<undefined>)} */
eco.artd.IFirebaseProfilingAide.reportDownloadDepsProfile
/** @typedef {function(this: eco.artd.IFirebaseProfilingAide, number, eco.artd.IFirebase.DownloadDepsNArgs, !Function, *): (undefined|!Promise<undefined>)} */
eco.artd.IFirebaseProfilingAide._reportDownloadDepsProfile
/** @typedef {typeof $$eco.artd.IFirebaseProfilingAide.__reportDownloadDepsProfile} */
eco.artd.IFirebaseProfilingAide.__reportDownloadDepsProfile

// nss:eco.artd.IFirebaseProfilingAide,$$eco.artd.IFirebaseProfilingAide,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseProfilingAide.reportDownloadMethodProfile no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {number} time
 * @param {eco.artd.IFirebase.DownloadMethodNArgs} args
 * @param {!Function} proc
 * @param {string} res
 * @return {(undefined|!Promise<undefined>)}
 */
$$eco.artd.IFirebaseProfilingAide.__reportDownloadMethodProfile = function(time, args, proc, res) {}
/** @typedef {function(number, eco.artd.IFirebase.DownloadMethodNArgs, !Function, string): (undefined|!Promise<undefined>)} */
eco.artd.IFirebaseProfilingAide.reportDownloadMethodProfile
/** @typedef {function(this: eco.artd.IFirebaseProfilingAide, number, eco.artd.IFirebase.DownloadMethodNArgs, !Function, string): (undefined|!Promise<undefined>)} */
eco.artd.IFirebaseProfilingAide._reportDownloadMethodProfile
/** @typedef {typeof $$eco.artd.IFirebaseProfilingAide.__reportDownloadMethodProfile} */
eco.artd.IFirebaseProfilingAide.__reportDownloadMethodProfile

// nss:eco.artd.IFirebaseProfilingAide,$$eco.artd.IFirebaseProfilingAide,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseProfilingAide.reportDownloadMethodDirectlyProfile no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {number} time
 * @param {eco.artd.IFirebase.DownloadMethodDirectlyNArgs} args
 * @param {!Function} proc
 * @param {eco.artd.IFirebase.downloadMethodDirectly.Return} res
 * @return {(undefined|!Promise<undefined>)}
 */
$$eco.artd.IFirebaseProfilingAide.__reportDownloadMethodDirectlyProfile = function(time, args, proc, res) {}
/** @typedef {function(number, eco.artd.IFirebase.DownloadMethodDirectlyNArgs, !Function, eco.artd.IFirebase.downloadMethodDirectly.Return): (undefined|!Promise<undefined>)} */
eco.artd.IFirebaseProfilingAide.reportDownloadMethodDirectlyProfile
/** @typedef {function(this: eco.artd.IFirebaseProfilingAide, number, eco.artd.IFirebase.DownloadMethodDirectlyNArgs, !Function, eco.artd.IFirebase.downloadMethodDirectly.Return): (undefined|!Promise<undefined>)} */
eco.artd.IFirebaseProfilingAide._reportDownloadMethodDirectlyProfile
/** @typedef {typeof $$eco.artd.IFirebaseProfilingAide.__reportDownloadMethodDirectlyProfile} */
eco.artd.IFirebaseProfilingAide.__reportDownloadMethodDirectlyProfile

// nss:eco.artd.IFirebaseProfilingAide,$$eco.artd.IFirebaseProfilingAide,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseProfilingAide.reportDownloadPageDataProfile no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {number} time
 * @param {eco.artd.IFirebase.DownloadPageDataNArgs} args
 * @param {!Function} proc
 * @param {string} res
 * @return {(undefined|!Promise<undefined>)}
 */
$$eco.artd.IFirebaseProfilingAide.__reportDownloadPageDataProfile = function(time, args, proc, res) {}
/** @typedef {function(number, eco.artd.IFirebase.DownloadPageDataNArgs, !Function, string): (undefined|!Promise<undefined>)} */
eco.artd.IFirebaseProfilingAide.reportDownloadPageDataProfile
/** @typedef {function(this: eco.artd.IFirebaseProfilingAide, number, eco.artd.IFirebase.DownloadPageDataNArgs, !Function, string): (undefined|!Promise<undefined>)} */
eco.artd.IFirebaseProfilingAide._reportDownloadPageDataProfile
/** @typedef {typeof $$eco.artd.IFirebaseProfilingAide.__reportDownloadPageDataProfile} */
eco.artd.IFirebaseProfilingAide.__reportDownloadPageDataProfile

// nss:eco.artd.IFirebaseProfilingAide,$$eco.artd.IFirebaseProfilingAide,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseProfilingAide.reportLoadRequirePageDataProfile no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {number} time
 * @param {eco.artd.IFirebase.LoadRequirePageDataNArgs} args
 * @param {!Function} proc
 * @param {!eco.artd.IFirebase.PageData} res
 * @return {(undefined|!Promise<undefined>)}
 */
$$eco.artd.IFirebaseProfilingAide.__reportLoadRequirePageDataProfile = function(time, args, proc, res) {}
/** @typedef {function(number, eco.artd.IFirebase.LoadRequirePageDataNArgs, !Function, !eco.artd.IFirebase.PageData): (undefined|!Promise<undefined>)} */
eco.artd.IFirebaseProfilingAide.reportLoadRequirePageDataProfile
/** @typedef {function(this: eco.artd.IFirebaseProfilingAide, number, eco.artd.IFirebase.LoadRequirePageDataNArgs, !Function, !eco.artd.IFirebase.PageData): (undefined|!Promise<undefined>)} */
eco.artd.IFirebaseProfilingAide._reportLoadRequirePageDataProfile
/** @typedef {typeof $$eco.artd.IFirebaseProfilingAide.__reportLoadRequirePageDataProfile} */
eco.artd.IFirebaseProfilingAide.__reportLoadRequirePageDataProfile

// nss:eco.artd.IFirebaseProfilingAide,$$eco.artd.IFirebaseProfilingAide,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseProfilingAide.reportLoadRequireProfile no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {number} time
 * @param {eco.artd.IFirebase.LoadRequireNArgs} args
 * @param {!Function} proc
 * @param {*} res
 * @return {(undefined|!Promise<undefined>)}
 */
$$eco.artd.IFirebaseProfilingAide.__reportLoadRequireProfile = function(time, args, proc, res) {}
/** @typedef {function(number, eco.artd.IFirebase.LoadRequireNArgs, !Function, *): (undefined|!Promise<undefined>)} */
eco.artd.IFirebaseProfilingAide.reportLoadRequireProfile
/** @typedef {function(this: eco.artd.IFirebaseProfilingAide, number, eco.artd.IFirebase.LoadRequireNArgs, !Function, *): (undefined|!Promise<undefined>)} */
eco.artd.IFirebaseProfilingAide._reportLoadRequireProfile
/** @typedef {typeof $$eco.artd.IFirebaseProfilingAide.__reportLoadRequireProfile} */
eco.artd.IFirebaseProfilingAide.__reportLoadRequireProfile

// nss:eco.artd.IFirebaseProfilingAide,$$eco.artd.IFirebaseProfilingAide,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseLoggingAspects.Initialese no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/** @record */
eco.artd.IFirebaseLoggingAspects.Initialese = function() {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseLoggingAspectsCaster no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/** @interface */
eco.artd.IFirebaseLoggingAspectsCaster
/** @type {!eco.artd.BoundIFirebase} */
eco.artd.IFirebaseLoggingAspectsCaster.prototype.asIFirebase

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseLoggingAspects no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {eco.artd.IFirebaseLoggingAspectsCaster}
 * @extends {eco.artd.BFirebaseAspects<!eco.artd.IFirebaseLoggingAspects>}
 */
eco.artd.IFirebaseLoggingAspects = function() {}
/** @param {...!eco.artd.IFirebaseLoggingAspects.Initialese} init */
eco.artd.IFirebaseLoggingAspects.prototype.constructor = function(...init) {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.FirebaseLoggingAspects no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @constructor
 * @param {...!eco.artd.IFirebaseLoggingAspects.Initialese} init
 * @implements {eco.artd.IFirebaseLoggingAspects}
 * @implements {engineering.type.IInitialiser<!eco.artd.IFirebaseLoggingAspects.Initialese>}
 */
eco.artd.FirebaseLoggingAspects = function(...init) {}
/** @param {...!eco.artd.IFirebaseLoggingAspects.Initialese} init */
eco.artd.FirebaseLoggingAspects.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof eco.artd.FirebaseLoggingAspects}
 */
eco.artd.FirebaseLoggingAspects.__extend = function(...Extensions) {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.AbstractFirebaseLoggingAspects no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @constructor
 * @extends {eco.artd.FirebaseLoggingAspects}
 */
eco.artd.AbstractFirebaseLoggingAspects = function() {}
/**
 * @param {...((!eco.artd.IFirebaseLoggingAspects|typeof eco.artd.FirebaseLoggingAspects)|(!eco.artd.BFirebaseAspects|typeof eco.artd.BFirebaseAspects))} Implementations
 * @return {typeof eco.artd.FirebaseLoggingAspects}
 * @nosideeffects
 */
eco.artd.AbstractFirebaseLoggingAspects.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof eco.artd.AbstractFirebaseLoggingAspects}
 */
eco.artd.AbstractFirebaseLoggingAspects.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof eco.artd.FirebaseLoggingAspects}
 */
eco.artd.AbstractFirebaseLoggingAspects.__extend = function(...Extensions) {}
/**
 * @param {...((!eco.artd.IFirebaseLoggingAspects|typeof eco.artd.FirebaseLoggingAspects)|(!eco.artd.BFirebaseAspects|typeof eco.artd.BFirebaseAspects))} Implementations
 * @return {typeof eco.artd.FirebaseLoggingAspects}
 */
eco.artd.AbstractFirebaseLoggingAspects.continues = function(...Implementations) {}
/**
 * @param {...((!eco.artd.IFirebaseLoggingAspects|typeof eco.artd.FirebaseLoggingAspects)|(!eco.artd.BFirebaseAspects|typeof eco.artd.BFirebaseAspects))} Implementations
 * @return {typeof eco.artd.FirebaseLoggingAspects}
 */
eco.artd.AbstractFirebaseLoggingAspects.__trait = function(...Implementations) {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.FirebaseLoggingAspectsConstructor no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/** @typedef {function(new: eco.artd.IFirebaseLoggingAspects, ...!eco.artd.IFirebaseLoggingAspects.Initialese)} */
eco.artd.FirebaseLoggingAspectsConstructor

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseConditioningAspects.Initialese no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/** @record */
eco.artd.IFirebaseConditioningAspects.Initialese = function() {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseConditioningAspectsCaster no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/** @interface */
eco.artd.IFirebaseConditioningAspectsCaster
/** @type {!eco.artd.BoundIFirebase} */
eco.artd.IFirebaseConditioningAspectsCaster.prototype.asIFirebase

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebaseConditioningAspects no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {eco.artd.IFirebaseConditioningAspectsCaster}
 * @extends {eco.artd.BFirebaseAspects<!eco.artd.IFirebaseConditioningAspects>}
 */
eco.artd.IFirebaseConditioningAspects = function() {}
/** @param {...!eco.artd.IFirebaseConditioningAspects.Initialese} init */
eco.artd.IFirebaseConditioningAspects.prototype.constructor = function(...init) {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.FirebaseConditioningAspects no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @constructor
 * @param {...!eco.artd.IFirebaseConditioningAspects.Initialese} init
 * @implements {eco.artd.IFirebaseConditioningAspects}
 * @implements {engineering.type.IInitialiser<!eco.artd.IFirebaseConditioningAspects.Initialese>}
 */
eco.artd.FirebaseConditioningAspects = function(...init) {}
/** @param {...!eco.artd.IFirebaseConditioningAspects.Initialese} init */
eco.artd.FirebaseConditioningAspects.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof eco.artd.FirebaseConditioningAspects}
 */
eco.artd.FirebaseConditioningAspects.__extend = function(...Extensions) {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.AbstractFirebaseConditioningAspects no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @constructor
 * @extends {eco.artd.FirebaseConditioningAspects}
 */
eco.artd.AbstractFirebaseConditioningAspects = function() {}
/**
 * @param {...((!eco.artd.IFirebaseConditioningAspects|typeof eco.artd.FirebaseConditioningAspects)|(!eco.artd.BFirebaseAspects|typeof eco.artd.BFirebaseAspects))} Implementations
 * @return {typeof eco.artd.FirebaseConditioningAspects}
 * @nosideeffects
 */
eco.artd.AbstractFirebaseConditioningAspects.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof eco.artd.AbstractFirebaseConditioningAspects}
 */
eco.artd.AbstractFirebaseConditioningAspects.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof eco.artd.FirebaseConditioningAspects}
 */
eco.artd.AbstractFirebaseConditioningAspects.__extend = function(...Extensions) {}
/**
 * @param {...((!eco.artd.IFirebaseConditioningAspects|typeof eco.artd.FirebaseConditioningAspects)|(!eco.artd.BFirebaseAspects|typeof eco.artd.BFirebaseAspects))} Implementations
 * @return {typeof eco.artd.FirebaseConditioningAspects}
 */
eco.artd.AbstractFirebaseConditioningAspects.continues = function(...Implementations) {}
/**
 * @param {...((!eco.artd.IFirebaseConditioningAspects|typeof eco.artd.FirebaseConditioningAspects)|(!eco.artd.BFirebaseAspects|typeof eco.artd.BFirebaseAspects))} Implementations
 * @return {typeof eco.artd.FirebaseConditioningAspects}
 */
eco.artd.AbstractFirebaseConditioningAspects.__trait = function(...Implementations) {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.FirebaseConditioningAspectsConstructor no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/** @typedef {function(new: eco.artd.IFirebaseConditioningAspects, ...!eco.artd.IFirebaseConditioningAspects.Initialese)} */
eco.artd.FirebaseConditioningAspectsConstructor

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IHyperFirebase.Initialese no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebase.Initialese}
 */
eco.artd.IHyperFirebase.Initialese = function() {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IHyperFirebaseCaster no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/** @interface */
eco.artd.IHyperFirebaseCaster
/** @type {!eco.artd.BoundIHyperFirebase} */
eco.artd.IHyperFirebaseCaster.prototype.asIHyperFirebase
/** @type {!eco.artd.BoundHyperFirebase} */
eco.artd.IHyperFirebaseCaster.prototype.superHyperFirebase

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IHyperFirebase no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {eco.artd.IHyperFirebaseCaster}
 * @extends {eco.artd.IFirebase}
 */
eco.artd.IHyperFirebase = function() {}
/** @param {...!eco.artd.IHyperFirebase.Initialese} init */
eco.artd.IHyperFirebase.prototype.constructor = function(...init) {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.HyperFirebase no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @constructor
 * @param {...!eco.artd.IHyperFirebase.Initialese} init
 * @implements {eco.artd.IHyperFirebase}
 * @implements {engineering.type.IInitialiser<!eco.artd.IHyperFirebase.Initialese>}
 */
eco.artd.HyperFirebase = function(...init) {}
/** @param {...!eco.artd.IHyperFirebase.Initialese} init */
eco.artd.HyperFirebase.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof eco.artd.HyperFirebase}
 */
eco.artd.HyperFirebase.__extend = function(...Extensions) {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.AbstractHyperFirebase no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @constructor
 * @extends {eco.artd.HyperFirebase}
 */
eco.artd.AbstractHyperFirebase = function() {}
/**
 * @param {...((!eco.artd.IHyperFirebase|typeof eco.artd.HyperFirebase)|(!eco.artd.IFirebase|typeof eco.artd.Firebase))} Implementations
 * @return {typeof eco.artd.HyperFirebase}
 * @nosideeffects
 */
eco.artd.AbstractHyperFirebase.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof eco.artd.AbstractHyperFirebase}
 */
eco.artd.AbstractHyperFirebase.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof eco.artd.HyperFirebase}
 */
eco.artd.AbstractHyperFirebase.__extend = function(...Extensions) {}
/**
 * @param {...((!eco.artd.IHyperFirebase|typeof eco.artd.HyperFirebase)|(!eco.artd.IFirebase|typeof eco.artd.Firebase))} Implementations
 * @return {typeof eco.artd.HyperFirebase}
 */
eco.artd.AbstractHyperFirebase.continues = function(...Implementations) {}
/**
 * @param {...((!eco.artd.IHyperFirebase|typeof eco.artd.HyperFirebase)|(!eco.artd.IFirebase|typeof eco.artd.Firebase))} Implementations
 * @return {typeof eco.artd.HyperFirebase}
 */
eco.artd.AbstractHyperFirebase.__trait = function(...Implementations) {}
/**
 * @param {...(!eco.artd.IFirebaseAspects|!Array<!eco.artd.IFirebaseAspects>|function(new: eco.artd.IFirebaseAspects)|!Function|!Array<!Function>|undefined|null)} aides
 * @return {typeof eco.artd.AbstractHyperFirebase}
 * @nosideeffects
 */
eco.artd.AbstractHyperFirebase.consults = function(...aides) {}
/**
 * @param {...(!Function|!Array<!Function>|undefined|null)} hypers
 * @return {typeof eco.artd.AbstractHyperFirebase}
 * @nosideeffects
 */
eco.artd.AbstractHyperFirebase.extends = function(...hypers) {}
/**
 * @param {...(!Function|!Array<!Function>|undefined|null)} aspectsInstallers
 * @return {typeof eco.artd.AbstractHyperFirebase}
 * @nosideeffects
 */
eco.artd.AbstractHyperFirebase.installs = function(...aspectsInstallers) {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.HyperFirebaseConstructor no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/** @typedef {function(new: eco.artd.IHyperFirebase, ...!eco.artd.IHyperFirebase.Initialese)} */
eco.artd.HyperFirebaseConstructor

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.RecordIHyperFirebase no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/** @typedef {typeof __$te_plain} */
eco.artd.RecordIHyperFirebase

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.BoundIHyperFirebase no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.RecordIHyperFirebase}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {eco.artd.IHyperFirebaseCaster}
 */
eco.artd.BoundIHyperFirebase = function() {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.BoundHyperFirebase no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.BoundIHyperFirebase}
 * @extends {engineering.type.BoundIInitialiser}
 */
eco.artd.BoundHyperFirebase = function() {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.FirebaseFactory.Config no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/** @record */
eco.artd.FirebaseFactory.Config = function() {}
/** @type {(!Array)|undefined} */
eco.artd.FirebaseFactory.Config.prototype.aspectsInstallers
/** @type {(!Array<eco.artd.IFirebase>)|undefined} */
eco.artd.FirebaseFactory.Config.prototype.subjects
/** @type {(!Array)|undefined} */
eco.artd.FirebaseFactory.Config.prototype.hypers
/** @type {((!Array<!eco.artd.IFirebaseAspects>|!eco.artd.IFirebaseAspects))|undefined} */
eco.artd.FirebaseFactory.Config.prototype.aspects

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.RecordIFirebase no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/** @typedef {{ function: eco.artd.IFirebase.function, evalBodyWithProfileTime: eco.artd.IFirebase.evalBodyWithProfileTime, getPageDataFromFirestore: eco.artd.IFirebase.getPageDataFromFirestore, downloadAndEvalPageData: eco.artd.IFirebase.downloadAndEvalPageData, evalBody: eco.artd.IFirebase.evalBody, logRpcInvocation: eco.artd.IFirebase.logRpcInvocation, fetchFunctionsFile: eco.artd.IFirebase.fetchFunctionsFile, downloadDeps: eco.artd.IFirebase.downloadDeps, downloadMethod: eco.artd.IFirebase.downloadMethod, downloadMethodDirectly: eco.artd.IFirebase.downloadMethodDirectly, getId: eco.artd.IFirebase.getId, patchDeps: eco.artd.IFirebase.patchDeps, downloadPageData: eco.artd.IFirebase.downloadPageData, loadRequirePageData: eco.artd.IFirebase.loadRequirePageData, loadRequire: eco.artd.IFirebase.loadRequire, updateErrorStack: eco.artd.IFirebase.updateErrorStack, validateMethod: eco.artd.IFirebase.validateMethod, validateLoadAndInvoke: eco.artd.IFirebase.validateLoadAndInvoke, validateLoadAndInvokeById: eco.artd.IFirebase.validateLoadAndInvokeById, makeInvoker: eco.artd.IFirebase.makeInvoker, loadAndInvokeById: eco.artd.IFirebase.loadAndInvokeById, loadAndInvoke: eco.artd.IFirebase.loadAndInvoke }} */
eco.artd.RecordIFirebase

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.BoundIFirebase no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.IFirebaseFields}
 * @extends {eco.artd.RecordIFirebase}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {eco.artd.IFirebaseCaster}
 */
eco.artd.BoundIFirebase = function() {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.BoundFirebase no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @record
 * @extends {eco.artd.BoundIFirebase}
 * @extends {engineering.type.BoundIInitialiser}
 */
eco.artd.BoundFirebase = function() {}

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebase.function no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {http.IncomingMessage} request
 * @param {http.ServerResponse} response
 * @param {(!Function|number)} cb
 * @param {!eco.artd.IFirebase.function.Meta} [meta]
 * @return {!Promise<void>}
 */
$$eco.artd.IFirebase.__function = function(request, response, cb, meta) {}
/** @typedef {function(http.IncomingMessage, http.ServerResponse, (!Function|number), !eco.artd.IFirebase.function.Meta=): !Promise<void>} */
eco.artd.IFirebase.function
/** @typedef {function(this: eco.artd.IFirebase, http.IncomingMessage, http.ServerResponse, (!Function|number), !eco.artd.IFirebase.function.Meta=): !Promise<void>} */
eco.artd.IFirebase._function
/** @typedef {typeof $$eco.artd.IFirebase.__function} */
eco.artd.IFirebase.__function

// nss:eco.artd.IFirebase,$$eco.artd.IFirebase,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebase.evalBodyWithProfileTime no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {string} body
 * @return {eco.artd.IFirebase.evalBodyWithProfileTime.Return}
 */
$$eco.artd.IFirebase.__evalBodyWithProfileTime = function(body) {}
/** @typedef {function(string): eco.artd.IFirebase.evalBodyWithProfileTime.Return} */
eco.artd.IFirebase.evalBodyWithProfileTime
/** @typedef {function(this: eco.artd.IFirebase, string): eco.artd.IFirebase.evalBodyWithProfileTime.Return} */
eco.artd.IFirebase._evalBodyWithProfileTime
/** @typedef {typeof $$eco.artd.IFirebase.__evalBodyWithProfileTime} */
eco.artd.IFirebase.__evalBodyWithProfileTime

// nss:eco.artd.IFirebase,$$eco.artd.IFirebase,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebase.getPageDataFromFirestore no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {number} cid
 * @return {!Promise<?eco.artd.IFirebase.PageData>}
 */
$$eco.artd.IFirebase.__getPageDataFromFirestore = function(cid) {}
/** @typedef {function(number): !Promise<?eco.artd.IFirebase.PageData>} */
eco.artd.IFirebase.getPageDataFromFirestore
/** @typedef {function(this: eco.artd.IFirebase, number): !Promise<?eco.artd.IFirebase.PageData>} */
eco.artd.IFirebase._getPageDataFromFirestore
/** @typedef {typeof $$eco.artd.IFirebase.__getPageDataFromFirestore} */
eco.artd.IFirebase.__getPageDataFromFirestore

// nss:eco.artd.IFirebase,$$eco.artd.IFirebase,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebase.downloadAndEvalPageData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {number} id
 * @return {!Promise}
 */
$$eco.artd.IFirebase.__downloadAndEvalPageData = function(id) {}
/** @typedef {function(number): !Promise} */
eco.artd.IFirebase.downloadAndEvalPageData
/** @typedef {function(this: eco.artd.IFirebase, number): !Promise} */
eco.artd.IFirebase._downloadAndEvalPageData
/** @typedef {typeof $$eco.artd.IFirebase.__downloadAndEvalPageData} */
eco.artd.IFirebase.__downloadAndEvalPageData

// nss:eco.artd.IFirebase,$$eco.artd.IFirebase,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebase.evalBody no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {string} body
 * @return {function(!http.IncomingMessage, !http.ServerResponse): (void|Promise<void>)}
 */
$$eco.artd.IFirebase.__evalBody = function(body) {}
/** @typedef {function(string): function(!http.IncomingMessage, !http.ServerResponse): (void|Promise<void>)} */
eco.artd.IFirebase.evalBody
/** @typedef {function(this: eco.artd.IFirebase, string): function(!http.IncomingMessage, !http.ServerResponse): (void|Promise<void>)} */
eco.artd.IFirebase._evalBody
/** @typedef {typeof $$eco.artd.IFirebase.__evalBody} */
eco.artd.IFirebase.__evalBody

// nss:eco.artd.IFirebase,$$eco.artd.IFirebase,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebase.logRpcInvocation no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!Date} dateTime
 * @param {eco.artd.IFirebase.Times} times
 * @param {eco.artd.IFirebase.Methods} methods
 * @param {{ error: Error, res: null, req: null }} invRes
 * @return {!Promise<void>}
 */
$$eco.artd.IFirebase.__logRpcInvocation = function(dateTime, times, methods, invRes) {}
/** @typedef {function(!Date, eco.artd.IFirebase.Times, eco.artd.IFirebase.Methods, { error: Error, res: null, req: null }): !Promise<void>} */
eco.artd.IFirebase.logRpcInvocation
/** @typedef {function(this: eco.artd.IFirebase, !Date, eco.artd.IFirebase.Times, eco.artd.IFirebase.Methods, { error: Error, res: null, req: null }): !Promise<void>} */
eco.artd.IFirebase._logRpcInvocation
/** @typedef {typeof $$eco.artd.IFirebase.__logRpcInvocation} */
eco.artd.IFirebase.__logRpcInvocation

// nss:eco.artd.IFirebase,$$eco.artd.IFirebase,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebase.fetchFunctionsFile no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {string} file
 * @return {!Promise<eco.artd.IFirebase.fetchFunctionsFile.Return>}
 */
$$eco.artd.IFirebase.__fetchFunctionsFile = function(file) {}
/** @typedef {function(string): !Promise<eco.artd.IFirebase.fetchFunctionsFile.Return>} */
eco.artd.IFirebase.fetchFunctionsFile
/** @typedef {function(this: eco.artd.IFirebase, string): !Promise<eco.artd.IFirebase.fetchFunctionsFile.Return>} */
eco.artd.IFirebase._fetchFunctionsFile
/** @typedef {typeof $$eco.artd.IFirebase.__fetchFunctionsFile} */
eco.artd.IFirebase.__fetchFunctionsFile

// nss:eco.artd.IFirebase,$$eco.artd.IFirebase,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebase.downloadDeps no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!Array<string>} deps
 * @param {string} methodName
 * @param {number} id
 * @return {!Promise}
 */
$$eco.artd.IFirebase.__downloadDeps = function(deps, methodName, id) {}
/** @typedef {function(!Array<string>, string, number): !Promise} */
eco.artd.IFirebase.downloadDeps
/** @typedef {function(this: eco.artd.IFirebase, !Array<string>, string, number): !Promise} */
eco.artd.IFirebase._downloadDeps
/** @typedef {typeof $$eco.artd.IFirebase.__downloadDeps} */
eco.artd.IFirebase.__downloadDeps

// nss:eco.artd.IFirebase,$$eco.artd.IFirebase,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebase.downloadMethod no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {number} cid
 * @param {string} methodName
 * @return {!Promise<string>}
 */
$$eco.artd.IFirebase.__downloadMethod = function(cid, methodName) {}
/** @typedef {function(number, string): !Promise<string>} */
eco.artd.IFirebase.downloadMethod
/** @typedef {function(this: eco.artd.IFirebase, number, string): !Promise<string>} */
eco.artd.IFirebase._downloadMethod
/** @typedef {typeof $$eco.artd.IFirebase.__downloadMethod} */
eco.artd.IFirebase.__downloadMethod

// nss:eco.artd.IFirebase,$$eco.artd.IFirebase,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebase.downloadMethodDirectly no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {number} cid
 * @param {string} methodId
 * @return {!Promise<eco.artd.IFirebase.downloadMethodDirectly.Return>}
 */
$$eco.artd.IFirebase.__downloadMethodDirectly = function(cid, methodId) {}
/** @typedef {function(number, string): !Promise<eco.artd.IFirebase.downloadMethodDirectly.Return>} */
eco.artd.IFirebase.downloadMethodDirectly
/** @typedef {function(this: eco.artd.IFirebase, number, string): !Promise<eco.artd.IFirebase.downloadMethodDirectly.Return>} */
eco.artd.IFirebase._downloadMethodDirectly
/** @typedef {typeof $$eco.artd.IFirebase.__downloadMethodDirectly} */
eco.artd.IFirebase.__downloadMethodDirectly

// nss:eco.artd.IFirebase,$$eco.artd.IFirebase,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebase.getId no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @return {number}
 */
$$eco.artd.IFirebase.__getId = function() {}
/** @typedef {function(): number} */
eco.artd.IFirebase.getId
/** @typedef {function(this: eco.artd.IFirebase): number} */
eco.artd.IFirebase._getId
/** @typedef {typeof $$eco.artd.IFirebase.__getId} */
eco.artd.IFirebase.__getId

// nss:eco.artd.IFirebase,$$eco.artd.IFirebase,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebase.patchDeps no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {string} body
 * @return {!Promise<eco.artd.IFirebase.patchDeps.Return>}
 */
$$eco.artd.IFirebase.__patchDeps = function(body) {}
/** @typedef {function(string): !Promise<eco.artd.IFirebase.patchDeps.Return>} */
eco.artd.IFirebase.patchDeps
/** @typedef {function(this: eco.artd.IFirebase, string): !Promise<eco.artd.IFirebase.patchDeps.Return>} */
eco.artd.IFirebase._patchDeps
/** @typedef {typeof $$eco.artd.IFirebase.__patchDeps} */
eco.artd.IFirebase.__patchDeps

// nss:eco.artd.IFirebase,$$eco.artd.IFirebase,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebase.downloadPageData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {number} cid
 * @return {!Promise<string>}
 */
$$eco.artd.IFirebase.__downloadPageData = function(cid) {}
/** @typedef {function(number): !Promise<string>} */
eco.artd.IFirebase.downloadPageData
/** @typedef {function(this: eco.artd.IFirebase, number): !Promise<string>} */
eco.artd.IFirebase._downloadPageData
/** @typedef {typeof $$eco.artd.IFirebase.__downloadPageData} */
eco.artd.IFirebase.__downloadPageData

// nss:eco.artd.IFirebase,$$eco.artd.IFirebase,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebase.loadRequirePageData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {number} cid
 * @return {!eco.artd.IFirebase.PageData}
 */
$$eco.artd.IFirebase.__loadRequirePageData = function(cid) {}
/** @typedef {function(number): !eco.artd.IFirebase.PageData} */
eco.artd.IFirebase.loadRequirePageData
/** @typedef {function(this: eco.artd.IFirebase, number): !eco.artd.IFirebase.PageData} */
eco.artd.IFirebase._loadRequirePageData
/** @typedef {typeof $$eco.artd.IFirebase.__loadRequirePageData} */
eco.artd.IFirebase.__loadRequirePageData

// nss:eco.artd.IFirebase,$$eco.artd.IFirebase,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebase.loadRequire no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {string} path
 * @param {string} dir
 */
$$eco.artd.IFirebase.__loadRequire = function(path, dir) {}
/** @typedef {function(string, string)} */
eco.artd.IFirebase.loadRequire
/** @typedef {function(this: eco.artd.IFirebase, string, string)} */
eco.artd.IFirebase._loadRequire
/** @typedef {typeof $$eco.artd.IFirebase.__loadRequire} */
eco.artd.IFirebase.__loadRequire

// nss:eco.artd.IFirebase,$$eco.artd.IFirebase,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebase.updateErrorStack no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!Error} error
 * @param {string} url
 * @return {void}
 */
$$eco.artd.IFirebase.__updateErrorStack = function(error, url) {}
/** @typedef {function(!Error, string): void} */
eco.artd.IFirebase.updateErrorStack
/** @typedef {function(this: eco.artd.IFirebase, !Error, string): void} */
eco.artd.IFirebase._updateErrorStack
/** @typedef {typeof $$eco.artd.IFirebase.__updateErrorStack} */
eco.artd.IFirebase.__updateErrorStack

// nss:eco.artd.IFirebase,$$eco.artd.IFirebase,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebase.validateMethod no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {Map<string, string>} [methodsMap]
 * @return {string}
 */
$$eco.artd.IFirebase.__validateMethod = function(methodsMap) {}
/** @typedef {function(Map<string, string>=): string} */
eco.artd.IFirebase.validateMethod
/** @typedef {function(this: eco.artd.IFirebase, Map<string, string>=): string} */
eco.artd.IFirebase._validateMethod
/** @typedef {typeof $$eco.artd.IFirebase.__validateMethod} */
eco.artd.IFirebase.__validateMethod

// nss:eco.artd.IFirebase,$$eco.artd.IFirebase,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebase.validateLoadAndInvoke no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!Object<string, eco.artd.IFirebase.PageData>} pageData
 * @param {number} id
 * @return {!Promise<void>}
 */
$$eco.artd.IFirebase.__validateLoadAndInvoke = function(pageData, id) {}
/** @typedef {function(!Object<string, eco.artd.IFirebase.PageData>, number): !Promise<void>} */
eco.artd.IFirebase.validateLoadAndInvoke
/** @typedef {function(this: eco.artd.IFirebase, !Object<string, eco.artd.IFirebase.PageData>, number): !Promise<void>} */
eco.artd.IFirebase._validateLoadAndInvoke
/** @typedef {typeof $$eco.artd.IFirebase.__validateLoadAndInvoke} */
eco.artd.IFirebase.__validateLoadAndInvoke

// nss:eco.artd.IFirebase,$$eco.artd.IFirebase,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebase.validateLoadAndInvokeById no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {number} cid
 * @return {!Promise<void>}
 */
$$eco.artd.IFirebase.__validateLoadAndInvokeById = function(cid) {}
/** @typedef {function(number): !Promise<void>} */
eco.artd.IFirebase.validateLoadAndInvokeById
/** @typedef {function(this: eco.artd.IFirebase, number): !Promise<void>} */
eco.artd.IFirebase._validateLoadAndInvokeById
/** @typedef {typeof $$eco.artd.IFirebase.__validateLoadAndInvokeById} */
eco.artd.IFirebase.__validateLoadAndInvokeById

// nss:eco.artd.IFirebase,$$eco.artd.IFirebase,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebase.makeInvoker no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {number} id
 * @return {function()}
 */
$$eco.artd.IFirebase.__makeInvoker = function(id) {}
/** @typedef {function(number): function()} */
eco.artd.IFirebase.makeInvoker
/** @typedef {function(this: eco.artd.IFirebase, number): function()} */
eco.artd.IFirebase._makeInvoker
/** @typedef {typeof $$eco.artd.IFirebase.__makeInvoker} */
eco.artd.IFirebase.__makeInvoker

// nss:eco.artd.IFirebase,$$eco.artd.IFirebase,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebase.loadAndInvokeById no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {number} cid
 * @param {string} mid
 */
$$eco.artd.IFirebase.__loadAndInvokeById = function(cid, mid) {}
/** @typedef {function(number, string)} */
eco.artd.IFirebase.loadAndInvokeById
/** @typedef {function(this: eco.artd.IFirebase, number, string)} */
eco.artd.IFirebase._loadAndInvokeById
/** @typedef {typeof $$eco.artd.IFirebase.__loadAndInvokeById} */
eco.artd.IFirebase.__loadAndInvokeById

// nss:eco.artd.IFirebase,$$eco.artd.IFirebase,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebase.loadAndInvoke no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @this {THIS}
 * @template THIS
 * @param {string} methodId
 * @param {!Object<string, eco.artd.IFirebase.PageData>} pageData
 * @param {number} id
 * @return {!Promise<void>}
 */
$$eco.artd.IFirebase.__loadAndInvoke = function(methodId, pageData, id) {}
/** @typedef {function(string, !Object<string, eco.artd.IFirebase.PageData>, number): !Promise<void>} */
eco.artd.IFirebase.loadAndInvoke
/** @typedef {function(this: eco.artd.IFirebase, string, !Object<string, eco.artd.IFirebase.PageData>, number): !Promise<void>} */
eco.artd.IFirebase._loadAndInvoke
/** @typedef {typeof $$eco.artd.IFirebase.__loadAndInvoke} */
eco.artd.IFirebase.__loadAndInvoke

// nss:eco.artd.IFirebase,$$eco.artd.IFirebase,eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebase.function.Meta no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/** @record */
eco.artd.IFirebase.function.Meta = function() {}
/** @type {string|undefined} */
eco.artd.IFirebase.function.Meta.prototype.region

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebase.evalBodyWithProfileTime.Return no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/** @record */
eco.artd.IFirebase.evalBodyWithProfileTime.Return = function() {}
/** @type {number} */
eco.artd.IFirebase.evalBodyWithProfileTime.Return.prototype.evalBodyTime
/** @type {!Function} */
eco.artd.IFirebase.evalBodyWithProfileTime.Return.prototype.evalBody

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebase.fetchFunctionsFile.Return no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/** @record */
eco.artd.IFirebase.fetchFunctionsFile.Return = function() {}
/** @type {string} */
eco.artd.IFirebase.fetchFunctionsFile.Return.prototype.body
/** @type {!Date} */
eco.artd.IFirebase.fetchFunctionsFile.Return.prototype.lastModified
/** @type {!Map} */
eco.artd.IFirebase.fetchFunctionsFile.Return.prototype.headers

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebase.downloadMethodDirectly.Return no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/** @record */
eco.artd.IFirebase.downloadMethodDirectly.Return = function() {}
/** @type {string} */
eco.artd.IFirebase.downloadMethodDirectly.Return.prototype.body
/** @type {!Date} */
eco.artd.IFirebase.downloadMethodDirectly.Return.prototype.lastModified
/** @type {string} */
eco.artd.IFirebase.downloadMethodDirectly.Return.prototype.etag

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebase.patchDeps.Return no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/** @record */
eco.artd.IFirebase.patchDeps.Return = function() {}
/** @type {string} */
eco.artd.IFirebase.patchDeps.Return.prototype.patchedBody
/** @type {!Map<string, string>} */
eco.artd.IFirebase.patchDeps.Return.prototype.deps

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebase.PageData no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/** @typedef {{ methodPaths: !Map<string, string>, methodNames: !Map<string, string>, dir: string }} */
eco.artd.IFirebase.PageData

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebase.Times no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/** @typedef {{ pageDataDownloadTime: ((?number)|undefined), pageDataFirestoreTime: ((?number)|undefined), invokeTime: ((?number)|undefined), loadTime: ((?number)|undefined), region: ((?string)|undefined), downloadTime: ((?number)|undefined), downloadMethodDirectlyTime: ((?number)|undefined) }} */
eco.artd.IFirebase.Times

// nss:eco.artd
/* @typal-end */
/* @typal-type {types/design/IFirebase.xml} eco.artd.IFirebase.Methods no-factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/** @typedef {{ methodDate: ((?Date)|undefined), methodVersion: ((?string)|undefined), methodName: ((?string)|undefined), methodEtag: ((?string)|undefined), methodDeps: ((Map<string, string>)|undefined), methodId: ((?string)|undefined), methodCid: ((?number)|undefined) }} */
eco.artd.IFirebase.Methods

// nss:eco.artd
/* @typal-end */

/* @typal-type {types/api.xml} eco.artd.firebase.Config no-functions e426e0b5402727bbd85fe26b051a8029 */
/** @record */
eco.artd.firebase.Config = function() {}
/** @type {boolean|undefined} */
eco.artd.firebase.Config.prototype.shouldRun
/** @type {string|undefined} */
eco.artd.firebase.Config.prototype.text

// nss:eco.artd
/* @typal-end */