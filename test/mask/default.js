import {makeTestSuite} from '@type.engineering/web-computing'
import {EOL} from 'os'
import Context from '../context'
import {Firebase} from '../../src'

// export default
makeTestSuite('test/result/default', {
 /**
  * @param {Context} ctx
  */
 async getResults({fixture, readFile}) {
  const text = readFile(fixture`${this.input}.txt`)
  const firebase=new Firebase({symbol:text})
  return `${this.input}:${EOL}${firebase.symbol}`
 },
 context: Context,
})