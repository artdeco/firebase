import {deepEqual as equal} from '@type.engineering/web-computing'
import Context from '../context'
import {Firebase} from '../../src'

/** @type {TestSuite} */
export const Default = {
 context: Context,
 'is a function'() {
  equal(typeof Firebase, 'function')
 },
 async'creates an instance'() {
  const firebase=new Firebase({})
 },
 async'gets a link to the fixture'({fixture:fixture,readFile:readFile}) {
  const path=fixture`test.txt`
  const text=readFile(path)
  const firebase=new Firebase({
   symbol:text,
  })
  equal(firebase.symbol,text)
 },
}