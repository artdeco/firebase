/**
 * @suppress {uselessCode}
 */
export const isDemoVersion = () => { try {
 return ECO_ARTD_FIREBASE_DEMO_VERSION
} catch (err) {
 return false
}}
