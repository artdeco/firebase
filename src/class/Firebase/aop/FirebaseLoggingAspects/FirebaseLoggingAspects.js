import AbstractFirebaseLoggingAspects from '../../gen/aspects/AbstractFirebaseLoggingAspects'
import {FirebaseProc, PageData} from '../../lib/syms'

const FetchFile=Symbol('firebase-fetch-file')

/** @extends {eco.artd.FirebaseLoggingAspects} */
export default class extends AbstractFirebaseLoggingAspects.continues(
 AbstractFirebaseLoggingAspects.class.prototype=/**@type {!eco.artd.FirebaseLoggingAspects} */({
  // constructor() {},
  beforeDownloadMethod:[
   ({proc})=>proc(FirebaseProc),
   ({proc})=>proc(FirebaseProc,{downloadStart:new Date}),
  ],
  beforeFunction:[
   ({proc})=>proc(PageData),
   ({proc,args:{meta:{region}}})=>proc(PageData,{region}),
   ({proc})=>proc(FirebaseProc),
   ({proc})=>proc(FirebaseProc,{functionStart:new Date}),
  ],
  beforeLogRpcInvocation:[
   function({proc:proc}){proc(PageData)},
   function({args:{times:times},proc}) {
    const{downloadPageDataTime:d}=proc(PageData) // how to cast the proc
    if(d) times.pageDataDownloadTime=d
   },
   function({args:{times:times},proc}) {
    const{getPageDataFromFirestoreTime:f}=proc(PageData)
    if(f) times.pageDataFirestoreTime=f
   },
   function({args:{times:times},proc}) {
    const{downloadMethodDirectlyTime:f}=proc(PageData)
    if(f) times.downloadMethodDirectlyTime=f
   },
   function({args:{times:times},proc}) {
    const{region:f}=proc(PageData)
    if(f) times.region=f
   },
  ],
  beforeGetPageDataFromFirestore:[
   ({proc:proc})=>proc(PageData),
  ],
  beforeDownloadMethodDirectly:[
   ({proc:proc})=>proc(PageData),
  ],
  afterDownloadMethodDirectly:[
   function({args:{cid,methodId},proc,res:{lastModified,body,etag}}) {
    const{downloadMethodDirectlyTime:dd}=proc(PageData)
    const{asIFirebase:{logger:{info}}}=this // but also the finally logger needs to flush this data
    // (${Buffer.from(body).byteLength}B)
    if(body) info(`📦 Directly downloaded ${cid}.${methodId} from hosting in ${dd}ms`,{length:Buffer.from(body).byteLength,lastModified,etag})
    else info(`📦❗️ [not] Downloaded {${cid}} method from hosting in ${dd}ms`)
   },
  ],
  afterGetPageDataFromFirestore:[
   function({args:{cid},proc}) {
    const{getPageDataFromFirestoreTime:dd}=proc(PageData)
    const{asIFirebase:{logger:{info}}}=this // but also the finally logger needs to flush this data
    info(`Loaded {${cid}} method data from firestore ${dd}ms`)
   },
  ],
  beforeDownloadPageData:[
   ({proc})=>proc(PageData),
  ],
  afterDownloadPageData:[
   function({args:{cid:cid},proc}) {
    const{downloadPageDataTime:dd}=proc(PageData)
    const{asIFirebase:{logger:{info}}}=this // but also the finally logger needs to flush this data
    info(`Downloaded {${cid}} method data in ${dd}ms`)
   },
  ],
  beforeFetchFunctionsFile:[
   function({proc}){proc(FetchFile)},
   function({proc}){proc(FetchFile,{startTime:new Date})},
  ],
  afterFetchFunctionsFile:[
   function({args:{file}, proc}) {
    const{startTime:d}=proc(FetchFile)
    const dd=Date.now()-d.getTime()
    const{asIFirebase:{logger:{info}}}=this
    info(`Fetched function file ${file} in ${dd}ms`)
   },
  ],
  afterDownloadMethod:[
   function({args:{methodName:methodName},proc:proc,res:index}){
    const{downloadStart,downloadDepsProfile,depsCount}=proc(FirebaseProc)

    const downloadEnd=Date.now()
    const downloadTime=downloadEnd-downloadStart.getTime()

    const{asIFirebase:{logger:{info}}}=this
    // this is logging aspects
    // load the function actually
    // how to know deps?
    // with ${deps.size} deps
    info(`Downloaded ${methodName} in ${downloadTime}ms (${downloadDepsProfile}ms for ${depsCount} deps)`,{
     downloadTime:downloadTime+'ms',
     size:index.length,
     totalSize:index.length,
    })
   },
  ],
  afterEvalBody({proc}){ // if logging aspects are
   // const R=proc('eval')
   // this.asIFirebase.logger.info(R)
   // this.lastEvalTime='?'
  },
  // method:_method,
 }),
) {}