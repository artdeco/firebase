import {$processes} from '@type.engineering/type-engineer';
import {FirebaseSymbols} from '../../gen/AbstractFirebase';
import AbstractFirebaseConditioningAspects from '../../gen/aspects/AbstractFirebaseConditioningAspects'
// import _method from './methods/method'

/** @extends {eco.artd.FirebaseConditioningAspects} */
export default class extends AbstractFirebaseConditioningAspects.continues(
 AbstractFirebaseConditioningAspects.class.prototype=/**@type {!eco.artd.FirebaseConditioningAspects} */({
  [$processes]:true,
  beforeFunction:[
   function({args:{request:request,response:response},cond}) {
    cond({
     [FirebaseSymbols._req]:request,
     [FirebaseSymbols._res]:response,
    })
   },
  ],
 }),
) {}