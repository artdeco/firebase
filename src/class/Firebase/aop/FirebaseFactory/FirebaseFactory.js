import AbstractHyperFirebase from "../../gen/hyper/AbstractHyperFirebase/AbstractHyperFirebase"
import Firebase from "../../Firebase"

/** @type {eco.artd.FirebaseFactory} */
export default function FirebaseFactory({
 aspectsInstallers,aspects,subjects=[],hypers,
}) {
 const MyFirebase=AbstractHyperFirebase
  .extends(/*HyperLocal,*/hypers)
  .installs(aspectsInstallers)
  .consults(
   // FirebaseHardAspects,
   aspects,
  )
  .implements(
   Firebase,
   ...subjects,
  )
 return MyFirebase
}