import { time, newAbstract, $implementations, iu, $initialese } from '@type.engineering/type-engineer'
import '../../../../types'

/**
 * @abstract
 * @extends {eco.artd.AbstractFirebase}
 */
class _AbstractFirebase { }
/**
 *     Various firebase utils and runtime to download JS files from HTTP hosting
 * for execution in functions (helps to eliminate reploy every so often).
 * @extends {eco.artd.AbstractFirebase} ‎
 */
class AbstractFirebase extends newAbstract(
 _AbstractFirebase,16817934091,null,{
  asIFirebase:1,
  superFirebase:2,
 },false,{
  firestorePackage:{_firestorePackage:8},
  rpcDir:{_rpcDir:10},
  res:{_res:6},
  req:{_req:7},
  gCloudProject:{_gCloudProject:3},
  functionTarget:{_functionTarget:4},
  functionsEmulator:{_functionsEmulator:5},
  db:{_db:11},
  logger:{_logger:2},
 }) {}

/** @type {typeof eco.artd.AbstractFirebase} */
AbstractFirebase.class=function(){}
/** @type {typeof eco.artd.AbstractFirebase} */
function AbstractFirebaseClass(){}

export default AbstractFirebase

/** @type {eco.artd.IFirebase.Symbols} */
export const FirebaseSymbols=AbstractFirebase.Symbols
/** @type {eco.artd.IFirebase.getSymbols} */
export const getFirebaseSymbols=AbstractFirebase.getSymbols
/** @type {eco.artd.IFirebase.setSymbols} */
export const setFirebaseSymbols=AbstractFirebase.setSymbols

AbstractFirebase[$implementations]=[
 AbstractFirebaseClass.prototype=/**@type {!eco.artd.Firebase}*/({
  constructor:function defaultSymbolsConstructor(){
   const{_firestorePackage,_rpcDir,_res,_req,_gCloudProject,_functionTarget,_functionsEmulator,_db,_logger}=getFirebaseSymbols(this)
   setFirebaseSymbols(this,{
    _firestorePackage:iu(_firestorePackage,'firebase-admin/firestore'),
    _rpcDir:iu(_rpcDir,'./_rpc'),
    _res:iu(_res,null),
    _req:iu(_req,null),
    _gCloudProject:iu(_gCloudProject,''),
    _functionTarget:iu(_functionTarget,''),
    _functionsEmulator:iu(_functionsEmulator,''),
    _db:iu(_db,''),
    _logger:iu(_logger,null),
   })
  },
 }),
 /** @type {!eco.artd.Firebase} */ ({
  preinitializer() {
   const{
    'GCLOUD_PROJECT':_gCloudProject,
    'FUNCTION_TARGET':_functionTarget,
    'FUNCTIONS_EMULATOR':_functionsEmulator,
   }=process.env

   return {
    gCloudProject: _gCloudProject?_gCloudProject.split(/\s*#/)[0]:_gCloudProject,
    functionTarget: _functionTarget?_functionTarget.split(/\s*#/)[0]:_functionTarget,
    functionsEmulator: _functionsEmulator?_functionsEmulator.split(/\s*#/)[0]:_functionsEmulator,
   }
  },
 }),
 /** @type {!eco.artd.Firebase} */ ({
  [$initialese]:/**@type {!eco.artd.IFirebase.Initialese}*/({
   firestorePackage:1,
   rpcDir:1,
   res:1,
   req:1,
   gCloudProject:1,
   functionTarget:1,
   functionsEmulator:1,
   db:1,
   logger:1,
  }),
  initializer({
   firestorePackage:_firestorePackage,
   rpcDir:_rpcDir,
   res:_res,
   req:_req,
   gCloudProject:_gCloudProject,
   functionTarget:_functionTarget,
   functionsEmulator:_functionsEmulator,
   db:_db,
   logger:_logger,
  }) {
   setFirebaseSymbols(this, {
    _firestorePackage:_firestorePackage,
    _rpcDir:_rpcDir,
    _res:_res,
    _req:_req,
    _gCloudProject:_gCloudProject,
    _functionTarget:_functionTarget,
    _functionsEmulator:_functionsEmulator,
    _db:_db,
    _logger:_logger,
   })
  },
 }),
]