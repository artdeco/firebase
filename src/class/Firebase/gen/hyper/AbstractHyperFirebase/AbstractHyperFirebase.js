import FirebaseAspectsInstaller from '../../aspects-installers/FirebaseAspectsInstaller'
import { newAbstract } from '@type.engineering/type-engineer'

/**
 * @abstract
 * @extends {eco.artd.AbstractHyperFirebase}
 */
class _AbstractHyperFirebase { }
/** @extends {eco.artd.AbstractHyperFirebase} ‎ */
class AbstractHyperFirebase extends newAbstract(
 _AbstractHyperFirebase,16817934098,null,{
  asIHyperFirebase:FirebaseAspectsInstaller,
  superHyperFirebase:2,
 },false) {}

/** @type {typeof eco.artd.AbstractHyperFirebase} */
AbstractHyperFirebase.class=function(){}
/** @type {typeof eco.artd.AbstractHyperFirebase} */
function AbstractHyperFirebaseClass(){}

export default AbstractHyperFirebase