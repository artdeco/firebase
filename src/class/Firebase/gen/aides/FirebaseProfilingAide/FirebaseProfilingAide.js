import AbstractFirebaseProfilingAide from '../AbstractFirebaseProfilingAide'

/** @extends {eco.artd.FirebaseProfilingAide} */
export class FirebaseProfilingAide extends AbstractFirebaseProfilingAide.continues() {}
export default FirebaseProfilingAide