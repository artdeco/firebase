import AbstractFirebaseAspects from '../AbstractFirebaseAspects'

/** @extends {eco.artd.AbstractFirebaseProfilingAspects} ‎*/
export default class AbstractFirebaseProfilingAspects extends /**@type {typeof eco.artd.AbstractFirebaseProfilingAspects}*/(AbstractFirebaseAspects.clone()) {}

/** @type {typeof eco.artd.AbstractFirebaseProfilingAspects} */
AbstractFirebaseProfilingAspects.class=function(){}