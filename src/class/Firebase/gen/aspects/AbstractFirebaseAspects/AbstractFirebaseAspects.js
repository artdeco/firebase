import { newAspects } from '@type.engineering/type-engineer'

/**
 * @abstract
 * @extends {eco.artd.AbstractFirebaseAspects}
 */
class _AbstractFirebaseAspects { }
/**
 * The aspects of the *IFirebase*.
 * @extends {eco.artd.AbstractFirebaseAspects} ‎
 */
class AbstractFirebaseAspects extends newAspects(
 _AbstractFirebaseAspects,16817934097,null,{},false) {}

/** @type {typeof eco.artd.AbstractFirebaseAspects} */
AbstractFirebaseAspects.class=function(){}
/** @type {typeof eco.artd.AbstractFirebaseAspects} */
function AbstractFirebaseAspectsClass(){}

export default AbstractFirebaseAspects