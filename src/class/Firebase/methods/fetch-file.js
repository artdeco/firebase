import {join} from 'path'

/** @type {eco.artd.IFirebase._fetchFunctionsFile} */
export default async function _fetchFunctionsFile(file) {
 const{asIFirebase:{host:HOST}}=this

 /**
  * The path to the file to load inside public/functions/_rpc folder.
  * @param {string} path
  */
 const makeFunctionsPath=(path)=>{
  path=join('/',path)
  path=join('_rpc',path)
  let host
  if(/:\d+$/.test(HOST)) {
   // place functions in public functions in mvc-modulator actually.
   host=`http://${HOST.replace(/^localhost:/,'127.0.0.1:')}/functions/${path}`
  }else{
   host=`https://${HOST}/functions/${path}`
  }
  // info({host})
  return host
 }
 const depSrc=makeFunctionsPath(file)
 const js=await fetch(depSrc)
 let lastModified=js.headers.get('Last-Modified')
 if(lastModified) lastModified=new Date(lastModified)
 const body=await js.text()
 return{
  body:body,lastModified:lastModified,headers:js.headers,
  url:depSrc,
 }
}