## 12 March 2024

### [0.3.0](https://gitlab.com/artdeco/firebase/compare/v0.2.6...v0.3.0)

- [improvement] store `referer` header in telemetry.
- [feature] update error stacks and log them into telemetry; report list of
  dependencies for methods during errors.

## 4 March 2024

### [0.2.6](https://gitlab.com/artdeco/firebase/compare/v0.1.2...v0.2.6)

- [fix] don't use local id generation which is the same as remote.
- [improvement] better decremental ids with process-based entropy.

## 2 March 2024

### [0.2.5](https://gitlab.com/artdeco/firebase/compare/v0.1.2...v0.2.5)

- [impr] log region for invocation also.
- [improvement] log the method name and version, as well as download method by
  name from the host immediately.

## 1 March 2024

### [0.2.3](https://gitlab.com/artdeco/firebase/compare/v0.1.1...v0.2.3)

- [feature] download methods directly by their IDs.
- [feature] download the circtuit gate metadata from firestore. Now supports 3
  methods: firestore, hosting and require.

## 29 February 2024

### [0.1.1](https://gitlab.com/artdeco/firebase/compare/v0.0.0-pre...v0.1.1)

- [code] condition firebase for reqquest-response using the `function` method as
  entry; allow to download page data using hosting too.
- [Package] Publish package on the _Luddites_ registry.

### 0.0.0-pre

- Create `@artdeco/firebase` with _[`NodeTools`](https://art-deco.github.io/nodetools)_.