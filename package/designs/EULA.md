# The End-Use License Agreement For Art Deco Software

> Version: 1.1.1 (October 2022) :: Any additional Open Source licenses (if any)
> as well as licensing notes can be found in the `licenses` folder.

> Legal Queries regarding license:
>                           legal@artdeco.support

> Additional Information:    info@artdeco.support
> Issues with software:    issues@artdeco.support
> Requests for features: requests@artdeco.support

---

Please read carefully this legal agreement between you (as an individual or a
legal entity you were authorised to represent) and Art Deco Code Ltd, UK, before
downloading and installing any of the Art Deco Software, and/or purchasing a
license key that would allow you to access the range of Art Deco software
products.

By purchasing a software license key and/or downloading either trial or full
versions of packages from the public and private registries, you agree to be
bound by the terms of this EULA. The license terms shall apply to software
packages published online via official Art Deco channels: either on npmjs.com
for *Trial* and *Free* versions or on specialised registry for Paid software,
including but not limited to ludds.io and luddites.io.

In every case including limited-time trials, always-free versions and full
packages, this license is a legally binding agreement between the software
publisher and you. When trialling out or making full use of software pieces, you
agree with its terms. If you do not agree please do not purchase and/or install
Art Deco software packages.

The text of this license is subject to change at any time to improve
readability, communicability and enforceability of its terms. When this happens,
you will be notified prior to changes being made which will require your
acknowledgement and acceptance in writing or by means of electronic signature,
if you have purchased the Paid version.

## DEFINITIONS

- _(Software) Publisher_: a software development company that has researched,
developed and published its software products for the use by digital citizens,
public and private companies for the benefit of solving their problems, in
particular those that arise during development, maintenance, documentation,
testing, debugging and other activities centred around Software Engineering
methodologies and practices.
- _Art Deco_: Art Deco Code Ltd, International House, 24 Holborn Viaduct,
London, United Kingdom, EC1A 2BN. Company number 11323401.
- _Trial version_: a version provided without charge on a public registry that
has an evaluation period during which the functionality of the software can be
tested against user and organisational requirements and after which the
installation must be ceased and a decision for the purchase made. Trial versions
are assigned per user and not per machine, for example, you cannot reinstall a
trial on a new server or PC after having completed a trial period once for any
given trial product.
- _Paid version_: a full-featured distribution of a software package that can be
purchased and downloaded via a private registry and that receives updates and
customer support in accordance with this EULA. Paid Software versions might be
issued for terms of different length and under varying renewal conditions, which
will be stated on the page of the store when purchase is made.
- _Free version_: any software published without the Trial and Paid restrictions
to the benefit of Software Engineering professionals, enthusiasts and students
free-of-charge and under no obligations from the publisher except for the terms
of this license applicable to Free versions. Such versions may be limited in
certain functionality with the aim of encouraging the purchase of full versions
and thus support of healthy software development business model.
- _License key (code)_: a computer-generated code provided to you at the time of
purchase that facilitates authorisation when packages are downloaded from
private registry.
- _(Software) Package_: a piece of software distributed in electronic form by
the means of network data transfer and installable by package managers including
yarn and npm. Additionally, all documentation (in delivered README files, or
published online on source code repositories and their wikis), meta-data about
versioning (CHANGELOG), licensing (EULA/LICENSE/COPYING) and other information
required for the installation, storage and transmission of such archives.
- _Compiled Code_: source code that has undergone a number of optimisations by a
software compiler, has been made non-human readable, obfuscated and concatenated
into a single file or a number of chunks that are meant to be run within the
same variable scope, with comments removed except for license and copyright
notices.
- _Library_: an implementation of a set of application programming interfaces
(APIs) that can be linked to statically and dynamically to perform computations
in accordance with programming algorithms, and their in-IDE documentation such
as autocompletion hints, type annotations and examples.
- _IDE_: an integrated development environment that helps developers create,
test, debug and document their software, usually with GUI.
- _Open Source_: certain pieces of software released under Open Source licenses
that permit free distribution and incorporation of packages into other software,
with a condition of preserving copyright and certain licensing restrictions.
- _Team License_: A number of software licenses issued to a company for its
employees, such that every employee occupies a single developer seat.
- _Continuous Integration (CI)_: a bot machine service that executes test suites
as part of quality assurance processes, and deploys new versions of software
automatically or semi-automatically as those control mechanisms were assured.
- _Requirements Specification_: a document with a collection of all functional
and non-functional requirements that a software package is solving using its
algorithms. The specification is backed by automated testing the results of
which can be published as proof of adherence to the requirements.

## LICENSE GRANT

Art Deco hereby grants you a limited, non-transferable, non-exclusive licence to
use the software on your devices in accordance with the terms of this EULA
agreement.

In general, you are permitted to load the software packages (for example on a
PC, in browser, or server) under your control. You are responsible for ensuring
your device meets the minimum requirements of the software. To install packages
and receive updates, you will ensure that your device is connected to the
internet. The specifics of the permitted use of software will depend on its
nature, i.e. whether it is a:

a) **DEVTOOL**: you are using the software as a developer tool on at most a
single machine at any given time, for example to compile, test and document your
programs; b) **WEBLIB**: you are using a compiled library on a web server that
you run for any number of your own projects. When deploying a web server with a
WEBLIB to host your clients' projects, you will be bound by additional
sublicensor terms; c) **LIB**: you are incorporating a library into your
private, non-public accessible organisational software to solve your own
business needs, where the end product will be used by other stakeholders. Please
contact Art Deco for this license grant. d) **SOFTWARE**: you are using an Art
Deco package as part of your own software which is to be distributed to your
users. This is never permitted under the terms of this license unless for Free
software versions that were released under a second, Open Source license which
will be indicated on the Free version's documentation page.

The type of the license grant under the terms of this EULA will be clearly
stated during the time of purchase and confirmed in the email following the
purchase. The grant constitutes the "use" of the license key and cannot be
changed.

## DEVTOOLS

You can use dev tools to create websites and software products of your own.
Devtools are not to be embedded into other software products and sold or
distributed for free as part of your computer systems. The devtools are to be
run as executables only and cannot be bundled with your software applications
which then call the executables from their own runtime (known as forking, or a
child process).

The entire purpose of devtools is to help you create programs, improve
productivity and enhance the developer experience. They can be used to compile
and programs, or to put together textual data documents including web-assets
such as CSS and HTML. The result product of the execution of the program can
then be distributed at your own discretion, but with Art Deco's limited
liability as outlined below, and only for lawful activities. For example, you
can generate static HTML websites for your clients with a static HTML generator
produced by Art Deco, or create documentation for your clients' API library
using JSDoc documentation generator as long as the tool itself does not get
distributed to your client.

If there are dynamic components to the devtools, such as interactive widgets
used on websites, or web server components (known as middleware) that are
executed dynamically client or server-side as a by-product of generated static
content, they fall under the terms of WEBLIBS such that you will need to
organise a payment for their license on behalf of your client (sublicense) for
them to constitute legal use by a 3rd party.

A devtool can be used by you on any machine at one point in time. This means the
software can be installed anywhere in parallel but it can only be run personally
by you in series. In case of batch licenses issues to teams, the devtool can
only be used by the agreed number of people at maximum, where "use" means fair
number of executions as part of a standard work routine during a business day.

Each person, and a development team, will receive an additional Continuous
Integration license key that can only be used to install the software on
automated testing, build and deployment servers. Such key cannot be used by a
developer himself and is to be used within automation infrastructure only.

The transfer of license codes to unauthorised 3rd parties is prohibited and will
lead to the termination of this EULA. Art Deco can monitor network requests for
its software packages, detect suspicious activity, and put on hold those keys
that were noticed to originate from multiple regions and unrelated to CI servers
in which case the end user will be notified by email.

## WEBLIBS AND SUBLICENSING

Art Deco acknowledges that some of its tools will need to be installed by either
free-lance developers or development companies for the benefit of their client.
In this case, additional software license keys can be purchased by them in order
to host the implementation of WEBLIBs on their servers. An example of this is
when a developer has created a website for her client run by a web server
created by Art Deco, so that a sublicense needs to be purchased.

A sublicense does not need to be purchased for developers' and studios' own
projects, whether they are commercial or hobby, once a single WEBLIB license key
has been paid for, unless stated otherwise at the time of purchase and confirmed
in the receipt email. When you as a developer or a consultancy that created a
website for your client, stop being their contractor, the sublicense is
transferred to them, and will be terminated upon the expiry of the license term.

The sublicense is issued with an informed consent of the receiving party which
must be given in writing and attached to the purchase of the WEBLIB sublicense.

## PAID, TRIAL, FREE

You are permitted to install Paid software packages only if your have purchased
the license key for the period of time specified on the purchase page. Trial
versions can be installed once for the duration of 30 days or other time period
explicitly stated on the trial description page. Free versions can be installed
any number of times and be kept for any duration, until an upgrade to the Full
version is made.

Trial versions provided to you under the terms of this EULA for the purpose of
evaluating the suitability of Art Deco's products for your development needs and
whether the Paid version might be able to meet your requirements. They can have
reduced functionality and support less features than Paid software products
published under the same name, and their evaluation period is limited to the
period of time declared on the README page of the published Trial packages,
which would normally be set to 30 days since the date of first installation of
the package, and should be considered as such if not stated explicitly. Once the
trial term has expired, your trial license is revoked and you need to uninstall
the software, or purchase a Paid version instead.

## RESTRICTIONS

With all trial, free and paid versions you receive, you are not permitted to:

- Edit, alter, modify, adapt, translate or otherwise change the whole or any
part of the Software nor permit the whole or any part of the Software to be
combined with or become incorporated in any other software, nor decompile,
disassemble or reverse engineer the Software or attempt to do any such things.
- Incorporate WEBLIBS into your clients' front-end web sites and back-end
servers without purchasing an additional sublicense for them. Developer's
clients will need their copy of each WEBLIB license key which can be
sublicensed.
- Distribute DEVTOOLS, LIBS and WEBLIBS as part of your own Open Source or
commercial software product.
- Specify Art Deco software packages as dependencies of your public software
packages published on the npm registry.
- Reproduce, copy, distribute, resell or otherwise use the Software for any
commercial and publish any versions of software on public and private
registries.
- Allow any third party to use the Software on behalf of or for the benefit of
any third party, until the party has entered into a sublicense agreement with
you.
- Use the Software in any way which breaches any applicable local, national or
international law.
- Use the Software for any purpose that Art Deco Code Ltd considers a breach of
this EULA agreement.

The aforementioned conditions apply to all software with the exception of Free
versions that were released under a secondary Open Source license which will be
stated on the package page.

## MAINTENANCE AND SUPPORT

During the term of the license, you will be able to receive minor (bug-fixes)
and major (new features) upgrades to software products. If you encounter any
behaviour inconsistent with requirements specification, you can report it to Art
Deco support using the issues@artdeco.support email address. Such issues are
logged and assigned to the relevant department for the acknowledgement,
work-around suggestions and further resolution in updates. Any suggestions for
the improvements and feature requests are also received at
requests@artdeco.support and will be dealt with in an adequate way, on "as is"
basis without any warranty.

The support is also available on Keybase chat with https://keybase.io/artdeco
account. All messages will be monitored and the best effort made to address and
resolve issues, however there is no legal guarantee that Art Deco will provide
support unless you have purchased the advanced support plan. Art Deco will main
to reply to chat messages on Keybase during 1-2 business days and help customers
resolve their problems within the scope of requirements specification.

## LICENSE

Art Deco Code Ltd shall at all times retain ownership of the Software as
originally downloaded by you and all subsequent downloads of the Software by
you. The Software (and the copyright, and other intellectual property rights of
whatever nature in the Software, including any modifications made thereto) are
and shall remain the property of Art Deco Code Ltd.

Art Deco Code reserves the right to grant licences to use the Software to third
parties. Art Deco may modify its software packages at any time at its sole
discretion and without prior notifications to you. You will maintain the ability
to lock received versions of your software in such metadata fields as
"dependencies" and "devDependencies" fields to prevent your package installation
client for installing upgrades. Older versions of software will not be removed
from package registries on periodic basis, Art Deco reserves the right to
publish and un-publish any version of its software at any time and remain the
exclusive publishing rights holder, and license such rights to its software
distributors.

Some parts of Art Deco software can be initially created by independent
developers, but either refactored or modified to be compiled into packages. In
this case, the copyright notice will remain in the compiled distributions and
the license information presented on the supporting documentation.

## LIABILITY LIMITATIONS

The liability of Art Deco and its affiliates and partners involved in
development, testing and delivery of the software, for all damages arising out
of or in connection with your ability to access or use the Trial, Paid or Free
software packages and/or this EULA, shall not exceed the amount of fifty dollars
($50).

Art Deco and its partners, distributors, suppliers or licensors will in no event
be liable for any indirect, incidental, consequential, special or exemplary
damages due to the use or inability to use any of the software packages. This
includes damage for loss of profit or savings, business interruption, loss of
data, and the like. To avoid potential interruption in the delivery services
because of involvement of malicious agents (such as a DDoS attack on the
registry where paid packages are hosted), please make a copy of the latest
version of software packages locally. Your package managers support caching, and
you can find installed packages in your projects' node_modules folder, from
which they can be linked globally, e.g., with `yarn link` command, and then
linked to in projects. Such strategy can provide a temporal remedy for the
interruption of services while we mitigate an attack.

## TERMINATION

This EULA agreement is effective from the date you first use the Software and
shall continue until terminated. You may terminate it at any time upon written
notice to Art Deco Code Ltd.

It will also terminate immediately if you fail to comply with any term of this
EULA agreement. Upon such termination, the licenses granted by this EULA
agreement will immediately terminate and you agree to stop all access and use of
the Software. The provisions that by their nature continue and survive will
survive any termination of this EULA agreement.

## GOVERNING LAW

This EULA agreement, and any dispute arising out of or in connection with this
EULA agreement, shall be governed by and construed in accordance with the laws
of Great Britain.