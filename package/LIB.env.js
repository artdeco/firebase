import {installLibTypeEngineer, LIB as lib, updateLibTypedefs} from '@artdeco/package'
import '@type.engineering/web-computing'
import {mkdirSync, writeFileSync} from 'fs'
import {dependencies, description, name} from '../package.json'
import {TYPEDEFS, TYPOLOGY} from './common'

export const LIB=lib(description, dependencies)

LIB.addFiles.push(
 'compile/dist/paid/5015254645',
)
LIB.ignore.push(
 '**/typal.js',
 '**/test/**/*',
 '**/*.typal.js',
 '**/precompile/index.js',
 '**/precompile/browser.js',
)
installLibTypeEngineer(LIB)

LIB.module='compile/module/server/server-lib.js'
LIB.browser='compile/module/browser/browser-lib.js'

LIB.postProcessing = async () => {
 console.log('Postprocessing lib')
 try {mkdirSync('package/lib/types')} catch (err) {}
 writeFileSync('package/lib/types/index.js', `import '${name}.h'
`+`import './typology.mjs'`)

 try {mkdirSync('package/lib/types/typedefs')} catch (err) {}
 writeFileSync('package/lib/types/typedefs/private.js', ``)

 updateLibTypedefs(name, TYPEDEFS)
}
LIB.copy={
 'compile/module/browser/browser-exports.js':'compile/module/browser/browser-exports.js',
 'compile/module/server/server-exports.js':'compile/module/server/server-exports.js',

 'compile/module/browser/browser-lib.js':'compile/module/browser/browser-lib.js',
 'compile/module/server/server-lib.js':'compile/module/server/server-lib.js',

 'compile/module/server/server-modules.txt':'compile/module/server/server-modules.txt',
 'compile/module/browser/browser-modules.txt':'compile/module/browser/browser-modules.txt',
 ...TYPOLOGY,
}
