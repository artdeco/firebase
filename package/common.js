export const TYPEDEFS = {
 // 'node_modules/@type.engineering/type-engineer.h/typedefs/index.js': 'types/type-engineer.typedefs.js',
}

export const TYPOLOGY={
 'compile/dist/paid/types/typology.js':'types/typology.js',
 'types/db/typology.js':'types/typology.mjs',
}

export const EXTERNS = {
 'types/runtypes-externs/ns/eco.artd.js':'types/ns/eco.artd.js',
 'types/runtypes-externs/index.externs.js':'types/externs.js',
}

export const RUNTYPES={
 'types/runtypes-externs/define.js':'types/define.js',
 'types/runtypes-externs/api.js':'types/api.js',
 'types/runtypes-externs/ns/$eco.ns':'types/ns/$eco.ns',
 'types/runtypes-externs/ns/$eco.artd.ns':'types/ns/$eco.artd.ns',
}