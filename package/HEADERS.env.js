import {importsToRequire, removeTypalMeta, removeTypology, renameVendor} from '@artdeco/package'
import '@type.engineering/web-computing'
import {writeFileSync} from 'fs'
import {description, name} from '../package.json'

/** @type {_lud.Env} */
export const HEADERS={
 name:`${name}.h`,
 ignore:[
  '**/typal.js',
  '**/*.typal.js',
  '**/private/*',
 ],
 module:'_.js',
 description:`${description} [HEADERS]`,
 runtypes:[
  'runtypes-externs/ns/__$te.ns',
  'runtypes-externs/index.js',
  'runtypes-externs/api.js',
  'runtypes-externs/define.js',
  'runtypes-externs/symbols.js',
 ],
 Externs:[
  'runtypes-externs/index.externs.js',
 ],
 externs:[
  'runtypes-externs/ns/eco.artd.js',
  'runtypes-externs/externs.js',
 ],
 addFiles:[
  'types/typedefs',
  'types/runtypes-externs',

  'types/index.js',
  'node_modules/@artdeco/license/EULA.md',
  'CHANGELOG.md',
 ],
}

HEADERS.postProcessing=async () => {
 console.log('Postprocessing types')
 renameVendor('headers')

 writeFileSync('package/headers/_.js', '') // module isn't required

 removeTypalMeta('package/headers', 'typedefs/api.js', 'runtypes-externs/api.js',
  'runtypes-externs/index.externs.js', 'runtypes-externs/index.js',
  'runtypes-externs/symbols.js',
  // 'typedefs/index.js',
  'typedefs/index.js',
 )

 writeFileSync('package/headers/typedefs/private.js','')

 importsToRequire('package/headers/index.js')
 removeTypology('package/headers')
}
