import {installLudditesTypeEngineer, LUDDITES as luddites, removeH, removeTypalMeta} from '@artdeco/package'
import {description} from '../package.json'
import {TYPEDEFS,EXTERNS,RUNTYPES,TYPOLOGY} from './common'

export const LUDDITES=luddites(description)

LUDDITES.copy={
 ...LUDDITES.copy,...TYPEDEFS,...EXTERNS,...RUNTYPES,...TYPOLOGY,
}
LUDDITES.ignore=[
 '**/typal.js','**/*.typal.js',
 '**/precompile/index.js',
 '**/precompile/browser.js',
]
LUDDITES.postProcessing=()=>{
 removeTypalMeta('package/luddites','types/typedefs.js','types/externs.js')
 removeH('package/luddites','browser/','node/')
}
// installShared(LUDDITES, '@can/install-shared')
installLudditesTypeEngineer(LUDDITES)

LUDDITES.addFiles.push(
 'compile/dist/paid/node',
 'compile/dist/paid/browser',
 'compile/dist/paid/version',
 'compile/dist/paid/5015254645',
 'compile/dist/paid/precompile',
)
LUDDITES.externs=[
 'types/ns/eco.artd.js',
 'types/externs.js',
]
LUDDITES.runtypes=[
 'types/ns/$COM.ns',
 'types/ns/$eco.artd.ns',
 'types/api.js',
 'types/define.js',
]
LUDDITES.module='node/index.js'
LUDDITES.browser='browser/index.js'
