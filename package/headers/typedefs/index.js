/** @nocompile */
eval(`var http={}
var eco={}
eco.artd={}
eco.artd.IFirebase={}
eco.artd.IFirebase.function={}
eco.artd.IFirebase.evalBodyWithProfileTime={}
eco.artd.IFirebase.fetchFunctionsFile={}
eco.artd.IFirebase.downloadMethodDirectly={}
eco.artd.IFirebase.patchDeps={}
eco.artd.IFirebaseAspectsInstaller={}
eco.artd.IFirebaseJoinpointModel={}
eco.artd.IFirebaseAspects={}
eco.artd.IFirebaseProfilingAspects={}
eco.artd.IFirebaseProfilingAide={}
eco.artd.IFirebaseLoggingAspects={}
eco.artd.IFirebaseConditioningAspects={}
eco.artd.IHyperFirebase={}
eco.artd.FirebaseFactory={}
eco.artd.firebase={}`)

/** */
var __$te_plain={}

/**
 * @typedef {Object} eco.artd.IFirebase.Initialese A record with object's initial values, dependencies and other configuration.
 * @prop {string} [firestorePackage]
 * @prop {string} [rpcDir]
 * @prop {http.ServerResponse} [res]
 * @prop {http.IncomingMessage} [req]
 * @prop {string} [gCloudProject] The name of the firebase project.
 * @prop {string} [functionTarget] Added on profuction functions to indicate their name.
 * @prop {string} [functionsEmulator] Set to _True_ when using local debugging.
 * @prop {string} [db] The database pointer from firebase package.
 * @prop {?{ info: function(...*) }} [logger] The logger.
 */

/** @typedef {function(new: eco.artd.Firebase)} eco.artd.AbstractFirebase.constructor */
/** @typedef {typeof eco.artd.Firebase} eco.artd.Firebase.typeof */
/**
 * An abstract class of `eco.artd.IFirebase` interface.
 * @constructor eco.artd.AbstractFirebase
 */
eco.artd.AbstractFirebase = class extends /** @type {eco.artd.AbstractFirebase.constructor&eco.artd.Firebase.typeof} */ (class {}) { }
eco.artd.AbstractFirebase.prototype.constructor = eco.artd.AbstractFirebase
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
eco.artd.AbstractFirebase.class = /** @type {typeof eco.artd.AbstractFirebase} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...!eco.artd.IFirebase|typeof eco.artd.Firebase} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof eco.artd.Firebase}
 * @nosideeffects
 */
eco.artd.AbstractFirebase.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof eco.artd.AbstractFirebase}
 */
eco.artd.AbstractFirebase.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof eco.artd.Firebase}
 */
eco.artd.AbstractFirebase.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...!eco.artd.IFirebase|typeof eco.artd.Firebase} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof eco.artd.Firebase}
 */
eco.artd.AbstractFirebase.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...!eco.artd.IFirebase|typeof eco.artd.Firebase} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof eco.artd.Firebase}
 */
eco.artd.AbstractFirebase.__trait = function(...Implementations) {}

/**
 * A hyper slice includes all combined methods of an interface.
 * @interface eco.artd.IFirebaseJoinpointModelHyperslice
 */
eco.artd.IFirebaseJoinpointModelHyperslice = class {
  constructor() {
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeFunction=/** @type {!eco.artd.IFirebaseJoinpointModel._beforeFunction|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._beforeFunction>} */ (void 0)
    /**
     * After the method.
     */
    this.afterFunction=/** @type {!eco.artd.IFirebaseJoinpointModel._afterFunction|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterFunction>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterFunctionThrows=/** @type {!eco.artd.IFirebaseJoinpointModel._afterFunctionThrows|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterFunctionThrows>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterFunctionReturns=/** @type {!eco.artd.IFirebaseJoinpointModel._afterFunctionReturns|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterFunctionReturns>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterFunctionCancels=/** @type {!eco.artd.IFirebaseJoinpointModel._afterFunctionCancels|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterFunctionCancels>} */ (void 0)
    /**
     * Immediately after the async method returned its promise.
     */
    this.immediatelyAfterFunction=/** @type {!eco.artd.IFirebaseJoinpointModel._immediatelyAfterFunction|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._immediatelyAfterFunction>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeEvalBodyWithProfileTime=/** @type {!eco.artd.IFirebaseJoinpointModel._beforeEvalBodyWithProfileTime|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._beforeEvalBodyWithProfileTime>} */ (void 0)
    /**
     * After the method.
     */
    this.afterEvalBodyWithProfileTime=/** @type {!eco.artd.IFirebaseJoinpointModel._afterEvalBodyWithProfileTime|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterEvalBodyWithProfileTime>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterEvalBodyWithProfileTimeThrows=/** @type {!eco.artd.IFirebaseJoinpointModel._afterEvalBodyWithProfileTimeThrows|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterEvalBodyWithProfileTimeThrows>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterEvalBodyWithProfileTimeReturns=/** @type {!eco.artd.IFirebaseJoinpointModel._afterEvalBodyWithProfileTimeReturns|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterEvalBodyWithProfileTimeReturns>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterEvalBodyWithProfileTimeCancels=/** @type {!eco.artd.IFirebaseJoinpointModel._afterEvalBodyWithProfileTimeCancels|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterEvalBodyWithProfileTimeCancels>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeGetPageDataFromFirestore=/** @type {!eco.artd.IFirebaseJoinpointModel._beforeGetPageDataFromFirestore|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._beforeGetPageDataFromFirestore>} */ (void 0)
    /**
     * After the method.
     */
    this.afterGetPageDataFromFirestore=/** @type {!eco.artd.IFirebaseJoinpointModel._afterGetPageDataFromFirestore|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterGetPageDataFromFirestore>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterGetPageDataFromFirestoreThrows=/** @type {!eco.artd.IFirebaseJoinpointModel._afterGetPageDataFromFirestoreThrows|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterGetPageDataFromFirestoreThrows>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterGetPageDataFromFirestoreReturns=/** @type {!eco.artd.IFirebaseJoinpointModel._afterGetPageDataFromFirestoreReturns|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterGetPageDataFromFirestoreReturns>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterGetPageDataFromFirestoreCancels=/** @type {!eco.artd.IFirebaseJoinpointModel._afterGetPageDataFromFirestoreCancels|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterGetPageDataFromFirestoreCancels>} */ (void 0)
    /**
     * Immediately after the async method returned its promise.
     */
    this.immediatelyAfterGetPageDataFromFirestore=/** @type {!eco.artd.IFirebaseJoinpointModel._immediatelyAfterGetPageDataFromFirestore|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._immediatelyAfterGetPageDataFromFirestore>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeDownloadAndEvalPageData=/** @type {!eco.artd.IFirebaseJoinpointModel._beforeDownloadAndEvalPageData|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._beforeDownloadAndEvalPageData>} */ (void 0)
    /**
     * After the method.
     */
    this.afterDownloadAndEvalPageData=/** @type {!eco.artd.IFirebaseJoinpointModel._afterDownloadAndEvalPageData|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterDownloadAndEvalPageData>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterDownloadAndEvalPageDataThrows=/** @type {!eco.artd.IFirebaseJoinpointModel._afterDownloadAndEvalPageDataThrows|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterDownloadAndEvalPageDataThrows>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterDownloadAndEvalPageDataReturns=/** @type {!eco.artd.IFirebaseJoinpointModel._afterDownloadAndEvalPageDataReturns|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterDownloadAndEvalPageDataReturns>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterDownloadAndEvalPageDataCancels=/** @type {!eco.artd.IFirebaseJoinpointModel._afterDownloadAndEvalPageDataCancels|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterDownloadAndEvalPageDataCancels>} */ (void 0)
    /**
     * Immediately after the async method returned its promise.
     */
    this.immediatelyAfterDownloadAndEvalPageData=/** @type {!eco.artd.IFirebaseJoinpointModel._immediatelyAfterDownloadAndEvalPageData|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._immediatelyAfterDownloadAndEvalPageData>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeEvalBody=/** @type {!eco.artd.IFirebaseJoinpointModel._beforeEvalBody|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._beforeEvalBody>} */ (void 0)
    /**
     * After the method.
     */
    this.afterEvalBody=/** @type {!eco.artd.IFirebaseJoinpointModel._afterEvalBody|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterEvalBody>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterEvalBodyThrows=/** @type {!eco.artd.IFirebaseJoinpointModel._afterEvalBodyThrows|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterEvalBodyThrows>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterEvalBodyReturns=/** @type {!eco.artd.IFirebaseJoinpointModel._afterEvalBodyReturns|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterEvalBodyReturns>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterEvalBodyCancels=/** @type {!eco.artd.IFirebaseJoinpointModel._afterEvalBodyCancels|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterEvalBodyCancels>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeLogRpcInvocation=/** @type {!eco.artd.IFirebaseJoinpointModel._beforeLogRpcInvocation|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._beforeLogRpcInvocation>} */ (void 0)
    /**
     * After the method.
     */
    this.afterLogRpcInvocation=/** @type {!eco.artd.IFirebaseJoinpointModel._afterLogRpcInvocation|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterLogRpcInvocation>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterLogRpcInvocationThrows=/** @type {!eco.artd.IFirebaseJoinpointModel._afterLogRpcInvocationThrows|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterLogRpcInvocationThrows>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterLogRpcInvocationReturns=/** @type {!eco.artd.IFirebaseJoinpointModel._afterLogRpcInvocationReturns|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterLogRpcInvocationReturns>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterLogRpcInvocationCancels=/** @type {!eco.artd.IFirebaseJoinpointModel._afterLogRpcInvocationCancels|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterLogRpcInvocationCancels>} */ (void 0)
    /**
     * Immediately after the async method returned its promise.
     */
    this.immediatelyAfterLogRpcInvocation=/** @type {!eco.artd.IFirebaseJoinpointModel._immediatelyAfterLogRpcInvocation|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._immediatelyAfterLogRpcInvocation>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeFetchFunctionsFile=/** @type {!eco.artd.IFirebaseJoinpointModel._beforeFetchFunctionsFile|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._beforeFetchFunctionsFile>} */ (void 0)
    /**
     * After the method.
     */
    this.afterFetchFunctionsFile=/** @type {!eco.artd.IFirebaseJoinpointModel._afterFetchFunctionsFile|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterFetchFunctionsFile>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterFetchFunctionsFileThrows=/** @type {!eco.artd.IFirebaseJoinpointModel._afterFetchFunctionsFileThrows|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterFetchFunctionsFileThrows>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterFetchFunctionsFileReturns=/** @type {!eco.artd.IFirebaseJoinpointModel._afterFetchFunctionsFileReturns|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterFetchFunctionsFileReturns>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterFetchFunctionsFileCancels=/** @type {!eco.artd.IFirebaseJoinpointModel._afterFetchFunctionsFileCancels|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterFetchFunctionsFileCancels>} */ (void 0)
    /**
     * Immediately after the async method returned its promise.
     */
    this.immediatelyAfterFetchFunctionsFile=/** @type {!eco.artd.IFirebaseJoinpointModel._immediatelyAfterFetchFunctionsFile|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._immediatelyAfterFetchFunctionsFile>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeDownloadDeps=/** @type {!eco.artd.IFirebaseJoinpointModel._beforeDownloadDeps|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._beforeDownloadDeps>} */ (void 0)
    /**
     * After the method.
     */
    this.afterDownloadDeps=/** @type {!eco.artd.IFirebaseJoinpointModel._afterDownloadDeps|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterDownloadDeps>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterDownloadDepsThrows=/** @type {!eco.artd.IFirebaseJoinpointModel._afterDownloadDepsThrows|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterDownloadDepsThrows>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterDownloadDepsReturns=/** @type {!eco.artd.IFirebaseJoinpointModel._afterDownloadDepsReturns|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterDownloadDepsReturns>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterDownloadDepsCancels=/** @type {!eco.artd.IFirebaseJoinpointModel._afterDownloadDepsCancels|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterDownloadDepsCancels>} */ (void 0)
    /**
     * Immediately after the async method returned its promise.
     */
    this.immediatelyAfterDownloadDeps=/** @type {!eco.artd.IFirebaseJoinpointModel._immediatelyAfterDownloadDeps|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._immediatelyAfterDownloadDeps>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeDownloadMethod=/** @type {!eco.artd.IFirebaseJoinpointModel._beforeDownloadMethod|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._beforeDownloadMethod>} */ (void 0)
    /**
     * After the method.
     */
    this.afterDownloadMethod=/** @type {!eco.artd.IFirebaseJoinpointModel._afterDownloadMethod|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterDownloadMethod>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterDownloadMethodThrows=/** @type {!eco.artd.IFirebaseJoinpointModel._afterDownloadMethodThrows|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterDownloadMethodThrows>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterDownloadMethodReturns=/** @type {!eco.artd.IFirebaseJoinpointModel._afterDownloadMethodReturns|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterDownloadMethodReturns>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterDownloadMethodCancels=/** @type {!eco.artd.IFirebaseJoinpointModel._afterDownloadMethodCancels|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterDownloadMethodCancels>} */ (void 0)
    /**
     * Immediately after the async method returned its promise.
     */
    this.immediatelyAfterDownloadMethod=/** @type {!eco.artd.IFirebaseJoinpointModel._immediatelyAfterDownloadMethod|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._immediatelyAfterDownloadMethod>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeDownloadMethodDirectly=/** @type {!eco.artd.IFirebaseJoinpointModel._beforeDownloadMethodDirectly|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._beforeDownloadMethodDirectly>} */ (void 0)
    /**
     * After the method.
     */
    this.afterDownloadMethodDirectly=/** @type {!eco.artd.IFirebaseJoinpointModel._afterDownloadMethodDirectly|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterDownloadMethodDirectly>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterDownloadMethodDirectlyThrows=/** @type {!eco.artd.IFirebaseJoinpointModel._afterDownloadMethodDirectlyThrows|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterDownloadMethodDirectlyThrows>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterDownloadMethodDirectlyReturns=/** @type {!eco.artd.IFirebaseJoinpointModel._afterDownloadMethodDirectlyReturns|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterDownloadMethodDirectlyReturns>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterDownloadMethodDirectlyCancels=/** @type {!eco.artd.IFirebaseJoinpointModel._afterDownloadMethodDirectlyCancels|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterDownloadMethodDirectlyCancels>} */ (void 0)
    /**
     * Immediately after the async method returned its promise.
     */
    this.immediatelyAfterDownloadMethodDirectly=/** @type {!eco.artd.IFirebaseJoinpointModel._immediatelyAfterDownloadMethodDirectly|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._immediatelyAfterDownloadMethodDirectly>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeGetId=/** @type {!eco.artd.IFirebaseJoinpointModel._beforeGetId|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._beforeGetId>} */ (void 0)
    /**
     * After the method.
     */
    this.afterGetId=/** @type {!eco.artd.IFirebaseJoinpointModel._afterGetId|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterGetId>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterGetIdThrows=/** @type {!eco.artd.IFirebaseJoinpointModel._afterGetIdThrows|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterGetIdThrows>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterGetIdReturns=/** @type {!eco.artd.IFirebaseJoinpointModel._afterGetIdReturns|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterGetIdReturns>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterGetIdCancels=/** @type {!eco.artd.IFirebaseJoinpointModel._afterGetIdCancels|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterGetIdCancels>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforePatchDeps=/** @type {!eco.artd.IFirebaseJoinpointModel._beforePatchDeps|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._beforePatchDeps>} */ (void 0)
    /**
     * After the method.
     */
    this.afterPatchDeps=/** @type {!eco.artd.IFirebaseJoinpointModel._afterPatchDeps|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterPatchDeps>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterPatchDepsThrows=/** @type {!eco.artd.IFirebaseJoinpointModel._afterPatchDepsThrows|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterPatchDepsThrows>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterPatchDepsReturns=/** @type {!eco.artd.IFirebaseJoinpointModel._afterPatchDepsReturns|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterPatchDepsReturns>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterPatchDepsCancels=/** @type {!eco.artd.IFirebaseJoinpointModel._afterPatchDepsCancels|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterPatchDepsCancels>} */ (void 0)
    /**
     * Immediately after the async method returned its promise.
     */
    this.immediatelyAfterPatchDeps=/** @type {!eco.artd.IFirebaseJoinpointModel._immediatelyAfterPatchDeps|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._immediatelyAfterPatchDeps>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeDownloadPageData=/** @type {!eco.artd.IFirebaseJoinpointModel._beforeDownloadPageData|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._beforeDownloadPageData>} */ (void 0)
    /**
     * After the method.
     */
    this.afterDownloadPageData=/** @type {!eco.artd.IFirebaseJoinpointModel._afterDownloadPageData|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterDownloadPageData>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterDownloadPageDataThrows=/** @type {!eco.artd.IFirebaseJoinpointModel._afterDownloadPageDataThrows|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterDownloadPageDataThrows>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterDownloadPageDataReturns=/** @type {!eco.artd.IFirebaseJoinpointModel._afterDownloadPageDataReturns|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterDownloadPageDataReturns>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterDownloadPageDataCancels=/** @type {!eco.artd.IFirebaseJoinpointModel._afterDownloadPageDataCancels|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterDownloadPageDataCancels>} */ (void 0)
    /**
     * Immediately after the async method returned its promise.
     */
    this.immediatelyAfterDownloadPageData=/** @type {!eco.artd.IFirebaseJoinpointModel._immediatelyAfterDownloadPageData|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._immediatelyAfterDownloadPageData>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeLoadRequirePageData=/** @type {!eco.artd.IFirebaseJoinpointModel._beforeLoadRequirePageData|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._beforeLoadRequirePageData>} */ (void 0)
    /**
     * After the method.
     */
    this.afterLoadRequirePageData=/** @type {!eco.artd.IFirebaseJoinpointModel._afterLoadRequirePageData|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterLoadRequirePageData>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterLoadRequirePageDataThrows=/** @type {!eco.artd.IFirebaseJoinpointModel._afterLoadRequirePageDataThrows|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterLoadRequirePageDataThrows>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterLoadRequirePageDataReturns=/** @type {!eco.artd.IFirebaseJoinpointModel._afterLoadRequirePageDataReturns|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterLoadRequirePageDataReturns>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterLoadRequirePageDataCancels=/** @type {!eco.artd.IFirebaseJoinpointModel._afterLoadRequirePageDataCancels|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterLoadRequirePageDataCancels>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeLoadRequire=/** @type {!eco.artd.IFirebaseJoinpointModel._beforeLoadRequire|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._beforeLoadRequire>} */ (void 0)
    /**
     * After the method.
     */
    this.afterLoadRequire=/** @type {!eco.artd.IFirebaseJoinpointModel._afterLoadRequire|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterLoadRequire>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterLoadRequireThrows=/** @type {!eco.artd.IFirebaseJoinpointModel._afterLoadRequireThrows|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterLoadRequireThrows>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterLoadRequireReturns=/** @type {!eco.artd.IFirebaseJoinpointModel._afterLoadRequireReturns|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterLoadRequireReturns>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterLoadRequireCancels=/** @type {!eco.artd.IFirebaseJoinpointModel._afterLoadRequireCancels|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterLoadRequireCancels>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeUpdateErrorStack=/** @type {!eco.artd.IFirebaseJoinpointModel._beforeUpdateErrorStack|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._beforeUpdateErrorStack>} */ (void 0)
    /**
     * After the method.
     */
    this.afterUpdateErrorStack=/** @type {!eco.artd.IFirebaseJoinpointModel._afterUpdateErrorStack|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterUpdateErrorStack>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterUpdateErrorStackThrows=/** @type {!eco.artd.IFirebaseJoinpointModel._afterUpdateErrorStackThrows|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterUpdateErrorStackThrows>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterUpdateErrorStackReturns=/** @type {!eco.artd.IFirebaseJoinpointModel._afterUpdateErrorStackReturns|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterUpdateErrorStackReturns>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterUpdateErrorStackCancels=/** @type {!eco.artd.IFirebaseJoinpointModel._afterUpdateErrorStackCancels|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterUpdateErrorStackCancels>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeValidateMethod=/** @type {!eco.artd.IFirebaseJoinpointModel._beforeValidateMethod|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._beforeValidateMethod>} */ (void 0)
    /**
     * After the method.
     */
    this.afterValidateMethod=/** @type {!eco.artd.IFirebaseJoinpointModel._afterValidateMethod|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterValidateMethod>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterValidateMethodThrows=/** @type {!eco.artd.IFirebaseJoinpointModel._afterValidateMethodThrows|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterValidateMethodThrows>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterValidateMethodReturns=/** @type {!eco.artd.IFirebaseJoinpointModel._afterValidateMethodReturns|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterValidateMethodReturns>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterValidateMethodCancels=/** @type {!eco.artd.IFirebaseJoinpointModel._afterValidateMethodCancels|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterValidateMethodCancels>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeValidateLoadAndInvoke=/** @type {!eco.artd.IFirebaseJoinpointModel._beforeValidateLoadAndInvoke|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._beforeValidateLoadAndInvoke>} */ (void 0)
    /**
     * After the method.
     */
    this.afterValidateLoadAndInvoke=/** @type {!eco.artd.IFirebaseJoinpointModel._afterValidateLoadAndInvoke|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterValidateLoadAndInvoke>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterValidateLoadAndInvokeThrows=/** @type {!eco.artd.IFirebaseJoinpointModel._afterValidateLoadAndInvokeThrows|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterValidateLoadAndInvokeThrows>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterValidateLoadAndInvokeReturns=/** @type {!eco.artd.IFirebaseJoinpointModel._afterValidateLoadAndInvokeReturns|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterValidateLoadAndInvokeReturns>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterValidateLoadAndInvokeCancels=/** @type {!eco.artd.IFirebaseJoinpointModel._afterValidateLoadAndInvokeCancels|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterValidateLoadAndInvokeCancels>} */ (void 0)
    /**
     * Immediately after the async method returned its promise.
     */
    this.immediatelyAfterValidateLoadAndInvoke=/** @type {!eco.artd.IFirebaseJoinpointModel._immediatelyAfterValidateLoadAndInvoke|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._immediatelyAfterValidateLoadAndInvoke>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeValidateLoadAndInvokeById=/** @type {!eco.artd.IFirebaseJoinpointModel._beforeValidateLoadAndInvokeById|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._beforeValidateLoadAndInvokeById>} */ (void 0)
    /**
     * After the method.
     */
    this.afterValidateLoadAndInvokeById=/** @type {!eco.artd.IFirebaseJoinpointModel._afterValidateLoadAndInvokeById|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterValidateLoadAndInvokeById>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterValidateLoadAndInvokeByIdThrows=/** @type {!eco.artd.IFirebaseJoinpointModel._afterValidateLoadAndInvokeByIdThrows|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterValidateLoadAndInvokeByIdThrows>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterValidateLoadAndInvokeByIdReturns=/** @type {!eco.artd.IFirebaseJoinpointModel._afterValidateLoadAndInvokeByIdReturns|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterValidateLoadAndInvokeByIdReturns>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterValidateLoadAndInvokeByIdCancels=/** @type {!eco.artd.IFirebaseJoinpointModel._afterValidateLoadAndInvokeByIdCancels|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterValidateLoadAndInvokeByIdCancels>} */ (void 0)
    /**
     * Immediately after the async method returned its promise.
     */
    this.immediatelyAfterValidateLoadAndInvokeById=/** @type {!eco.artd.IFirebaseJoinpointModel._immediatelyAfterValidateLoadAndInvokeById|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._immediatelyAfterValidateLoadAndInvokeById>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeMakeInvoker=/** @type {!eco.artd.IFirebaseJoinpointModel._beforeMakeInvoker|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._beforeMakeInvoker>} */ (void 0)
    /**
     * After the method.
     */
    this.afterMakeInvoker=/** @type {!eco.artd.IFirebaseJoinpointModel._afterMakeInvoker|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterMakeInvoker>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterMakeInvokerThrows=/** @type {!eco.artd.IFirebaseJoinpointModel._afterMakeInvokerThrows|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterMakeInvokerThrows>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterMakeInvokerReturns=/** @type {!eco.artd.IFirebaseJoinpointModel._afterMakeInvokerReturns|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterMakeInvokerReturns>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterMakeInvokerCancels=/** @type {!eco.artd.IFirebaseJoinpointModel._afterMakeInvokerCancels|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterMakeInvokerCancels>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeLoadAndInvokeById=/** @type {!eco.artd.IFirebaseJoinpointModel._beforeLoadAndInvokeById|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._beforeLoadAndInvokeById>} */ (void 0)
    /**
     * After the method.
     */
    this.afterLoadAndInvokeById=/** @type {!eco.artd.IFirebaseJoinpointModel._afterLoadAndInvokeById|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterLoadAndInvokeById>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterLoadAndInvokeByIdThrows=/** @type {!eco.artd.IFirebaseJoinpointModel._afterLoadAndInvokeByIdThrows|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterLoadAndInvokeByIdThrows>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterLoadAndInvokeByIdReturns=/** @type {!eco.artd.IFirebaseJoinpointModel._afterLoadAndInvokeByIdReturns|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterLoadAndInvokeByIdReturns>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterLoadAndInvokeByIdCancels=/** @type {!eco.artd.IFirebaseJoinpointModel._afterLoadAndInvokeByIdCancels|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterLoadAndInvokeByIdCancels>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeLoadAndInvoke=/** @type {!eco.artd.IFirebaseJoinpointModel._beforeLoadAndInvoke|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._beforeLoadAndInvoke>} */ (void 0)
    /**
     * After the method.
     */
    this.afterLoadAndInvoke=/** @type {!eco.artd.IFirebaseJoinpointModel._afterLoadAndInvoke|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterLoadAndInvoke>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterLoadAndInvokeThrows=/** @type {!eco.artd.IFirebaseJoinpointModel._afterLoadAndInvokeThrows|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterLoadAndInvokeThrows>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterLoadAndInvokeReturns=/** @type {!eco.artd.IFirebaseJoinpointModel._afterLoadAndInvokeReturns|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterLoadAndInvokeReturns>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterLoadAndInvokeCancels=/** @type {!eco.artd.IFirebaseJoinpointModel._afterLoadAndInvokeCancels|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._afterLoadAndInvokeCancels>} */ (void 0)
    /**
     * Immediately after the async method returned its promise.
     */
    this.immediatelyAfterLoadAndInvoke=/** @type {!eco.artd.IFirebaseJoinpointModel._immediatelyAfterLoadAndInvoke|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel._immediatelyAfterLoadAndInvoke>} */ (void 0)
  }
}
eco.artd.IFirebaseJoinpointModelHyperslice.prototype.constructor = eco.artd.IFirebaseJoinpointModelHyperslice

/**
 * A concrete class of _IFirebaseJoinpointModelHyperslice_ instances.
 * @constructor eco.artd.FirebaseJoinpointModelHyperslice
 * @implements {eco.artd.IFirebaseJoinpointModelHyperslice} A hyper slice includes all combined methods of an interface.
 */
eco.artd.FirebaseJoinpointModelHyperslice = class extends eco.artd.IFirebaseJoinpointModelHyperslice { }
eco.artd.FirebaseJoinpointModelHyperslice.prototype.constructor = eco.artd.FirebaseJoinpointModelHyperslice

/**
 * A hyper-slice includes all combined methods of an interface.
 * @interface eco.artd.IFirebaseJoinpointModelBindingHyperslice
 * @template THIS
 */
eco.artd.IFirebaseJoinpointModelBindingHyperslice = class {
  constructor() {
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeFunction=/** @type {!eco.artd.IFirebaseJoinpointModel.__beforeFunction<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__beforeFunction<THIS>>} */ (void 0)
    /**
     * After the method.
     */
    this.afterFunction=/** @type {!eco.artd.IFirebaseJoinpointModel.__afterFunction<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterFunction<THIS>>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterFunctionThrows=/** @type {!eco.artd.IFirebaseJoinpointModel.__afterFunctionThrows<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterFunctionThrows<THIS>>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterFunctionReturns=/** @type {!eco.artd.IFirebaseJoinpointModel.__afterFunctionReturns<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterFunctionReturns<THIS>>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterFunctionCancels=/** @type {!eco.artd.IFirebaseJoinpointModel.__afterFunctionCancels<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterFunctionCancels<THIS>>} */ (void 0)
    /**
     * Immediately after the async method returned its promise.
     */
    this.immediatelyAfterFunction=/** @type {!eco.artd.IFirebaseJoinpointModel.__immediatelyAfterFunction<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__immediatelyAfterFunction<THIS>>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeEvalBodyWithProfileTime=/** @type {!eco.artd.IFirebaseJoinpointModel.__beforeEvalBodyWithProfileTime<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__beforeEvalBodyWithProfileTime<THIS>>} */ (void 0)
    /**
     * After the method.
     */
    this.afterEvalBodyWithProfileTime=/** @type {!eco.artd.IFirebaseJoinpointModel.__afterEvalBodyWithProfileTime<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterEvalBodyWithProfileTime<THIS>>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterEvalBodyWithProfileTimeThrows=/** @type {!eco.artd.IFirebaseJoinpointModel.__afterEvalBodyWithProfileTimeThrows<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterEvalBodyWithProfileTimeThrows<THIS>>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterEvalBodyWithProfileTimeReturns=/** @type {!eco.artd.IFirebaseJoinpointModel.__afterEvalBodyWithProfileTimeReturns<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterEvalBodyWithProfileTimeReturns<THIS>>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterEvalBodyWithProfileTimeCancels=/** @type {!eco.artd.IFirebaseJoinpointModel.__afterEvalBodyWithProfileTimeCancels<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterEvalBodyWithProfileTimeCancels<THIS>>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeGetPageDataFromFirestore=/** @type {!eco.artd.IFirebaseJoinpointModel.__beforeGetPageDataFromFirestore<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__beforeGetPageDataFromFirestore<THIS>>} */ (void 0)
    /**
     * After the method.
     */
    this.afterGetPageDataFromFirestore=/** @type {!eco.artd.IFirebaseJoinpointModel.__afterGetPageDataFromFirestore<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterGetPageDataFromFirestore<THIS>>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterGetPageDataFromFirestoreThrows=/** @type {!eco.artd.IFirebaseJoinpointModel.__afterGetPageDataFromFirestoreThrows<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterGetPageDataFromFirestoreThrows<THIS>>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterGetPageDataFromFirestoreReturns=/** @type {!eco.artd.IFirebaseJoinpointModel.__afterGetPageDataFromFirestoreReturns<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterGetPageDataFromFirestoreReturns<THIS>>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterGetPageDataFromFirestoreCancels=/** @type {!eco.artd.IFirebaseJoinpointModel.__afterGetPageDataFromFirestoreCancels<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterGetPageDataFromFirestoreCancels<THIS>>} */ (void 0)
    /**
     * Immediately after the async method returned its promise.
     */
    this.immediatelyAfterGetPageDataFromFirestore=/** @type {!eco.artd.IFirebaseJoinpointModel.__immediatelyAfterGetPageDataFromFirestore<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__immediatelyAfterGetPageDataFromFirestore<THIS>>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeDownloadAndEvalPageData=/** @type {!eco.artd.IFirebaseJoinpointModel.__beforeDownloadAndEvalPageData<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__beforeDownloadAndEvalPageData<THIS>>} */ (void 0)
    /**
     * After the method.
     */
    this.afterDownloadAndEvalPageData=/** @type {!eco.artd.IFirebaseJoinpointModel.__afterDownloadAndEvalPageData<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterDownloadAndEvalPageData<THIS>>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterDownloadAndEvalPageDataThrows=/** @type {!eco.artd.IFirebaseJoinpointModel.__afterDownloadAndEvalPageDataThrows<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterDownloadAndEvalPageDataThrows<THIS>>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterDownloadAndEvalPageDataReturns=/** @type {!eco.artd.IFirebaseJoinpointModel.__afterDownloadAndEvalPageDataReturns<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterDownloadAndEvalPageDataReturns<THIS>>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterDownloadAndEvalPageDataCancels=/** @type {!eco.artd.IFirebaseJoinpointModel.__afterDownloadAndEvalPageDataCancels<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterDownloadAndEvalPageDataCancels<THIS>>} */ (void 0)
    /**
     * Immediately after the async method returned its promise.
     */
    this.immediatelyAfterDownloadAndEvalPageData=/** @type {!eco.artd.IFirebaseJoinpointModel.__immediatelyAfterDownloadAndEvalPageData<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__immediatelyAfterDownloadAndEvalPageData<THIS>>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeEvalBody=/** @type {!eco.artd.IFirebaseJoinpointModel.__beforeEvalBody<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__beforeEvalBody<THIS>>} */ (void 0)
    /**
     * After the method.
     */
    this.afterEvalBody=/** @type {!eco.artd.IFirebaseJoinpointModel.__afterEvalBody<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterEvalBody<THIS>>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterEvalBodyThrows=/** @type {!eco.artd.IFirebaseJoinpointModel.__afterEvalBodyThrows<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterEvalBodyThrows<THIS>>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterEvalBodyReturns=/** @type {!eco.artd.IFirebaseJoinpointModel.__afterEvalBodyReturns<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterEvalBodyReturns<THIS>>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterEvalBodyCancels=/** @type {!eco.artd.IFirebaseJoinpointModel.__afterEvalBodyCancels<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterEvalBodyCancels<THIS>>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeLogRpcInvocation=/** @type {!eco.artd.IFirebaseJoinpointModel.__beforeLogRpcInvocation<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__beforeLogRpcInvocation<THIS>>} */ (void 0)
    /**
     * After the method.
     */
    this.afterLogRpcInvocation=/** @type {!eco.artd.IFirebaseJoinpointModel.__afterLogRpcInvocation<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterLogRpcInvocation<THIS>>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterLogRpcInvocationThrows=/** @type {!eco.artd.IFirebaseJoinpointModel.__afterLogRpcInvocationThrows<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterLogRpcInvocationThrows<THIS>>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterLogRpcInvocationReturns=/** @type {!eco.artd.IFirebaseJoinpointModel.__afterLogRpcInvocationReturns<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterLogRpcInvocationReturns<THIS>>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterLogRpcInvocationCancels=/** @type {!eco.artd.IFirebaseJoinpointModel.__afterLogRpcInvocationCancels<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterLogRpcInvocationCancels<THIS>>} */ (void 0)
    /**
     * Immediately after the async method returned its promise.
     */
    this.immediatelyAfterLogRpcInvocation=/** @type {!eco.artd.IFirebaseJoinpointModel.__immediatelyAfterLogRpcInvocation<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__immediatelyAfterLogRpcInvocation<THIS>>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeFetchFunctionsFile=/** @type {!eco.artd.IFirebaseJoinpointModel.__beforeFetchFunctionsFile<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__beforeFetchFunctionsFile<THIS>>} */ (void 0)
    /**
     * After the method.
     */
    this.afterFetchFunctionsFile=/** @type {!eco.artd.IFirebaseJoinpointModel.__afterFetchFunctionsFile<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterFetchFunctionsFile<THIS>>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterFetchFunctionsFileThrows=/** @type {!eco.artd.IFirebaseJoinpointModel.__afterFetchFunctionsFileThrows<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterFetchFunctionsFileThrows<THIS>>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterFetchFunctionsFileReturns=/** @type {!eco.artd.IFirebaseJoinpointModel.__afterFetchFunctionsFileReturns<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterFetchFunctionsFileReturns<THIS>>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterFetchFunctionsFileCancels=/** @type {!eco.artd.IFirebaseJoinpointModel.__afterFetchFunctionsFileCancels<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterFetchFunctionsFileCancels<THIS>>} */ (void 0)
    /**
     * Immediately after the async method returned its promise.
     */
    this.immediatelyAfterFetchFunctionsFile=/** @type {!eco.artd.IFirebaseJoinpointModel.__immediatelyAfterFetchFunctionsFile<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__immediatelyAfterFetchFunctionsFile<THIS>>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeDownloadDeps=/** @type {!eco.artd.IFirebaseJoinpointModel.__beforeDownloadDeps<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__beforeDownloadDeps<THIS>>} */ (void 0)
    /**
     * After the method.
     */
    this.afterDownloadDeps=/** @type {!eco.artd.IFirebaseJoinpointModel.__afterDownloadDeps<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterDownloadDeps<THIS>>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterDownloadDepsThrows=/** @type {!eco.artd.IFirebaseJoinpointModel.__afterDownloadDepsThrows<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterDownloadDepsThrows<THIS>>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterDownloadDepsReturns=/** @type {!eco.artd.IFirebaseJoinpointModel.__afterDownloadDepsReturns<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterDownloadDepsReturns<THIS>>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterDownloadDepsCancels=/** @type {!eco.artd.IFirebaseJoinpointModel.__afterDownloadDepsCancels<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterDownloadDepsCancels<THIS>>} */ (void 0)
    /**
     * Immediately after the async method returned its promise.
     */
    this.immediatelyAfterDownloadDeps=/** @type {!eco.artd.IFirebaseJoinpointModel.__immediatelyAfterDownloadDeps<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__immediatelyAfterDownloadDeps<THIS>>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeDownloadMethod=/** @type {!eco.artd.IFirebaseJoinpointModel.__beforeDownloadMethod<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__beforeDownloadMethod<THIS>>} */ (void 0)
    /**
     * After the method.
     */
    this.afterDownloadMethod=/** @type {!eco.artd.IFirebaseJoinpointModel.__afterDownloadMethod<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterDownloadMethod<THIS>>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterDownloadMethodThrows=/** @type {!eco.artd.IFirebaseJoinpointModel.__afterDownloadMethodThrows<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterDownloadMethodThrows<THIS>>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterDownloadMethodReturns=/** @type {!eco.artd.IFirebaseJoinpointModel.__afterDownloadMethodReturns<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterDownloadMethodReturns<THIS>>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterDownloadMethodCancels=/** @type {!eco.artd.IFirebaseJoinpointModel.__afterDownloadMethodCancels<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterDownloadMethodCancels<THIS>>} */ (void 0)
    /**
     * Immediately after the async method returned its promise.
     */
    this.immediatelyAfterDownloadMethod=/** @type {!eco.artd.IFirebaseJoinpointModel.__immediatelyAfterDownloadMethod<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__immediatelyAfterDownloadMethod<THIS>>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeDownloadMethodDirectly=/** @type {!eco.artd.IFirebaseJoinpointModel.__beforeDownloadMethodDirectly<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__beforeDownloadMethodDirectly<THIS>>} */ (void 0)
    /**
     * After the method.
     */
    this.afterDownloadMethodDirectly=/** @type {!eco.artd.IFirebaseJoinpointModel.__afterDownloadMethodDirectly<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterDownloadMethodDirectly<THIS>>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterDownloadMethodDirectlyThrows=/** @type {!eco.artd.IFirebaseJoinpointModel.__afterDownloadMethodDirectlyThrows<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterDownloadMethodDirectlyThrows<THIS>>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterDownloadMethodDirectlyReturns=/** @type {!eco.artd.IFirebaseJoinpointModel.__afterDownloadMethodDirectlyReturns<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterDownloadMethodDirectlyReturns<THIS>>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterDownloadMethodDirectlyCancels=/** @type {!eco.artd.IFirebaseJoinpointModel.__afterDownloadMethodDirectlyCancels<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterDownloadMethodDirectlyCancels<THIS>>} */ (void 0)
    /**
     * Immediately after the async method returned its promise.
     */
    this.immediatelyAfterDownloadMethodDirectly=/** @type {!eco.artd.IFirebaseJoinpointModel.__immediatelyAfterDownloadMethodDirectly<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__immediatelyAfterDownloadMethodDirectly<THIS>>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeGetId=/** @type {!eco.artd.IFirebaseJoinpointModel.__beforeGetId<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__beforeGetId<THIS>>} */ (void 0)
    /**
     * After the method.
     */
    this.afterGetId=/** @type {!eco.artd.IFirebaseJoinpointModel.__afterGetId<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterGetId<THIS>>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterGetIdThrows=/** @type {!eco.artd.IFirebaseJoinpointModel.__afterGetIdThrows<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterGetIdThrows<THIS>>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterGetIdReturns=/** @type {!eco.artd.IFirebaseJoinpointModel.__afterGetIdReturns<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterGetIdReturns<THIS>>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterGetIdCancels=/** @type {!eco.artd.IFirebaseJoinpointModel.__afterGetIdCancels<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterGetIdCancels<THIS>>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforePatchDeps=/** @type {!eco.artd.IFirebaseJoinpointModel.__beforePatchDeps<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__beforePatchDeps<THIS>>} */ (void 0)
    /**
     * After the method.
     */
    this.afterPatchDeps=/** @type {!eco.artd.IFirebaseJoinpointModel.__afterPatchDeps<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterPatchDeps<THIS>>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterPatchDepsThrows=/** @type {!eco.artd.IFirebaseJoinpointModel.__afterPatchDepsThrows<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterPatchDepsThrows<THIS>>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterPatchDepsReturns=/** @type {!eco.artd.IFirebaseJoinpointModel.__afterPatchDepsReturns<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterPatchDepsReturns<THIS>>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterPatchDepsCancels=/** @type {!eco.artd.IFirebaseJoinpointModel.__afterPatchDepsCancels<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterPatchDepsCancels<THIS>>} */ (void 0)
    /**
     * Immediately after the async method returned its promise.
     */
    this.immediatelyAfterPatchDeps=/** @type {!eco.artd.IFirebaseJoinpointModel.__immediatelyAfterPatchDeps<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__immediatelyAfterPatchDeps<THIS>>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeDownloadPageData=/** @type {!eco.artd.IFirebaseJoinpointModel.__beforeDownloadPageData<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__beforeDownloadPageData<THIS>>} */ (void 0)
    /**
     * After the method.
     */
    this.afterDownloadPageData=/** @type {!eco.artd.IFirebaseJoinpointModel.__afterDownloadPageData<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterDownloadPageData<THIS>>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterDownloadPageDataThrows=/** @type {!eco.artd.IFirebaseJoinpointModel.__afterDownloadPageDataThrows<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterDownloadPageDataThrows<THIS>>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterDownloadPageDataReturns=/** @type {!eco.artd.IFirebaseJoinpointModel.__afterDownloadPageDataReturns<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterDownloadPageDataReturns<THIS>>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterDownloadPageDataCancels=/** @type {!eco.artd.IFirebaseJoinpointModel.__afterDownloadPageDataCancels<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterDownloadPageDataCancels<THIS>>} */ (void 0)
    /**
     * Immediately after the async method returned its promise.
     */
    this.immediatelyAfterDownloadPageData=/** @type {!eco.artd.IFirebaseJoinpointModel.__immediatelyAfterDownloadPageData<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__immediatelyAfterDownloadPageData<THIS>>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeLoadRequirePageData=/** @type {!eco.artd.IFirebaseJoinpointModel.__beforeLoadRequirePageData<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__beforeLoadRequirePageData<THIS>>} */ (void 0)
    /**
     * After the method.
     */
    this.afterLoadRequirePageData=/** @type {!eco.artd.IFirebaseJoinpointModel.__afterLoadRequirePageData<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterLoadRequirePageData<THIS>>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterLoadRequirePageDataThrows=/** @type {!eco.artd.IFirebaseJoinpointModel.__afterLoadRequirePageDataThrows<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterLoadRequirePageDataThrows<THIS>>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterLoadRequirePageDataReturns=/** @type {!eco.artd.IFirebaseJoinpointModel.__afterLoadRequirePageDataReturns<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterLoadRequirePageDataReturns<THIS>>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterLoadRequirePageDataCancels=/** @type {!eco.artd.IFirebaseJoinpointModel.__afterLoadRequirePageDataCancels<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterLoadRequirePageDataCancels<THIS>>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeLoadRequire=/** @type {!eco.artd.IFirebaseJoinpointModel.__beforeLoadRequire<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__beforeLoadRequire<THIS>>} */ (void 0)
    /**
     * After the method.
     */
    this.afterLoadRequire=/** @type {!eco.artd.IFirebaseJoinpointModel.__afterLoadRequire<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterLoadRequire<THIS>>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterLoadRequireThrows=/** @type {!eco.artd.IFirebaseJoinpointModel.__afterLoadRequireThrows<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterLoadRequireThrows<THIS>>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterLoadRequireReturns=/** @type {!eco.artd.IFirebaseJoinpointModel.__afterLoadRequireReturns<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterLoadRequireReturns<THIS>>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterLoadRequireCancels=/** @type {!eco.artd.IFirebaseJoinpointModel.__afterLoadRequireCancels<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterLoadRequireCancels<THIS>>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeUpdateErrorStack=/** @type {!eco.artd.IFirebaseJoinpointModel.__beforeUpdateErrorStack<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__beforeUpdateErrorStack<THIS>>} */ (void 0)
    /**
     * After the method.
     */
    this.afterUpdateErrorStack=/** @type {!eco.artd.IFirebaseJoinpointModel.__afterUpdateErrorStack<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterUpdateErrorStack<THIS>>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterUpdateErrorStackThrows=/** @type {!eco.artd.IFirebaseJoinpointModel.__afterUpdateErrorStackThrows<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterUpdateErrorStackThrows<THIS>>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterUpdateErrorStackReturns=/** @type {!eco.artd.IFirebaseJoinpointModel.__afterUpdateErrorStackReturns<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterUpdateErrorStackReturns<THIS>>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterUpdateErrorStackCancels=/** @type {!eco.artd.IFirebaseJoinpointModel.__afterUpdateErrorStackCancels<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterUpdateErrorStackCancels<THIS>>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeValidateMethod=/** @type {!eco.artd.IFirebaseJoinpointModel.__beforeValidateMethod<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__beforeValidateMethod<THIS>>} */ (void 0)
    /**
     * After the method.
     */
    this.afterValidateMethod=/** @type {!eco.artd.IFirebaseJoinpointModel.__afterValidateMethod<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterValidateMethod<THIS>>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterValidateMethodThrows=/** @type {!eco.artd.IFirebaseJoinpointModel.__afterValidateMethodThrows<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterValidateMethodThrows<THIS>>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterValidateMethodReturns=/** @type {!eco.artd.IFirebaseJoinpointModel.__afterValidateMethodReturns<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterValidateMethodReturns<THIS>>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterValidateMethodCancels=/** @type {!eco.artd.IFirebaseJoinpointModel.__afterValidateMethodCancels<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterValidateMethodCancels<THIS>>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeValidateLoadAndInvoke=/** @type {!eco.artd.IFirebaseJoinpointModel.__beforeValidateLoadAndInvoke<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__beforeValidateLoadAndInvoke<THIS>>} */ (void 0)
    /**
     * After the method.
     */
    this.afterValidateLoadAndInvoke=/** @type {!eco.artd.IFirebaseJoinpointModel.__afterValidateLoadAndInvoke<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterValidateLoadAndInvoke<THIS>>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterValidateLoadAndInvokeThrows=/** @type {!eco.artd.IFirebaseJoinpointModel.__afterValidateLoadAndInvokeThrows<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterValidateLoadAndInvokeThrows<THIS>>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterValidateLoadAndInvokeReturns=/** @type {!eco.artd.IFirebaseJoinpointModel.__afterValidateLoadAndInvokeReturns<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterValidateLoadAndInvokeReturns<THIS>>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterValidateLoadAndInvokeCancels=/** @type {!eco.artd.IFirebaseJoinpointModel.__afterValidateLoadAndInvokeCancels<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterValidateLoadAndInvokeCancels<THIS>>} */ (void 0)
    /**
     * Immediately after the async method returned its promise.
     */
    this.immediatelyAfterValidateLoadAndInvoke=/** @type {!eco.artd.IFirebaseJoinpointModel.__immediatelyAfterValidateLoadAndInvoke<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__immediatelyAfterValidateLoadAndInvoke<THIS>>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeValidateLoadAndInvokeById=/** @type {!eco.artd.IFirebaseJoinpointModel.__beforeValidateLoadAndInvokeById<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__beforeValidateLoadAndInvokeById<THIS>>} */ (void 0)
    /**
     * After the method.
     */
    this.afterValidateLoadAndInvokeById=/** @type {!eco.artd.IFirebaseJoinpointModel.__afterValidateLoadAndInvokeById<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterValidateLoadAndInvokeById<THIS>>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterValidateLoadAndInvokeByIdThrows=/** @type {!eco.artd.IFirebaseJoinpointModel.__afterValidateLoadAndInvokeByIdThrows<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterValidateLoadAndInvokeByIdThrows<THIS>>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterValidateLoadAndInvokeByIdReturns=/** @type {!eco.artd.IFirebaseJoinpointModel.__afterValidateLoadAndInvokeByIdReturns<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterValidateLoadAndInvokeByIdReturns<THIS>>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterValidateLoadAndInvokeByIdCancels=/** @type {!eco.artd.IFirebaseJoinpointModel.__afterValidateLoadAndInvokeByIdCancels<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterValidateLoadAndInvokeByIdCancels<THIS>>} */ (void 0)
    /**
     * Immediately after the async method returned its promise.
     */
    this.immediatelyAfterValidateLoadAndInvokeById=/** @type {!eco.artd.IFirebaseJoinpointModel.__immediatelyAfterValidateLoadAndInvokeById<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__immediatelyAfterValidateLoadAndInvokeById<THIS>>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeMakeInvoker=/** @type {!eco.artd.IFirebaseJoinpointModel.__beforeMakeInvoker<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__beforeMakeInvoker<THIS>>} */ (void 0)
    /**
     * After the method.
     */
    this.afterMakeInvoker=/** @type {!eco.artd.IFirebaseJoinpointModel.__afterMakeInvoker<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterMakeInvoker<THIS>>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterMakeInvokerThrows=/** @type {!eco.artd.IFirebaseJoinpointModel.__afterMakeInvokerThrows<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterMakeInvokerThrows<THIS>>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterMakeInvokerReturns=/** @type {!eco.artd.IFirebaseJoinpointModel.__afterMakeInvokerReturns<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterMakeInvokerReturns<THIS>>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterMakeInvokerCancels=/** @type {!eco.artd.IFirebaseJoinpointModel.__afterMakeInvokerCancels<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterMakeInvokerCancels<THIS>>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeLoadAndInvokeById=/** @type {!eco.artd.IFirebaseJoinpointModel.__beforeLoadAndInvokeById<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__beforeLoadAndInvokeById<THIS>>} */ (void 0)
    /**
     * After the method.
     */
    this.afterLoadAndInvokeById=/** @type {!eco.artd.IFirebaseJoinpointModel.__afterLoadAndInvokeById<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterLoadAndInvokeById<THIS>>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterLoadAndInvokeByIdThrows=/** @type {!eco.artd.IFirebaseJoinpointModel.__afterLoadAndInvokeByIdThrows<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterLoadAndInvokeByIdThrows<THIS>>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterLoadAndInvokeByIdReturns=/** @type {!eco.artd.IFirebaseJoinpointModel.__afterLoadAndInvokeByIdReturns<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterLoadAndInvokeByIdReturns<THIS>>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterLoadAndInvokeByIdCancels=/** @type {!eco.artd.IFirebaseJoinpointModel.__afterLoadAndInvokeByIdCancels<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterLoadAndInvokeByIdCancels<THIS>>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeLoadAndInvoke=/** @type {!eco.artd.IFirebaseJoinpointModel.__beforeLoadAndInvoke<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__beforeLoadAndInvoke<THIS>>} */ (void 0)
    /**
     * After the method.
     */
    this.afterLoadAndInvoke=/** @type {!eco.artd.IFirebaseJoinpointModel.__afterLoadAndInvoke<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterLoadAndInvoke<THIS>>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterLoadAndInvokeThrows=/** @type {!eco.artd.IFirebaseJoinpointModel.__afterLoadAndInvokeThrows<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterLoadAndInvokeThrows<THIS>>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterLoadAndInvokeReturns=/** @type {!eco.artd.IFirebaseJoinpointModel.__afterLoadAndInvokeReturns<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterLoadAndInvokeReturns<THIS>>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterLoadAndInvokeCancels=/** @type {!eco.artd.IFirebaseJoinpointModel.__afterLoadAndInvokeCancels<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__afterLoadAndInvokeCancels<THIS>>} */ (void 0)
    /**
     * Immediately after the async method returned its promise.
     */
    this.immediatelyAfterLoadAndInvoke=/** @type {!eco.artd.IFirebaseJoinpointModel.__immediatelyAfterLoadAndInvoke<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseJoinpointModel.__immediatelyAfterLoadAndInvoke<THIS>>} */ (void 0)
  }
}
eco.artd.IFirebaseJoinpointModelBindingHyperslice.prototype.constructor = eco.artd.IFirebaseJoinpointModelBindingHyperslice

/**
 * A concrete class of _IFirebaseJoinpointModelBindingHyperslice_ instances.
 * @constructor eco.artd.FirebaseJoinpointModelBindingHyperslice
 * @implements {eco.artd.IFirebaseJoinpointModelBindingHyperslice<THIS>} A hyper-slice includes all combined methods of an interface.
 * @template THIS
 * @extends {eco.artd.IFirebaseJoinpointModelBindingHyperslice<THIS>}
 */
eco.artd.FirebaseJoinpointModelBindingHyperslice = class extends eco.artd.IFirebaseJoinpointModelBindingHyperslice { }
eco.artd.FirebaseJoinpointModelBindingHyperslice.prototype.constructor = eco.artd.FirebaseJoinpointModelBindingHyperslice

/**
 * An interface that enumerates the joinpoints of `IFirebase`'s methods.
 * @interface eco.artd.IFirebaseJoinpointModel
 */
eco.artd.IFirebaseJoinpointModel = class { }
/** @type {eco.artd.IFirebaseJoinpointModel.beforeFunction} */
eco.artd.IFirebaseJoinpointModel.prototype.beforeFunction = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.afterFunction} */
eco.artd.IFirebaseJoinpointModel.prototype.afterFunction = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.afterFunctionThrows} */
eco.artd.IFirebaseJoinpointModel.prototype.afterFunctionThrows = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.afterFunctionReturns} */
eco.artd.IFirebaseJoinpointModel.prototype.afterFunctionReturns = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.afterFunctionCancels} */
eco.artd.IFirebaseJoinpointModel.prototype.afterFunctionCancels = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.immediatelyAfterFunction} */
eco.artd.IFirebaseJoinpointModel.prototype.immediatelyAfterFunction = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.beforeEvalBodyWithProfileTime} */
eco.artd.IFirebaseJoinpointModel.prototype.beforeEvalBodyWithProfileTime = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.afterEvalBodyWithProfileTime} */
eco.artd.IFirebaseJoinpointModel.prototype.afterEvalBodyWithProfileTime = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.afterEvalBodyWithProfileTimeThrows} */
eco.artd.IFirebaseJoinpointModel.prototype.afterEvalBodyWithProfileTimeThrows = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.afterEvalBodyWithProfileTimeReturns} */
eco.artd.IFirebaseJoinpointModel.prototype.afterEvalBodyWithProfileTimeReturns = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.afterEvalBodyWithProfileTimeCancels} */
eco.artd.IFirebaseJoinpointModel.prototype.afterEvalBodyWithProfileTimeCancels = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.beforeGetPageDataFromFirestore} */
eco.artd.IFirebaseJoinpointModel.prototype.beforeGetPageDataFromFirestore = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.afterGetPageDataFromFirestore} */
eco.artd.IFirebaseJoinpointModel.prototype.afterGetPageDataFromFirestore = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.afterGetPageDataFromFirestoreThrows} */
eco.artd.IFirebaseJoinpointModel.prototype.afterGetPageDataFromFirestoreThrows = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.afterGetPageDataFromFirestoreReturns} */
eco.artd.IFirebaseJoinpointModel.prototype.afterGetPageDataFromFirestoreReturns = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.afterGetPageDataFromFirestoreCancels} */
eco.artd.IFirebaseJoinpointModel.prototype.afterGetPageDataFromFirestoreCancels = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.immediatelyAfterGetPageDataFromFirestore} */
eco.artd.IFirebaseJoinpointModel.prototype.immediatelyAfterGetPageDataFromFirestore = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.beforeDownloadAndEvalPageData} */
eco.artd.IFirebaseJoinpointModel.prototype.beforeDownloadAndEvalPageData = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.afterDownloadAndEvalPageData} */
eco.artd.IFirebaseJoinpointModel.prototype.afterDownloadAndEvalPageData = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.afterDownloadAndEvalPageDataThrows} */
eco.artd.IFirebaseJoinpointModel.prototype.afterDownloadAndEvalPageDataThrows = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.afterDownloadAndEvalPageDataReturns} */
eco.artd.IFirebaseJoinpointModel.prototype.afterDownloadAndEvalPageDataReturns = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.afterDownloadAndEvalPageDataCancels} */
eco.artd.IFirebaseJoinpointModel.prototype.afterDownloadAndEvalPageDataCancels = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.immediatelyAfterDownloadAndEvalPageData} */
eco.artd.IFirebaseJoinpointModel.prototype.immediatelyAfterDownloadAndEvalPageData = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.beforeEvalBody} */
eco.artd.IFirebaseJoinpointModel.prototype.beforeEvalBody = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.afterEvalBody} */
eco.artd.IFirebaseJoinpointModel.prototype.afterEvalBody = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.afterEvalBodyThrows} */
eco.artd.IFirebaseJoinpointModel.prototype.afterEvalBodyThrows = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.afterEvalBodyReturns} */
eco.artd.IFirebaseJoinpointModel.prototype.afterEvalBodyReturns = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.afterEvalBodyCancels} */
eco.artd.IFirebaseJoinpointModel.prototype.afterEvalBodyCancels = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.beforeLogRpcInvocation} */
eco.artd.IFirebaseJoinpointModel.prototype.beforeLogRpcInvocation = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.afterLogRpcInvocation} */
eco.artd.IFirebaseJoinpointModel.prototype.afterLogRpcInvocation = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.afterLogRpcInvocationThrows} */
eco.artd.IFirebaseJoinpointModel.prototype.afterLogRpcInvocationThrows = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.afterLogRpcInvocationReturns} */
eco.artd.IFirebaseJoinpointModel.prototype.afterLogRpcInvocationReturns = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.afterLogRpcInvocationCancels} */
eco.artd.IFirebaseJoinpointModel.prototype.afterLogRpcInvocationCancels = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.immediatelyAfterLogRpcInvocation} */
eco.artd.IFirebaseJoinpointModel.prototype.immediatelyAfterLogRpcInvocation = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.beforeFetchFunctionsFile} */
eco.artd.IFirebaseJoinpointModel.prototype.beforeFetchFunctionsFile = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.afterFetchFunctionsFile} */
eco.artd.IFirebaseJoinpointModel.prototype.afterFetchFunctionsFile = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.afterFetchFunctionsFileThrows} */
eco.artd.IFirebaseJoinpointModel.prototype.afterFetchFunctionsFileThrows = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.afterFetchFunctionsFileReturns} */
eco.artd.IFirebaseJoinpointModel.prototype.afterFetchFunctionsFileReturns = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.afterFetchFunctionsFileCancels} */
eco.artd.IFirebaseJoinpointModel.prototype.afterFetchFunctionsFileCancels = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.immediatelyAfterFetchFunctionsFile} */
eco.artd.IFirebaseJoinpointModel.prototype.immediatelyAfterFetchFunctionsFile = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.beforeDownloadDeps} */
eco.artd.IFirebaseJoinpointModel.prototype.beforeDownloadDeps = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.afterDownloadDeps} */
eco.artd.IFirebaseJoinpointModel.prototype.afterDownloadDeps = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.afterDownloadDepsThrows} */
eco.artd.IFirebaseJoinpointModel.prototype.afterDownloadDepsThrows = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.afterDownloadDepsReturns} */
eco.artd.IFirebaseJoinpointModel.prototype.afterDownloadDepsReturns = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.afterDownloadDepsCancels} */
eco.artd.IFirebaseJoinpointModel.prototype.afterDownloadDepsCancels = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.immediatelyAfterDownloadDeps} */
eco.artd.IFirebaseJoinpointModel.prototype.immediatelyAfterDownloadDeps = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.beforeDownloadMethod} */
eco.artd.IFirebaseJoinpointModel.prototype.beforeDownloadMethod = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.afterDownloadMethod} */
eco.artd.IFirebaseJoinpointModel.prototype.afterDownloadMethod = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.afterDownloadMethodThrows} */
eco.artd.IFirebaseJoinpointModel.prototype.afterDownloadMethodThrows = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.afterDownloadMethodReturns} */
eco.artd.IFirebaseJoinpointModel.prototype.afterDownloadMethodReturns = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.afterDownloadMethodCancels} */
eco.artd.IFirebaseJoinpointModel.prototype.afterDownloadMethodCancels = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.immediatelyAfterDownloadMethod} */
eco.artd.IFirebaseJoinpointModel.prototype.immediatelyAfterDownloadMethod = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.beforeDownloadMethodDirectly} */
eco.artd.IFirebaseJoinpointModel.prototype.beforeDownloadMethodDirectly = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.afterDownloadMethodDirectly} */
eco.artd.IFirebaseJoinpointModel.prototype.afterDownloadMethodDirectly = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.afterDownloadMethodDirectlyThrows} */
eco.artd.IFirebaseJoinpointModel.prototype.afterDownloadMethodDirectlyThrows = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.afterDownloadMethodDirectlyReturns} */
eco.artd.IFirebaseJoinpointModel.prototype.afterDownloadMethodDirectlyReturns = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.afterDownloadMethodDirectlyCancels} */
eco.artd.IFirebaseJoinpointModel.prototype.afterDownloadMethodDirectlyCancels = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.immediatelyAfterDownloadMethodDirectly} */
eco.artd.IFirebaseJoinpointModel.prototype.immediatelyAfterDownloadMethodDirectly = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.beforeGetId} */
eco.artd.IFirebaseJoinpointModel.prototype.beforeGetId = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.afterGetId} */
eco.artd.IFirebaseJoinpointModel.prototype.afterGetId = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.afterGetIdThrows} */
eco.artd.IFirebaseJoinpointModel.prototype.afterGetIdThrows = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.afterGetIdReturns} */
eco.artd.IFirebaseJoinpointModel.prototype.afterGetIdReturns = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.afterGetIdCancels} */
eco.artd.IFirebaseJoinpointModel.prototype.afterGetIdCancels = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.beforePatchDeps} */
eco.artd.IFirebaseJoinpointModel.prototype.beforePatchDeps = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.afterPatchDeps} */
eco.artd.IFirebaseJoinpointModel.prototype.afterPatchDeps = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.afterPatchDepsThrows} */
eco.artd.IFirebaseJoinpointModel.prototype.afterPatchDepsThrows = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.afterPatchDepsReturns} */
eco.artd.IFirebaseJoinpointModel.prototype.afterPatchDepsReturns = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.afterPatchDepsCancels} */
eco.artd.IFirebaseJoinpointModel.prototype.afterPatchDepsCancels = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.immediatelyAfterPatchDeps} */
eco.artd.IFirebaseJoinpointModel.prototype.immediatelyAfterPatchDeps = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.beforeDownloadPageData} */
eco.artd.IFirebaseJoinpointModel.prototype.beforeDownloadPageData = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.afterDownloadPageData} */
eco.artd.IFirebaseJoinpointModel.prototype.afterDownloadPageData = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.afterDownloadPageDataThrows} */
eco.artd.IFirebaseJoinpointModel.prototype.afterDownloadPageDataThrows = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.afterDownloadPageDataReturns} */
eco.artd.IFirebaseJoinpointModel.prototype.afterDownloadPageDataReturns = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.afterDownloadPageDataCancels} */
eco.artd.IFirebaseJoinpointModel.prototype.afterDownloadPageDataCancels = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.immediatelyAfterDownloadPageData} */
eco.artd.IFirebaseJoinpointModel.prototype.immediatelyAfterDownloadPageData = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.beforeLoadRequirePageData} */
eco.artd.IFirebaseJoinpointModel.prototype.beforeLoadRequirePageData = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.afterLoadRequirePageData} */
eco.artd.IFirebaseJoinpointModel.prototype.afterLoadRequirePageData = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.afterLoadRequirePageDataThrows} */
eco.artd.IFirebaseJoinpointModel.prototype.afterLoadRequirePageDataThrows = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.afterLoadRequirePageDataReturns} */
eco.artd.IFirebaseJoinpointModel.prototype.afterLoadRequirePageDataReturns = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.afterLoadRequirePageDataCancels} */
eco.artd.IFirebaseJoinpointModel.prototype.afterLoadRequirePageDataCancels = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.beforeLoadRequire} */
eco.artd.IFirebaseJoinpointModel.prototype.beforeLoadRequire = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.afterLoadRequire} */
eco.artd.IFirebaseJoinpointModel.prototype.afterLoadRequire = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.afterLoadRequireThrows} */
eco.artd.IFirebaseJoinpointModel.prototype.afterLoadRequireThrows = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.afterLoadRequireReturns} */
eco.artd.IFirebaseJoinpointModel.prototype.afterLoadRequireReturns = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.afterLoadRequireCancels} */
eco.artd.IFirebaseJoinpointModel.prototype.afterLoadRequireCancels = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.beforeUpdateErrorStack} */
eco.artd.IFirebaseJoinpointModel.prototype.beforeUpdateErrorStack = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.afterUpdateErrorStack} */
eco.artd.IFirebaseJoinpointModel.prototype.afterUpdateErrorStack = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.afterUpdateErrorStackThrows} */
eco.artd.IFirebaseJoinpointModel.prototype.afterUpdateErrorStackThrows = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.afterUpdateErrorStackReturns} */
eco.artd.IFirebaseJoinpointModel.prototype.afterUpdateErrorStackReturns = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.afterUpdateErrorStackCancels} */
eco.artd.IFirebaseJoinpointModel.prototype.afterUpdateErrorStackCancels = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.beforeValidateMethod} */
eco.artd.IFirebaseJoinpointModel.prototype.beforeValidateMethod = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.afterValidateMethod} */
eco.artd.IFirebaseJoinpointModel.prototype.afterValidateMethod = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.afterValidateMethodThrows} */
eco.artd.IFirebaseJoinpointModel.prototype.afterValidateMethodThrows = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.afterValidateMethodReturns} */
eco.artd.IFirebaseJoinpointModel.prototype.afterValidateMethodReturns = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.afterValidateMethodCancels} */
eco.artd.IFirebaseJoinpointModel.prototype.afterValidateMethodCancels = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.beforeValidateLoadAndInvoke} */
eco.artd.IFirebaseJoinpointModel.prototype.beforeValidateLoadAndInvoke = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.afterValidateLoadAndInvoke} */
eco.artd.IFirebaseJoinpointModel.prototype.afterValidateLoadAndInvoke = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.afterValidateLoadAndInvokeThrows} */
eco.artd.IFirebaseJoinpointModel.prototype.afterValidateLoadAndInvokeThrows = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.afterValidateLoadAndInvokeReturns} */
eco.artd.IFirebaseJoinpointModel.prototype.afterValidateLoadAndInvokeReturns = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.afterValidateLoadAndInvokeCancels} */
eco.artd.IFirebaseJoinpointModel.prototype.afterValidateLoadAndInvokeCancels = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.immediatelyAfterValidateLoadAndInvoke} */
eco.artd.IFirebaseJoinpointModel.prototype.immediatelyAfterValidateLoadAndInvoke = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.beforeValidateLoadAndInvokeById} */
eco.artd.IFirebaseJoinpointModel.prototype.beforeValidateLoadAndInvokeById = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.afterValidateLoadAndInvokeById} */
eco.artd.IFirebaseJoinpointModel.prototype.afterValidateLoadAndInvokeById = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.afterValidateLoadAndInvokeByIdThrows} */
eco.artd.IFirebaseJoinpointModel.prototype.afterValidateLoadAndInvokeByIdThrows = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.afterValidateLoadAndInvokeByIdReturns} */
eco.artd.IFirebaseJoinpointModel.prototype.afterValidateLoadAndInvokeByIdReturns = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.afterValidateLoadAndInvokeByIdCancels} */
eco.artd.IFirebaseJoinpointModel.prototype.afterValidateLoadAndInvokeByIdCancels = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.immediatelyAfterValidateLoadAndInvokeById} */
eco.artd.IFirebaseJoinpointModel.prototype.immediatelyAfterValidateLoadAndInvokeById = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.beforeMakeInvoker} */
eco.artd.IFirebaseJoinpointModel.prototype.beforeMakeInvoker = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.afterMakeInvoker} */
eco.artd.IFirebaseJoinpointModel.prototype.afterMakeInvoker = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.afterMakeInvokerThrows} */
eco.artd.IFirebaseJoinpointModel.prototype.afterMakeInvokerThrows = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.afterMakeInvokerReturns} */
eco.artd.IFirebaseJoinpointModel.prototype.afterMakeInvokerReturns = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.afterMakeInvokerCancels} */
eco.artd.IFirebaseJoinpointModel.prototype.afterMakeInvokerCancels = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.beforeLoadAndInvokeById} */
eco.artd.IFirebaseJoinpointModel.prototype.beforeLoadAndInvokeById = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.afterLoadAndInvokeById} */
eco.artd.IFirebaseJoinpointModel.prototype.afterLoadAndInvokeById = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.afterLoadAndInvokeByIdThrows} */
eco.artd.IFirebaseJoinpointModel.prototype.afterLoadAndInvokeByIdThrows = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.afterLoadAndInvokeByIdReturns} */
eco.artd.IFirebaseJoinpointModel.prototype.afterLoadAndInvokeByIdReturns = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.afterLoadAndInvokeByIdCancels} */
eco.artd.IFirebaseJoinpointModel.prototype.afterLoadAndInvokeByIdCancels = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.beforeLoadAndInvoke} */
eco.artd.IFirebaseJoinpointModel.prototype.beforeLoadAndInvoke = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.afterLoadAndInvoke} */
eco.artd.IFirebaseJoinpointModel.prototype.afterLoadAndInvoke = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.afterLoadAndInvokeThrows} */
eco.artd.IFirebaseJoinpointModel.prototype.afterLoadAndInvokeThrows = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.afterLoadAndInvokeReturns} */
eco.artd.IFirebaseJoinpointModel.prototype.afterLoadAndInvokeReturns = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.afterLoadAndInvokeCancels} */
eco.artd.IFirebaseJoinpointModel.prototype.afterLoadAndInvokeCancels = function() {}
/** @type {eco.artd.IFirebaseJoinpointModel.immediatelyAfterLoadAndInvoke} */
eco.artd.IFirebaseJoinpointModel.prototype.immediatelyAfterLoadAndInvoke = function() {}

/**
 * A concrete class of _IFirebaseJoinpointModel_ instances.
 * @constructor eco.artd.FirebaseJoinpointModel
 * @implements {eco.artd.IFirebaseJoinpointModel} An interface that enumerates the joinpoints of `IFirebase`'s methods.
 */
eco.artd.FirebaseJoinpointModel = class extends eco.artd.IFirebaseJoinpointModel { }
eco.artd.FirebaseJoinpointModel.prototype.constructor = eco.artd.FirebaseJoinpointModel

/** @typedef {eco.artd.IFirebaseJoinpointModel} */
eco.artd.RecordIFirebaseJoinpointModel

/** @typedef {eco.artd.IFirebaseJoinpointModel} eco.artd.BoundIFirebaseJoinpointModel */

/** @typedef {eco.artd.FirebaseJoinpointModel} eco.artd.BoundFirebaseJoinpointModel */

/** @typedef {typeof __$te_plain} eco.artd.IFirebaseAspectsInstaller.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: eco.artd.FirebaseAspectsInstaller)} eco.artd.AbstractFirebaseAspectsInstaller.constructor */
/** @typedef {typeof eco.artd.FirebaseAspectsInstaller} eco.artd.FirebaseAspectsInstaller.typeof */
/**
 * An abstract class of `eco.artd.IFirebaseAspectsInstaller` interface.
 * @constructor eco.artd.AbstractFirebaseAspectsInstaller
 */
eco.artd.AbstractFirebaseAspectsInstaller = class extends /** @type {eco.artd.AbstractFirebaseAspectsInstaller.constructor&eco.artd.FirebaseAspectsInstaller.typeof} */ (class {}) { }
eco.artd.AbstractFirebaseAspectsInstaller.prototype.constructor = eco.artd.AbstractFirebaseAspectsInstaller
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
eco.artd.AbstractFirebaseAspectsInstaller.class = /** @type {typeof eco.artd.AbstractFirebaseAspectsInstaller} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...!eco.artd.IFirebaseAspectsInstaller|typeof eco.artd.FirebaseAspectsInstaller} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof eco.artd.FirebaseAspectsInstaller}
 * @nosideeffects
 */
eco.artd.AbstractFirebaseAspectsInstaller.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof eco.artd.AbstractFirebaseAspectsInstaller}
 */
eco.artd.AbstractFirebaseAspectsInstaller.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof eco.artd.FirebaseAspectsInstaller}
 */
eco.artd.AbstractFirebaseAspectsInstaller.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...!eco.artd.IFirebaseAspectsInstaller|typeof eco.artd.FirebaseAspectsInstaller} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof eco.artd.FirebaseAspectsInstaller}
 */
eco.artd.AbstractFirebaseAspectsInstaller.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...!eco.artd.IFirebaseAspectsInstaller|typeof eco.artd.FirebaseAspectsInstaller} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof eco.artd.FirebaseAspectsInstaller}
 */
eco.artd.AbstractFirebaseAspectsInstaller.__trait = function(...Implementations) {}

/** @typedef {new (...args: !eco.artd.IFirebaseAspectsInstaller.Initialese[]) => eco.artd.IFirebaseAspectsInstaller} eco.artd.FirebaseAspectsInstallerConstructor */

/** @typedef {function(new: engineering.type.IEngineer)} eco.artd.IFirebaseAspectsInstaller.constructor */
/** @typedef {typeof engineering.type.IEngineer} engineering.type.IEngineer.typeof */
/** @interface eco.artd.IFirebaseAspectsInstaller */
eco.artd.IFirebaseAspectsInstaller = class extends /** @type {eco.artd.IFirebaseAspectsInstaller.constructor&engineering.type.IEngineer.typeof} */ (class {}) {
  constructor() {
    super(...init)
    this.beforeFunction=/** @type {number} */ (void 0)
    this.afterFunction=/** @type {number} */ (void 0)
    this.afterFunctionThrows=/** @type {number} */ (void 0)
    this.afterFunctionReturns=/** @type {number} */ (void 0)
    this.afterFunctionCancels=/** @type {number} */ (void 0)
    this.immediateAfterFunction=/** @type {number} */ (void 0)
    this.beforeEvalBodyWithProfileTime=/** @type {number} */ (void 0)
    this.afterEvalBodyWithProfileTime=/** @type {number} */ (void 0)
    this.afterEvalBodyWithProfileTimeThrows=/** @type {number} */ (void 0)
    this.afterEvalBodyWithProfileTimeReturns=/** @type {number} */ (void 0)
    this.afterEvalBodyWithProfileTimeCancels=/** @type {number} */ (void 0)
    this.beforeGetPageDataFromFirestore=/** @type {number} */ (void 0)
    this.afterGetPageDataFromFirestore=/** @type {number} */ (void 0)
    this.afterGetPageDataFromFirestoreThrows=/** @type {number} */ (void 0)
    this.afterGetPageDataFromFirestoreReturns=/** @type {number} */ (void 0)
    this.afterGetPageDataFromFirestoreCancels=/** @type {number} */ (void 0)
    this.immediateAfterGetPageDataFromFirestore=/** @type {number} */ (void 0)
    this.beforeDownloadAndEvalPageData=/** @type {number} */ (void 0)
    this.afterDownloadAndEvalPageData=/** @type {number} */ (void 0)
    this.afterDownloadAndEvalPageDataThrows=/** @type {number} */ (void 0)
    this.afterDownloadAndEvalPageDataReturns=/** @type {number} */ (void 0)
    this.afterDownloadAndEvalPageDataCancels=/** @type {number} */ (void 0)
    this.immediateAfterDownloadAndEvalPageData=/** @type {number} */ (void 0)
    this.beforeEvalBody=/** @type {number} */ (void 0)
    this.afterEvalBody=/** @type {number} */ (void 0)
    this.afterEvalBodyThrows=/** @type {number} */ (void 0)
    this.afterEvalBodyReturns=/** @type {number} */ (void 0)
    this.afterEvalBodyCancels=/** @type {number} */ (void 0)
    this.beforeLogRpcInvocation=/** @type {number} */ (void 0)
    this.afterLogRpcInvocation=/** @type {number} */ (void 0)
    this.afterLogRpcInvocationThrows=/** @type {number} */ (void 0)
    this.afterLogRpcInvocationReturns=/** @type {number} */ (void 0)
    this.afterLogRpcInvocationCancels=/** @type {number} */ (void 0)
    this.immediateAfterLogRpcInvocation=/** @type {number} */ (void 0)
    this.beforeFetchFunctionsFile=/** @type {number} */ (void 0)
    this.afterFetchFunctionsFile=/** @type {number} */ (void 0)
    this.afterFetchFunctionsFileThrows=/** @type {number} */ (void 0)
    this.afterFetchFunctionsFileReturns=/** @type {number} */ (void 0)
    this.afterFetchFunctionsFileCancels=/** @type {number} */ (void 0)
    this.immediateAfterFetchFunctionsFile=/** @type {number} */ (void 0)
    this.beforeDownloadDeps=/** @type {number} */ (void 0)
    this.afterDownloadDeps=/** @type {number} */ (void 0)
    this.afterDownloadDepsThrows=/** @type {number} */ (void 0)
    this.afterDownloadDepsReturns=/** @type {number} */ (void 0)
    this.afterDownloadDepsCancels=/** @type {number} */ (void 0)
    this.immediateAfterDownloadDeps=/** @type {number} */ (void 0)
    this.beforeDownloadMethod=/** @type {number} */ (void 0)
    this.afterDownloadMethod=/** @type {number} */ (void 0)
    this.afterDownloadMethodThrows=/** @type {number} */ (void 0)
    this.afterDownloadMethodReturns=/** @type {number} */ (void 0)
    this.afterDownloadMethodCancels=/** @type {number} */ (void 0)
    this.immediateAfterDownloadMethod=/** @type {number} */ (void 0)
    this.beforeDownloadMethodDirectly=/** @type {number} */ (void 0)
    this.afterDownloadMethodDirectly=/** @type {number} */ (void 0)
    this.afterDownloadMethodDirectlyThrows=/** @type {number} */ (void 0)
    this.afterDownloadMethodDirectlyReturns=/** @type {number} */ (void 0)
    this.afterDownloadMethodDirectlyCancels=/** @type {number} */ (void 0)
    this.immediateAfterDownloadMethodDirectly=/** @type {number} */ (void 0)
    this.beforeGetId=/** @type {number} */ (void 0)
    this.afterGetId=/** @type {number} */ (void 0)
    this.afterGetIdThrows=/** @type {number} */ (void 0)
    this.afterGetIdReturns=/** @type {number} */ (void 0)
    this.afterGetIdCancels=/** @type {number} */ (void 0)
    this.beforePatchDeps=/** @type {number} */ (void 0)
    this.afterPatchDeps=/** @type {number} */ (void 0)
    this.afterPatchDepsThrows=/** @type {number} */ (void 0)
    this.afterPatchDepsReturns=/** @type {number} */ (void 0)
    this.afterPatchDepsCancels=/** @type {number} */ (void 0)
    this.immediateAfterPatchDeps=/** @type {number} */ (void 0)
    this.beforeDownloadPageData=/** @type {number} */ (void 0)
    this.afterDownloadPageData=/** @type {number} */ (void 0)
    this.afterDownloadPageDataThrows=/** @type {number} */ (void 0)
    this.afterDownloadPageDataReturns=/** @type {number} */ (void 0)
    this.afterDownloadPageDataCancels=/** @type {number} */ (void 0)
    this.immediateAfterDownloadPageData=/** @type {number} */ (void 0)
    this.beforeLoadRequirePageData=/** @type {number} */ (void 0)
    this.afterLoadRequirePageData=/** @type {number} */ (void 0)
    this.afterLoadRequirePageDataThrows=/** @type {number} */ (void 0)
    this.afterLoadRequirePageDataReturns=/** @type {number} */ (void 0)
    this.afterLoadRequirePageDataCancels=/** @type {number} */ (void 0)
    this.beforeLoadRequire=/** @type {number} */ (void 0)
    this.afterLoadRequire=/** @type {number} */ (void 0)
    this.afterLoadRequireThrows=/** @type {number} */ (void 0)
    this.afterLoadRequireReturns=/** @type {number} */ (void 0)
    this.afterLoadRequireCancels=/** @type {number} */ (void 0)
    this.beforeUpdateErrorStack=/** @type {number} */ (void 0)
    this.afterUpdateErrorStack=/** @type {number} */ (void 0)
    this.afterUpdateErrorStackThrows=/** @type {number} */ (void 0)
    this.afterUpdateErrorStackReturns=/** @type {number} */ (void 0)
    this.afterUpdateErrorStackCancels=/** @type {number} */ (void 0)
    this.beforeValidateMethod=/** @type {number} */ (void 0)
    this.afterValidateMethod=/** @type {number} */ (void 0)
    this.afterValidateMethodThrows=/** @type {number} */ (void 0)
    this.afterValidateMethodReturns=/** @type {number} */ (void 0)
    this.afterValidateMethodCancels=/** @type {number} */ (void 0)
    this.beforeValidateLoadAndInvoke=/** @type {number} */ (void 0)
    this.afterValidateLoadAndInvoke=/** @type {number} */ (void 0)
    this.afterValidateLoadAndInvokeThrows=/** @type {number} */ (void 0)
    this.afterValidateLoadAndInvokeReturns=/** @type {number} */ (void 0)
    this.afterValidateLoadAndInvokeCancels=/** @type {number} */ (void 0)
    this.immediateAfterValidateLoadAndInvoke=/** @type {number} */ (void 0)
    this.beforeValidateLoadAndInvokeById=/** @type {number} */ (void 0)
    this.afterValidateLoadAndInvokeById=/** @type {number} */ (void 0)
    this.afterValidateLoadAndInvokeByIdThrows=/** @type {number} */ (void 0)
    this.afterValidateLoadAndInvokeByIdReturns=/** @type {number} */ (void 0)
    this.afterValidateLoadAndInvokeByIdCancels=/** @type {number} */ (void 0)
    this.immediateAfterValidateLoadAndInvokeById=/** @type {number} */ (void 0)
    this.beforeMakeInvoker=/** @type {number} */ (void 0)
    this.afterMakeInvoker=/** @type {number} */ (void 0)
    this.afterMakeInvokerThrows=/** @type {number} */ (void 0)
    this.afterMakeInvokerReturns=/** @type {number} */ (void 0)
    this.afterMakeInvokerCancels=/** @type {number} */ (void 0)
    this.beforeLoadAndInvokeById=/** @type {number} */ (void 0)
    this.afterLoadAndInvokeById=/** @type {number} */ (void 0)
    this.afterLoadAndInvokeByIdThrows=/** @type {number} */ (void 0)
    this.afterLoadAndInvokeByIdReturns=/** @type {number} */ (void 0)
    this.afterLoadAndInvokeByIdCancels=/** @type {number} */ (void 0)
    this.beforeLoadAndInvoke=/** @type {number} */ (void 0)
    this.afterLoadAndInvoke=/** @type {number} */ (void 0)
    this.afterLoadAndInvokeThrows=/** @type {number} */ (void 0)
    this.afterLoadAndInvokeReturns=/** @type {number} */ (void 0)
    this.afterLoadAndInvokeCancels=/** @type {number} */ (void 0)
    this.immediateAfterLoadAndInvoke=/** @type {number} */ (void 0)
  }
  /**
   * A joinpointer.
   * @return {?}
   */
  function() { }
  /**
   * A joinpointer.
   * @return {?}
   */
  evalBodyWithProfileTime() { }
  /**
   * A joinpointer.
   * @return {?}
   */
  getPageDataFromFirestore() { }
  /**
   * A joinpointer.
   * @return {?}
   */
  downloadAndEvalPageData() { }
  /**
   * A joinpointer.
   * @return {?}
   */
  evalBody() { }
  /**
   * A joinpointer.
   * @return {?}
   */
  logRpcInvocation() { }
  /**
   * A joinpointer.
   * @return {?}
   */
  fetchFunctionsFile() { }
  /**
   * A joinpointer.
   * @return {?}
   */
  downloadDeps() { }
  /**
   * A joinpointer.
   * @return {?}
   */
  downloadMethod() { }
  /**
   * A joinpointer.
   * @return {?}
   */
  downloadMethodDirectly() { }
  /**
   * A joinpointer.
   * @return {void}
   */
  getId() { }
  /**
   * A joinpointer.
   * @return {?}
   */
  patchDeps() { }
  /**
   * A joinpointer.
   * @return {?}
   */
  downloadPageData() { }
  /**
   * A joinpointer.
   * @return {?}
   */
  loadRequirePageData() { }
  /**
   * A joinpointer.
   * @return {?}
   */
  loadRequire() { }
  /**
   * A joinpointer.
   * @return {?}
   */
  updateErrorStack() { }
  /**
   * A joinpointer.
   * @return {?}
   */
  validateMethod() { }
  /**
   * A joinpointer.
   * @return {?}
   */
  validateLoadAndInvoke() { }
  /**
   * A joinpointer.
   * @return {?}
   */
  validateLoadAndInvokeById() { }
  /**
   * A joinpointer.
   * @return {?}
   */
  makeInvoker() { }
  /**
   * A joinpointer.
   * @return {?}
   */
  loadAndInvokeById() { }
  /**
   * A joinpointer.
   * @return {?}
   */
  loadAndInvoke() { }
}
/**
 * Create a new *IFirebaseAspectsInstaller* instance and automatically initialise it with options.
 * @param {...!eco.artd.IFirebaseAspectsInstaller.Initialese} init The initialisation options.
 */
eco.artd.IFirebaseAspectsInstaller.prototype.constructor = function(...init) {}

/** @typedef {function(new: eco.artd.IFirebaseAspectsInstaller&engineering.type.IInitialiser<!eco.artd.IFirebaseAspectsInstaller.Initialese>)} eco.artd.FirebaseAspectsInstaller.constructor */
/** @typedef {typeof eco.artd.IFirebaseAspectsInstaller} eco.artd.IFirebaseAspectsInstaller.typeof */
/** @typedef {typeof engineering.type.IInitialiser} engineering.type.IInitialiser.typeof */
/**
 * A concrete class of _IFirebaseAspectsInstaller_ instances.
 * @constructor eco.artd.FirebaseAspectsInstaller
 * @implements {eco.artd.IFirebaseAspectsInstaller} ‎
 * @implements {engineering.type.IInitialiser<!eco.artd.IFirebaseAspectsInstaller.Initialese>} ‎
 */
eco.artd.FirebaseAspectsInstaller = class extends /** @type {eco.artd.FirebaseAspectsInstaller.constructor&eco.artd.IFirebaseAspectsInstaller.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IFirebaseAspectsInstaller* instance and automatically initialise it with options.
   * @param {...!eco.artd.IFirebaseAspectsInstaller.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IFirebaseAspectsInstaller* instance and automatically initialise it with options.
 * @param {...!eco.artd.IFirebaseAspectsInstaller.Initialese} init The initialisation options.
 */
eco.artd.FirebaseAspectsInstaller.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof eco.artd.FirebaseAspectsInstaller}
 */
eco.artd.FirebaseAspectsInstaller.__extend = function(...Extensions) {}

/**
 * @typedef {Object} eco.artd.IFirebase.FunctionNArgs
 * @prop {http.IncomingMessage} request The req.
 * @prop {http.ServerResponse} response The res.
 * @prop {!Function|number} cb The callback.
 * @prop {!eco.artd.IFirebase.function.Meta} meta The meta.
 */

/**
 * @typedef {Object} eco.artd.IFirebaseJoinpointModel.FunctionPointcutData The general pointcut data.
 * @prop {symbol} ticket A symbol unique to the particular execution.
 * @prop {eco.artd.IFirebase.FunctionNArgs} args The arguments passed to the method converted to a lambda-n record.
 * @prop {!Function} proc Allows to work with the process.
 */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.BeforeFunctionPointcutData
 * @prop {(arg0: !Object<string, *>) => void} cond Conditions the object to the process.
 * @prop {(args: eco.artd.IFirebase.FunctionNArgs) => void} setArgs Allows to set or modify arguments passed to the method and futher aspects.
 * @prop {(reason?: string) => void} cancel Allows to stop the `function` method from being executed.
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.BeforeFunctionPointcutData&eco.artd.IFirebaseJoinpointModel.FunctionPointcutData} eco.artd.IFirebaseJoinpointModel.BeforeFunctionPointcutData The pointcut data passed to _before_ aspects. */

/** @typedef {eco.artd.IFirebaseJoinpointModel.FunctionPointcutData} eco.artd.IFirebaseJoinpointModel.AfterFunctionPointcutData The pointcut data passed to _after_ aspects. */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.AfterThrowsFunctionPointcutData
 * @prop {!Error} err The error with which the method failed.
 * @prop {() => void} hide Hides the error in the `function` (use only at the top-level of program flow).
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.AfterThrowsFunctionPointcutData&eco.artd.IFirebaseJoinpointModel.FunctionPointcutData} eco.artd.IFirebaseJoinpointModel.AfterThrowsFunctionPointcutData The pointcut data passed to _afterThrows_ aspects. */

/** @typedef {eco.artd.IFirebaseJoinpointModel.FunctionPointcutData} eco.artd.IFirebaseJoinpointModel.AfterReturnsFunctionPointcutData The pointcut data passed to _afterReturns_ aspects. */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.AfterCancelsFunctionPointcutData
 * @prop {!Set<string>} reasons The list of reasons why the method was cancelled.
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.AfterCancelsFunctionPointcutData&eco.artd.IFirebaseJoinpointModel.FunctionPointcutData} eco.artd.IFirebaseJoinpointModel.AfterCancelsFunctionPointcutData The pointcut data passed to _afterCancels_ aspects. */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterFunctionPointcutData
 * @prop {!Promise<void>} promise The promise with the return of the method after it's successfully run.
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterFunctionPointcutData&eco.artd.IFirebaseJoinpointModel.FunctionPointcutData} eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterFunctionPointcutData The pointcut data passed to _immediatelyAfter_ aspects. */

/**
 * @typedef {Object} eco.artd.IFirebase.EvalBodyWithProfileTimeNArgs
 * @prop {string} body
 */

/**
 * @typedef {Object} eco.artd.IFirebaseJoinpointModel.EvalBodyWithProfileTimePointcutData The general pointcut data.
 * @prop {symbol} ticket A symbol unique to the particular execution.
 * @prop {eco.artd.IFirebase.EvalBodyWithProfileTimeNArgs} args The arguments passed to the method converted to a lambda-n record.
 * @prop {!Function} proc Allows to work with the process.
 */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.BeforeEvalBodyWithProfileTimePointcutData
 * @prop {(arg0: !Object<string, *>) => void} cond Conditions the object to the process.
 * @prop {(args: eco.artd.IFirebase.EvalBodyWithProfileTimeNArgs) => void} setArgs Allows to set or modify arguments passed to the method and futher aspects.
 * @prop {(reason?: string) => void} cancel Allows to stop the `evalBodyWithProfileTime` method from being executed.
 * @prop {(value: eco.artd.IFirebase.evalBodyWithProfileTime.Return) => void} sub Cancels a call to `evalBodyWithProfileTime` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.BeforeEvalBodyWithProfileTimePointcutData&eco.artd.IFirebaseJoinpointModel.EvalBodyWithProfileTimePointcutData} eco.artd.IFirebaseJoinpointModel.BeforeEvalBodyWithProfileTimePointcutData The pointcut data passed to _before_ aspects. */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.AfterEvalBodyWithProfileTimePointcutData
 * @prop {eco.artd.IFirebase.evalBodyWithProfileTime.Return} res The return of the method after it's successfully run.
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.AfterEvalBodyWithProfileTimePointcutData&eco.artd.IFirebaseJoinpointModel.EvalBodyWithProfileTimePointcutData} eco.artd.IFirebaseJoinpointModel.AfterEvalBodyWithProfileTimePointcutData The pointcut data passed to _after_ aspects. */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.AfterThrowsEvalBodyWithProfileTimePointcutData
 * @prop {!Error} err The error with which the method failed.
 * @prop {() => void} hide Hides the error in the `evalBodyWithProfileTime` (use only at the top-level of program flow).
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.AfterThrowsEvalBodyWithProfileTimePointcutData&eco.artd.IFirebaseJoinpointModel.EvalBodyWithProfileTimePointcutData} eco.artd.IFirebaseJoinpointModel.AfterThrowsEvalBodyWithProfileTimePointcutData The pointcut data passed to _afterThrows_ aspects. */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.AfterReturnsEvalBodyWithProfileTimePointcutData
 * @prop {eco.artd.IFirebase.evalBodyWithProfileTime.Return} res The return of the method after it's successfully run.
 * @prop {(value: eco.artd.IFirebase.evalBodyWithProfileTime.Return) => ?} sub Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.AfterReturnsEvalBodyWithProfileTimePointcutData&eco.artd.IFirebaseJoinpointModel.EvalBodyWithProfileTimePointcutData} eco.artd.IFirebaseJoinpointModel.AfterReturnsEvalBodyWithProfileTimePointcutData The pointcut data passed to _afterReturns_ aspects. */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.AfterCancelsEvalBodyWithProfileTimePointcutData
 * @prop {!Set<string>} reasons The list of reasons why the method was cancelled.
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.AfterCancelsEvalBodyWithProfileTimePointcutData&eco.artd.IFirebaseJoinpointModel.EvalBodyWithProfileTimePointcutData} eco.artd.IFirebaseJoinpointModel.AfterCancelsEvalBodyWithProfileTimePointcutData The pointcut data passed to _afterCancels_ aspects. */

/**
 * @typedef {Object} eco.artd.IFirebase.GetPageDataFromFirestoreNArgs
 * @prop {number} cid The circuit id.
 */

/**
 * @typedef {Object} eco.artd.IFirebaseJoinpointModel.GetPageDataFromFirestorePointcutData The general pointcut data.
 * @prop {symbol} ticket A symbol unique to the particular execution.
 * @prop {eco.artd.IFirebase.GetPageDataFromFirestoreNArgs} args The arguments passed to the method converted to a lambda-n record.
 * @prop {!Function} proc Allows to work with the process.
 */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.BeforeGetPageDataFromFirestorePointcutData
 * @prop {(arg0: !Object<string, *>) => void} cond Conditions the object to the process.
 * @prop {(args: eco.artd.IFirebase.GetPageDataFromFirestoreNArgs) => void} setArgs Allows to set or modify arguments passed to the method and futher aspects.
 * @prop {(reason?: string) => void} cancel Allows to stop the `getPageDataFromFirestore` method from being executed.
 * @prop {(value: !Promise<?eco.artd.IFirebase.PageData>) => void} sub Cancels a call to `getPageDataFromFirestore` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.BeforeGetPageDataFromFirestorePointcutData&eco.artd.IFirebaseJoinpointModel.GetPageDataFromFirestorePointcutData} eco.artd.IFirebaseJoinpointModel.BeforeGetPageDataFromFirestorePointcutData The pointcut data passed to _before_ aspects. */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.AfterGetPageDataFromFirestorePointcutData
 * @prop {?eco.artd.IFirebase.PageData} res The return of the method after it's successfully run.
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.AfterGetPageDataFromFirestorePointcutData&eco.artd.IFirebaseJoinpointModel.GetPageDataFromFirestorePointcutData} eco.artd.IFirebaseJoinpointModel.AfterGetPageDataFromFirestorePointcutData The pointcut data passed to _after_ aspects. */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.AfterThrowsGetPageDataFromFirestorePointcutData
 * @prop {!Error} err The error with which the method failed.
 * @prop {() => void} hide Hides the error in the `getPageDataFromFirestore` (use only at the top-level of program flow).
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.AfterThrowsGetPageDataFromFirestorePointcutData&eco.artd.IFirebaseJoinpointModel.GetPageDataFromFirestorePointcutData} eco.artd.IFirebaseJoinpointModel.AfterThrowsGetPageDataFromFirestorePointcutData The pointcut data passed to _afterThrows_ aspects. */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.AfterReturnsGetPageDataFromFirestorePointcutData
 * @prop {?eco.artd.IFirebase.PageData} res The return of the method after it's successfully run.
 * @prop {(value: !Promise<?eco.artd.IFirebase.PageData>|?eco.artd.IFirebase.PageData) => ?} sub Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.AfterReturnsGetPageDataFromFirestorePointcutData&eco.artd.IFirebaseJoinpointModel.GetPageDataFromFirestorePointcutData} eco.artd.IFirebaseJoinpointModel.AfterReturnsGetPageDataFromFirestorePointcutData The pointcut data passed to _afterReturns_ aspects. */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.AfterCancelsGetPageDataFromFirestorePointcutData
 * @prop {!Set<string>} reasons The list of reasons why the method was cancelled.
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.AfterCancelsGetPageDataFromFirestorePointcutData&eco.artd.IFirebaseJoinpointModel.GetPageDataFromFirestorePointcutData} eco.artd.IFirebaseJoinpointModel.AfterCancelsGetPageDataFromFirestorePointcutData The pointcut data passed to _afterCancels_ aspects. */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterGetPageDataFromFirestorePointcutData
 * @prop {!Promise<?eco.artd.IFirebase.PageData>} promise The promise with the return of the method after it's successfully run.
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterGetPageDataFromFirestorePointcutData&eco.artd.IFirebaseJoinpointModel.GetPageDataFromFirestorePointcutData} eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterGetPageDataFromFirestorePointcutData The pointcut data passed to _immediatelyAfter_ aspects. */

/**
 * @typedef {Object} eco.artd.IFirebase.DownloadAndEvalPageDataNArgs
 * @prop {number} id
 */

/**
 * @typedef {Object} eco.artd.IFirebaseJoinpointModel.DownloadAndEvalPageDataPointcutData The general pointcut data.
 * @prop {symbol} ticket A symbol unique to the particular execution.
 * @prop {eco.artd.IFirebase.DownloadAndEvalPageDataNArgs} args The arguments passed to the method converted to a lambda-n record.
 * @prop {!Function} proc Allows to work with the process.
 */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.BeforeDownloadAndEvalPageDataPointcutData
 * @prop {(arg0: !Object<string, *>) => void} cond Conditions the object to the process.
 * @prop {(args: eco.artd.IFirebase.DownloadAndEvalPageDataNArgs) => void} setArgs Allows to set or modify arguments passed to the method and futher aspects.
 * @prop {(reason?: string) => void} cancel Allows to stop the `downloadAndEvalPageData` method from being executed.
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.BeforeDownloadAndEvalPageDataPointcutData&eco.artd.IFirebaseJoinpointModel.DownloadAndEvalPageDataPointcutData} eco.artd.IFirebaseJoinpointModel.BeforeDownloadAndEvalPageDataPointcutData The pointcut data passed to _before_ aspects. */

/** @typedef {eco.artd.IFirebaseJoinpointModel.DownloadAndEvalPageDataPointcutData} eco.artd.IFirebaseJoinpointModel.AfterDownloadAndEvalPageDataPointcutData The pointcut data passed to _after_ aspects. */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.AfterThrowsDownloadAndEvalPageDataPointcutData
 * @prop {!Error} err The error with which the method failed.
 * @prop {() => void} hide Hides the error in the `downloadAndEvalPageData` (use only at the top-level of program flow).
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.AfterThrowsDownloadAndEvalPageDataPointcutData&eco.artd.IFirebaseJoinpointModel.DownloadAndEvalPageDataPointcutData} eco.artd.IFirebaseJoinpointModel.AfterThrowsDownloadAndEvalPageDataPointcutData The pointcut data passed to _afterThrows_ aspects. */

/** @typedef {eco.artd.IFirebaseJoinpointModel.DownloadAndEvalPageDataPointcutData} eco.artd.IFirebaseJoinpointModel.AfterReturnsDownloadAndEvalPageDataPointcutData The pointcut data passed to _afterReturns_ aspects. */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.AfterCancelsDownloadAndEvalPageDataPointcutData
 * @prop {!Set<string>} reasons The list of reasons why the method was cancelled.
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.AfterCancelsDownloadAndEvalPageDataPointcutData&eco.artd.IFirebaseJoinpointModel.DownloadAndEvalPageDataPointcutData} eco.artd.IFirebaseJoinpointModel.AfterCancelsDownloadAndEvalPageDataPointcutData The pointcut data passed to _afterCancels_ aspects. */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterDownloadAndEvalPageDataPointcutData
 * @prop {!Promise<void>} promise The promise with the return of the method after it's successfully run.
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterDownloadAndEvalPageDataPointcutData&eco.artd.IFirebaseJoinpointModel.DownloadAndEvalPageDataPointcutData} eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterDownloadAndEvalPageDataPointcutData The pointcut data passed to _immediatelyAfter_ aspects. */

/**
 * @typedef {Object} eco.artd.IFirebase.EvalBodyNArgs
 * @prop {string} body The config to run the method against.
 */

/**
 * @typedef {Object} eco.artd.IFirebaseJoinpointModel.EvalBodyPointcutData The general pointcut data.
 * @prop {symbol} ticket A symbol unique to the particular execution.
 * @prop {eco.artd.IFirebase.EvalBodyNArgs} args The arguments passed to the method converted to a lambda-n record.
 * @prop {!Function} proc Allows to work with the process.
 */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.BeforeEvalBodyPointcutData
 * @prop {(arg0: !Object<string, *>) => void} cond Conditions the object to the process.
 * @prop {(args: eco.artd.IFirebase.EvalBodyNArgs) => void} setArgs Allows to set or modify arguments passed to the method and futher aspects.
 * @prop {(reason?: string) => void} cancel Allows to stop the `evalBody` method from being executed.
 * @prop {(value: (arg0: !http.IncomingMessage, arg1: !http.ServerResponse) => (void|Promise<void>)) => void} sub Cancels a call to `evalBody` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.BeforeEvalBodyPointcutData&eco.artd.IFirebaseJoinpointModel.EvalBodyPointcutData} eco.artd.IFirebaseJoinpointModel.BeforeEvalBodyPointcutData The pointcut data passed to _before_ aspects. */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.AfterEvalBodyPointcutData
 * @prop {(arg0: !http.IncomingMessage, arg1: !http.ServerResponse) => (void|Promise<void>)} res The return of the method after it's successfully run.
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.AfterEvalBodyPointcutData&eco.artd.IFirebaseJoinpointModel.EvalBodyPointcutData} eco.artd.IFirebaseJoinpointModel.AfterEvalBodyPointcutData The pointcut data passed to _after_ aspects. */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.AfterThrowsEvalBodyPointcutData
 * @prop {!Error} err The error with which the method failed.
 * @prop {() => void} hide Hides the error in the `evalBody` (use only at the top-level of program flow).
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.AfterThrowsEvalBodyPointcutData&eco.artd.IFirebaseJoinpointModel.EvalBodyPointcutData} eco.artd.IFirebaseJoinpointModel.AfterThrowsEvalBodyPointcutData The pointcut data passed to _afterThrows_ aspects. */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.AfterReturnsEvalBodyPointcutData
 * @prop {(arg0: !http.IncomingMessage, arg1: !http.ServerResponse) => (void|Promise<void>)} res The return of the method after it's successfully run.
 * @prop {(value: (arg0: !http.IncomingMessage, arg1: !http.ServerResponse) => (void|Promise<void>)) => ?} sub Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.AfterReturnsEvalBodyPointcutData&eco.artd.IFirebaseJoinpointModel.EvalBodyPointcutData} eco.artd.IFirebaseJoinpointModel.AfterReturnsEvalBodyPointcutData The pointcut data passed to _afterReturns_ aspects. */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.AfterCancelsEvalBodyPointcutData
 * @prop {!Set<string>} reasons The list of reasons why the method was cancelled.
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.AfterCancelsEvalBodyPointcutData&eco.artd.IFirebaseJoinpointModel.EvalBodyPointcutData} eco.artd.IFirebaseJoinpointModel.AfterCancelsEvalBodyPointcutData The pointcut data passed to _afterCancels_ aspects. */

/**
 * @typedef {Object} eco.artd.IFirebase.LogRpcInvocationNArgs
 * @prop {!Date} dateTime The date.
 * @prop {eco.artd.IFirebase.Times} times The times.
 * @prop {eco.artd.IFirebase.Methods} methods The methods.
 * @prop {{ error: Error, res: null, req: null }} invRes
 */

/**
 * @typedef {Object} eco.artd.IFirebaseJoinpointModel.LogRpcInvocationPointcutData The general pointcut data.
 * @prop {symbol} ticket A symbol unique to the particular execution.
 * @prop {eco.artd.IFirebase.LogRpcInvocationNArgs} args The arguments passed to the method converted to a lambda-n record.
 * @prop {!Function} proc Allows to work with the process.
 */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.BeforeLogRpcInvocationPointcutData
 * @prop {(arg0: !Object<string, *>) => void} cond Conditions the object to the process.
 * @prop {(args: eco.artd.IFirebase.LogRpcInvocationNArgs) => void} setArgs Allows to set or modify arguments passed to the method and futher aspects.
 * @prop {(reason?: string) => void} cancel Allows to stop the `logRpcInvocation` method from being executed.
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.BeforeLogRpcInvocationPointcutData&eco.artd.IFirebaseJoinpointModel.LogRpcInvocationPointcutData} eco.artd.IFirebaseJoinpointModel.BeforeLogRpcInvocationPointcutData The pointcut data passed to _before_ aspects. */

/** @typedef {eco.artd.IFirebaseJoinpointModel.LogRpcInvocationPointcutData} eco.artd.IFirebaseJoinpointModel.AfterLogRpcInvocationPointcutData The pointcut data passed to _after_ aspects. */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.AfterThrowsLogRpcInvocationPointcutData
 * @prop {!Error} err The error with which the method failed.
 * @prop {() => void} hide Hides the error in the `logRpcInvocation` (use only at the top-level of program flow).
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.AfterThrowsLogRpcInvocationPointcutData&eco.artd.IFirebaseJoinpointModel.LogRpcInvocationPointcutData} eco.artd.IFirebaseJoinpointModel.AfterThrowsLogRpcInvocationPointcutData The pointcut data passed to _afterThrows_ aspects. */

/** @typedef {eco.artd.IFirebaseJoinpointModel.LogRpcInvocationPointcutData} eco.artd.IFirebaseJoinpointModel.AfterReturnsLogRpcInvocationPointcutData The pointcut data passed to _afterReturns_ aspects. */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.AfterCancelsLogRpcInvocationPointcutData
 * @prop {!Set<string>} reasons The list of reasons why the method was cancelled.
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.AfterCancelsLogRpcInvocationPointcutData&eco.artd.IFirebaseJoinpointModel.LogRpcInvocationPointcutData} eco.artd.IFirebaseJoinpointModel.AfterCancelsLogRpcInvocationPointcutData The pointcut data passed to _afterCancels_ aspects. */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterLogRpcInvocationPointcutData
 * @prop {!Promise<void>} promise The promise with the return of the method after it's successfully run.
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterLogRpcInvocationPointcutData&eco.artd.IFirebaseJoinpointModel.LogRpcInvocationPointcutData} eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterLogRpcInvocationPointcutData The pointcut data passed to _immediatelyAfter_ aspects. */

/**
 * @typedef {Object} eco.artd.IFirebase.FetchFunctionsFileNArgs
 * @prop {string} file
 */

/**
 * @typedef {Object} eco.artd.IFirebaseJoinpointModel.FetchFunctionsFilePointcutData The general pointcut data.
 * @prop {symbol} ticket A symbol unique to the particular execution.
 * @prop {eco.artd.IFirebase.FetchFunctionsFileNArgs} args The arguments passed to the method converted to a lambda-n record.
 * @prop {!Function} proc Allows to work with the process.
 */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.BeforeFetchFunctionsFilePointcutData
 * @prop {(arg0: !Object<string, *>) => void} cond Conditions the object to the process.
 * @prop {(args: eco.artd.IFirebase.FetchFunctionsFileNArgs) => void} setArgs Allows to set or modify arguments passed to the method and futher aspects.
 * @prop {(reason?: string) => void} cancel Allows to stop the `fetchFunctionsFile` method from being executed.
 * @prop {(value: !Promise<eco.artd.IFirebase.fetchFunctionsFile.Return>) => void} sub Cancels a call to `fetchFunctionsFile` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.BeforeFetchFunctionsFilePointcutData&eco.artd.IFirebaseJoinpointModel.FetchFunctionsFilePointcutData} eco.artd.IFirebaseJoinpointModel.BeforeFetchFunctionsFilePointcutData The pointcut data passed to _before_ aspects. */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.AfterFetchFunctionsFilePointcutData
 * @prop {eco.artd.IFirebase.fetchFunctionsFile.Return} res The return of the method after it's successfully run.
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.AfterFetchFunctionsFilePointcutData&eco.artd.IFirebaseJoinpointModel.FetchFunctionsFilePointcutData} eco.artd.IFirebaseJoinpointModel.AfterFetchFunctionsFilePointcutData The pointcut data passed to _after_ aspects. */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.AfterThrowsFetchFunctionsFilePointcutData
 * @prop {!Error} err The error with which the method failed.
 * @prop {() => void} hide Hides the error in the `fetchFunctionsFile` (use only at the top-level of program flow).
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.AfterThrowsFetchFunctionsFilePointcutData&eco.artd.IFirebaseJoinpointModel.FetchFunctionsFilePointcutData} eco.artd.IFirebaseJoinpointModel.AfterThrowsFetchFunctionsFilePointcutData The pointcut data passed to _afterThrows_ aspects. */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.AfterReturnsFetchFunctionsFilePointcutData
 * @prop {eco.artd.IFirebase.fetchFunctionsFile.Return} res The return of the method after it's successfully run.
 * @prop {(value: !Promise<eco.artd.IFirebase.fetchFunctionsFile.Return>|eco.artd.IFirebase.fetchFunctionsFile.Return) => ?} sub Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.AfterReturnsFetchFunctionsFilePointcutData&eco.artd.IFirebaseJoinpointModel.FetchFunctionsFilePointcutData} eco.artd.IFirebaseJoinpointModel.AfterReturnsFetchFunctionsFilePointcutData The pointcut data passed to _afterReturns_ aspects. */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.AfterCancelsFetchFunctionsFilePointcutData
 * @prop {!Set<string>} reasons The list of reasons why the method was cancelled.
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.AfterCancelsFetchFunctionsFilePointcutData&eco.artd.IFirebaseJoinpointModel.FetchFunctionsFilePointcutData} eco.artd.IFirebaseJoinpointModel.AfterCancelsFetchFunctionsFilePointcutData The pointcut data passed to _afterCancels_ aspects. */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterFetchFunctionsFilePointcutData
 * @prop {!Promise<eco.artd.IFirebase.fetchFunctionsFile.Return>} promise The promise with the return of the method after it's successfully run.
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterFetchFunctionsFilePointcutData&eco.artd.IFirebaseJoinpointModel.FetchFunctionsFilePointcutData} eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterFetchFunctionsFilePointcutData The pointcut data passed to _immediatelyAfter_ aspects. */

/**
 * @typedef {Object} eco.artd.IFirebase.DownloadDepsNArgs
 * @prop {!Array<string>} deps </prop>,<prop name="methodName" type="string">
 * @prop {number} id
 */

/**
 * @typedef {Object} eco.artd.IFirebaseJoinpointModel.DownloadDepsPointcutData The general pointcut data.
 * @prop {symbol} ticket A symbol unique to the particular execution.
 * @prop {eco.artd.IFirebase.DownloadDepsNArgs} args The arguments passed to the method converted to a lambda-n record.
 * @prop {!Function} proc Allows to work with the process.
 */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.BeforeDownloadDepsPointcutData
 * @prop {(arg0: !Object<string, *>) => void} cond Conditions the object to the process.
 * @prop {(args: eco.artd.IFirebase.DownloadDepsNArgs) => void} setArgs Allows to set or modify arguments passed to the method and futher aspects.
 * @prop {(reason?: string) => void} cancel Allows to stop the `downloadDeps` method from being executed.
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.BeforeDownloadDepsPointcutData&eco.artd.IFirebaseJoinpointModel.DownloadDepsPointcutData} eco.artd.IFirebaseJoinpointModel.BeforeDownloadDepsPointcutData The pointcut data passed to _before_ aspects. */

/** @typedef {eco.artd.IFirebaseJoinpointModel.DownloadDepsPointcutData} eco.artd.IFirebaseJoinpointModel.AfterDownloadDepsPointcutData The pointcut data passed to _after_ aspects. */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.AfterThrowsDownloadDepsPointcutData
 * @prop {!Error} err The error with which the method failed.
 * @prop {() => void} hide Hides the error in the `downloadDeps` (use only at the top-level of program flow).
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.AfterThrowsDownloadDepsPointcutData&eco.artd.IFirebaseJoinpointModel.DownloadDepsPointcutData} eco.artd.IFirebaseJoinpointModel.AfterThrowsDownloadDepsPointcutData The pointcut data passed to _afterThrows_ aspects. */

/** @typedef {eco.artd.IFirebaseJoinpointModel.DownloadDepsPointcutData} eco.artd.IFirebaseJoinpointModel.AfterReturnsDownloadDepsPointcutData The pointcut data passed to _afterReturns_ aspects. */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.AfterCancelsDownloadDepsPointcutData
 * @prop {!Set<string>} reasons The list of reasons why the method was cancelled.
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.AfterCancelsDownloadDepsPointcutData&eco.artd.IFirebaseJoinpointModel.DownloadDepsPointcutData} eco.artd.IFirebaseJoinpointModel.AfterCancelsDownloadDepsPointcutData The pointcut data passed to _afterCancels_ aspects. */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterDownloadDepsPointcutData
 * @prop {!Promise<void>} promise The promise with the return of the method after it's successfully run.
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterDownloadDepsPointcutData&eco.artd.IFirebaseJoinpointModel.DownloadDepsPointcutData} eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterDownloadDepsPointcutData The pointcut data passed to _immediatelyAfter_ aspects. */

/**
 * @typedef {Object} eco.artd.IFirebase.DownloadMethodNArgs
 * @prop {number} cid The id of the circuit.
 * @prop {string} methodName
 */

/**
 * @typedef {Object} eco.artd.IFirebaseJoinpointModel.DownloadMethodPointcutData The general pointcut data.
 * @prop {symbol} ticket A symbol unique to the particular execution.
 * @prop {eco.artd.IFirebase.DownloadMethodNArgs} args The arguments passed to the method converted to a lambda-n record.
 * @prop {!Function} proc Allows to work with the process.
 */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.BeforeDownloadMethodPointcutData
 * @prop {(arg0: !Object<string, *>) => void} cond Conditions the object to the process.
 * @prop {(args: eco.artd.IFirebase.DownloadMethodNArgs) => void} setArgs Allows to set or modify arguments passed to the method and futher aspects.
 * @prop {(reason?: string) => void} cancel Allows to stop the `downloadMethod` method from being executed.
 * @prop {(value: !Promise<string>) => void} sub Cancels a call to `downloadMethod` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.BeforeDownloadMethodPointcutData&eco.artd.IFirebaseJoinpointModel.DownloadMethodPointcutData} eco.artd.IFirebaseJoinpointModel.BeforeDownloadMethodPointcutData The pointcut data passed to _before_ aspects. */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.AfterDownloadMethodPointcutData
 * @prop {string} res The return of the method after it's successfully run.
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.AfterDownloadMethodPointcutData&eco.artd.IFirebaseJoinpointModel.DownloadMethodPointcutData} eco.artd.IFirebaseJoinpointModel.AfterDownloadMethodPointcutData The pointcut data passed to _after_ aspects. */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.AfterThrowsDownloadMethodPointcutData
 * @prop {!Error} err The error with which the method failed.
 * @prop {() => void} hide Hides the error in the `downloadMethod` (use only at the top-level of program flow).
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.AfterThrowsDownloadMethodPointcutData&eco.artd.IFirebaseJoinpointModel.DownloadMethodPointcutData} eco.artd.IFirebaseJoinpointModel.AfterThrowsDownloadMethodPointcutData The pointcut data passed to _afterThrows_ aspects. */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.AfterReturnsDownloadMethodPointcutData
 * @prop {string} res The return of the method after it's successfully run.
 * @prop {(value: !Promise<string>|string) => ?} sub Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.AfterReturnsDownloadMethodPointcutData&eco.artd.IFirebaseJoinpointModel.DownloadMethodPointcutData} eco.artd.IFirebaseJoinpointModel.AfterReturnsDownloadMethodPointcutData The pointcut data passed to _afterReturns_ aspects. */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.AfterCancelsDownloadMethodPointcutData
 * @prop {!Set<string>} reasons The list of reasons why the method was cancelled.
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.AfterCancelsDownloadMethodPointcutData&eco.artd.IFirebaseJoinpointModel.DownloadMethodPointcutData} eco.artd.IFirebaseJoinpointModel.AfterCancelsDownloadMethodPointcutData The pointcut data passed to _afterCancels_ aspects. */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterDownloadMethodPointcutData
 * @prop {!Promise<string>} promise The promise with the return of the method after it's successfully run.
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterDownloadMethodPointcutData&eco.artd.IFirebaseJoinpointModel.DownloadMethodPointcutData} eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterDownloadMethodPointcutData The pointcut data passed to _immediatelyAfter_ aspects. */

/**
 * @typedef {Object} eco.artd.IFirebase.DownloadMethodDirectlyNArgs
 * @prop {number} cid The id of the circuit.
 * @prop {string} methodId
 */

/**
 * @typedef {Object} eco.artd.IFirebaseJoinpointModel.DownloadMethodDirectlyPointcutData The general pointcut data.
 * @prop {symbol} ticket A symbol unique to the particular execution.
 * @prop {eco.artd.IFirebase.DownloadMethodDirectlyNArgs} args The arguments passed to the method converted to a lambda-n record.
 * @prop {!Function} proc Allows to work with the process.
 */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.BeforeDownloadMethodDirectlyPointcutData
 * @prop {(arg0: !Object<string, *>) => void} cond Conditions the object to the process.
 * @prop {(args: eco.artd.IFirebase.DownloadMethodDirectlyNArgs) => void} setArgs Allows to set or modify arguments passed to the method and futher aspects.
 * @prop {(reason?: string) => void} cancel Allows to stop the `downloadMethodDirectly` method from being executed.
 * @prop {(value: !Promise<eco.artd.IFirebase.downloadMethodDirectly.Return>) => void} sub Cancels a call to `downloadMethodDirectly` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.BeforeDownloadMethodDirectlyPointcutData&eco.artd.IFirebaseJoinpointModel.DownloadMethodDirectlyPointcutData} eco.artd.IFirebaseJoinpointModel.BeforeDownloadMethodDirectlyPointcutData The pointcut data passed to _before_ aspects. */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.AfterDownloadMethodDirectlyPointcutData
 * @prop {eco.artd.IFirebase.downloadMethodDirectly.Return} res The return of the method after it's successfully run.
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.AfterDownloadMethodDirectlyPointcutData&eco.artd.IFirebaseJoinpointModel.DownloadMethodDirectlyPointcutData} eco.artd.IFirebaseJoinpointModel.AfterDownloadMethodDirectlyPointcutData The pointcut data passed to _after_ aspects. */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.AfterThrowsDownloadMethodDirectlyPointcutData
 * @prop {!Error} err The error with which the method failed.
 * @prop {() => void} hide Hides the error in the `downloadMethodDirectly` (use only at the top-level of program flow).
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.AfterThrowsDownloadMethodDirectlyPointcutData&eco.artd.IFirebaseJoinpointModel.DownloadMethodDirectlyPointcutData} eco.artd.IFirebaseJoinpointModel.AfterThrowsDownloadMethodDirectlyPointcutData The pointcut data passed to _afterThrows_ aspects. */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.AfterReturnsDownloadMethodDirectlyPointcutData
 * @prop {eco.artd.IFirebase.downloadMethodDirectly.Return} res The return of the method after it's successfully run.
 * @prop {(value: !Promise<eco.artd.IFirebase.downloadMethodDirectly.Return>|eco.artd.IFirebase.downloadMethodDirectly.Return) => ?} sub Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.AfterReturnsDownloadMethodDirectlyPointcutData&eco.artd.IFirebaseJoinpointModel.DownloadMethodDirectlyPointcutData} eco.artd.IFirebaseJoinpointModel.AfterReturnsDownloadMethodDirectlyPointcutData The pointcut data passed to _afterReturns_ aspects. */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.AfterCancelsDownloadMethodDirectlyPointcutData
 * @prop {!Set<string>} reasons The list of reasons why the method was cancelled.
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.AfterCancelsDownloadMethodDirectlyPointcutData&eco.artd.IFirebaseJoinpointModel.DownloadMethodDirectlyPointcutData} eco.artd.IFirebaseJoinpointModel.AfterCancelsDownloadMethodDirectlyPointcutData The pointcut data passed to _afterCancels_ aspects. */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterDownloadMethodDirectlyPointcutData
 * @prop {!Promise<eco.artd.IFirebase.downloadMethodDirectly.Return>} promise The promise with the return of the method after it's successfully run.
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterDownloadMethodDirectlyPointcutData&eco.artd.IFirebaseJoinpointModel.DownloadMethodDirectlyPointcutData} eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterDownloadMethodDirectlyPointcutData The pointcut data passed to _immediatelyAfter_ aspects. */

/**
 * @typedef {Object} eco.artd.IFirebaseJoinpointModel.GetIdPointcutData The general pointcut data.
 * @prop {symbol} ticket A symbol unique to the particular execution.
 * @prop {!Function} proc Allows to work with the process.
 */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.BeforeGetIdPointcutData
 * @prop {(arg0: !Object<string, *>) => void} cond Conditions the object to the process.
 * @prop {(reason?: string) => void} cancel Allows to stop the `getId` method from being executed.
 * @prop {(value: number) => void} sub Cancels a call to `getId` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.BeforeGetIdPointcutData&eco.artd.IFirebaseJoinpointModel.GetIdPointcutData} eco.artd.IFirebaseJoinpointModel.BeforeGetIdPointcutData The pointcut data passed to _before_ aspects. */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.AfterGetIdPointcutData
 * @prop {number} res The return of the method after it's successfully run.
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.AfterGetIdPointcutData&eco.artd.IFirebaseJoinpointModel.GetIdPointcutData} eco.artd.IFirebaseJoinpointModel.AfterGetIdPointcutData The pointcut data passed to _after_ aspects. */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.AfterThrowsGetIdPointcutData
 * @prop {!Error} err The error with which the method failed.
 * @prop {() => void} hide Hides the error in the `getId` (use only at the top-level of program flow).
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.AfterThrowsGetIdPointcutData&eco.artd.IFirebaseJoinpointModel.GetIdPointcutData} eco.artd.IFirebaseJoinpointModel.AfterThrowsGetIdPointcutData The pointcut data passed to _afterThrows_ aspects. */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.AfterReturnsGetIdPointcutData
 * @prop {number} res The return of the method after it's successfully run.
 * @prop {(value: number) => ?} sub Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.AfterReturnsGetIdPointcutData&eco.artd.IFirebaseJoinpointModel.GetIdPointcutData} eco.artd.IFirebaseJoinpointModel.AfterReturnsGetIdPointcutData The pointcut data passed to _afterReturns_ aspects. */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.AfterCancelsGetIdPointcutData
 * @prop {!Set<string>} reasons The list of reasons why the method was cancelled.
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.AfterCancelsGetIdPointcutData&eco.artd.IFirebaseJoinpointModel.GetIdPointcutData} eco.artd.IFirebaseJoinpointModel.AfterCancelsGetIdPointcutData The pointcut data passed to _afterCancels_ aspects. */

/**
 * @typedef {Object} eco.artd.IFirebase.PatchDepsNArgs
 * @prop {string} body
 */

/**
 * @typedef {Object} eco.artd.IFirebaseJoinpointModel.PatchDepsPointcutData The general pointcut data.
 * @prop {symbol} ticket A symbol unique to the particular execution.
 * @prop {eco.artd.IFirebase.PatchDepsNArgs} args The arguments passed to the method converted to a lambda-n record.
 * @prop {!Function} proc Allows to work with the process.
 */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.BeforePatchDepsPointcutData
 * @prop {(arg0: !Object<string, *>) => void} cond Conditions the object to the process.
 * @prop {(args: eco.artd.IFirebase.PatchDepsNArgs) => void} setArgs Allows to set or modify arguments passed to the method and futher aspects.
 * @prop {(reason?: string) => void} cancel Allows to stop the `patchDeps` method from being executed.
 * @prop {(value: !Promise<eco.artd.IFirebase.patchDeps.Return>) => void} sub Cancels a call to `patchDeps` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.BeforePatchDepsPointcutData&eco.artd.IFirebaseJoinpointModel.PatchDepsPointcutData} eco.artd.IFirebaseJoinpointModel.BeforePatchDepsPointcutData The pointcut data passed to _before_ aspects. */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.AfterPatchDepsPointcutData
 * @prop {eco.artd.IFirebase.patchDeps.Return} res The return of the method after it's successfully run.
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.AfterPatchDepsPointcutData&eco.artd.IFirebaseJoinpointModel.PatchDepsPointcutData} eco.artd.IFirebaseJoinpointModel.AfterPatchDepsPointcutData The pointcut data passed to _after_ aspects. */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.AfterThrowsPatchDepsPointcutData
 * @prop {!Error} err The error with which the method failed.
 * @prop {() => void} hide Hides the error in the `patchDeps` (use only at the top-level of program flow).
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.AfterThrowsPatchDepsPointcutData&eco.artd.IFirebaseJoinpointModel.PatchDepsPointcutData} eco.artd.IFirebaseJoinpointModel.AfterThrowsPatchDepsPointcutData The pointcut data passed to _afterThrows_ aspects. */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.AfterReturnsPatchDepsPointcutData
 * @prop {eco.artd.IFirebase.patchDeps.Return} res The return of the method after it's successfully run.
 * @prop {(value: !Promise<eco.artd.IFirebase.patchDeps.Return>|eco.artd.IFirebase.patchDeps.Return) => ?} sub Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.AfterReturnsPatchDepsPointcutData&eco.artd.IFirebaseJoinpointModel.PatchDepsPointcutData} eco.artd.IFirebaseJoinpointModel.AfterReturnsPatchDepsPointcutData The pointcut data passed to _afterReturns_ aspects. */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.AfterCancelsPatchDepsPointcutData
 * @prop {!Set<string>} reasons The list of reasons why the method was cancelled.
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.AfterCancelsPatchDepsPointcutData&eco.artd.IFirebaseJoinpointModel.PatchDepsPointcutData} eco.artd.IFirebaseJoinpointModel.AfterCancelsPatchDepsPointcutData The pointcut data passed to _afterCancels_ aspects. */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterPatchDepsPointcutData
 * @prop {!Promise<eco.artd.IFirebase.patchDeps.Return>} promise The promise with the return of the method after it's successfully run.
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterPatchDepsPointcutData&eco.artd.IFirebaseJoinpointModel.PatchDepsPointcutData} eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterPatchDepsPointcutData The pointcut data passed to _immediatelyAfter_ aspects. */

/**
 * @typedef {Object} eco.artd.IFirebase.DownloadPageDataNArgs
 * @prop {number} cid
 */

/**
 * @typedef {Object} eco.artd.IFirebaseJoinpointModel.DownloadPageDataPointcutData The general pointcut data.
 * @prop {symbol} ticket A symbol unique to the particular execution.
 * @prop {eco.artd.IFirebase.DownloadPageDataNArgs} args The arguments passed to the method converted to a lambda-n record.
 * @prop {!Function} proc Allows to work with the process.
 */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.BeforeDownloadPageDataPointcutData
 * @prop {(arg0: !Object<string, *>) => void} cond Conditions the object to the process.
 * @prop {(args: eco.artd.IFirebase.DownloadPageDataNArgs) => void} setArgs Allows to set or modify arguments passed to the method and futher aspects.
 * @prop {(reason?: string) => void} cancel Allows to stop the `downloadPageData` method from being executed.
 * @prop {(value: !Promise<string>) => void} sub Cancels a call to `downloadPageData` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.BeforeDownloadPageDataPointcutData&eco.artd.IFirebaseJoinpointModel.DownloadPageDataPointcutData} eco.artd.IFirebaseJoinpointModel.BeforeDownloadPageDataPointcutData The pointcut data passed to _before_ aspects. */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.AfterDownloadPageDataPointcutData
 * @prop {string} res The return of the method after it's successfully run.
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.AfterDownloadPageDataPointcutData&eco.artd.IFirebaseJoinpointModel.DownloadPageDataPointcutData} eco.artd.IFirebaseJoinpointModel.AfterDownloadPageDataPointcutData The pointcut data passed to _after_ aspects. */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.AfterThrowsDownloadPageDataPointcutData
 * @prop {!Error} err The error with which the method failed.
 * @prop {() => void} hide Hides the error in the `downloadPageData` (use only at the top-level of program flow).
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.AfterThrowsDownloadPageDataPointcutData&eco.artd.IFirebaseJoinpointModel.DownloadPageDataPointcutData} eco.artd.IFirebaseJoinpointModel.AfterThrowsDownloadPageDataPointcutData The pointcut data passed to _afterThrows_ aspects. */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.AfterReturnsDownloadPageDataPointcutData
 * @prop {string} res The return of the method after it's successfully run.
 * @prop {(value: !Promise<string>|string) => ?} sub Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.AfterReturnsDownloadPageDataPointcutData&eco.artd.IFirebaseJoinpointModel.DownloadPageDataPointcutData} eco.artd.IFirebaseJoinpointModel.AfterReturnsDownloadPageDataPointcutData The pointcut data passed to _afterReturns_ aspects. */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.AfterCancelsDownloadPageDataPointcutData
 * @prop {!Set<string>} reasons The list of reasons why the method was cancelled.
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.AfterCancelsDownloadPageDataPointcutData&eco.artd.IFirebaseJoinpointModel.DownloadPageDataPointcutData} eco.artd.IFirebaseJoinpointModel.AfterCancelsDownloadPageDataPointcutData The pointcut data passed to _afterCancels_ aspects. */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterDownloadPageDataPointcutData
 * @prop {!Promise<string>} promise The promise with the return of the method after it's successfully run.
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterDownloadPageDataPointcutData&eco.artd.IFirebaseJoinpointModel.DownloadPageDataPointcutData} eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterDownloadPageDataPointcutData The pointcut data passed to _immediatelyAfter_ aspects. */

/**
 * @typedef {Object} eco.artd.IFirebase.LoadRequirePageDataNArgs
 * @prop {number} cid
 */

/**
 * @typedef {Object} eco.artd.IFirebaseJoinpointModel.LoadRequirePageDataPointcutData The general pointcut data.
 * @prop {symbol} ticket A symbol unique to the particular execution.
 * @prop {eco.artd.IFirebase.LoadRequirePageDataNArgs} args The arguments passed to the method converted to a lambda-n record.
 * @prop {!Function} proc Allows to work with the process.
 */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.BeforeLoadRequirePageDataPointcutData
 * @prop {(arg0: !Object<string, *>) => void} cond Conditions the object to the process.
 * @prop {(args: eco.artd.IFirebase.LoadRequirePageDataNArgs) => void} setArgs Allows to set or modify arguments passed to the method and futher aspects.
 * @prop {(reason?: string) => void} cancel Allows to stop the `loadRequirePageData` method from being executed.
 * @prop {(value: !eco.artd.IFirebase.PageData) => void} sub Cancels a call to `loadRequirePageData` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.BeforeLoadRequirePageDataPointcutData&eco.artd.IFirebaseJoinpointModel.LoadRequirePageDataPointcutData} eco.artd.IFirebaseJoinpointModel.BeforeLoadRequirePageDataPointcutData The pointcut data passed to _before_ aspects. */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.AfterLoadRequirePageDataPointcutData
 * @prop {!eco.artd.IFirebase.PageData} res The return of the method after it's successfully run.
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.AfterLoadRequirePageDataPointcutData&eco.artd.IFirebaseJoinpointModel.LoadRequirePageDataPointcutData} eco.artd.IFirebaseJoinpointModel.AfterLoadRequirePageDataPointcutData The pointcut data passed to _after_ aspects. */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.AfterThrowsLoadRequirePageDataPointcutData
 * @prop {!Error} err The error with which the method failed.
 * @prop {() => void} hide Hides the error in the `loadRequirePageData` (use only at the top-level of program flow).
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.AfterThrowsLoadRequirePageDataPointcutData&eco.artd.IFirebaseJoinpointModel.LoadRequirePageDataPointcutData} eco.artd.IFirebaseJoinpointModel.AfterThrowsLoadRequirePageDataPointcutData The pointcut data passed to _afterThrows_ aspects. */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.AfterReturnsLoadRequirePageDataPointcutData
 * @prop {!eco.artd.IFirebase.PageData} res The return of the method after it's successfully run.
 * @prop {(value: !eco.artd.IFirebase.PageData) => ?} sub Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.AfterReturnsLoadRequirePageDataPointcutData&eco.artd.IFirebaseJoinpointModel.LoadRequirePageDataPointcutData} eco.artd.IFirebaseJoinpointModel.AfterReturnsLoadRequirePageDataPointcutData The pointcut data passed to _afterReturns_ aspects. */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.AfterCancelsLoadRequirePageDataPointcutData
 * @prop {!Set<string>} reasons The list of reasons why the method was cancelled.
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.AfterCancelsLoadRequirePageDataPointcutData&eco.artd.IFirebaseJoinpointModel.LoadRequirePageDataPointcutData} eco.artd.IFirebaseJoinpointModel.AfterCancelsLoadRequirePageDataPointcutData The pointcut data passed to _afterCancels_ aspects. */

/**
 * @typedef {Object} eco.artd.IFirebase.LoadRequireNArgs
 * @prop {string} path </prop>,<prop name="dir" type="string">
 */

/**
 * @typedef {Object} eco.artd.IFirebaseJoinpointModel.LoadRequirePointcutData The general pointcut data.
 * @prop {symbol} ticket A symbol unique to the particular execution.
 * @prop {eco.artd.IFirebase.LoadRequireNArgs} args The arguments passed to the method converted to a lambda-n record.
 * @prop {!Function} proc Allows to work with the process.
 */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.BeforeLoadRequirePointcutData
 * @prop {(arg0: !Object<string, *>) => void} cond Conditions the object to the process.
 * @prop {(args: eco.artd.IFirebase.LoadRequireNArgs) => void} setArgs Allows to set or modify arguments passed to the method and futher aspects.
 * @prop {(reason?: string) => void} cancel Allows to stop the `loadRequire` method from being executed.
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.BeforeLoadRequirePointcutData&eco.artd.IFirebaseJoinpointModel.LoadRequirePointcutData} eco.artd.IFirebaseJoinpointModel.BeforeLoadRequirePointcutData The pointcut data passed to _before_ aspects. */

/** @typedef {eco.artd.IFirebaseJoinpointModel.LoadRequirePointcutData} eco.artd.IFirebaseJoinpointModel.AfterLoadRequirePointcutData The pointcut data passed to _after_ aspects. */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.AfterThrowsLoadRequirePointcutData
 * @prop {!Error} err The error with which the method failed.
 * @prop {() => void} hide Hides the error in the `loadRequire` (use only at the top-level of program flow).
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.AfterThrowsLoadRequirePointcutData&eco.artd.IFirebaseJoinpointModel.LoadRequirePointcutData} eco.artd.IFirebaseJoinpointModel.AfterThrowsLoadRequirePointcutData The pointcut data passed to _afterThrows_ aspects. */

/** @typedef {eco.artd.IFirebaseJoinpointModel.LoadRequirePointcutData} eco.artd.IFirebaseJoinpointModel.AfterReturnsLoadRequirePointcutData The pointcut data passed to _afterReturns_ aspects. */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.AfterCancelsLoadRequirePointcutData
 * @prop {!Set<string>} reasons The list of reasons why the method was cancelled.
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.AfterCancelsLoadRequirePointcutData&eco.artd.IFirebaseJoinpointModel.LoadRequirePointcutData} eco.artd.IFirebaseJoinpointModel.AfterCancelsLoadRequirePointcutData The pointcut data passed to _afterCancels_ aspects. */

/**
 * @typedef {Object} eco.artd.IFirebase.UpdateErrorStackNArgs
 * @prop {!Error} error The error.
 * @prop {string} url The url from which the method code was downloaded.
 */

/**
 * @typedef {Object} eco.artd.IFirebaseJoinpointModel.UpdateErrorStackPointcutData The general pointcut data.
 * @prop {symbol} ticket A symbol unique to the particular execution.
 * @prop {eco.artd.IFirebase.UpdateErrorStackNArgs} args The arguments passed to the method converted to a lambda-n record.
 * @prop {!Function} proc Allows to work with the process.
 */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.BeforeUpdateErrorStackPointcutData
 * @prop {(arg0: !Object<string, *>) => void} cond Conditions the object to the process.
 * @prop {(args: eco.artd.IFirebase.UpdateErrorStackNArgs) => void} setArgs Allows to set or modify arguments passed to the method and futher aspects.
 * @prop {(reason?: string) => void} cancel Allows to stop the `updateErrorStack` method from being executed.
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.BeforeUpdateErrorStackPointcutData&eco.artd.IFirebaseJoinpointModel.UpdateErrorStackPointcutData} eco.artd.IFirebaseJoinpointModel.BeforeUpdateErrorStackPointcutData The pointcut data passed to _before_ aspects. */

/** @typedef {eco.artd.IFirebaseJoinpointModel.UpdateErrorStackPointcutData} eco.artd.IFirebaseJoinpointModel.AfterUpdateErrorStackPointcutData The pointcut data passed to _after_ aspects. */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.AfterThrowsUpdateErrorStackPointcutData
 * @prop {!Error} err The error with which the method failed.
 * @prop {() => void} hide Hides the error in the `updateErrorStack` (use only at the top-level of program flow).
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.AfterThrowsUpdateErrorStackPointcutData&eco.artd.IFirebaseJoinpointModel.UpdateErrorStackPointcutData} eco.artd.IFirebaseJoinpointModel.AfterThrowsUpdateErrorStackPointcutData The pointcut data passed to _afterThrows_ aspects. */

/** @typedef {eco.artd.IFirebaseJoinpointModel.UpdateErrorStackPointcutData} eco.artd.IFirebaseJoinpointModel.AfterReturnsUpdateErrorStackPointcutData The pointcut data passed to _afterReturns_ aspects. */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.AfterCancelsUpdateErrorStackPointcutData
 * @prop {!Set<string>} reasons The list of reasons why the method was cancelled.
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.AfterCancelsUpdateErrorStackPointcutData&eco.artd.IFirebaseJoinpointModel.UpdateErrorStackPointcutData} eco.artd.IFirebaseJoinpointModel.AfterCancelsUpdateErrorStackPointcutData The pointcut data passed to _afterCancels_ aspects. */

/**
 * @typedef {Object} eco.artd.IFirebase.ValidateMethodNArgs
 * @prop {Map<string, string>} methodsMap If not passed, jsut returns method from the headers.
 */

/**
 * @typedef {Object} eco.artd.IFirebaseJoinpointModel.ValidateMethodPointcutData The general pointcut data.
 * @prop {symbol} ticket A symbol unique to the particular execution.
 * @prop {eco.artd.IFirebase.ValidateMethodNArgs} args The arguments passed to the method converted to a lambda-n record.
 * @prop {!Function} proc Allows to work with the process.
 */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.BeforeValidateMethodPointcutData
 * @prop {(arg0: !Object<string, *>) => void} cond Conditions the object to the process.
 * @prop {(args: eco.artd.IFirebase.ValidateMethodNArgs) => void} setArgs Allows to set or modify arguments passed to the method and futher aspects.
 * @prop {(reason?: string) => void} cancel Allows to stop the `validateMethod` method from being executed.
 * @prop {(value: string) => void} sub Cancels a call to `validateMethod` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.BeforeValidateMethodPointcutData&eco.artd.IFirebaseJoinpointModel.ValidateMethodPointcutData} eco.artd.IFirebaseJoinpointModel.BeforeValidateMethodPointcutData The pointcut data passed to _before_ aspects. */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.AfterValidateMethodPointcutData
 * @prop {string} res The return of the method after it's successfully run.
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.AfterValidateMethodPointcutData&eco.artd.IFirebaseJoinpointModel.ValidateMethodPointcutData} eco.artd.IFirebaseJoinpointModel.AfterValidateMethodPointcutData The pointcut data passed to _after_ aspects. */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.AfterThrowsValidateMethodPointcutData
 * @prop {!Error} err The error with which the method failed.
 * @prop {() => void} hide Hides the error in the `validateMethod` (use only at the top-level of program flow).
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.AfterThrowsValidateMethodPointcutData&eco.artd.IFirebaseJoinpointModel.ValidateMethodPointcutData} eco.artd.IFirebaseJoinpointModel.AfterThrowsValidateMethodPointcutData The pointcut data passed to _afterThrows_ aspects. */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.AfterReturnsValidateMethodPointcutData
 * @prop {string} res The return of the method after it's successfully run.
 * @prop {(value: string) => ?} sub Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.AfterReturnsValidateMethodPointcutData&eco.artd.IFirebaseJoinpointModel.ValidateMethodPointcutData} eco.artd.IFirebaseJoinpointModel.AfterReturnsValidateMethodPointcutData The pointcut data passed to _afterReturns_ aspects. */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.AfterCancelsValidateMethodPointcutData
 * @prop {!Set<string>} reasons The list of reasons why the method was cancelled.
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.AfterCancelsValidateMethodPointcutData&eco.artd.IFirebaseJoinpointModel.ValidateMethodPointcutData} eco.artd.IFirebaseJoinpointModel.AfterCancelsValidateMethodPointcutData The pointcut data passed to _afterCancels_ aspects. */

/**
 * @typedef {Object} eco.artd.IFirebase.ValidateLoadAndInvokeNArgs
 * @prop {!Object<string, eco.artd.IFirebase.PageData>} pageData </prop>,<prop name="id" type="number">
 */

/**
 * @typedef {Object} eco.artd.IFirebaseJoinpointModel.ValidateLoadAndInvokePointcutData The general pointcut data.
 * @prop {symbol} ticket A symbol unique to the particular execution.
 * @prop {eco.artd.IFirebase.ValidateLoadAndInvokeNArgs} args The arguments passed to the method converted to a lambda-n record.
 * @prop {!Function} proc Allows to work with the process.
 */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.BeforeValidateLoadAndInvokePointcutData
 * @prop {(arg0: !Object<string, *>) => void} cond Conditions the object to the process.
 * @prop {(args: eco.artd.IFirebase.ValidateLoadAndInvokeNArgs) => void} setArgs Allows to set or modify arguments passed to the method and futher aspects.
 * @prop {(reason?: string) => void} cancel Allows to stop the `validateLoadAndInvoke` method from being executed.
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.BeforeValidateLoadAndInvokePointcutData&eco.artd.IFirebaseJoinpointModel.ValidateLoadAndInvokePointcutData} eco.artd.IFirebaseJoinpointModel.BeforeValidateLoadAndInvokePointcutData The pointcut data passed to _before_ aspects. */

/** @typedef {eco.artd.IFirebaseJoinpointModel.ValidateLoadAndInvokePointcutData} eco.artd.IFirebaseJoinpointModel.AfterValidateLoadAndInvokePointcutData The pointcut data passed to _after_ aspects. */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.AfterThrowsValidateLoadAndInvokePointcutData
 * @prop {!Error} err The error with which the method failed.
 * @prop {() => void} hide Hides the error in the `validateLoadAndInvoke` (use only at the top-level of program flow).
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.AfterThrowsValidateLoadAndInvokePointcutData&eco.artd.IFirebaseJoinpointModel.ValidateLoadAndInvokePointcutData} eco.artd.IFirebaseJoinpointModel.AfterThrowsValidateLoadAndInvokePointcutData The pointcut data passed to _afterThrows_ aspects. */

/** @typedef {eco.artd.IFirebaseJoinpointModel.ValidateLoadAndInvokePointcutData} eco.artd.IFirebaseJoinpointModel.AfterReturnsValidateLoadAndInvokePointcutData The pointcut data passed to _afterReturns_ aspects. */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.AfterCancelsValidateLoadAndInvokePointcutData
 * @prop {!Set<string>} reasons The list of reasons why the method was cancelled.
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.AfterCancelsValidateLoadAndInvokePointcutData&eco.artd.IFirebaseJoinpointModel.ValidateLoadAndInvokePointcutData} eco.artd.IFirebaseJoinpointModel.AfterCancelsValidateLoadAndInvokePointcutData The pointcut data passed to _afterCancels_ aspects. */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterValidateLoadAndInvokePointcutData
 * @prop {!Promise<void>} promise The promise with the return of the method after it's successfully run.
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterValidateLoadAndInvokePointcutData&eco.artd.IFirebaseJoinpointModel.ValidateLoadAndInvokePointcutData} eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterValidateLoadAndInvokePointcutData The pointcut data passed to _immediatelyAfter_ aspects. */

/**
 * @typedef {Object} eco.artd.IFirebase.ValidateLoadAndInvokeByIdNArgs
 * @prop {number} cid
 */

/**
 * @typedef {Object} eco.artd.IFirebaseJoinpointModel.ValidateLoadAndInvokeByIdPointcutData The general pointcut data.
 * @prop {symbol} ticket A symbol unique to the particular execution.
 * @prop {eco.artd.IFirebase.ValidateLoadAndInvokeByIdNArgs} args The arguments passed to the method converted to a lambda-n record.
 * @prop {!Function} proc Allows to work with the process.
 */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.BeforeValidateLoadAndInvokeByIdPointcutData
 * @prop {(arg0: !Object<string, *>) => void} cond Conditions the object to the process.
 * @prop {(args: eco.artd.IFirebase.ValidateLoadAndInvokeByIdNArgs) => void} setArgs Allows to set or modify arguments passed to the method and futher aspects.
 * @prop {(reason?: string) => void} cancel Allows to stop the `validateLoadAndInvokeById` method from being executed.
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.BeforeValidateLoadAndInvokeByIdPointcutData&eco.artd.IFirebaseJoinpointModel.ValidateLoadAndInvokeByIdPointcutData} eco.artd.IFirebaseJoinpointModel.BeforeValidateLoadAndInvokeByIdPointcutData The pointcut data passed to _before_ aspects. */

/** @typedef {eco.artd.IFirebaseJoinpointModel.ValidateLoadAndInvokeByIdPointcutData} eco.artd.IFirebaseJoinpointModel.AfterValidateLoadAndInvokeByIdPointcutData The pointcut data passed to _after_ aspects. */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.AfterThrowsValidateLoadAndInvokeByIdPointcutData
 * @prop {!Error} err The error with which the method failed.
 * @prop {() => void} hide Hides the error in the `validateLoadAndInvokeById` (use only at the top-level of program flow).
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.AfterThrowsValidateLoadAndInvokeByIdPointcutData&eco.artd.IFirebaseJoinpointModel.ValidateLoadAndInvokeByIdPointcutData} eco.artd.IFirebaseJoinpointModel.AfterThrowsValidateLoadAndInvokeByIdPointcutData The pointcut data passed to _afterThrows_ aspects. */

/** @typedef {eco.artd.IFirebaseJoinpointModel.ValidateLoadAndInvokeByIdPointcutData} eco.artd.IFirebaseJoinpointModel.AfterReturnsValidateLoadAndInvokeByIdPointcutData The pointcut data passed to _afterReturns_ aspects. */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.AfterCancelsValidateLoadAndInvokeByIdPointcutData
 * @prop {!Set<string>} reasons The list of reasons why the method was cancelled.
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.AfterCancelsValidateLoadAndInvokeByIdPointcutData&eco.artd.IFirebaseJoinpointModel.ValidateLoadAndInvokeByIdPointcutData} eco.artd.IFirebaseJoinpointModel.AfterCancelsValidateLoadAndInvokeByIdPointcutData The pointcut data passed to _afterCancels_ aspects. */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterValidateLoadAndInvokeByIdPointcutData
 * @prop {!Promise<void>} promise The promise with the return of the method after it's successfully run.
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterValidateLoadAndInvokeByIdPointcutData&eco.artd.IFirebaseJoinpointModel.ValidateLoadAndInvokeByIdPointcutData} eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterValidateLoadAndInvokeByIdPointcutData The pointcut data passed to _immediatelyAfter_ aspects. */

/**
 * @typedef {Object} eco.artd.IFirebase.MakeInvokerNArgs
 * @prop {number} id
 */

/**
 * @typedef {Object} eco.artd.IFirebaseJoinpointModel.MakeInvokerPointcutData The general pointcut data.
 * @prop {symbol} ticket A symbol unique to the particular execution.
 * @prop {eco.artd.IFirebase.MakeInvokerNArgs} args The arguments passed to the method converted to a lambda-n record.
 * @prop {!Function} proc Allows to work with the process.
 */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.BeforeMakeInvokerPointcutData
 * @prop {(arg0: !Object<string, *>) => void} cond Conditions the object to the process.
 * @prop {(args: eco.artd.IFirebase.MakeInvokerNArgs) => void} setArgs Allows to set or modify arguments passed to the method and futher aspects.
 * @prop {(reason?: string) => void} cancel Allows to stop the `makeInvoker` method from being executed.
 * @prop {(value: () => ?) => void} sub Cancels a call to `makeInvoker` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.BeforeMakeInvokerPointcutData&eco.artd.IFirebaseJoinpointModel.MakeInvokerPointcutData} eco.artd.IFirebaseJoinpointModel.BeforeMakeInvokerPointcutData The pointcut data passed to _before_ aspects. */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.AfterMakeInvokerPointcutData
 * @prop {() => ?} res The return of the method after it's successfully run.
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.AfterMakeInvokerPointcutData&eco.artd.IFirebaseJoinpointModel.MakeInvokerPointcutData} eco.artd.IFirebaseJoinpointModel.AfterMakeInvokerPointcutData The pointcut data passed to _after_ aspects. */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.AfterThrowsMakeInvokerPointcutData
 * @prop {!Error} err The error with which the method failed.
 * @prop {() => void} hide Hides the error in the `makeInvoker` (use only at the top-level of program flow).
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.AfterThrowsMakeInvokerPointcutData&eco.artd.IFirebaseJoinpointModel.MakeInvokerPointcutData} eco.artd.IFirebaseJoinpointModel.AfterThrowsMakeInvokerPointcutData The pointcut data passed to _afterThrows_ aspects. */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.AfterReturnsMakeInvokerPointcutData
 * @prop {() => ?} res The return of the method after it's successfully run.
 * @prop {(value: () => ?) => ?} sub Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.AfterReturnsMakeInvokerPointcutData&eco.artd.IFirebaseJoinpointModel.MakeInvokerPointcutData} eco.artd.IFirebaseJoinpointModel.AfterReturnsMakeInvokerPointcutData The pointcut data passed to _afterReturns_ aspects. */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.AfterCancelsMakeInvokerPointcutData
 * @prop {!Set<string>} reasons The list of reasons why the method was cancelled.
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.AfterCancelsMakeInvokerPointcutData&eco.artd.IFirebaseJoinpointModel.MakeInvokerPointcutData} eco.artd.IFirebaseJoinpointModel.AfterCancelsMakeInvokerPointcutData The pointcut data passed to _afterCancels_ aspects. */

/**
 * @typedef {Object} eco.artd.IFirebase.LoadAndInvokeByIdNArgs
 * @prop {number} cid </prop>,<prop name="mid" type="string">
 */

/**
 * @typedef {Object} eco.artd.IFirebaseJoinpointModel.LoadAndInvokeByIdPointcutData The general pointcut data.
 * @prop {symbol} ticket A symbol unique to the particular execution.
 * @prop {eco.artd.IFirebase.LoadAndInvokeByIdNArgs} args The arguments passed to the method converted to a lambda-n record.
 * @prop {!Function} proc Allows to work with the process.
 */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.BeforeLoadAndInvokeByIdPointcutData
 * @prop {(arg0: !Object<string, *>) => void} cond Conditions the object to the process.
 * @prop {(args: eco.artd.IFirebase.LoadAndInvokeByIdNArgs) => void} setArgs Allows to set or modify arguments passed to the method and futher aspects.
 * @prop {(reason?: string) => void} cancel Allows to stop the `loadAndInvokeById` method from being executed.
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.BeforeLoadAndInvokeByIdPointcutData&eco.artd.IFirebaseJoinpointModel.LoadAndInvokeByIdPointcutData} eco.artd.IFirebaseJoinpointModel.BeforeLoadAndInvokeByIdPointcutData The pointcut data passed to _before_ aspects. */

/** @typedef {eco.artd.IFirebaseJoinpointModel.LoadAndInvokeByIdPointcutData} eco.artd.IFirebaseJoinpointModel.AfterLoadAndInvokeByIdPointcutData The pointcut data passed to _after_ aspects. */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.AfterThrowsLoadAndInvokeByIdPointcutData
 * @prop {!Error} err The error with which the method failed.
 * @prop {() => void} hide Hides the error in the `loadAndInvokeById` (use only at the top-level of program flow).
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.AfterThrowsLoadAndInvokeByIdPointcutData&eco.artd.IFirebaseJoinpointModel.LoadAndInvokeByIdPointcutData} eco.artd.IFirebaseJoinpointModel.AfterThrowsLoadAndInvokeByIdPointcutData The pointcut data passed to _afterThrows_ aspects. */

/** @typedef {eco.artd.IFirebaseJoinpointModel.LoadAndInvokeByIdPointcutData} eco.artd.IFirebaseJoinpointModel.AfterReturnsLoadAndInvokeByIdPointcutData The pointcut data passed to _afterReturns_ aspects. */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.AfterCancelsLoadAndInvokeByIdPointcutData
 * @prop {!Set<string>} reasons The list of reasons why the method was cancelled.
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.AfterCancelsLoadAndInvokeByIdPointcutData&eco.artd.IFirebaseJoinpointModel.LoadAndInvokeByIdPointcutData} eco.artd.IFirebaseJoinpointModel.AfterCancelsLoadAndInvokeByIdPointcutData The pointcut data passed to _afterCancels_ aspects. */

/**
 * @typedef {Object} eco.artd.IFirebase.LoadAndInvokeNArgs
 * @prop {string} methodId </prop>,<prop name="pageData" type="!Object<string,IFirebase.PageData>">
 * @prop {number} id
 */

/**
 * @typedef {Object} eco.artd.IFirebaseJoinpointModel.LoadAndInvokePointcutData The general pointcut data.
 * @prop {symbol} ticket A symbol unique to the particular execution.
 * @prop {eco.artd.IFirebase.LoadAndInvokeNArgs} args The arguments passed to the method converted to a lambda-n record.
 * @prop {!Function} proc Allows to work with the process.
 */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.BeforeLoadAndInvokePointcutData
 * @prop {(arg0: !Object<string, *>) => void} cond Conditions the object to the process.
 * @prop {(args: eco.artd.IFirebase.LoadAndInvokeNArgs) => void} setArgs Allows to set or modify arguments passed to the method and futher aspects.
 * @prop {(reason?: string) => void} cancel Allows to stop the `loadAndInvoke` method from being executed.
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.BeforeLoadAndInvokePointcutData&eco.artd.IFirebaseJoinpointModel.LoadAndInvokePointcutData} eco.artd.IFirebaseJoinpointModel.BeforeLoadAndInvokePointcutData The pointcut data passed to _before_ aspects. */

/** @typedef {eco.artd.IFirebaseJoinpointModel.LoadAndInvokePointcutData} eco.artd.IFirebaseJoinpointModel.AfterLoadAndInvokePointcutData The pointcut data passed to _after_ aspects. */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.AfterThrowsLoadAndInvokePointcutData
 * @prop {!Error} err The error with which the method failed.
 * @prop {() => void} hide Hides the error in the `loadAndInvoke` (use only at the top-level of program flow).
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.AfterThrowsLoadAndInvokePointcutData&eco.artd.IFirebaseJoinpointModel.LoadAndInvokePointcutData} eco.artd.IFirebaseJoinpointModel.AfterThrowsLoadAndInvokePointcutData The pointcut data passed to _afterThrows_ aspects. */

/** @typedef {eco.artd.IFirebaseJoinpointModel.LoadAndInvokePointcutData} eco.artd.IFirebaseJoinpointModel.AfterReturnsLoadAndInvokePointcutData The pointcut data passed to _afterReturns_ aspects. */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.AfterCancelsLoadAndInvokePointcutData
 * @prop {!Set<string>} reasons The list of reasons why the method was cancelled.
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.AfterCancelsLoadAndInvokePointcutData&eco.artd.IFirebaseJoinpointModel.LoadAndInvokePointcutData} eco.artd.IFirebaseJoinpointModel.AfterCancelsLoadAndInvokePointcutData The pointcut data passed to _afterCancels_ aspects. */

/**
 * @typedef {Object} $eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterLoadAndInvokePointcutData
 * @prop {!Promise<void>} promise The promise with the return of the method after it's successfully run.
 */
/** @typedef {$eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterLoadAndInvokePointcutData&eco.artd.IFirebaseJoinpointModel.LoadAndInvokePointcutData} eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterLoadAndInvokePointcutData The pointcut data passed to _immediatelyAfter_ aspects. */

/** @typedef {new (...args: !eco.artd.IFirebase.Initialese[]) => eco.artd.IFirebase} eco.artd.FirebaseConstructor */

/** @typedef {function(new: eco.artd.BFirebaseAspectsCaster<THIS>&eco.artd.IFirebaseJoinpointModelBindingHyperslice<THIS>)} eco.artd.BFirebaseAspects.constructor */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModelBindingHyperslice} eco.artd.IFirebaseJoinpointModelBindingHyperslice.typeof */
/**
 * The aspects of the *IFirebase* that bind to an instance.
 * @interface eco.artd.BFirebaseAspects
 * @template THIS
 */
eco.artd.BFirebaseAspects = class extends /** @type {eco.artd.BFirebaseAspects.constructor&eco.artd.IFirebaseJoinpointModelBindingHyperslice.typeof} */ (class {}) { }
eco.artd.BFirebaseAspects.prototype.constructor = eco.artd.BFirebaseAspects

/** @typedef {typeof __$te_plain} eco.artd.IFirebaseAspects.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: eco.artd.FirebaseAspects)} eco.artd.AbstractFirebaseAspects.constructor */
/** @typedef {typeof eco.artd.FirebaseAspects} eco.artd.FirebaseAspects.typeof */
/**
 * An abstract class of `eco.artd.IFirebaseAspects` interface.
 * @constructor eco.artd.AbstractFirebaseAspects
 */
eco.artd.AbstractFirebaseAspects = class extends /** @type {eco.artd.AbstractFirebaseAspects.constructor&eco.artd.FirebaseAspects.typeof} */ (class {}) { }
eco.artd.AbstractFirebaseAspects.prototype.constructor = eco.artd.AbstractFirebaseAspects
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
eco.artd.AbstractFirebaseAspects.class = /** @type {typeof eco.artd.AbstractFirebaseAspects} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!eco.artd.IFirebaseAspects|typeof eco.artd.FirebaseAspects)|(!eco.artd.BFirebaseAspects|typeof eco.artd.BFirebaseAspects)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof eco.artd.FirebaseAspects}
 * @nosideeffects
 */
eco.artd.AbstractFirebaseAspects.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof eco.artd.AbstractFirebaseAspects}
 */
eco.artd.AbstractFirebaseAspects.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof eco.artd.FirebaseAspects}
 */
eco.artd.AbstractFirebaseAspects.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!eco.artd.IFirebaseAspects|typeof eco.artd.FirebaseAspects)|(!eco.artd.BFirebaseAspects|typeof eco.artd.BFirebaseAspects)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof eco.artd.FirebaseAspects}
 */
eco.artd.AbstractFirebaseAspects.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!eco.artd.IFirebaseAspects|typeof eco.artd.FirebaseAspects)|(!eco.artd.BFirebaseAspects|typeof eco.artd.BFirebaseAspects)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof eco.artd.FirebaseAspects}
 */
eco.artd.AbstractFirebaseAspects.__trait = function(...Implementations) {}

/** @typedef {new (...args: !eco.artd.IFirebaseAspects.Initialese[]) => eco.artd.IFirebaseAspects} eco.artd.FirebaseAspectsConstructor */

/** @typedef {eco.artd.IFirebaseProfilingAide.Initialese} eco.artd.IFirebaseProfilingAspects.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: eco.artd.FirebaseProfilingAspects)} eco.artd.AbstractFirebaseProfilingAspects.constructor */
/** @typedef {typeof eco.artd.FirebaseProfilingAspects} eco.artd.FirebaseProfilingAspects.typeof */
/**
 * An abstract class of `eco.artd.IFirebaseProfilingAspects` interface.
 * @constructor eco.artd.AbstractFirebaseProfilingAspects
 */
eco.artd.AbstractFirebaseProfilingAspects = class extends /** @type {eco.artd.AbstractFirebaseProfilingAspects.constructor&eco.artd.FirebaseProfilingAspects.typeof} */ (class {}) { }
eco.artd.AbstractFirebaseProfilingAspects.prototype.constructor = eco.artd.AbstractFirebaseProfilingAspects
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
eco.artd.AbstractFirebaseProfilingAspects.class = /** @type {typeof eco.artd.AbstractFirebaseProfilingAspects} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!eco.artd.IFirebaseProfilingAspects|typeof eco.artd.FirebaseProfilingAspects)|(!eco.artd.BFirebaseAspects|typeof eco.artd.BFirebaseAspects)|(!eco.artd.IFirebaseProfilingAide|typeof eco.artd.FirebaseProfilingAide)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof eco.artd.FirebaseProfilingAspects}
 * @nosideeffects
 */
eco.artd.AbstractFirebaseProfilingAspects.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof eco.artd.AbstractFirebaseProfilingAspects}
 */
eco.artd.AbstractFirebaseProfilingAspects.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof eco.artd.FirebaseProfilingAspects}
 */
eco.artd.AbstractFirebaseProfilingAspects.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!eco.artd.IFirebaseProfilingAspects|typeof eco.artd.FirebaseProfilingAspects)|(!eco.artd.BFirebaseAspects|typeof eco.artd.BFirebaseAspects)|(!eco.artd.IFirebaseProfilingAide|typeof eco.artd.FirebaseProfilingAide)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof eco.artd.FirebaseProfilingAspects}
 */
eco.artd.AbstractFirebaseProfilingAspects.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!eco.artd.IFirebaseProfilingAspects|typeof eco.artd.FirebaseProfilingAspects)|(!eco.artd.BFirebaseAspects|typeof eco.artd.BFirebaseAspects)|(!eco.artd.IFirebaseProfilingAide|typeof eco.artd.FirebaseProfilingAide)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof eco.artd.FirebaseProfilingAspects}
 */
eco.artd.AbstractFirebaseProfilingAspects.__trait = function(...Implementations) {}

/** @typedef {new (...args: !eco.artd.IFirebaseProfilingAspects.Initialese[]) => eco.artd.IFirebaseProfilingAspects} eco.artd.FirebaseProfilingAspectsConstructor */

/** @typedef {function(new: engineering.type.IEngineer&eco.artd.IFirebaseProfilingAspectsCaster&eco.artd.BFirebaseAspects<!eco.artd.IFirebaseProfilingAspects>&eco.artd.IFirebaseProfilingAide)} eco.artd.IFirebaseProfilingAspects.constructor */
/** @typedef {typeof eco.artd.BFirebaseAspects} eco.artd.BFirebaseAspects.typeof */
/** @typedef {typeof eco.artd.IFirebaseProfilingAide} eco.artd.IFirebaseProfilingAide.typeof */
/** @interface eco.artd.IFirebaseProfilingAspects */
eco.artd.IFirebaseProfilingAspects = class extends /** @type {eco.artd.IFirebaseProfilingAspects.constructor&engineering.type.IEngineer.typeof&eco.artd.BFirebaseAspects.typeof&eco.artd.IFirebaseProfilingAide.typeof} */ (class {}) {
}
/**
 * Create a new *IFirebaseProfilingAspects* instance and automatically initialise it with options.
 * @param {...!eco.artd.IFirebaseProfilingAspects.Initialese} init The initialisation options.
 */
eco.artd.IFirebaseProfilingAspects.prototype.constructor = function(...init) {}

/** @typedef {function(new: eco.artd.IFirebaseProfilingAspects&engineering.type.IInitialiser<!eco.artd.IFirebaseProfilingAspects.Initialese>)} eco.artd.FirebaseProfilingAspects.constructor */
/** @typedef {typeof eco.artd.IFirebaseProfilingAspects} eco.artd.IFirebaseProfilingAspects.typeof */
/**
 * A concrete class of _IFirebaseProfilingAspects_ instances.
 * @constructor eco.artd.FirebaseProfilingAspects
 * @implements {eco.artd.IFirebaseProfilingAspects} ‎
 * @implements {engineering.type.IInitialiser<!eco.artd.IFirebaseProfilingAspects.Initialese>} ‎
 */
eco.artd.FirebaseProfilingAspects = class extends /** @type {eco.artd.FirebaseProfilingAspects.constructor&eco.artd.IFirebaseProfilingAspects.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IFirebaseProfilingAspects* instance and automatically initialise it with options.
   * @param {...!eco.artd.IFirebaseProfilingAspects.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IFirebaseProfilingAspects* instance and automatically initialise it with options.
 * @param {...!eco.artd.IFirebaseProfilingAspects.Initialese} init The initialisation options.
 */
eco.artd.FirebaseProfilingAspects.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof eco.artd.FirebaseProfilingAspects}
 */
eco.artd.FirebaseProfilingAspects.__extend = function(...Extensions) {}

/** @typedef {engineering.type.IProfilingAide.Initialese} eco.artd.IFirebaseProfilingAide.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: eco.artd.FirebaseProfilingAide)} eco.artd.AbstractFirebaseProfilingAide.constructor */
/** @typedef {typeof eco.artd.FirebaseProfilingAide} eco.artd.FirebaseProfilingAide.typeof */
/**
 * An abstract class of `eco.artd.IFirebaseProfilingAide` interface.
 * @constructor eco.artd.AbstractFirebaseProfilingAide
 */
eco.artd.AbstractFirebaseProfilingAide = class extends /** @type {eco.artd.AbstractFirebaseProfilingAide.constructor&eco.artd.FirebaseProfilingAide.typeof} */ (class {}) { }
eco.artd.AbstractFirebaseProfilingAide.prototype.constructor = eco.artd.AbstractFirebaseProfilingAide
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
eco.artd.AbstractFirebaseProfilingAide.class = /** @type {typeof eco.artd.AbstractFirebaseProfilingAide} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!eco.artd.IFirebaseProfilingAide|typeof eco.artd.FirebaseProfilingAide)|(!engineering.type.IProfilingAide|typeof engineering.type.ProfilingAide)|!eco.artd.IFirebaseProfilingAideHyperslice} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof eco.artd.FirebaseProfilingAide}
 * @nosideeffects
 */
eco.artd.AbstractFirebaseProfilingAide.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof eco.artd.AbstractFirebaseProfilingAide}
 */
eco.artd.AbstractFirebaseProfilingAide.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof eco.artd.FirebaseProfilingAide}
 */
eco.artd.AbstractFirebaseProfilingAide.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!eco.artd.IFirebaseProfilingAide|typeof eco.artd.FirebaseProfilingAide)|(!engineering.type.IProfilingAide|typeof engineering.type.ProfilingAide)|!eco.artd.IFirebaseProfilingAideHyperslice} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof eco.artd.FirebaseProfilingAide}
 */
eco.artd.AbstractFirebaseProfilingAide.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!eco.artd.IFirebaseProfilingAide|typeof eco.artd.FirebaseProfilingAide)|(!engineering.type.IProfilingAide|typeof engineering.type.ProfilingAide)|!eco.artd.IFirebaseProfilingAideHyperslice} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof eco.artd.FirebaseProfilingAide}
 */
eco.artd.AbstractFirebaseProfilingAide.__trait = function(...Implementations) {}

/** @typedef {new (...args: !eco.artd.IFirebaseProfilingAide.Initialese[]) => eco.artd.IFirebaseProfilingAide} eco.artd.FirebaseProfilingAideConstructor */

/**
 * A hyper slice includes all combined methods of an interface.
 * @interface eco.artd.IFirebaseProfilingAideHyperslice
 */
eco.artd.IFirebaseProfilingAideHyperslice = class {
  constructor() {
    /**
     * Reports the execution time of the _getPageDataFromFirestore_ method after its completion.
     */
    this.reportGetPageDataFromFirestoreProfile=/** @type {!eco.artd.IFirebaseProfilingAide._reportGetPageDataFromFirestoreProfile|!engineering.type.RecursiveArray<!eco.artd.IFirebaseProfilingAide._reportGetPageDataFromFirestoreProfile>} */ (void 0)
    /**
     * Reports the execution time of the _downloadAndEvalPageData_ method after its completion.
     */
    this.reportDownloadAndEvalPageDataProfile=/** @type {!eco.artd.IFirebaseProfilingAide._reportDownloadAndEvalPageDataProfile|!engineering.type.RecursiveArray<!eco.artd.IFirebaseProfilingAide._reportDownloadAndEvalPageDataProfile>} */ (void 0)
    /**
     * Reports the execution time of the _evalBody_ method after its completion.
     */
    this.reportEvalBodyProfile=/** @type {!eco.artd.IFirebaseProfilingAide._reportEvalBodyProfile|!engineering.type.RecursiveArray<!eco.artd.IFirebaseProfilingAide._reportEvalBodyProfile>} */ (void 0)
    /**
     * Reports the execution time of the _fetchFunctionsFile_ method after its completion.
     */
    this.reportFetchFunctionsFileProfile=/** @type {!eco.artd.IFirebaseProfilingAide._reportFetchFunctionsFileProfile|!engineering.type.RecursiveArray<!eco.artd.IFirebaseProfilingAide._reportFetchFunctionsFileProfile>} */ (void 0)
    /**
     * Reports the execution time of the _downloadDeps_ method after its completion.
     */
    this.reportDownloadDepsProfile=/** @type {!eco.artd.IFirebaseProfilingAide._reportDownloadDepsProfile|!engineering.type.RecursiveArray<!eco.artd.IFirebaseProfilingAide._reportDownloadDepsProfile>} */ (void 0)
    /**
     * Reports the execution time of the _downloadMethod_ method after its completion.
     */
    this.reportDownloadMethodProfile=/** @type {!eco.artd.IFirebaseProfilingAide._reportDownloadMethodProfile|!engineering.type.RecursiveArray<!eco.artd.IFirebaseProfilingAide._reportDownloadMethodProfile>} */ (void 0)
    /**
     * Reports the execution time of the _downloadMethodDirectly_ method after its completion.
     */
    this.reportDownloadMethodDirectlyProfile=/** @type {!eco.artd.IFirebaseProfilingAide._reportDownloadMethodDirectlyProfile|!engineering.type.RecursiveArray<!eco.artd.IFirebaseProfilingAide._reportDownloadMethodDirectlyProfile>} */ (void 0)
    /**
     * Reports the execution time of the _downloadPageData_ method after its completion.
     */
    this.reportDownloadPageDataProfile=/** @type {!eco.artd.IFirebaseProfilingAide._reportDownloadPageDataProfile|!engineering.type.RecursiveArray<!eco.artd.IFirebaseProfilingAide._reportDownloadPageDataProfile>} */ (void 0)
    /**
     * Reports the execution time of the _loadRequirePageData_ method after its completion.
     */
    this.reportLoadRequirePageDataProfile=/** @type {!eco.artd.IFirebaseProfilingAide._reportLoadRequirePageDataProfile|!engineering.type.RecursiveArray<!eco.artd.IFirebaseProfilingAide._reportLoadRequirePageDataProfile>} */ (void 0)
    /**
     * Reports the execution time of the _loadRequire_ method after its completion.
     */
    this.reportLoadRequireProfile=/** @type {!eco.artd.IFirebaseProfilingAide._reportLoadRequireProfile|!engineering.type.RecursiveArray<!eco.artd.IFirebaseProfilingAide._reportLoadRequireProfile>} */ (void 0)
  }
}
eco.artd.IFirebaseProfilingAideHyperslice.prototype.constructor = eco.artd.IFirebaseProfilingAideHyperslice

/**
 * A concrete class of _IFirebaseProfilingAideHyperslice_ instances.
 * @constructor eco.artd.FirebaseProfilingAideHyperslice
 * @implements {eco.artd.IFirebaseProfilingAideHyperslice} A hyper slice includes all combined methods of an interface.
 */
eco.artd.FirebaseProfilingAideHyperslice = class extends eco.artd.IFirebaseProfilingAideHyperslice { }
eco.artd.FirebaseProfilingAideHyperslice.prototype.constructor = eco.artd.FirebaseProfilingAideHyperslice

/**
 * A hyper-slice includes all combined methods of an interface.
 * @interface eco.artd.IFirebaseProfilingAideBindingHyperslice
 * @template THIS
 */
eco.artd.IFirebaseProfilingAideBindingHyperslice = class {
  constructor() {
    /**
     * Reports the execution time of the _getPageDataFromFirestore_ method after its completion.
     */
    this.reportGetPageDataFromFirestoreProfile=/** @type {!eco.artd.IFirebaseProfilingAide.__reportGetPageDataFromFirestoreProfile<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseProfilingAide.__reportGetPageDataFromFirestoreProfile<THIS>>} */ (void 0)
    /**
     * Reports the execution time of the _downloadAndEvalPageData_ method after its completion.
     */
    this.reportDownloadAndEvalPageDataProfile=/** @type {!eco.artd.IFirebaseProfilingAide.__reportDownloadAndEvalPageDataProfile<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseProfilingAide.__reportDownloadAndEvalPageDataProfile<THIS>>} */ (void 0)
    /**
     * Reports the execution time of the _evalBody_ method after its completion.
     */
    this.reportEvalBodyProfile=/** @type {!eco.artd.IFirebaseProfilingAide.__reportEvalBodyProfile<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseProfilingAide.__reportEvalBodyProfile<THIS>>} */ (void 0)
    /**
     * Reports the execution time of the _fetchFunctionsFile_ method after its completion.
     */
    this.reportFetchFunctionsFileProfile=/** @type {!eco.artd.IFirebaseProfilingAide.__reportFetchFunctionsFileProfile<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseProfilingAide.__reportFetchFunctionsFileProfile<THIS>>} */ (void 0)
    /**
     * Reports the execution time of the _downloadDeps_ method after its completion.
     */
    this.reportDownloadDepsProfile=/** @type {!eco.artd.IFirebaseProfilingAide.__reportDownloadDepsProfile<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseProfilingAide.__reportDownloadDepsProfile<THIS>>} */ (void 0)
    /**
     * Reports the execution time of the _downloadMethod_ method after its completion.
     */
    this.reportDownloadMethodProfile=/** @type {!eco.artd.IFirebaseProfilingAide.__reportDownloadMethodProfile<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseProfilingAide.__reportDownloadMethodProfile<THIS>>} */ (void 0)
    /**
     * Reports the execution time of the _downloadMethodDirectly_ method after its completion.
     */
    this.reportDownloadMethodDirectlyProfile=/** @type {!eco.artd.IFirebaseProfilingAide.__reportDownloadMethodDirectlyProfile<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseProfilingAide.__reportDownloadMethodDirectlyProfile<THIS>>} */ (void 0)
    /**
     * Reports the execution time of the _downloadPageData_ method after its completion.
     */
    this.reportDownloadPageDataProfile=/** @type {!eco.artd.IFirebaseProfilingAide.__reportDownloadPageDataProfile<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseProfilingAide.__reportDownloadPageDataProfile<THIS>>} */ (void 0)
    /**
     * Reports the execution time of the _loadRequirePageData_ method after its completion.
     */
    this.reportLoadRequirePageDataProfile=/** @type {!eco.artd.IFirebaseProfilingAide.__reportLoadRequirePageDataProfile<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseProfilingAide.__reportLoadRequirePageDataProfile<THIS>>} */ (void 0)
    /**
     * Reports the execution time of the _loadRequire_ method after its completion.
     */
    this.reportLoadRequireProfile=/** @type {!eco.artd.IFirebaseProfilingAide.__reportLoadRequireProfile<THIS>|!engineering.type.RecursiveArray<!eco.artd.IFirebaseProfilingAide.__reportLoadRequireProfile<THIS>>} */ (void 0)
  }
}
eco.artd.IFirebaseProfilingAideBindingHyperslice.prototype.constructor = eco.artd.IFirebaseProfilingAideBindingHyperslice

/**
 * A concrete class of _IFirebaseProfilingAideBindingHyperslice_ instances.
 * @constructor eco.artd.FirebaseProfilingAideBindingHyperslice
 * @implements {eco.artd.IFirebaseProfilingAideBindingHyperslice<THIS>} A hyper-slice includes all combined methods of an interface.
 * @template THIS
 * @extends {eco.artd.IFirebaseProfilingAideBindingHyperslice<THIS>}
 */
eco.artd.FirebaseProfilingAideBindingHyperslice = class extends eco.artd.IFirebaseProfilingAideBindingHyperslice { }
eco.artd.FirebaseProfilingAideBindingHyperslice.prototype.constructor = eco.artd.FirebaseProfilingAideBindingHyperslice

/** @typedef {function(new: engineering.type.IEngineer&eco.artd.IFirebaseProfilingAideCaster&engineering.type.IProfilingAide)} eco.artd.IFirebaseProfilingAide.constructor */
/** @typedef {typeof engineering.type.IProfilingAide} engineering.type.IProfilingAide.typeof */
/**
 * Records the results of the profiling.
 * @interface eco.artd.IFirebaseProfilingAide
 */
eco.artd.IFirebaseProfilingAide = class extends /** @type {eco.artd.IFirebaseProfilingAide.constructor&engineering.type.IEngineer.typeof&engineering.type.IProfilingAide.typeof} */ (class {}) {
}
/**
 * Create a new *IFirebaseProfilingAide* instance and automatically initialise it with options.
 * @param {...!eco.artd.IFirebaseProfilingAide.Initialese} init The initialisation options.
 */
eco.artd.IFirebaseProfilingAide.prototype.constructor = function(...init) {}
/** @type {eco.artd.IFirebaseProfilingAide.reportGetPageDataFromFirestoreProfile} */
eco.artd.IFirebaseProfilingAide.prototype.reportGetPageDataFromFirestoreProfile = function() {}
/** @type {eco.artd.IFirebaseProfilingAide.reportDownloadAndEvalPageDataProfile} */
eco.artd.IFirebaseProfilingAide.prototype.reportDownloadAndEvalPageDataProfile = function() {}
/** @type {eco.artd.IFirebaseProfilingAide.reportEvalBodyProfile} */
eco.artd.IFirebaseProfilingAide.prototype.reportEvalBodyProfile = function() {}
/** @type {eco.artd.IFirebaseProfilingAide.reportFetchFunctionsFileProfile} */
eco.artd.IFirebaseProfilingAide.prototype.reportFetchFunctionsFileProfile = function() {}
/** @type {eco.artd.IFirebaseProfilingAide.reportDownloadDepsProfile} */
eco.artd.IFirebaseProfilingAide.prototype.reportDownloadDepsProfile = function() {}
/** @type {eco.artd.IFirebaseProfilingAide.reportDownloadMethodProfile} */
eco.artd.IFirebaseProfilingAide.prototype.reportDownloadMethodProfile = function() {}
/** @type {eco.artd.IFirebaseProfilingAide.reportDownloadMethodDirectlyProfile} */
eco.artd.IFirebaseProfilingAide.prototype.reportDownloadMethodDirectlyProfile = function() {}
/** @type {eco.artd.IFirebaseProfilingAide.reportDownloadPageDataProfile} */
eco.artd.IFirebaseProfilingAide.prototype.reportDownloadPageDataProfile = function() {}
/** @type {eco.artd.IFirebaseProfilingAide.reportLoadRequirePageDataProfile} */
eco.artd.IFirebaseProfilingAide.prototype.reportLoadRequirePageDataProfile = function() {}
/** @type {eco.artd.IFirebaseProfilingAide.reportLoadRequireProfile} */
eco.artd.IFirebaseProfilingAide.prototype.reportLoadRequireProfile = function() {}

/** @typedef {function(new: eco.artd.IFirebaseProfilingAide&engineering.type.IInitialiser<!eco.artd.IFirebaseProfilingAide.Initialese>)} eco.artd.FirebaseProfilingAide.constructor */
/**
 * A concrete class of _IFirebaseProfilingAide_ instances.
 * @constructor eco.artd.FirebaseProfilingAide
 * @implements {eco.artd.IFirebaseProfilingAide} Records the results of the profiling.
 * @implements {engineering.type.IInitialiser<!eco.artd.IFirebaseProfilingAide.Initialese>} ‎
 */
eco.artd.FirebaseProfilingAide = class extends /** @type {eco.artd.FirebaseProfilingAide.constructor&eco.artd.IFirebaseProfilingAide.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IFirebaseProfilingAide* instance and automatically initialise it with options.
   * @param {...!eco.artd.IFirebaseProfilingAide.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IFirebaseProfilingAide* instance and automatically initialise it with options.
 * @param {...!eco.artd.IFirebaseProfilingAide.Initialese} init The initialisation options.
 */
eco.artd.FirebaseProfilingAide.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof eco.artd.FirebaseProfilingAide}
 */
eco.artd.FirebaseProfilingAide.__extend = function(...Extensions) {}

/** @typedef {eco.artd.IFirebaseProfilingAide} */
eco.artd.RecordIFirebaseProfilingAide

/** @typedef {eco.artd.IFirebaseProfilingAide} eco.artd.BoundIFirebaseProfilingAide */

/** @typedef {eco.artd.FirebaseProfilingAide} eco.artd.BoundFirebaseProfilingAide */

/** @typedef {typeof __$te_plain} eco.artd.IFirebaseLoggingAspects.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: eco.artd.FirebaseLoggingAspects)} eco.artd.AbstractFirebaseLoggingAspects.constructor */
/** @typedef {typeof eco.artd.FirebaseLoggingAspects} eco.artd.FirebaseLoggingAspects.typeof */
/**
 * An abstract class of `eco.artd.IFirebaseLoggingAspects` interface.
 * @constructor eco.artd.AbstractFirebaseLoggingAspects
 */
eco.artd.AbstractFirebaseLoggingAspects = class extends /** @type {eco.artd.AbstractFirebaseLoggingAspects.constructor&eco.artd.FirebaseLoggingAspects.typeof} */ (class {}) { }
eco.artd.AbstractFirebaseLoggingAspects.prototype.constructor = eco.artd.AbstractFirebaseLoggingAspects
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
eco.artd.AbstractFirebaseLoggingAspects.class = /** @type {typeof eco.artd.AbstractFirebaseLoggingAspects} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!eco.artd.IFirebaseLoggingAspects|typeof eco.artd.FirebaseLoggingAspects)|(!eco.artd.BFirebaseAspects|typeof eco.artd.BFirebaseAspects)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof eco.artd.FirebaseLoggingAspects}
 * @nosideeffects
 */
eco.artd.AbstractFirebaseLoggingAspects.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof eco.artd.AbstractFirebaseLoggingAspects}
 */
eco.artd.AbstractFirebaseLoggingAspects.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof eco.artd.FirebaseLoggingAspects}
 */
eco.artd.AbstractFirebaseLoggingAspects.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!eco.artd.IFirebaseLoggingAspects|typeof eco.artd.FirebaseLoggingAspects)|(!eco.artd.BFirebaseAspects|typeof eco.artd.BFirebaseAspects)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof eco.artd.FirebaseLoggingAspects}
 */
eco.artd.AbstractFirebaseLoggingAspects.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!eco.artd.IFirebaseLoggingAspects|typeof eco.artd.FirebaseLoggingAspects)|(!eco.artd.BFirebaseAspects|typeof eco.artd.BFirebaseAspects)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof eco.artd.FirebaseLoggingAspects}
 */
eco.artd.AbstractFirebaseLoggingAspects.__trait = function(...Implementations) {}

/** @typedef {new (...args: !eco.artd.IFirebaseLoggingAspects.Initialese[]) => eco.artd.IFirebaseLoggingAspects} eco.artd.FirebaseLoggingAspectsConstructor */

/** @typedef {function(new: engineering.type.IEngineer&eco.artd.IFirebaseLoggingAspectsCaster&eco.artd.BFirebaseAspects<!eco.artd.IFirebaseLoggingAspects>)} eco.artd.IFirebaseLoggingAspects.constructor */
/**
 * Logs about method executions.
 * @interface eco.artd.IFirebaseLoggingAspects
 */
eco.artd.IFirebaseLoggingAspects = class extends /** @type {eco.artd.IFirebaseLoggingAspects.constructor&engineering.type.IEngineer.typeof&eco.artd.BFirebaseAspects.typeof} */ (class {}) {
}
/**
 * Create a new *IFirebaseLoggingAspects* instance and automatically initialise it with options.
 * @param {...!eco.artd.IFirebaseLoggingAspects.Initialese} init The initialisation options.
 */
eco.artd.IFirebaseLoggingAspects.prototype.constructor = function(...init) {}

/** @typedef {function(new: eco.artd.IFirebaseLoggingAspects&engineering.type.IInitialiser<!eco.artd.IFirebaseLoggingAspects.Initialese>)} eco.artd.FirebaseLoggingAspects.constructor */
/** @typedef {typeof eco.artd.IFirebaseLoggingAspects} eco.artd.IFirebaseLoggingAspects.typeof */
/**
 * A concrete class of _IFirebaseLoggingAspects_ instances.
 * @constructor eco.artd.FirebaseLoggingAspects
 * @implements {eco.artd.IFirebaseLoggingAspects} Logs about method executions.
 * @implements {engineering.type.IInitialiser<!eco.artd.IFirebaseLoggingAspects.Initialese>} ‎
 */
eco.artd.FirebaseLoggingAspects = class extends /** @type {eco.artd.FirebaseLoggingAspects.constructor&eco.artd.IFirebaseLoggingAspects.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IFirebaseLoggingAspects* instance and automatically initialise it with options.
   * @param {...!eco.artd.IFirebaseLoggingAspects.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IFirebaseLoggingAspects* instance and automatically initialise it with options.
 * @param {...!eco.artd.IFirebaseLoggingAspects.Initialese} init The initialisation options.
 */
eco.artd.FirebaseLoggingAspects.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof eco.artd.FirebaseLoggingAspects}
 */
eco.artd.FirebaseLoggingAspects.__extend = function(...Extensions) {}

/** @typedef {typeof __$te_plain} eco.artd.IFirebaseConditioningAspects.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: eco.artd.FirebaseConditioningAspects)} eco.artd.AbstractFirebaseConditioningAspects.constructor */
/** @typedef {typeof eco.artd.FirebaseConditioningAspects} eco.artd.FirebaseConditioningAspects.typeof */
/**
 * An abstract class of `eco.artd.IFirebaseConditioningAspects` interface.
 * @constructor eco.artd.AbstractFirebaseConditioningAspects
 */
eco.artd.AbstractFirebaseConditioningAspects = class extends /** @type {eco.artd.AbstractFirebaseConditioningAspects.constructor&eco.artd.FirebaseConditioningAspects.typeof} */ (class {}) { }
eco.artd.AbstractFirebaseConditioningAspects.prototype.constructor = eco.artd.AbstractFirebaseConditioningAspects
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
eco.artd.AbstractFirebaseConditioningAspects.class = /** @type {typeof eco.artd.AbstractFirebaseConditioningAspects} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!eco.artd.IFirebaseConditioningAspects|typeof eco.artd.FirebaseConditioningAspects)|(!eco.artd.BFirebaseAspects|typeof eco.artd.BFirebaseAspects)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof eco.artd.FirebaseConditioningAspects}
 * @nosideeffects
 */
eco.artd.AbstractFirebaseConditioningAspects.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof eco.artd.AbstractFirebaseConditioningAspects}
 */
eco.artd.AbstractFirebaseConditioningAspects.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof eco.artd.FirebaseConditioningAspects}
 */
eco.artd.AbstractFirebaseConditioningAspects.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!eco.artd.IFirebaseConditioningAspects|typeof eco.artd.FirebaseConditioningAspects)|(!eco.artd.BFirebaseAspects|typeof eco.artd.BFirebaseAspects)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof eco.artd.FirebaseConditioningAspects}
 */
eco.artd.AbstractFirebaseConditioningAspects.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!eco.artd.IFirebaseConditioningAspects|typeof eco.artd.FirebaseConditioningAspects)|(!eco.artd.BFirebaseAspects|typeof eco.artd.BFirebaseAspects)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof eco.artd.FirebaseConditioningAspects}
 */
eco.artd.AbstractFirebaseConditioningAspects.__trait = function(...Implementations) {}

/** @typedef {new (...args: !eco.artd.IFirebaseConditioningAspects.Initialese[]) => eco.artd.IFirebaseConditioningAspects} eco.artd.FirebaseConditioningAspectsConstructor */

/** @typedef {function(new: engineering.type.IEngineer&eco.artd.IFirebaseConditioningAspectsCaster&eco.artd.BFirebaseAspects<!eco.artd.IFirebaseConditioningAspects>)} eco.artd.IFirebaseConditioningAspects.constructor */
/** @interface eco.artd.IFirebaseConditioningAspects */
eco.artd.IFirebaseConditioningAspects = class extends /** @type {eco.artd.IFirebaseConditioningAspects.constructor&engineering.type.IEngineer.typeof&eco.artd.BFirebaseAspects.typeof} */ (class {}) {
}
/**
 * Create a new *IFirebaseConditioningAspects* instance and automatically initialise it with options.
 * @param {...!eco.artd.IFirebaseConditioningAspects.Initialese} init The initialisation options.
 */
eco.artd.IFirebaseConditioningAspects.prototype.constructor = function(...init) {}

/** @typedef {function(new: eco.artd.IFirebaseConditioningAspects&engineering.type.IInitialiser<!eco.artd.IFirebaseConditioningAspects.Initialese>)} eco.artd.FirebaseConditioningAspects.constructor */
/** @typedef {typeof eco.artd.IFirebaseConditioningAspects} eco.artd.IFirebaseConditioningAspects.typeof */
/**
 * A concrete class of _IFirebaseConditioningAspects_ instances.
 * @constructor eco.artd.FirebaseConditioningAspects
 * @implements {eco.artd.IFirebaseConditioningAspects} ‎
 * @implements {engineering.type.IInitialiser<!eco.artd.IFirebaseConditioningAspects.Initialese>} ‎
 */
eco.artd.FirebaseConditioningAspects = class extends /** @type {eco.artd.FirebaseConditioningAspects.constructor&eco.artd.IFirebaseConditioningAspects.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IFirebaseConditioningAspects* instance and automatically initialise it with options.
   * @param {...!eco.artd.IFirebaseConditioningAspects.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IFirebaseConditioningAspects* instance and automatically initialise it with options.
 * @param {...!eco.artd.IFirebaseConditioningAspects.Initialese} init The initialisation options.
 */
eco.artd.FirebaseConditioningAspects.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof eco.artd.FirebaseConditioningAspects}
 */
eco.artd.FirebaseConditioningAspects.__extend = function(...Extensions) {}

/**
 * Contains getters to cast the _IFirebaseProfilingAspects_ interface.
 * @interface eco.artd.IFirebaseProfilingAspectsCaster
 */
eco.artd.IFirebaseProfilingAspectsCaster = class { }
/**
 * Cast the _IFirebaseProfilingAspects_ instance into the _BoundIFirebase_ type.
 * @type {!eco.artd.BoundIFirebase}
 */
eco.artd.IFirebaseProfilingAspectsCaster.prototype.asIFirebase

/**
 * Contains getters to cast the _IFirebaseProfilingAide_ interface.
 * @interface eco.artd.IFirebaseProfilingAideCaster
 */
eco.artd.IFirebaseProfilingAideCaster = class { }
/**
 * Cast the _IFirebaseProfilingAide_ instance into the _BoundIFirebaseProfilingAide_ type.
 * @type {!eco.artd.BoundIFirebaseProfilingAide}
 */
eco.artd.IFirebaseProfilingAideCaster.prototype.asIFirebaseProfilingAide
/**
 * Cast the _IFirebaseProfilingAide_ instance into the _BoundIFirebase_ type.
 * @type {!eco.artd.BoundIFirebase}
 */
eco.artd.IFirebaseProfilingAideCaster.prototype.asIFirebase
/**
 * Access the _FirebaseProfilingAide_ prototype.
 * @type {!eco.artd.BoundFirebaseProfilingAide}
 */
eco.artd.IFirebaseProfilingAideCaster.prototype.superFirebaseProfilingAide

/**
 * Contains getters to cast the _IFirebaseLoggingAspects_ interface.
 * @interface eco.artd.IFirebaseLoggingAspectsCaster
 */
eco.artd.IFirebaseLoggingAspectsCaster = class { }
/**
 * Cast the _IFirebaseLoggingAspects_ instance into the _BoundIFirebase_ type.
 * @type {!eco.artd.BoundIFirebase}
 */
eco.artd.IFirebaseLoggingAspectsCaster.prototype.asIFirebase

/**
 * Contains getters to cast the _IFirebaseConditioningAspects_ interface.
 * @interface eco.artd.IFirebaseConditioningAspectsCaster
 */
eco.artd.IFirebaseConditioningAspectsCaster = class { }
/**
 * Cast the _IFirebaseConditioningAspects_ instance into the _BoundIFirebase_ type.
 * @type {!eco.artd.BoundIFirebase}
 */
eco.artd.IFirebaseConditioningAspectsCaster.prototype.asIFirebase

/** @typedef {function(new: engineering.type.IEngineer&eco.artd.IFirebaseAspectsCaster&eco.artd.BFirebaseAspects<!eco.artd.IFirebaseAspects>)} eco.artd.IFirebaseAspects.constructor */
/**
 * The aspects of the *IFirebase*.
 * @interface eco.artd.IFirebaseAspects
 */
eco.artd.IFirebaseAspects = class extends /** @type {eco.artd.IFirebaseAspects.constructor&engineering.type.IEngineer.typeof&eco.artd.BFirebaseAspects.typeof} */ (class {}) {
}
/**
 * Create a new *IFirebaseAspects* instance and automatically initialise it with options.
 * @param {...!eco.artd.IFirebaseAspects.Initialese} init The initialisation options.
 */
eco.artd.IFirebaseAspects.prototype.constructor = function(...init) {}

/** @typedef {function(new: eco.artd.IFirebaseAspects&engineering.type.IInitialiser<!eco.artd.IFirebaseAspects.Initialese>)} eco.artd.FirebaseAspects.constructor */
/** @typedef {typeof eco.artd.IFirebaseAspects} eco.artd.IFirebaseAspects.typeof */
/**
 * A concrete class of _IFirebaseAspects_ instances.
 * @constructor eco.artd.FirebaseAspects
 * @implements {eco.artd.IFirebaseAspects} The aspects of the *IFirebase*.
 * @implements {engineering.type.IInitialiser<!eco.artd.IFirebaseAspects.Initialese>} ‎
 */
eco.artd.FirebaseAspects = class extends /** @type {eco.artd.FirebaseAspects.constructor&eco.artd.IFirebaseAspects.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IFirebaseAspects* instance and automatically initialise it with options.
   * @param {...!eco.artd.IFirebaseAspects.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IFirebaseAspects* instance and automatically initialise it with options.
 * @param {...!eco.artd.IFirebaseAspects.Initialese} init The initialisation options.
 */
eco.artd.FirebaseAspects.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof eco.artd.FirebaseAspects}
 */
eco.artd.FirebaseAspects.__extend = function(...Extensions) {}

/**
 * Contains getters to cast the _BFirebaseAspects_ interface.
 * @interface eco.artd.BFirebaseAspectsCaster
 * @template THIS
 */
eco.artd.BFirebaseAspectsCaster = class { }
/**
 * Cast the _BFirebaseAspects_ instance into the _BoundIFirebase_ type.
 * @type {!eco.artd.BoundIFirebase}
 */
eco.artd.BFirebaseAspectsCaster.prototype.asIFirebase

/**
 * Contains getters to cast the _IFirebaseAspects_ interface.
 * @interface eco.artd.IFirebaseAspectsCaster
 */
eco.artd.IFirebaseAspectsCaster = class { }
/**
 * Cast the _IFirebaseAspects_ instance into the _BoundIFirebase_ type.
 * @type {!eco.artd.BoundIFirebase}
 */
eco.artd.IFirebaseAspectsCaster.prototype.asIFirebase

/** @typedef {eco.artd.IFirebase.Initialese} eco.artd.IHyperFirebase.Initialese */

/** @typedef {function(new: eco.artd.HyperFirebase)} eco.artd.AbstractHyperFirebase.constructor */
/** @typedef {typeof eco.artd.HyperFirebase} eco.artd.HyperFirebase.typeof */
/**
 * An abstract class of `eco.artd.IHyperFirebase` interface.
 * @constructor eco.artd.AbstractHyperFirebase
 */
eco.artd.AbstractHyperFirebase = class extends /** @type {eco.artd.AbstractHyperFirebase.constructor&eco.artd.HyperFirebase.typeof} */ (class {}) { }
eco.artd.AbstractHyperFirebase.prototype.constructor = eco.artd.AbstractHyperFirebase
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
eco.artd.AbstractHyperFirebase.class = /** @type {typeof eco.artd.AbstractHyperFirebase} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!eco.artd.IHyperFirebase|typeof eco.artd.HyperFirebase)|(!eco.artd.IFirebase|typeof eco.artd.Firebase)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof eco.artd.HyperFirebase}
 * @nosideeffects
 */
eco.artd.AbstractHyperFirebase.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof eco.artd.AbstractHyperFirebase}
 */
eco.artd.AbstractHyperFirebase.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof eco.artd.HyperFirebase}
 */
eco.artd.AbstractHyperFirebase.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!eco.artd.IHyperFirebase|typeof eco.artd.HyperFirebase)|(!eco.artd.IFirebase|typeof eco.artd.Firebase)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof eco.artd.HyperFirebase}
 */
eco.artd.AbstractHyperFirebase.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!eco.artd.IHyperFirebase|typeof eco.artd.HyperFirebase)|(!eco.artd.IFirebase|typeof eco.artd.Firebase)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof eco.artd.HyperFirebase}
 */
eco.artd.AbstractHyperFirebase.__trait = function(...Implementations) {}
/**
 * Consult aides for their advice on methods to be implemented, so that all implementation methods provided on subsequent `.implements` calls will be advised according to this consultation.
 * @param {...!eco.artd.IFirebaseAspects|!Array<!eco.artd.IFirebaseAspects>|function(new: eco.artd.IFirebaseAspects)|!Function|!Array<!Function>|void|null} aides The list of aides that advise the IFirebase to implement aspects.
 * @return {typeof eco.artd.AbstractHyperFirebase}
 * @nosideeffects
 */
eco.artd.AbstractHyperFirebase.consults = function(...aides) {}
/**
 * Creates a new abstract class which extends passed hyper-classes by installing
 * their aspects and implementations.
 * @param {...!Function|!Array<!Function>|void|null} hypers The list of hyper classes.
 * @return {typeof eco.artd.AbstractHyperFirebase}
 * @nosideeffects
 */
eco.artd.AbstractHyperFirebase.extends = function(...hypers) {}
/**
 * Adds the aspects installers in order for advices to work.
 * @param {...!Function|!Array<!Function>|void|null} aspectsInstallers The list of aspects installers.
 * @return {typeof eco.artd.AbstractHyperFirebase}
 * @nosideeffects
 */
eco.artd.AbstractHyperFirebase.installs = function(...aspectsInstallers) {}

/** @typedef {new (...args: !eco.artd.IHyperFirebase.Initialese[]) => eco.artd.IHyperFirebase} eco.artd.HyperFirebaseConstructor */

/** @typedef {function(new: engineering.type.IEngineer&eco.artd.IHyperFirebaseCaster&eco.artd.IFirebase)} eco.artd.IHyperFirebase.constructor */
/** @typedef {typeof eco.artd.IFirebase} eco.artd.IFirebase.typeof */
/** @interface eco.artd.IHyperFirebase */
eco.artd.IHyperFirebase = class extends /** @type {eco.artd.IHyperFirebase.constructor&engineering.type.IEngineer.typeof&eco.artd.IFirebase.typeof} */ (class {}) {
}
/**
 * Create a new *IHyperFirebase* instance and automatically initialise it with options.
 * @param {...!eco.artd.IHyperFirebase.Initialese} init The initialisation options.
 */
eco.artd.IHyperFirebase.prototype.constructor = function(...init) {}

/** @typedef {function(new: eco.artd.IHyperFirebase&engineering.type.IInitialiser<!eco.artd.IHyperFirebase.Initialese>)} eco.artd.HyperFirebase.constructor */
/** @typedef {typeof eco.artd.IHyperFirebase} eco.artd.IHyperFirebase.typeof */
/**
 * A concrete class of _IHyperFirebase_ instances.
 * @constructor eco.artd.HyperFirebase
 * @implements {eco.artd.IHyperFirebase} ‎
 * @implements {engineering.type.IInitialiser<!eco.artd.IHyperFirebase.Initialese>} ‎
 */
eco.artd.HyperFirebase = class extends /** @type {eco.artd.HyperFirebase.constructor&eco.artd.IHyperFirebase.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IHyperFirebase* instance and automatically initialise it with options.
   * @param {...!eco.artd.IHyperFirebase.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IHyperFirebase* instance and automatically initialise it with options.
 * @param {...!eco.artd.IHyperFirebase.Initialese} init The initialisation options.
 */
eco.artd.HyperFirebase.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof eco.artd.HyperFirebase}
 */
eco.artd.HyperFirebase.__extend = function(...Extensions) {}

/** @typedef {eco.artd.IHyperFirebase} */
eco.artd.RecordIHyperFirebase

/** @typedef {eco.artd.IHyperFirebase} eco.artd.BoundIHyperFirebase */

/** @typedef {eco.artd.HyperFirebase} eco.artd.BoundHyperFirebase */

/**
 * Contains getters to cast the _IHyperFirebase_ interface.
 * @interface eco.artd.IHyperFirebaseCaster
 */
eco.artd.IHyperFirebaseCaster = class { }
/**
 * Cast the _IHyperFirebase_ instance into the _BoundIHyperFirebase_ type.
 * @type {!eco.artd.BoundIHyperFirebase}
 */
eco.artd.IHyperFirebaseCaster.prototype.asIHyperFirebase
/**
 * Access the _HyperFirebase_ prototype.
 * @type {!eco.artd.BoundHyperFirebase}
 */
eco.artd.IHyperFirebaseCaster.prototype.superHyperFirebase

/** @typedef {function(new: eco.artd.IFirebaseFields&engineering.type.IEngineer&eco.artd.IFirebaseCaster)} eco.artd.IFirebase.constructor */
/**
 *     Various firebase utils and runtime to download JS files from HTTP hosting
 * for execution in functions (helps to eliminate reploy every so often).
 * @interface eco.artd.IFirebase
 */
eco.artd.IFirebase = class extends /** @type {eco.artd.IFirebase.constructor&engineering.type.IEngineer.typeof} */ (class {}) {
}
/**
 * Create a new *IFirebase* instance and automatically initialise it with options.
 * @param {...!eco.artd.IFirebase.Initialese} init The initialisation options.
 */
eco.artd.IFirebase.prototype.constructor = function(...init) {}
/** @type {eco.artd.IFirebase.function} */
eco.artd.IFirebase.prototype.function = function() {}
/** @type {eco.artd.IFirebase.evalBodyWithProfileTime} */
eco.artd.IFirebase.prototype.evalBodyWithProfileTime = function() {}
/** @type {eco.artd.IFirebase.getPageDataFromFirestore} */
eco.artd.IFirebase.prototype.getPageDataFromFirestore = function() {}
/** @type {eco.artd.IFirebase.downloadAndEvalPageData} */
eco.artd.IFirebase.prototype.downloadAndEvalPageData = function() {}
/** @type {eco.artd.IFirebase.evalBody} */
eco.artd.IFirebase.prototype.evalBody = function() {}
/** @type {eco.artd.IFirebase.logRpcInvocation} */
eco.artd.IFirebase.prototype.logRpcInvocation = function() {}
/** @type {eco.artd.IFirebase.fetchFunctionsFile} */
eco.artd.IFirebase.prototype.fetchFunctionsFile = function() {}
/** @type {eco.artd.IFirebase.downloadDeps} */
eco.artd.IFirebase.prototype.downloadDeps = function() {}
/** @type {eco.artd.IFirebase.downloadMethod} */
eco.artd.IFirebase.prototype.downloadMethod = function() {}
/** @type {eco.artd.IFirebase.downloadMethodDirectly} */
eco.artd.IFirebase.prototype.downloadMethodDirectly = function() {}
/** @type {eco.artd.IFirebase.getId} */
eco.artd.IFirebase.prototype.getId = function() {}
/** @type {eco.artd.IFirebase.patchDeps} */
eco.artd.IFirebase.prototype.patchDeps = function() {}
/** @type {eco.artd.IFirebase.downloadPageData} */
eco.artd.IFirebase.prototype.downloadPageData = function() {}
/** @type {eco.artd.IFirebase.loadRequirePageData} */
eco.artd.IFirebase.prototype.loadRequirePageData = function() {}
/** @type {eco.artd.IFirebase.loadRequire} */
eco.artd.IFirebase.prototype.loadRequire = function() {}
/** @type {eco.artd.IFirebase.updateErrorStack} */
eco.artd.IFirebase.prototype.updateErrorStack = function() {}
/** @type {eco.artd.IFirebase.validateMethod} */
eco.artd.IFirebase.prototype.validateMethod = function() {}
/** @type {eco.artd.IFirebase.validateLoadAndInvoke} */
eco.artd.IFirebase.prototype.validateLoadAndInvoke = function() {}
/** @type {eco.artd.IFirebase.validateLoadAndInvokeById} */
eco.artd.IFirebase.prototype.validateLoadAndInvokeById = function() {}
/** @type {eco.artd.IFirebase.makeInvoker} */
eco.artd.IFirebase.prototype.makeInvoker = function() {}
/** @type {eco.artd.IFirebase.loadAndInvokeById} */
eco.artd.IFirebase.prototype.loadAndInvokeById = function() {}
/** @type {eco.artd.IFirebase.loadAndInvoke} */
eco.artd.IFirebase.prototype.loadAndInvoke = function() {}

/** @typedef {function(new: eco.artd.IFirebase&engineering.type.IInitialiser<!eco.artd.IFirebase.Initialese>)} eco.artd.Firebase.constructor */
/**
 * A concrete class of _IFirebase_ instances.
 * @constructor eco.artd.Firebase
 * @implements {eco.artd.IFirebase}     Various firebase utils and runtime to download JS files from HTTP hosting
 * for execution in functions (helps to eliminate reploy every so often).
 * @implements {engineering.type.IInitialiser<!eco.artd.IFirebase.Initialese>} ‎
 */
eco.artd.Firebase = class extends /** @type {eco.artd.Firebase.constructor&eco.artd.IFirebase.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IFirebase* instance and automatically initialise it with options.
   * @param {...!eco.artd.IFirebase.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IFirebase* instance and automatically initialise it with options.
 * @param {...!eco.artd.IFirebase.Initialese} init The initialisation options.
 */
eco.artd.Firebase.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof eco.artd.Firebase}
 */
eco.artd.Firebase.__extend = function(...Extensions) {}

/**
 * Fields of the IFirebase.
 * @interface eco.artd.IFirebaseFields
 */
eco.artd.IFirebaseFields = class { }
/**
 * Default `firebase-admin/firestore`.
 */
eco.artd.IFirebaseFields.prototype.firestorePackage = /** @type {string} */ (void 0)
/**
 * Default `./_rpc`.
 */
eco.artd.IFirebaseFields.prototype.rpcDir = /** @type {string} */ (void 0)
/**
 * Default `null`.
 */
eco.artd.IFirebaseFields.prototype.res = /** @type {http.ServerResponse} */ (void 0)
/**
 * Default `null`.
 */
eco.artd.IFirebaseFields.prototype.req = /** @type {http.IncomingMessage} */ (void 0)
/**
 * The name of the firebase project. Default empty string.
 */
eco.artd.IFirebaseFields.prototype.gCloudProject = /** @type {string} */ (void 0)
/**
 * Added on profuction functions to indicate their name. Default empty string.
 */
eco.artd.IFirebaseFields.prototype.functionTarget = /** @type {string} */ (void 0)
/**
 * Set to _True_ when using local debugging. Default empty string.
 */
eco.artd.IFirebaseFields.prototype.functionsEmulator = /** @type {string} */ (void 0)
/**
 * The database pointer from firebase package. Default empty string.
 */
eco.artd.IFirebaseFields.prototype.db = /** @type {string} */ (void 0)
/**
 * Looks up the x-forwarded-host header.
 */
eco.artd.IFirebaseFields.prototype.host = /** @type {string} */ (void 0)
/**
 * Whether working on the localhost:5000.
 */
eco.artd.IFirebaseFields.prototype.isLocal = /** @type {boolean} */ (void 0)
/**
 * The logger. Default `null`.
 */
eco.artd.IFirebaseFields.prototype.logger = /** @type {?{ info: function(...*) }} */ (void 0)

/** @typedef {eco.artd.IFirebase} */
eco.artd.RecordIFirebase

/** @typedef {eco.artd.IFirebase} eco.artd.BoundIFirebase */

/** @typedef {eco.artd.Firebase} eco.artd.BoundFirebase */

/**
 * @typedef {Object} eco.artd.IFirebase.PageData The loaded data about the remote circuit hosted on a Firebase site that the
 * worker will have access to.
 * @prop {!Map<string, string>} methodPaths
 * @prop {!Map<string, string>} methodNames
 * @prop {string} dir
 */

/**
 * @typedef {Object} eco.artd.IFirebase.Times
 * @prop {?number} [pageDataDownloadTime]
 * @prop {?number} [pageDataFirestoreTime]
 * @prop {?number} [invokeTime]
 * @prop {?number} [loadTime]
 * @prop {?string} [region]
 * @prop {?number} [downloadTime]
 * @prop {?number} [downloadMethodDirectlyTime] How long it took to download the method source code knowing
 * just its id.
 */

/**
 * @typedef {Object} eco.artd.IFirebase.Methods
 * @prop {?Date} [methodDate]
 * @prop {?string} [methodVersion]
 * @prop {?string} [methodName]
 * @prop {?string} [methodEtag]
 * @prop {Map<string, string>} [methodDeps]
 * @prop {?string} [methodId]
 * @prop {?number} [methodCid]
 */

/**
 * Contains getters to cast the _IFirebase_ interface.
 * @interface eco.artd.IFirebaseCaster
 */
eco.artd.IFirebaseCaster = class { }
/**
 * Cast the _IFirebase_ instance into the _BoundIFirebase_ type.
 * @type {!eco.artd.BoundIFirebase}
 */
eco.artd.IFirebaseCaster.prototype.asIFirebase
/**
 * Access the _Firebase_ prototype.
 * @type {!eco.artd.BoundFirebase}
 */
eco.artd.IFirebaseCaster.prototype.superFirebase

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.BeforeFunctionPointcutData) => void} eco.artd.IFirebaseJoinpointModel.__beforeFunction
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__beforeFunction<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._beforeFunction */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.beforeFunction} */
/**
 * Before the method executes. Allows to cancel the method. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.BeforeFunctionPointcutData} [data] Metadata passed to the pointcuts of _function_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: IFirebase.FunctionNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `function` method from being executed.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.FunctionPointcutData*
 * - `args` _IFirebase.FunctionNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.FunctionPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.FunctionPointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.beforeFunction = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.AfterFunctionPointcutData) => void} eco.artd.IFirebaseJoinpointModel.__afterFunction
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__afterFunction<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._afterFunction */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.afterFunction} */
/**
 * After the method. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterFunctionPointcutData} [data] Metadata passed to the pointcuts of _function_ at the `after` joinpoint.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.FunctionPointcutData*
 * - `args` _IFirebase.FunctionNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.FunctionPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.FunctionPointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.afterFunction = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.AfterThrowsFunctionPointcutData) => void} eco.artd.IFirebaseJoinpointModel.__afterFunctionThrows
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__afterFunctionThrows<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._afterFunctionThrows */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.afterFunctionThrows} */
/**
 * After the method throw. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterThrowsFunctionPointcutData} [data] Metadata passed to the pointcuts of _function_ at the `afterThrows` joinpoint.
 * - `err` _!Error_ The error with which the method failed.
 * - `hide` _() =&gt; void_ Hides the error in the `function` (use only at the top-level of program flow).
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.FunctionPointcutData*
 * - `args` _IFirebase.FunctionNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.FunctionPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.FunctionPointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.afterFunctionThrows = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.AfterReturnsFunctionPointcutData) => void} eco.artd.IFirebaseJoinpointModel.__afterFunctionReturns
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__afterFunctionReturns<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._afterFunctionReturns */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.afterFunctionReturns} */
/**
 * After the method returned. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterReturnsFunctionPointcutData} [data] Metadata passed to the pointcuts of _function_ at the `afterReturns` joinpoint.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.FunctionPointcutData*
 * - `args` _IFirebase.FunctionNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.FunctionPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.FunctionPointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.afterFunctionReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.AfterCancelsFunctionPointcutData) => void} eco.artd.IFirebaseJoinpointModel.__afterFunctionCancels
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__afterFunctionCancels<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._afterFunctionCancels */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.afterFunctionCancels} */
/**
 * After the method cancels. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterCancelsFunctionPointcutData} [data] Metadata passed to the pointcuts of _function_ at the `afterCancels` joinpoint.
 * - `reasons` _!Set&lt;string&gt;_ The list of reasons why the method was cancelled.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.FunctionPointcutData*
 * - `args` _IFirebase.FunctionNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.FunctionPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.FunctionPointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.afterFunctionCancels = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterFunctionPointcutData) => void} eco.artd.IFirebaseJoinpointModel.__immediatelyAfterFunction
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__immediatelyAfterFunction<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._immediatelyAfterFunction */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.immediatelyAfterFunction} */
/**
 * Immediately after the async method returned its promise. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterFunctionPointcutData} [data] Metadata passed to the pointcuts of _function_ at the `immediatelyAfter` joinpoint.
 * - `promise` _!Promise&lt;void&gt;_ The promise with the return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.FunctionPointcutData*
 * - `args` _IFirebase.FunctionNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.FunctionPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.FunctionPointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.immediatelyAfterFunction = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.BeforeEvalBodyWithProfileTimePointcutData) => void} eco.artd.IFirebaseJoinpointModel.__beforeEvalBodyWithProfileTime
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__beforeEvalBodyWithProfileTime<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._beforeEvalBodyWithProfileTime */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.beforeEvalBodyWithProfileTime} */
/**
 * Before the method executes. Allows to cancel the method. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.BeforeEvalBodyWithProfileTimePointcutData} [data] Metadata passed to the pointcuts of _evalBodyWithProfileTime_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: IFirebase.EvalBodyWithProfileTimeNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `evalBodyWithProfileTime` method from being executed.
 * - `sub` _(value: IFirebase.evalBodyWithProfileTime.Return) =&gt; void_ Cancels a call to `evalBodyWithProfileTime` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.EvalBodyWithProfileTimePointcutData*
 * - `args` _IFirebase.EvalBodyWithProfileTimeNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.EvalBodyWithProfileTimePointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.EvalBodyWithProfileTimePointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.beforeEvalBodyWithProfileTime = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.AfterEvalBodyWithProfileTimePointcutData) => void} eco.artd.IFirebaseJoinpointModel.__afterEvalBodyWithProfileTime
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__afterEvalBodyWithProfileTime<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._afterEvalBodyWithProfileTime */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.afterEvalBodyWithProfileTime} */
/**
 * After the method. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterEvalBodyWithProfileTimePointcutData} [data] Metadata passed to the pointcuts of _evalBodyWithProfileTime_ at the `after` joinpoint.
 * - `res` _IFirebase.evalBodyWithProfileTime.Return_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.EvalBodyWithProfileTimePointcutData*
 * - `args` _IFirebase.EvalBodyWithProfileTimeNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.EvalBodyWithProfileTimePointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.EvalBodyWithProfileTimePointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.afterEvalBodyWithProfileTime = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.AfterThrowsEvalBodyWithProfileTimePointcutData) => void} eco.artd.IFirebaseJoinpointModel.__afterEvalBodyWithProfileTimeThrows
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__afterEvalBodyWithProfileTimeThrows<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._afterEvalBodyWithProfileTimeThrows */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.afterEvalBodyWithProfileTimeThrows} */
/**
 * After the method throw. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterThrowsEvalBodyWithProfileTimePointcutData} [data] Metadata passed to the pointcuts of _evalBodyWithProfileTime_ at the `afterThrows` joinpoint.
 * - `err` _!Error_ The error with which the method failed.
 * - `hide` _() =&gt; void_ Hides the error in the `evalBodyWithProfileTime` (use only at the top-level of program flow).
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.EvalBodyWithProfileTimePointcutData*
 * - `args` _IFirebase.EvalBodyWithProfileTimeNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.EvalBodyWithProfileTimePointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.EvalBodyWithProfileTimePointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.afterEvalBodyWithProfileTimeThrows = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.AfterReturnsEvalBodyWithProfileTimePointcutData) => void} eco.artd.IFirebaseJoinpointModel.__afterEvalBodyWithProfileTimeReturns
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__afterEvalBodyWithProfileTimeReturns<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._afterEvalBodyWithProfileTimeReturns */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.afterEvalBodyWithProfileTimeReturns} */
/**
 * After the method returned. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterReturnsEvalBodyWithProfileTimePointcutData} [data] Metadata passed to the pointcuts of _evalBodyWithProfileTime_ at the `afterReturns` joinpoint.
 * - `res` _IFirebase.evalBodyWithProfileTime.Return_ The return of the method after it's successfully run.
 * - `sub` _(value: IFirebase.evalBodyWithProfileTime.Return) =&gt; ?_ Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.EvalBodyWithProfileTimePointcutData*
 * - `args` _IFirebase.EvalBodyWithProfileTimeNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.EvalBodyWithProfileTimePointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.EvalBodyWithProfileTimePointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.afterEvalBodyWithProfileTimeReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.AfterCancelsEvalBodyWithProfileTimePointcutData) => void} eco.artd.IFirebaseJoinpointModel.__afterEvalBodyWithProfileTimeCancels
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__afterEvalBodyWithProfileTimeCancels<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._afterEvalBodyWithProfileTimeCancels */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.afterEvalBodyWithProfileTimeCancels} */
/**
 * After the method cancels. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterCancelsEvalBodyWithProfileTimePointcutData} [data] Metadata passed to the pointcuts of _evalBodyWithProfileTime_ at the `afterCancels` joinpoint.
 * - `reasons` _!Set&lt;string&gt;_ The list of reasons why the method was cancelled.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.EvalBodyWithProfileTimePointcutData*
 * - `args` _IFirebase.EvalBodyWithProfileTimeNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.EvalBodyWithProfileTimePointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.EvalBodyWithProfileTimePointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.afterEvalBodyWithProfileTimeCancels = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.BeforeGetPageDataFromFirestorePointcutData) => void} eco.artd.IFirebaseJoinpointModel.__beforeGetPageDataFromFirestore
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__beforeGetPageDataFromFirestore<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._beforeGetPageDataFromFirestore */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.beforeGetPageDataFromFirestore} */
/**
 * Before the method executes. Allows to cancel the method. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.BeforeGetPageDataFromFirestorePointcutData} [data] Metadata passed to the pointcuts of _getPageDataFromFirestore_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: IFirebase.GetPageDataFromFirestoreNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `getPageDataFromFirestore` method from being executed.
 * - `sub` _(value: !Promise&lt;?IFirebase.PageData&gt;) =&gt; void_ Cancels a call to `getPageDataFromFirestore` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.GetPageDataFromFirestorePointcutData*
 * - `args` _IFirebase.GetPageDataFromFirestoreNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.GetPageDataFromFirestorePointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.GetPageDataFromFirestorePointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.beforeGetPageDataFromFirestore = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.AfterGetPageDataFromFirestorePointcutData) => void} eco.artd.IFirebaseJoinpointModel.__afterGetPageDataFromFirestore
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__afterGetPageDataFromFirestore<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._afterGetPageDataFromFirestore */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.afterGetPageDataFromFirestore} */
/**
 * After the method. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterGetPageDataFromFirestorePointcutData} [data] Metadata passed to the pointcuts of _getPageDataFromFirestore_ at the `after` joinpoint.
 * - `res` _?IFirebase.PageData_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.GetPageDataFromFirestorePointcutData*
 * - `args` _IFirebase.GetPageDataFromFirestoreNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.GetPageDataFromFirestorePointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.GetPageDataFromFirestorePointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.afterGetPageDataFromFirestore = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.AfterThrowsGetPageDataFromFirestorePointcutData) => void} eco.artd.IFirebaseJoinpointModel.__afterGetPageDataFromFirestoreThrows
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__afterGetPageDataFromFirestoreThrows<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._afterGetPageDataFromFirestoreThrows */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.afterGetPageDataFromFirestoreThrows} */
/**
 * After the method throw. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterThrowsGetPageDataFromFirestorePointcutData} [data] Metadata passed to the pointcuts of _getPageDataFromFirestore_ at the `afterThrows` joinpoint.
 * - `err` _!Error_ The error with which the method failed.
 * - `hide` _() =&gt; void_ Hides the error in the `getPageDataFromFirestore` (use only at the top-level of program flow).
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.GetPageDataFromFirestorePointcutData*
 * - `args` _IFirebase.GetPageDataFromFirestoreNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.GetPageDataFromFirestorePointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.GetPageDataFromFirestorePointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.afterGetPageDataFromFirestoreThrows = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.AfterReturnsGetPageDataFromFirestorePointcutData) => void} eco.artd.IFirebaseJoinpointModel.__afterGetPageDataFromFirestoreReturns
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__afterGetPageDataFromFirestoreReturns<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._afterGetPageDataFromFirestoreReturns */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.afterGetPageDataFromFirestoreReturns} */
/**
 * After the method returned. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterReturnsGetPageDataFromFirestorePointcutData} [data] Metadata passed to the pointcuts of _getPageDataFromFirestore_ at the `afterReturns` joinpoint.
 * - `res` _?IFirebase.PageData_ The return of the method after it's successfully run.
 * - `sub` _(value: !Promise&lt;?IFirebase.PageData&gt;&vert;?IFirebase.PageData) =&gt; ?_ Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.GetPageDataFromFirestorePointcutData*
 * - `args` _IFirebase.GetPageDataFromFirestoreNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.GetPageDataFromFirestorePointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.GetPageDataFromFirestorePointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.afterGetPageDataFromFirestoreReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.AfterCancelsGetPageDataFromFirestorePointcutData) => void} eco.artd.IFirebaseJoinpointModel.__afterGetPageDataFromFirestoreCancels
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__afterGetPageDataFromFirestoreCancels<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._afterGetPageDataFromFirestoreCancels */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.afterGetPageDataFromFirestoreCancels} */
/**
 * After the method cancels. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterCancelsGetPageDataFromFirestorePointcutData} [data] Metadata passed to the pointcuts of _getPageDataFromFirestore_ at the `afterCancels` joinpoint.
 * - `reasons` _!Set&lt;string&gt;_ The list of reasons why the method was cancelled.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.GetPageDataFromFirestorePointcutData*
 * - `args` _IFirebase.GetPageDataFromFirestoreNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.GetPageDataFromFirestorePointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.GetPageDataFromFirestorePointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.afterGetPageDataFromFirestoreCancels = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterGetPageDataFromFirestorePointcutData) => void} eco.artd.IFirebaseJoinpointModel.__immediatelyAfterGetPageDataFromFirestore
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__immediatelyAfterGetPageDataFromFirestore<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._immediatelyAfterGetPageDataFromFirestore */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.immediatelyAfterGetPageDataFromFirestore} */
/**
 * Immediately after the async method returned its promise. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterGetPageDataFromFirestorePointcutData} [data] Metadata passed to the pointcuts of _getPageDataFromFirestore_ at the `immediatelyAfter` joinpoint.
 * - `promise` _!Promise&lt;?IFirebase.PageData&gt;_ The promise with the return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.GetPageDataFromFirestorePointcutData*
 * - `args` _IFirebase.GetPageDataFromFirestoreNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.GetPageDataFromFirestorePointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.GetPageDataFromFirestorePointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.immediatelyAfterGetPageDataFromFirestore = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.BeforeDownloadAndEvalPageDataPointcutData) => void} eco.artd.IFirebaseJoinpointModel.__beforeDownloadAndEvalPageData
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__beforeDownloadAndEvalPageData<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._beforeDownloadAndEvalPageData */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.beforeDownloadAndEvalPageData} */
/**
 * Before the method executes. Allows to cancel the method. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.BeforeDownloadAndEvalPageDataPointcutData} [data] Metadata passed to the pointcuts of _downloadAndEvalPageData_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: IFirebase.DownloadAndEvalPageDataNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `downloadAndEvalPageData` method from being executed.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.DownloadAndEvalPageDataPointcutData*
 * - `args` _IFirebase.DownloadAndEvalPageDataNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.DownloadAndEvalPageDataPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.DownloadAndEvalPageDataPointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.beforeDownloadAndEvalPageData = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.AfterDownloadAndEvalPageDataPointcutData) => void} eco.artd.IFirebaseJoinpointModel.__afterDownloadAndEvalPageData
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__afterDownloadAndEvalPageData<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._afterDownloadAndEvalPageData */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.afterDownloadAndEvalPageData} */
/**
 * After the method. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterDownloadAndEvalPageDataPointcutData} [data] Metadata passed to the pointcuts of _downloadAndEvalPageData_ at the `after` joinpoint.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.DownloadAndEvalPageDataPointcutData*
 * - `args` _IFirebase.DownloadAndEvalPageDataNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.DownloadAndEvalPageDataPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.DownloadAndEvalPageDataPointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.afterDownloadAndEvalPageData = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.AfterThrowsDownloadAndEvalPageDataPointcutData) => void} eco.artd.IFirebaseJoinpointModel.__afterDownloadAndEvalPageDataThrows
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__afterDownloadAndEvalPageDataThrows<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._afterDownloadAndEvalPageDataThrows */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.afterDownloadAndEvalPageDataThrows} */
/**
 * After the method throw. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterThrowsDownloadAndEvalPageDataPointcutData} [data] Metadata passed to the pointcuts of _downloadAndEvalPageData_ at the `afterThrows` joinpoint.
 * - `err` _!Error_ The error with which the method failed.
 * - `hide` _() =&gt; void_ Hides the error in the `downloadAndEvalPageData` (use only at the top-level of program flow).
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.DownloadAndEvalPageDataPointcutData*
 * - `args` _IFirebase.DownloadAndEvalPageDataNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.DownloadAndEvalPageDataPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.DownloadAndEvalPageDataPointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.afterDownloadAndEvalPageDataThrows = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.AfterReturnsDownloadAndEvalPageDataPointcutData) => void} eco.artd.IFirebaseJoinpointModel.__afterDownloadAndEvalPageDataReturns
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__afterDownloadAndEvalPageDataReturns<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._afterDownloadAndEvalPageDataReturns */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.afterDownloadAndEvalPageDataReturns} */
/**
 * After the method returned. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterReturnsDownloadAndEvalPageDataPointcutData} [data] Metadata passed to the pointcuts of _downloadAndEvalPageData_ at the `afterReturns` joinpoint.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.DownloadAndEvalPageDataPointcutData*
 * - `args` _IFirebase.DownloadAndEvalPageDataNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.DownloadAndEvalPageDataPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.DownloadAndEvalPageDataPointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.afterDownloadAndEvalPageDataReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.AfterCancelsDownloadAndEvalPageDataPointcutData) => void} eco.artd.IFirebaseJoinpointModel.__afterDownloadAndEvalPageDataCancels
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__afterDownloadAndEvalPageDataCancels<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._afterDownloadAndEvalPageDataCancels */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.afterDownloadAndEvalPageDataCancels} */
/**
 * After the method cancels. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterCancelsDownloadAndEvalPageDataPointcutData} [data] Metadata passed to the pointcuts of _downloadAndEvalPageData_ at the `afterCancels` joinpoint.
 * - `reasons` _!Set&lt;string&gt;_ The list of reasons why the method was cancelled.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.DownloadAndEvalPageDataPointcutData*
 * - `args` _IFirebase.DownloadAndEvalPageDataNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.DownloadAndEvalPageDataPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.DownloadAndEvalPageDataPointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.afterDownloadAndEvalPageDataCancels = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterDownloadAndEvalPageDataPointcutData) => void} eco.artd.IFirebaseJoinpointModel.__immediatelyAfterDownloadAndEvalPageData
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__immediatelyAfterDownloadAndEvalPageData<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._immediatelyAfterDownloadAndEvalPageData */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.immediatelyAfterDownloadAndEvalPageData} */
/**
 * Immediately after the async method returned its promise. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterDownloadAndEvalPageDataPointcutData} [data] Metadata passed to the pointcuts of _downloadAndEvalPageData_ at the `immediatelyAfter` joinpoint.
 * - `promise` _!Promise&lt;void&gt;_ The promise with the return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.DownloadAndEvalPageDataPointcutData*
 * - `args` _IFirebase.DownloadAndEvalPageDataNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.DownloadAndEvalPageDataPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.DownloadAndEvalPageDataPointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.immediatelyAfterDownloadAndEvalPageData = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.BeforeEvalBodyPointcutData) => void} eco.artd.IFirebaseJoinpointModel.__beforeEvalBody
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__beforeEvalBody<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._beforeEvalBody */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.beforeEvalBody} */
/**
 * Before the method executes. Allows to cancel the method. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.BeforeEvalBodyPointcutData} [data] Metadata passed to the pointcuts of _evalBody_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: IFirebase.EvalBodyNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `evalBody` method from being executed.
 * - `sub` _(value: (arg0: !IncomingMessage, arg1: !ServerResponse) =&gt; (void&vert;Promise&lt;void&gt;)) =&gt; void_ Cancels a call to `evalBody` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.EvalBodyPointcutData*
 * - `args` _IFirebase.EvalBodyNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.EvalBodyPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.EvalBodyPointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.beforeEvalBody = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.AfterEvalBodyPointcutData) => void} eco.artd.IFirebaseJoinpointModel.__afterEvalBody
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__afterEvalBody<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._afterEvalBody */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.afterEvalBody} */
/**
 * After the method. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterEvalBodyPointcutData} [data] Metadata passed to the pointcuts of _evalBody_ at the `after` joinpoint.
 * - `res` _(arg0: !IncomingMessage, arg1: !ServerResponse) =&gt; (void&vert;Promise&lt;void&gt;)_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.EvalBodyPointcutData*
 * - `args` _IFirebase.EvalBodyNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.EvalBodyPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.EvalBodyPointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.afterEvalBody = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.AfterThrowsEvalBodyPointcutData) => void} eco.artd.IFirebaseJoinpointModel.__afterEvalBodyThrows
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__afterEvalBodyThrows<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._afterEvalBodyThrows */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.afterEvalBodyThrows} */
/**
 * After the method throw. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterThrowsEvalBodyPointcutData} [data] Metadata passed to the pointcuts of _evalBody_ at the `afterThrows` joinpoint.
 * - `err` _!Error_ The error with which the method failed.
 * - `hide` _() =&gt; void_ Hides the error in the `evalBody` (use only at the top-level of program flow).
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.EvalBodyPointcutData*
 * - `args` _IFirebase.EvalBodyNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.EvalBodyPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.EvalBodyPointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.afterEvalBodyThrows = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.AfterReturnsEvalBodyPointcutData) => void} eco.artd.IFirebaseJoinpointModel.__afterEvalBodyReturns
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__afterEvalBodyReturns<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._afterEvalBodyReturns */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.afterEvalBodyReturns} */
/**
 * After the method returned. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterReturnsEvalBodyPointcutData} [data] Metadata passed to the pointcuts of _evalBody_ at the `afterReturns` joinpoint.
 * - `res` _(arg0: !IncomingMessage, arg1: !ServerResponse) =&gt; (void&vert;Promise&lt;void&gt;)_ The return of the method after it's successfully run.
 * - `sub` _(value: (arg0: !IncomingMessage, arg1: !ServerResponse) =&gt; (void&vert;Promise&lt;void&gt;)) =&gt; ?_ Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.EvalBodyPointcutData*
 * - `args` _IFirebase.EvalBodyNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.EvalBodyPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.EvalBodyPointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.afterEvalBodyReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.AfterCancelsEvalBodyPointcutData) => void} eco.artd.IFirebaseJoinpointModel.__afterEvalBodyCancels
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__afterEvalBodyCancels<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._afterEvalBodyCancels */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.afterEvalBodyCancels} */
/**
 * After the method cancels. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterCancelsEvalBodyPointcutData} [data] Metadata passed to the pointcuts of _evalBody_ at the `afterCancels` joinpoint.
 * - `reasons` _!Set&lt;string&gt;_ The list of reasons why the method was cancelled.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.EvalBodyPointcutData*
 * - `args` _IFirebase.EvalBodyNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.EvalBodyPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.EvalBodyPointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.afterEvalBodyCancels = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.BeforeLogRpcInvocationPointcutData) => void} eco.artd.IFirebaseJoinpointModel.__beforeLogRpcInvocation
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__beforeLogRpcInvocation<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._beforeLogRpcInvocation */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.beforeLogRpcInvocation} */
/**
 * Before the method executes. Allows to cancel the method. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.BeforeLogRpcInvocationPointcutData} [data] Metadata passed to the pointcuts of _logRpcInvocation_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: IFirebase.LogRpcInvocationNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `logRpcInvocation` method from being executed.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.LogRpcInvocationPointcutData*
 * - `args` _IFirebase.LogRpcInvocationNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.LogRpcInvocationPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.LogRpcInvocationPointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.beforeLogRpcInvocation = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.AfterLogRpcInvocationPointcutData) => void} eco.artd.IFirebaseJoinpointModel.__afterLogRpcInvocation
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__afterLogRpcInvocation<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._afterLogRpcInvocation */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.afterLogRpcInvocation} */
/**
 * After the method. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterLogRpcInvocationPointcutData} [data] Metadata passed to the pointcuts of _logRpcInvocation_ at the `after` joinpoint.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.LogRpcInvocationPointcutData*
 * - `args` _IFirebase.LogRpcInvocationNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.LogRpcInvocationPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.LogRpcInvocationPointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.afterLogRpcInvocation = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.AfterThrowsLogRpcInvocationPointcutData) => void} eco.artd.IFirebaseJoinpointModel.__afterLogRpcInvocationThrows
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__afterLogRpcInvocationThrows<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._afterLogRpcInvocationThrows */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.afterLogRpcInvocationThrows} */
/**
 * After the method throw. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterThrowsLogRpcInvocationPointcutData} [data] Metadata passed to the pointcuts of _logRpcInvocation_ at the `afterThrows` joinpoint.
 * - `err` _!Error_ The error with which the method failed.
 * - `hide` _() =&gt; void_ Hides the error in the `logRpcInvocation` (use only at the top-level of program flow).
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.LogRpcInvocationPointcutData*
 * - `args` _IFirebase.LogRpcInvocationNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.LogRpcInvocationPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.LogRpcInvocationPointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.afterLogRpcInvocationThrows = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.AfterReturnsLogRpcInvocationPointcutData) => void} eco.artd.IFirebaseJoinpointModel.__afterLogRpcInvocationReturns
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__afterLogRpcInvocationReturns<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._afterLogRpcInvocationReturns */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.afterLogRpcInvocationReturns} */
/**
 * After the method returned. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterReturnsLogRpcInvocationPointcutData} [data] Metadata passed to the pointcuts of _logRpcInvocation_ at the `afterReturns` joinpoint.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.LogRpcInvocationPointcutData*
 * - `args` _IFirebase.LogRpcInvocationNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.LogRpcInvocationPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.LogRpcInvocationPointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.afterLogRpcInvocationReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.AfterCancelsLogRpcInvocationPointcutData) => void} eco.artd.IFirebaseJoinpointModel.__afterLogRpcInvocationCancels
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__afterLogRpcInvocationCancels<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._afterLogRpcInvocationCancels */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.afterLogRpcInvocationCancels} */
/**
 * After the method cancels. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterCancelsLogRpcInvocationPointcutData} [data] Metadata passed to the pointcuts of _logRpcInvocation_ at the `afterCancels` joinpoint.
 * - `reasons` _!Set&lt;string&gt;_ The list of reasons why the method was cancelled.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.LogRpcInvocationPointcutData*
 * - `args` _IFirebase.LogRpcInvocationNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.LogRpcInvocationPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.LogRpcInvocationPointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.afterLogRpcInvocationCancels = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterLogRpcInvocationPointcutData) => void} eco.artd.IFirebaseJoinpointModel.__immediatelyAfterLogRpcInvocation
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__immediatelyAfterLogRpcInvocation<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._immediatelyAfterLogRpcInvocation */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.immediatelyAfterLogRpcInvocation} */
/**
 * Immediately after the async method returned its promise. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterLogRpcInvocationPointcutData} [data] Metadata passed to the pointcuts of _logRpcInvocation_ at the `immediatelyAfter` joinpoint.
 * - `promise` _!Promise&lt;void&gt;_ The promise with the return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.LogRpcInvocationPointcutData*
 * - `args` _IFirebase.LogRpcInvocationNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.LogRpcInvocationPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.LogRpcInvocationPointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.immediatelyAfterLogRpcInvocation = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.BeforeFetchFunctionsFilePointcutData) => void} eco.artd.IFirebaseJoinpointModel.__beforeFetchFunctionsFile
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__beforeFetchFunctionsFile<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._beforeFetchFunctionsFile */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.beforeFetchFunctionsFile} */
/**
 * Before the method executes. Allows to cancel the method. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.BeforeFetchFunctionsFilePointcutData} [data] Metadata passed to the pointcuts of _fetchFunctionsFile_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: IFirebase.FetchFunctionsFileNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `fetchFunctionsFile` method from being executed.
 * - `sub` _(value: !Promise&lt;IFirebase.fetchFunctionsFile.Return&gt;) =&gt; void_ Cancels a call to `fetchFunctionsFile` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.FetchFunctionsFilePointcutData*
 * - `args` _IFirebase.FetchFunctionsFileNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.FetchFunctionsFilePointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.FetchFunctionsFilePointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.beforeFetchFunctionsFile = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.AfterFetchFunctionsFilePointcutData) => void} eco.artd.IFirebaseJoinpointModel.__afterFetchFunctionsFile
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__afterFetchFunctionsFile<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._afterFetchFunctionsFile */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.afterFetchFunctionsFile} */
/**
 * After the method. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterFetchFunctionsFilePointcutData} [data] Metadata passed to the pointcuts of _fetchFunctionsFile_ at the `after` joinpoint.
 * - `res` _IFirebase.fetchFunctionsFile.Return_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.FetchFunctionsFilePointcutData*
 * - `args` _IFirebase.FetchFunctionsFileNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.FetchFunctionsFilePointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.FetchFunctionsFilePointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.afterFetchFunctionsFile = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.AfterThrowsFetchFunctionsFilePointcutData) => void} eco.artd.IFirebaseJoinpointModel.__afterFetchFunctionsFileThrows
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__afterFetchFunctionsFileThrows<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._afterFetchFunctionsFileThrows */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.afterFetchFunctionsFileThrows} */
/**
 * After the method throw. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterThrowsFetchFunctionsFilePointcutData} [data] Metadata passed to the pointcuts of _fetchFunctionsFile_ at the `afterThrows` joinpoint.
 * - `err` _!Error_ The error with which the method failed.
 * - `hide` _() =&gt; void_ Hides the error in the `fetchFunctionsFile` (use only at the top-level of program flow).
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.FetchFunctionsFilePointcutData*
 * - `args` _IFirebase.FetchFunctionsFileNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.FetchFunctionsFilePointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.FetchFunctionsFilePointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.afterFetchFunctionsFileThrows = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.AfterReturnsFetchFunctionsFilePointcutData) => void} eco.artd.IFirebaseJoinpointModel.__afterFetchFunctionsFileReturns
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__afterFetchFunctionsFileReturns<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._afterFetchFunctionsFileReturns */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.afterFetchFunctionsFileReturns} */
/**
 * After the method returned. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterReturnsFetchFunctionsFilePointcutData} [data] Metadata passed to the pointcuts of _fetchFunctionsFile_ at the `afterReturns` joinpoint.
 * - `res` _IFirebase.fetchFunctionsFile.Return_ The return of the method after it's successfully run.
 * - `sub` _(value: !Promise&lt;IFirebase.fetchFunctionsFile.Return&gt;&vert;IFirebase.fetchFunctionsFile.Return) =&gt; ?_ Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.FetchFunctionsFilePointcutData*
 * - `args` _IFirebase.FetchFunctionsFileNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.FetchFunctionsFilePointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.FetchFunctionsFilePointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.afterFetchFunctionsFileReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.AfterCancelsFetchFunctionsFilePointcutData) => void} eco.artd.IFirebaseJoinpointModel.__afterFetchFunctionsFileCancels
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__afterFetchFunctionsFileCancels<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._afterFetchFunctionsFileCancels */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.afterFetchFunctionsFileCancels} */
/**
 * After the method cancels. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterCancelsFetchFunctionsFilePointcutData} [data] Metadata passed to the pointcuts of _fetchFunctionsFile_ at the `afterCancels` joinpoint.
 * - `reasons` _!Set&lt;string&gt;_ The list of reasons why the method was cancelled.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.FetchFunctionsFilePointcutData*
 * - `args` _IFirebase.FetchFunctionsFileNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.FetchFunctionsFilePointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.FetchFunctionsFilePointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.afterFetchFunctionsFileCancels = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterFetchFunctionsFilePointcutData) => void} eco.artd.IFirebaseJoinpointModel.__immediatelyAfterFetchFunctionsFile
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__immediatelyAfterFetchFunctionsFile<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._immediatelyAfterFetchFunctionsFile */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.immediatelyAfterFetchFunctionsFile} */
/**
 * Immediately after the async method returned its promise. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterFetchFunctionsFilePointcutData} [data] Metadata passed to the pointcuts of _fetchFunctionsFile_ at the `immediatelyAfter` joinpoint.
 * - `promise` _!Promise&lt;IFirebase.fetchFunctionsFile.Return&gt;_ The promise with the return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.FetchFunctionsFilePointcutData*
 * - `args` _IFirebase.FetchFunctionsFileNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.FetchFunctionsFilePointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.FetchFunctionsFilePointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.immediatelyAfterFetchFunctionsFile = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.BeforeDownloadDepsPointcutData) => void} eco.artd.IFirebaseJoinpointModel.__beforeDownloadDeps
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__beforeDownloadDeps<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._beforeDownloadDeps */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.beforeDownloadDeps} */
/**
 * Before the method executes. Allows to cancel the method. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.BeforeDownloadDepsPointcutData} [data] Metadata passed to the pointcuts of _downloadDeps_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: IFirebase.DownloadDepsNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `downloadDeps` method from being executed.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.DownloadDepsPointcutData*
 * - `args` _IFirebase.DownloadDepsNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.DownloadDepsPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.DownloadDepsPointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.beforeDownloadDeps = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.AfterDownloadDepsPointcutData) => void} eco.artd.IFirebaseJoinpointModel.__afterDownloadDeps
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__afterDownloadDeps<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._afterDownloadDeps */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.afterDownloadDeps} */
/**
 * After the method. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterDownloadDepsPointcutData} [data] Metadata passed to the pointcuts of _downloadDeps_ at the `after` joinpoint.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.DownloadDepsPointcutData*
 * - `args` _IFirebase.DownloadDepsNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.DownloadDepsPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.DownloadDepsPointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.afterDownloadDeps = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.AfterThrowsDownloadDepsPointcutData) => void} eco.artd.IFirebaseJoinpointModel.__afterDownloadDepsThrows
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__afterDownloadDepsThrows<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._afterDownloadDepsThrows */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.afterDownloadDepsThrows} */
/**
 * After the method throw. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterThrowsDownloadDepsPointcutData} [data] Metadata passed to the pointcuts of _downloadDeps_ at the `afterThrows` joinpoint.
 * - `err` _!Error_ The error with which the method failed.
 * - `hide` _() =&gt; void_ Hides the error in the `downloadDeps` (use only at the top-level of program flow).
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.DownloadDepsPointcutData*
 * - `args` _IFirebase.DownloadDepsNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.DownloadDepsPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.DownloadDepsPointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.afterDownloadDepsThrows = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.AfterReturnsDownloadDepsPointcutData) => void} eco.artd.IFirebaseJoinpointModel.__afterDownloadDepsReturns
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__afterDownloadDepsReturns<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._afterDownloadDepsReturns */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.afterDownloadDepsReturns} */
/**
 * After the method returned. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterReturnsDownloadDepsPointcutData} [data] Metadata passed to the pointcuts of _downloadDeps_ at the `afterReturns` joinpoint.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.DownloadDepsPointcutData*
 * - `args` _IFirebase.DownloadDepsNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.DownloadDepsPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.DownloadDepsPointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.afterDownloadDepsReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.AfterCancelsDownloadDepsPointcutData) => void} eco.artd.IFirebaseJoinpointModel.__afterDownloadDepsCancels
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__afterDownloadDepsCancels<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._afterDownloadDepsCancels */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.afterDownloadDepsCancels} */
/**
 * After the method cancels. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterCancelsDownloadDepsPointcutData} [data] Metadata passed to the pointcuts of _downloadDeps_ at the `afterCancels` joinpoint.
 * - `reasons` _!Set&lt;string&gt;_ The list of reasons why the method was cancelled.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.DownloadDepsPointcutData*
 * - `args` _IFirebase.DownloadDepsNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.DownloadDepsPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.DownloadDepsPointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.afterDownloadDepsCancels = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterDownloadDepsPointcutData) => void} eco.artd.IFirebaseJoinpointModel.__immediatelyAfterDownloadDeps
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__immediatelyAfterDownloadDeps<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._immediatelyAfterDownloadDeps */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.immediatelyAfterDownloadDeps} */
/**
 * Immediately after the async method returned its promise. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterDownloadDepsPointcutData} [data] Metadata passed to the pointcuts of _downloadDeps_ at the `immediatelyAfter` joinpoint.
 * - `promise` _!Promise&lt;void&gt;_ The promise with the return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.DownloadDepsPointcutData*
 * - `args` _IFirebase.DownloadDepsNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.DownloadDepsPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.DownloadDepsPointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.immediatelyAfterDownloadDeps = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.BeforeDownloadMethodPointcutData) => void} eco.artd.IFirebaseJoinpointModel.__beforeDownloadMethod
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__beforeDownloadMethod<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._beforeDownloadMethod */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.beforeDownloadMethod} */
/**
 * Before the method executes. Allows to cancel the method. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.BeforeDownloadMethodPointcutData} [data] Metadata passed to the pointcuts of _downloadMethod_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: IFirebase.DownloadMethodNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `downloadMethod` method from being executed.
 * - `sub` _(value: !Promise&lt;string&gt;) =&gt; void_ Cancels a call to `downloadMethod` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.DownloadMethodPointcutData*
 * - `args` _IFirebase.DownloadMethodNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.DownloadMethodPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.DownloadMethodPointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.beforeDownloadMethod = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.AfterDownloadMethodPointcutData) => void} eco.artd.IFirebaseJoinpointModel.__afterDownloadMethod
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__afterDownloadMethod<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._afterDownloadMethod */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.afterDownloadMethod} */
/**
 * After the method. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterDownloadMethodPointcutData} [data] Metadata passed to the pointcuts of _downloadMethod_ at the `after` joinpoint.
 * - `res` _string_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.DownloadMethodPointcutData*
 * - `args` _IFirebase.DownloadMethodNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.DownloadMethodPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.DownloadMethodPointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.afterDownloadMethod = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.AfterThrowsDownloadMethodPointcutData) => void} eco.artd.IFirebaseJoinpointModel.__afterDownloadMethodThrows
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__afterDownloadMethodThrows<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._afterDownloadMethodThrows */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.afterDownloadMethodThrows} */
/**
 * After the method throw. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterThrowsDownloadMethodPointcutData} [data] Metadata passed to the pointcuts of _downloadMethod_ at the `afterThrows` joinpoint.
 * - `err` _!Error_ The error with which the method failed.
 * - `hide` _() =&gt; void_ Hides the error in the `downloadMethod` (use only at the top-level of program flow).
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.DownloadMethodPointcutData*
 * - `args` _IFirebase.DownloadMethodNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.DownloadMethodPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.DownloadMethodPointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.afterDownloadMethodThrows = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.AfterReturnsDownloadMethodPointcutData) => void} eco.artd.IFirebaseJoinpointModel.__afterDownloadMethodReturns
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__afterDownloadMethodReturns<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._afterDownloadMethodReturns */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.afterDownloadMethodReturns} */
/**
 * After the method returned. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterReturnsDownloadMethodPointcutData} [data] Metadata passed to the pointcuts of _downloadMethod_ at the `afterReturns` joinpoint.
 * - `res` _string_ The return of the method after it's successfully run.
 * - `sub` _(value: !Promise&lt;string&gt;&vert;string) =&gt; ?_ Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.DownloadMethodPointcutData*
 * - `args` _IFirebase.DownloadMethodNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.DownloadMethodPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.DownloadMethodPointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.afterDownloadMethodReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.AfterCancelsDownloadMethodPointcutData) => void} eco.artd.IFirebaseJoinpointModel.__afterDownloadMethodCancels
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__afterDownloadMethodCancels<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._afterDownloadMethodCancels */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.afterDownloadMethodCancels} */
/**
 * After the method cancels. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterCancelsDownloadMethodPointcutData} [data] Metadata passed to the pointcuts of _downloadMethod_ at the `afterCancels` joinpoint.
 * - `reasons` _!Set&lt;string&gt;_ The list of reasons why the method was cancelled.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.DownloadMethodPointcutData*
 * - `args` _IFirebase.DownloadMethodNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.DownloadMethodPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.DownloadMethodPointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.afterDownloadMethodCancels = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterDownloadMethodPointcutData) => void} eco.artd.IFirebaseJoinpointModel.__immediatelyAfterDownloadMethod
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__immediatelyAfterDownloadMethod<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._immediatelyAfterDownloadMethod */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.immediatelyAfterDownloadMethod} */
/**
 * Immediately after the async method returned its promise. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterDownloadMethodPointcutData} [data] Metadata passed to the pointcuts of _downloadMethod_ at the `immediatelyAfter` joinpoint.
 * - `promise` _!Promise&lt;string&gt;_ The promise with the return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.DownloadMethodPointcutData*
 * - `args` _IFirebase.DownloadMethodNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.DownloadMethodPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.DownloadMethodPointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.immediatelyAfterDownloadMethod = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.BeforeDownloadMethodDirectlyPointcutData) => void} eco.artd.IFirebaseJoinpointModel.__beforeDownloadMethodDirectly
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__beforeDownloadMethodDirectly<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._beforeDownloadMethodDirectly */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.beforeDownloadMethodDirectly} */
/**
 * Before the method executes. Allows to cancel the method. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.BeforeDownloadMethodDirectlyPointcutData} [data] Metadata passed to the pointcuts of _downloadMethodDirectly_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: IFirebase.DownloadMethodDirectlyNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `downloadMethodDirectly` method from being executed.
 * - `sub` _(value: !Promise&lt;IFirebase.downloadMethodDirectly.Return&gt;) =&gt; void_ Cancels a call to `downloadMethodDirectly` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.DownloadMethodDirectlyPointcutData*
 * - `args` _IFirebase.DownloadMethodDirectlyNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.DownloadMethodDirectlyPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.DownloadMethodDirectlyPointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.beforeDownloadMethodDirectly = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.AfterDownloadMethodDirectlyPointcutData) => void} eco.artd.IFirebaseJoinpointModel.__afterDownloadMethodDirectly
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__afterDownloadMethodDirectly<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._afterDownloadMethodDirectly */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.afterDownloadMethodDirectly} */
/**
 * After the method. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterDownloadMethodDirectlyPointcutData} [data] Metadata passed to the pointcuts of _downloadMethodDirectly_ at the `after` joinpoint.
 * - `res` _IFirebase.downloadMethodDirectly.Return_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.DownloadMethodDirectlyPointcutData*
 * - `args` _IFirebase.DownloadMethodDirectlyNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.DownloadMethodDirectlyPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.DownloadMethodDirectlyPointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.afterDownloadMethodDirectly = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.AfterThrowsDownloadMethodDirectlyPointcutData) => void} eco.artd.IFirebaseJoinpointModel.__afterDownloadMethodDirectlyThrows
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__afterDownloadMethodDirectlyThrows<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._afterDownloadMethodDirectlyThrows */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.afterDownloadMethodDirectlyThrows} */
/**
 * After the method throw. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterThrowsDownloadMethodDirectlyPointcutData} [data] Metadata passed to the pointcuts of _downloadMethodDirectly_ at the `afterThrows` joinpoint.
 * - `err` _!Error_ The error with which the method failed.
 * - `hide` _() =&gt; void_ Hides the error in the `downloadMethodDirectly` (use only at the top-level of program flow).
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.DownloadMethodDirectlyPointcutData*
 * - `args` _IFirebase.DownloadMethodDirectlyNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.DownloadMethodDirectlyPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.DownloadMethodDirectlyPointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.afterDownloadMethodDirectlyThrows = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.AfterReturnsDownloadMethodDirectlyPointcutData) => void} eco.artd.IFirebaseJoinpointModel.__afterDownloadMethodDirectlyReturns
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__afterDownloadMethodDirectlyReturns<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._afterDownloadMethodDirectlyReturns */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.afterDownloadMethodDirectlyReturns} */
/**
 * After the method returned. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterReturnsDownloadMethodDirectlyPointcutData} [data] Metadata passed to the pointcuts of _downloadMethodDirectly_ at the `afterReturns` joinpoint.
 * - `res` _IFirebase.downloadMethodDirectly.Return_ The return of the method after it's successfully run.
 * - `sub` _(value: !Promise&lt;IFirebase.downloadMethodDirectly.Return&gt;&vert;IFirebase.downloadMethodDirectly.Return) =&gt; ?_ Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.DownloadMethodDirectlyPointcutData*
 * - `args` _IFirebase.DownloadMethodDirectlyNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.DownloadMethodDirectlyPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.DownloadMethodDirectlyPointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.afterDownloadMethodDirectlyReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.AfterCancelsDownloadMethodDirectlyPointcutData) => void} eco.artd.IFirebaseJoinpointModel.__afterDownloadMethodDirectlyCancels
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__afterDownloadMethodDirectlyCancels<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._afterDownloadMethodDirectlyCancels */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.afterDownloadMethodDirectlyCancels} */
/**
 * After the method cancels. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterCancelsDownloadMethodDirectlyPointcutData} [data] Metadata passed to the pointcuts of _downloadMethodDirectly_ at the `afterCancels` joinpoint.
 * - `reasons` _!Set&lt;string&gt;_ The list of reasons why the method was cancelled.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.DownloadMethodDirectlyPointcutData*
 * - `args` _IFirebase.DownloadMethodDirectlyNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.DownloadMethodDirectlyPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.DownloadMethodDirectlyPointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.afterDownloadMethodDirectlyCancels = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterDownloadMethodDirectlyPointcutData) => void} eco.artd.IFirebaseJoinpointModel.__immediatelyAfterDownloadMethodDirectly
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__immediatelyAfterDownloadMethodDirectly<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._immediatelyAfterDownloadMethodDirectly */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.immediatelyAfterDownloadMethodDirectly} */
/**
 * Immediately after the async method returned its promise. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterDownloadMethodDirectlyPointcutData} [data] Metadata passed to the pointcuts of _downloadMethodDirectly_ at the `immediatelyAfter` joinpoint.
 * - `promise` _!Promise&lt;IFirebase.downloadMethodDirectly.Return&gt;_ The promise with the return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.DownloadMethodDirectlyPointcutData*
 * - `args` _IFirebase.DownloadMethodDirectlyNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.DownloadMethodDirectlyPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.DownloadMethodDirectlyPointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.immediatelyAfterDownloadMethodDirectly = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.BeforeGetIdPointcutData) => void} eco.artd.IFirebaseJoinpointModel.__beforeGetId
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__beforeGetId<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._beforeGetId */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.beforeGetId} */
/**
 * Before the method executes. Allows to cancel the method. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.BeforeGetIdPointcutData} [data] Metadata passed to the pointcuts of _getId_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `getId` method from being executed.
 * - `sub` _(value: number) =&gt; void_ Cancels a call to `getId` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.GetIdPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.GetIdPointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.beforeGetId = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.AfterGetIdPointcutData) => void} eco.artd.IFirebaseJoinpointModel.__afterGetId
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__afterGetId<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._afterGetId */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.afterGetId} */
/**
 * After the method. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterGetIdPointcutData} [data] Metadata passed to the pointcuts of _getId_ at the `after` joinpoint.
 * - `res` _number_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.GetIdPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.GetIdPointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.afterGetId = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.AfterThrowsGetIdPointcutData) => void} eco.artd.IFirebaseJoinpointModel.__afterGetIdThrows
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__afterGetIdThrows<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._afterGetIdThrows */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.afterGetIdThrows} */
/**
 * After the method throw. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterThrowsGetIdPointcutData} [data] Metadata passed to the pointcuts of _getId_ at the `afterThrows` joinpoint.
 * - `err` _!Error_ The error with which the method failed.
 * - `hide` _() =&gt; void_ Hides the error in the `getId` (use only at the top-level of program flow).
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.GetIdPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.GetIdPointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.afterGetIdThrows = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.AfterReturnsGetIdPointcutData) => void} eco.artd.IFirebaseJoinpointModel.__afterGetIdReturns
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__afterGetIdReturns<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._afterGetIdReturns */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.afterGetIdReturns} */
/**
 * After the method returned. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterReturnsGetIdPointcutData} [data] Metadata passed to the pointcuts of _getId_ at the `afterReturns` joinpoint.
 * - `res` _number_ The return of the method after it's successfully run.
 * - `sub` _(value: number) =&gt; ?_ Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.GetIdPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.GetIdPointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.afterGetIdReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.AfterCancelsGetIdPointcutData) => void} eco.artd.IFirebaseJoinpointModel.__afterGetIdCancels
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__afterGetIdCancels<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._afterGetIdCancels */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.afterGetIdCancels} */
/**
 * After the method cancels. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterCancelsGetIdPointcutData} [data] Metadata passed to the pointcuts of _getId_ at the `afterCancels` joinpoint.
 * - `reasons` _!Set&lt;string&gt;_ The list of reasons why the method was cancelled.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.GetIdPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.GetIdPointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.afterGetIdCancels = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.BeforePatchDepsPointcutData) => void} eco.artd.IFirebaseJoinpointModel.__beforePatchDeps
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__beforePatchDeps<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._beforePatchDeps */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.beforePatchDeps} */
/**
 * Before the method executes. Allows to cancel the method. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.BeforePatchDepsPointcutData} [data] Metadata passed to the pointcuts of _patchDeps_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: IFirebase.PatchDepsNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `patchDeps` method from being executed.
 * - `sub` _(value: !Promise&lt;IFirebase.patchDeps.Return&gt;) =&gt; void_ Cancels a call to `patchDeps` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.PatchDepsPointcutData*
 * - `args` _IFirebase.PatchDepsNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.PatchDepsPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.PatchDepsPointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.beforePatchDeps = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.AfterPatchDepsPointcutData) => void} eco.artd.IFirebaseJoinpointModel.__afterPatchDeps
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__afterPatchDeps<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._afterPatchDeps */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.afterPatchDeps} */
/**
 * After the method. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterPatchDepsPointcutData} [data] Metadata passed to the pointcuts of _patchDeps_ at the `after` joinpoint.
 * - `res` _IFirebase.patchDeps.Return_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.PatchDepsPointcutData*
 * - `args` _IFirebase.PatchDepsNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.PatchDepsPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.PatchDepsPointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.afterPatchDeps = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.AfterThrowsPatchDepsPointcutData) => void} eco.artd.IFirebaseJoinpointModel.__afterPatchDepsThrows
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__afterPatchDepsThrows<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._afterPatchDepsThrows */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.afterPatchDepsThrows} */
/**
 * After the method throw. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterThrowsPatchDepsPointcutData} [data] Metadata passed to the pointcuts of _patchDeps_ at the `afterThrows` joinpoint.
 * - `err` _!Error_ The error with which the method failed.
 * - `hide` _() =&gt; void_ Hides the error in the `patchDeps` (use only at the top-level of program flow).
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.PatchDepsPointcutData*
 * - `args` _IFirebase.PatchDepsNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.PatchDepsPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.PatchDepsPointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.afterPatchDepsThrows = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.AfterReturnsPatchDepsPointcutData) => void} eco.artd.IFirebaseJoinpointModel.__afterPatchDepsReturns
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__afterPatchDepsReturns<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._afterPatchDepsReturns */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.afterPatchDepsReturns} */
/**
 * After the method returned. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterReturnsPatchDepsPointcutData} [data] Metadata passed to the pointcuts of _patchDeps_ at the `afterReturns` joinpoint.
 * - `res` _IFirebase.patchDeps.Return_ The return of the method after it's successfully run.
 * - `sub` _(value: !Promise&lt;IFirebase.patchDeps.Return&gt;&vert;IFirebase.patchDeps.Return) =&gt; ?_ Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.PatchDepsPointcutData*
 * - `args` _IFirebase.PatchDepsNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.PatchDepsPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.PatchDepsPointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.afterPatchDepsReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.AfterCancelsPatchDepsPointcutData) => void} eco.artd.IFirebaseJoinpointModel.__afterPatchDepsCancels
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__afterPatchDepsCancels<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._afterPatchDepsCancels */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.afterPatchDepsCancels} */
/**
 * After the method cancels. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterCancelsPatchDepsPointcutData} [data] Metadata passed to the pointcuts of _patchDeps_ at the `afterCancels` joinpoint.
 * - `reasons` _!Set&lt;string&gt;_ The list of reasons why the method was cancelled.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.PatchDepsPointcutData*
 * - `args` _IFirebase.PatchDepsNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.PatchDepsPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.PatchDepsPointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.afterPatchDepsCancels = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterPatchDepsPointcutData) => void} eco.artd.IFirebaseJoinpointModel.__immediatelyAfterPatchDeps
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__immediatelyAfterPatchDeps<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._immediatelyAfterPatchDeps */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.immediatelyAfterPatchDeps} */
/**
 * Immediately after the async method returned its promise. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterPatchDepsPointcutData} [data] Metadata passed to the pointcuts of _patchDeps_ at the `immediatelyAfter` joinpoint.
 * - `promise` _!Promise&lt;IFirebase.patchDeps.Return&gt;_ The promise with the return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.PatchDepsPointcutData*
 * - `args` _IFirebase.PatchDepsNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.PatchDepsPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.PatchDepsPointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.immediatelyAfterPatchDeps = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.BeforeDownloadPageDataPointcutData) => void} eco.artd.IFirebaseJoinpointModel.__beforeDownloadPageData
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__beforeDownloadPageData<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._beforeDownloadPageData */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.beforeDownloadPageData} */
/**
 * Before the method executes. Allows to cancel the method. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.BeforeDownloadPageDataPointcutData} [data] Metadata passed to the pointcuts of _downloadPageData_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: IFirebase.DownloadPageDataNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `downloadPageData` method from being executed.
 * - `sub` _(value: !Promise&lt;string&gt;) =&gt; void_ Cancels a call to `downloadPageData` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.DownloadPageDataPointcutData*
 * - `args` _IFirebase.DownloadPageDataNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.DownloadPageDataPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.DownloadPageDataPointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.beforeDownloadPageData = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.AfterDownloadPageDataPointcutData) => void} eco.artd.IFirebaseJoinpointModel.__afterDownloadPageData
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__afterDownloadPageData<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._afterDownloadPageData */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.afterDownloadPageData} */
/**
 * After the method. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterDownloadPageDataPointcutData} [data] Metadata passed to the pointcuts of _downloadPageData_ at the `after` joinpoint.
 * - `res` _string_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.DownloadPageDataPointcutData*
 * - `args` _IFirebase.DownloadPageDataNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.DownloadPageDataPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.DownloadPageDataPointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.afterDownloadPageData = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.AfterThrowsDownloadPageDataPointcutData) => void} eco.artd.IFirebaseJoinpointModel.__afterDownloadPageDataThrows
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__afterDownloadPageDataThrows<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._afterDownloadPageDataThrows */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.afterDownloadPageDataThrows} */
/**
 * After the method throw. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterThrowsDownloadPageDataPointcutData} [data] Metadata passed to the pointcuts of _downloadPageData_ at the `afterThrows` joinpoint.
 * - `err` _!Error_ The error with which the method failed.
 * - `hide` _() =&gt; void_ Hides the error in the `downloadPageData` (use only at the top-level of program flow).
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.DownloadPageDataPointcutData*
 * - `args` _IFirebase.DownloadPageDataNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.DownloadPageDataPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.DownloadPageDataPointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.afterDownloadPageDataThrows = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.AfterReturnsDownloadPageDataPointcutData) => void} eco.artd.IFirebaseJoinpointModel.__afterDownloadPageDataReturns
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__afterDownloadPageDataReturns<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._afterDownloadPageDataReturns */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.afterDownloadPageDataReturns} */
/**
 * After the method returned. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterReturnsDownloadPageDataPointcutData} [data] Metadata passed to the pointcuts of _downloadPageData_ at the `afterReturns` joinpoint.
 * - `res` _string_ The return of the method after it's successfully run.
 * - `sub` _(value: !Promise&lt;string&gt;&vert;string) =&gt; ?_ Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.DownloadPageDataPointcutData*
 * - `args` _IFirebase.DownloadPageDataNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.DownloadPageDataPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.DownloadPageDataPointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.afterDownloadPageDataReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.AfterCancelsDownloadPageDataPointcutData) => void} eco.artd.IFirebaseJoinpointModel.__afterDownloadPageDataCancels
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__afterDownloadPageDataCancels<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._afterDownloadPageDataCancels */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.afterDownloadPageDataCancels} */
/**
 * After the method cancels. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterCancelsDownloadPageDataPointcutData} [data] Metadata passed to the pointcuts of _downloadPageData_ at the `afterCancels` joinpoint.
 * - `reasons` _!Set&lt;string&gt;_ The list of reasons why the method was cancelled.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.DownloadPageDataPointcutData*
 * - `args` _IFirebase.DownloadPageDataNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.DownloadPageDataPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.DownloadPageDataPointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.afterDownloadPageDataCancels = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterDownloadPageDataPointcutData) => void} eco.artd.IFirebaseJoinpointModel.__immediatelyAfterDownloadPageData
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__immediatelyAfterDownloadPageData<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._immediatelyAfterDownloadPageData */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.immediatelyAfterDownloadPageData} */
/**
 * Immediately after the async method returned its promise. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterDownloadPageDataPointcutData} [data] Metadata passed to the pointcuts of _downloadPageData_ at the `immediatelyAfter` joinpoint.
 * - `promise` _!Promise&lt;string&gt;_ The promise with the return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.DownloadPageDataPointcutData*
 * - `args` _IFirebase.DownloadPageDataNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.DownloadPageDataPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.DownloadPageDataPointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.immediatelyAfterDownloadPageData = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.BeforeLoadRequirePageDataPointcutData) => void} eco.artd.IFirebaseJoinpointModel.__beforeLoadRequirePageData
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__beforeLoadRequirePageData<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._beforeLoadRequirePageData */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.beforeLoadRequirePageData} */
/**
 * Before the method executes. Allows to cancel the method. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.BeforeLoadRequirePageDataPointcutData} [data] Metadata passed to the pointcuts of _loadRequirePageData_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: IFirebase.LoadRequirePageDataNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `loadRequirePageData` method from being executed.
 * - `sub` _(value: !IFirebase.PageData) =&gt; void_ Cancels a call to `loadRequirePageData` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.LoadRequirePageDataPointcutData*
 * - `args` _IFirebase.LoadRequirePageDataNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.LoadRequirePageDataPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.LoadRequirePageDataPointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.beforeLoadRequirePageData = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.AfterLoadRequirePageDataPointcutData) => void} eco.artd.IFirebaseJoinpointModel.__afterLoadRequirePageData
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__afterLoadRequirePageData<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._afterLoadRequirePageData */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.afterLoadRequirePageData} */
/**
 * After the method. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterLoadRequirePageDataPointcutData} [data] Metadata passed to the pointcuts of _loadRequirePageData_ at the `after` joinpoint.
 * - `res` _!IFirebase.PageData_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.LoadRequirePageDataPointcutData*
 * - `args` _IFirebase.LoadRequirePageDataNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.LoadRequirePageDataPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.LoadRequirePageDataPointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.afterLoadRequirePageData = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.AfterThrowsLoadRequirePageDataPointcutData) => void} eco.artd.IFirebaseJoinpointModel.__afterLoadRequirePageDataThrows
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__afterLoadRequirePageDataThrows<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._afterLoadRequirePageDataThrows */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.afterLoadRequirePageDataThrows} */
/**
 * After the method throw. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterThrowsLoadRequirePageDataPointcutData} [data] Metadata passed to the pointcuts of _loadRequirePageData_ at the `afterThrows` joinpoint.
 * - `err` _!Error_ The error with which the method failed.
 * - `hide` _() =&gt; void_ Hides the error in the `loadRequirePageData` (use only at the top-level of program flow).
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.LoadRequirePageDataPointcutData*
 * - `args` _IFirebase.LoadRequirePageDataNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.LoadRequirePageDataPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.LoadRequirePageDataPointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.afterLoadRequirePageDataThrows = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.AfterReturnsLoadRequirePageDataPointcutData) => void} eco.artd.IFirebaseJoinpointModel.__afterLoadRequirePageDataReturns
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__afterLoadRequirePageDataReturns<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._afterLoadRequirePageDataReturns */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.afterLoadRequirePageDataReturns} */
/**
 * After the method returned. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterReturnsLoadRequirePageDataPointcutData} [data] Metadata passed to the pointcuts of _loadRequirePageData_ at the `afterReturns` joinpoint.
 * - `res` _!IFirebase.PageData_ The return of the method after it's successfully run.
 * - `sub` _(value: !IFirebase.PageData) =&gt; ?_ Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.LoadRequirePageDataPointcutData*
 * - `args` _IFirebase.LoadRequirePageDataNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.LoadRequirePageDataPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.LoadRequirePageDataPointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.afterLoadRequirePageDataReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.AfterCancelsLoadRequirePageDataPointcutData) => void} eco.artd.IFirebaseJoinpointModel.__afterLoadRequirePageDataCancels
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__afterLoadRequirePageDataCancels<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._afterLoadRequirePageDataCancels */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.afterLoadRequirePageDataCancels} */
/**
 * After the method cancels. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterCancelsLoadRequirePageDataPointcutData} [data] Metadata passed to the pointcuts of _loadRequirePageData_ at the `afterCancels` joinpoint.
 * - `reasons` _!Set&lt;string&gt;_ The list of reasons why the method was cancelled.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.LoadRequirePageDataPointcutData*
 * - `args` _IFirebase.LoadRequirePageDataNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.LoadRequirePageDataPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.LoadRequirePageDataPointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.afterLoadRequirePageDataCancels = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.BeforeLoadRequirePointcutData) => void} eco.artd.IFirebaseJoinpointModel.__beforeLoadRequire
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__beforeLoadRequire<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._beforeLoadRequire */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.beforeLoadRequire} */
/**
 * Before the method executes. Allows to cancel the method. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.BeforeLoadRequirePointcutData} [data] Metadata passed to the pointcuts of _loadRequire_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: IFirebase.LoadRequireNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `loadRequire` method from being executed.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.LoadRequirePointcutData*
 * - `args` _IFirebase.LoadRequireNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.LoadRequirePointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.LoadRequirePointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.beforeLoadRequire = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.AfterLoadRequirePointcutData) => void} eco.artd.IFirebaseJoinpointModel.__afterLoadRequire
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__afterLoadRequire<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._afterLoadRequire */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.afterLoadRequire} */
/**
 * After the method. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterLoadRequirePointcutData} [data] Metadata passed to the pointcuts of _loadRequire_ at the `after` joinpoint.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.LoadRequirePointcutData*
 * - `args` _IFirebase.LoadRequireNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.LoadRequirePointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.LoadRequirePointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.afterLoadRequire = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.AfterThrowsLoadRequirePointcutData) => void} eco.artd.IFirebaseJoinpointModel.__afterLoadRequireThrows
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__afterLoadRequireThrows<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._afterLoadRequireThrows */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.afterLoadRequireThrows} */
/**
 * After the method throw. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterThrowsLoadRequirePointcutData} [data] Metadata passed to the pointcuts of _loadRequire_ at the `afterThrows` joinpoint.
 * - `err` _!Error_ The error with which the method failed.
 * - `hide` _() =&gt; void_ Hides the error in the `loadRequire` (use only at the top-level of program flow).
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.LoadRequirePointcutData*
 * - `args` _IFirebase.LoadRequireNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.LoadRequirePointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.LoadRequirePointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.afterLoadRequireThrows = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.AfterReturnsLoadRequirePointcutData) => void} eco.artd.IFirebaseJoinpointModel.__afterLoadRequireReturns
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__afterLoadRequireReturns<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._afterLoadRequireReturns */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.afterLoadRequireReturns} */
/**
 * After the method returned. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterReturnsLoadRequirePointcutData} [data] Metadata passed to the pointcuts of _loadRequire_ at the `afterReturns` joinpoint.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.LoadRequirePointcutData*
 * - `args` _IFirebase.LoadRequireNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.LoadRequirePointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.LoadRequirePointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.afterLoadRequireReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.AfterCancelsLoadRequirePointcutData) => void} eco.artd.IFirebaseJoinpointModel.__afterLoadRequireCancels
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__afterLoadRequireCancels<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._afterLoadRequireCancels */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.afterLoadRequireCancels} */
/**
 * After the method cancels. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterCancelsLoadRequirePointcutData} [data] Metadata passed to the pointcuts of _loadRequire_ at the `afterCancels` joinpoint.
 * - `reasons` _!Set&lt;string&gt;_ The list of reasons why the method was cancelled.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.LoadRequirePointcutData*
 * - `args` _IFirebase.LoadRequireNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.LoadRequirePointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.LoadRequirePointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.afterLoadRequireCancels = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.BeforeUpdateErrorStackPointcutData) => void} eco.artd.IFirebaseJoinpointModel.__beforeUpdateErrorStack
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__beforeUpdateErrorStack<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._beforeUpdateErrorStack */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.beforeUpdateErrorStack} */
/**
 * Before the method executes. Allows to cancel the method. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.BeforeUpdateErrorStackPointcutData} [data] Metadata passed to the pointcuts of _updateErrorStack_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: IFirebase.UpdateErrorStackNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `updateErrorStack` method from being executed.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.UpdateErrorStackPointcutData*
 * - `args` _IFirebase.UpdateErrorStackNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.UpdateErrorStackPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.UpdateErrorStackPointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.beforeUpdateErrorStack = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.AfterUpdateErrorStackPointcutData) => void} eco.artd.IFirebaseJoinpointModel.__afterUpdateErrorStack
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__afterUpdateErrorStack<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._afterUpdateErrorStack */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.afterUpdateErrorStack} */
/**
 * After the method. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterUpdateErrorStackPointcutData} [data] Metadata passed to the pointcuts of _updateErrorStack_ at the `after` joinpoint.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.UpdateErrorStackPointcutData*
 * - `args` _IFirebase.UpdateErrorStackNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.UpdateErrorStackPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.UpdateErrorStackPointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.afterUpdateErrorStack = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.AfterThrowsUpdateErrorStackPointcutData) => void} eco.artd.IFirebaseJoinpointModel.__afterUpdateErrorStackThrows
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__afterUpdateErrorStackThrows<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._afterUpdateErrorStackThrows */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.afterUpdateErrorStackThrows} */
/**
 * After the method throw. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterThrowsUpdateErrorStackPointcutData} [data] Metadata passed to the pointcuts of _updateErrorStack_ at the `afterThrows` joinpoint.
 * - `err` _!Error_ The error with which the method failed.
 * - `hide` _() =&gt; void_ Hides the error in the `updateErrorStack` (use only at the top-level of program flow).
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.UpdateErrorStackPointcutData*
 * - `args` _IFirebase.UpdateErrorStackNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.UpdateErrorStackPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.UpdateErrorStackPointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.afterUpdateErrorStackThrows = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.AfterReturnsUpdateErrorStackPointcutData) => void} eco.artd.IFirebaseJoinpointModel.__afterUpdateErrorStackReturns
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__afterUpdateErrorStackReturns<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._afterUpdateErrorStackReturns */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.afterUpdateErrorStackReturns} */
/**
 * After the method returned. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterReturnsUpdateErrorStackPointcutData} [data] Metadata passed to the pointcuts of _updateErrorStack_ at the `afterReturns` joinpoint.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.UpdateErrorStackPointcutData*
 * - `args` _IFirebase.UpdateErrorStackNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.UpdateErrorStackPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.UpdateErrorStackPointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.afterUpdateErrorStackReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.AfterCancelsUpdateErrorStackPointcutData) => void} eco.artd.IFirebaseJoinpointModel.__afterUpdateErrorStackCancels
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__afterUpdateErrorStackCancels<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._afterUpdateErrorStackCancels */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.afterUpdateErrorStackCancels} */
/**
 * After the method cancels. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterCancelsUpdateErrorStackPointcutData} [data] Metadata passed to the pointcuts of _updateErrorStack_ at the `afterCancels` joinpoint.
 * - `reasons` _!Set&lt;string&gt;_ The list of reasons why the method was cancelled.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.UpdateErrorStackPointcutData*
 * - `args` _IFirebase.UpdateErrorStackNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.UpdateErrorStackPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.UpdateErrorStackPointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.afterUpdateErrorStackCancels = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.BeforeValidateMethodPointcutData) => void} eco.artd.IFirebaseJoinpointModel.__beforeValidateMethod
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__beforeValidateMethod<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._beforeValidateMethod */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.beforeValidateMethod} */
/**
 * Before the method executes. Allows to cancel the method. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.BeforeValidateMethodPointcutData} [data] Metadata passed to the pointcuts of _validateMethod_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: IFirebase.ValidateMethodNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `validateMethod` method from being executed.
 * - `sub` _(value: string) =&gt; void_ Cancels a call to `validateMethod` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.ValidateMethodPointcutData*
 * - `args` _IFirebase.ValidateMethodNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.ValidateMethodPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.ValidateMethodPointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.beforeValidateMethod = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.AfterValidateMethodPointcutData) => void} eco.artd.IFirebaseJoinpointModel.__afterValidateMethod
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__afterValidateMethod<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._afterValidateMethod */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.afterValidateMethod} */
/**
 * After the method. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterValidateMethodPointcutData} [data] Metadata passed to the pointcuts of _validateMethod_ at the `after` joinpoint.
 * - `res` _string_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.ValidateMethodPointcutData*
 * - `args` _IFirebase.ValidateMethodNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.ValidateMethodPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.ValidateMethodPointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.afterValidateMethod = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.AfterThrowsValidateMethodPointcutData) => void} eco.artd.IFirebaseJoinpointModel.__afterValidateMethodThrows
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__afterValidateMethodThrows<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._afterValidateMethodThrows */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.afterValidateMethodThrows} */
/**
 * After the method throw. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterThrowsValidateMethodPointcutData} [data] Metadata passed to the pointcuts of _validateMethod_ at the `afterThrows` joinpoint.
 * - `err` _!Error_ The error with which the method failed.
 * - `hide` _() =&gt; void_ Hides the error in the `validateMethod` (use only at the top-level of program flow).
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.ValidateMethodPointcutData*
 * - `args` _IFirebase.ValidateMethodNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.ValidateMethodPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.ValidateMethodPointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.afterValidateMethodThrows = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.AfterReturnsValidateMethodPointcutData) => void} eco.artd.IFirebaseJoinpointModel.__afterValidateMethodReturns
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__afterValidateMethodReturns<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._afterValidateMethodReturns */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.afterValidateMethodReturns} */
/**
 * After the method returned. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterReturnsValidateMethodPointcutData} [data] Metadata passed to the pointcuts of _validateMethod_ at the `afterReturns` joinpoint.
 * - `res` _string_ The return of the method after it's successfully run.
 * - `sub` _(value: string) =&gt; ?_ Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.ValidateMethodPointcutData*
 * - `args` _IFirebase.ValidateMethodNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.ValidateMethodPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.ValidateMethodPointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.afterValidateMethodReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.AfterCancelsValidateMethodPointcutData) => void} eco.artd.IFirebaseJoinpointModel.__afterValidateMethodCancels
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__afterValidateMethodCancels<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._afterValidateMethodCancels */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.afterValidateMethodCancels} */
/**
 * After the method cancels. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterCancelsValidateMethodPointcutData} [data] Metadata passed to the pointcuts of _validateMethod_ at the `afterCancels` joinpoint.
 * - `reasons` _!Set&lt;string&gt;_ The list of reasons why the method was cancelled.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.ValidateMethodPointcutData*
 * - `args` _IFirebase.ValidateMethodNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.ValidateMethodPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.ValidateMethodPointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.afterValidateMethodCancels = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.BeforeValidateLoadAndInvokePointcutData) => void} eco.artd.IFirebaseJoinpointModel.__beforeValidateLoadAndInvoke
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__beforeValidateLoadAndInvoke<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._beforeValidateLoadAndInvoke */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.beforeValidateLoadAndInvoke} */
/**
 * Before the method executes. Allows to cancel the method. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.BeforeValidateLoadAndInvokePointcutData} [data] Metadata passed to the pointcuts of _validateLoadAndInvoke_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: IFirebase.ValidateLoadAndInvokeNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `validateLoadAndInvoke` method from being executed.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.ValidateLoadAndInvokePointcutData*
 * - `args` _IFirebase.ValidateLoadAndInvokeNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.ValidateLoadAndInvokePointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.ValidateLoadAndInvokePointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.beforeValidateLoadAndInvoke = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.AfterValidateLoadAndInvokePointcutData) => void} eco.artd.IFirebaseJoinpointModel.__afterValidateLoadAndInvoke
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__afterValidateLoadAndInvoke<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._afterValidateLoadAndInvoke */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.afterValidateLoadAndInvoke} */
/**
 * After the method. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterValidateLoadAndInvokePointcutData} [data] Metadata passed to the pointcuts of _validateLoadAndInvoke_ at the `after` joinpoint.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.ValidateLoadAndInvokePointcutData*
 * - `args` _IFirebase.ValidateLoadAndInvokeNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.ValidateLoadAndInvokePointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.ValidateLoadAndInvokePointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.afterValidateLoadAndInvoke = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.AfterThrowsValidateLoadAndInvokePointcutData) => void} eco.artd.IFirebaseJoinpointModel.__afterValidateLoadAndInvokeThrows
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__afterValidateLoadAndInvokeThrows<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._afterValidateLoadAndInvokeThrows */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.afterValidateLoadAndInvokeThrows} */
/**
 * After the method throw. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterThrowsValidateLoadAndInvokePointcutData} [data] Metadata passed to the pointcuts of _validateLoadAndInvoke_ at the `afterThrows` joinpoint.
 * - `err` _!Error_ The error with which the method failed.
 * - `hide` _() =&gt; void_ Hides the error in the `validateLoadAndInvoke` (use only at the top-level of program flow).
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.ValidateLoadAndInvokePointcutData*
 * - `args` _IFirebase.ValidateLoadAndInvokeNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.ValidateLoadAndInvokePointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.ValidateLoadAndInvokePointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.afterValidateLoadAndInvokeThrows = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.AfterReturnsValidateLoadAndInvokePointcutData) => void} eco.artd.IFirebaseJoinpointModel.__afterValidateLoadAndInvokeReturns
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__afterValidateLoadAndInvokeReturns<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._afterValidateLoadAndInvokeReturns */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.afterValidateLoadAndInvokeReturns} */
/**
 * After the method returned. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterReturnsValidateLoadAndInvokePointcutData} [data] Metadata passed to the pointcuts of _validateLoadAndInvoke_ at the `afterReturns` joinpoint.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.ValidateLoadAndInvokePointcutData*
 * - `args` _IFirebase.ValidateLoadAndInvokeNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.ValidateLoadAndInvokePointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.ValidateLoadAndInvokePointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.afterValidateLoadAndInvokeReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.AfterCancelsValidateLoadAndInvokePointcutData) => void} eco.artd.IFirebaseJoinpointModel.__afterValidateLoadAndInvokeCancels
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__afterValidateLoadAndInvokeCancels<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._afterValidateLoadAndInvokeCancels */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.afterValidateLoadAndInvokeCancels} */
/**
 * After the method cancels. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterCancelsValidateLoadAndInvokePointcutData} [data] Metadata passed to the pointcuts of _validateLoadAndInvoke_ at the `afterCancels` joinpoint.
 * - `reasons` _!Set&lt;string&gt;_ The list of reasons why the method was cancelled.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.ValidateLoadAndInvokePointcutData*
 * - `args` _IFirebase.ValidateLoadAndInvokeNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.ValidateLoadAndInvokePointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.ValidateLoadAndInvokePointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.afterValidateLoadAndInvokeCancels = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterValidateLoadAndInvokePointcutData) => void} eco.artd.IFirebaseJoinpointModel.__immediatelyAfterValidateLoadAndInvoke
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__immediatelyAfterValidateLoadAndInvoke<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._immediatelyAfterValidateLoadAndInvoke */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.immediatelyAfterValidateLoadAndInvoke} */
/**
 * Immediately after the async method returned its promise. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterValidateLoadAndInvokePointcutData} [data] Metadata passed to the pointcuts of _validateLoadAndInvoke_ at the `immediatelyAfter` joinpoint.
 * - `promise` _!Promise&lt;void&gt;_ The promise with the return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.ValidateLoadAndInvokePointcutData*
 * - `args` _IFirebase.ValidateLoadAndInvokeNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.ValidateLoadAndInvokePointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.ValidateLoadAndInvokePointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.immediatelyAfterValidateLoadAndInvoke = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.BeforeValidateLoadAndInvokeByIdPointcutData) => void} eco.artd.IFirebaseJoinpointModel.__beforeValidateLoadAndInvokeById
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__beforeValidateLoadAndInvokeById<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._beforeValidateLoadAndInvokeById */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.beforeValidateLoadAndInvokeById} */
/**
 * Before the method executes. Allows to cancel the method. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.BeforeValidateLoadAndInvokeByIdPointcutData} [data] Metadata passed to the pointcuts of _validateLoadAndInvokeById_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: IFirebase.ValidateLoadAndInvokeByIdNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `validateLoadAndInvokeById` method from being executed.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.ValidateLoadAndInvokeByIdPointcutData*
 * - `args` _IFirebase.ValidateLoadAndInvokeByIdNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.ValidateLoadAndInvokeByIdPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.ValidateLoadAndInvokeByIdPointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.beforeValidateLoadAndInvokeById = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.AfterValidateLoadAndInvokeByIdPointcutData) => void} eco.artd.IFirebaseJoinpointModel.__afterValidateLoadAndInvokeById
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__afterValidateLoadAndInvokeById<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._afterValidateLoadAndInvokeById */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.afterValidateLoadAndInvokeById} */
/**
 * After the method. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterValidateLoadAndInvokeByIdPointcutData} [data] Metadata passed to the pointcuts of _validateLoadAndInvokeById_ at the `after` joinpoint.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.ValidateLoadAndInvokeByIdPointcutData*
 * - `args` _IFirebase.ValidateLoadAndInvokeByIdNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.ValidateLoadAndInvokeByIdPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.ValidateLoadAndInvokeByIdPointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.afterValidateLoadAndInvokeById = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.AfterThrowsValidateLoadAndInvokeByIdPointcutData) => void} eco.artd.IFirebaseJoinpointModel.__afterValidateLoadAndInvokeByIdThrows
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__afterValidateLoadAndInvokeByIdThrows<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._afterValidateLoadAndInvokeByIdThrows */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.afterValidateLoadAndInvokeByIdThrows} */
/**
 * After the method throw. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterThrowsValidateLoadAndInvokeByIdPointcutData} [data] Metadata passed to the pointcuts of _validateLoadAndInvokeById_ at the `afterThrows` joinpoint.
 * - `err` _!Error_ The error with which the method failed.
 * - `hide` _() =&gt; void_ Hides the error in the `validateLoadAndInvokeById` (use only at the top-level of program flow).
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.ValidateLoadAndInvokeByIdPointcutData*
 * - `args` _IFirebase.ValidateLoadAndInvokeByIdNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.ValidateLoadAndInvokeByIdPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.ValidateLoadAndInvokeByIdPointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.afterValidateLoadAndInvokeByIdThrows = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.AfterReturnsValidateLoadAndInvokeByIdPointcutData) => void} eco.artd.IFirebaseJoinpointModel.__afterValidateLoadAndInvokeByIdReturns
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__afterValidateLoadAndInvokeByIdReturns<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._afterValidateLoadAndInvokeByIdReturns */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.afterValidateLoadAndInvokeByIdReturns} */
/**
 * After the method returned. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterReturnsValidateLoadAndInvokeByIdPointcutData} [data] Metadata passed to the pointcuts of _validateLoadAndInvokeById_ at the `afterReturns` joinpoint.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.ValidateLoadAndInvokeByIdPointcutData*
 * - `args` _IFirebase.ValidateLoadAndInvokeByIdNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.ValidateLoadAndInvokeByIdPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.ValidateLoadAndInvokeByIdPointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.afterValidateLoadAndInvokeByIdReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.AfterCancelsValidateLoadAndInvokeByIdPointcutData) => void} eco.artd.IFirebaseJoinpointModel.__afterValidateLoadAndInvokeByIdCancels
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__afterValidateLoadAndInvokeByIdCancels<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._afterValidateLoadAndInvokeByIdCancels */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.afterValidateLoadAndInvokeByIdCancels} */
/**
 * After the method cancels. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterCancelsValidateLoadAndInvokeByIdPointcutData} [data] Metadata passed to the pointcuts of _validateLoadAndInvokeById_ at the `afterCancels` joinpoint.
 * - `reasons` _!Set&lt;string&gt;_ The list of reasons why the method was cancelled.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.ValidateLoadAndInvokeByIdPointcutData*
 * - `args` _IFirebase.ValidateLoadAndInvokeByIdNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.ValidateLoadAndInvokeByIdPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.ValidateLoadAndInvokeByIdPointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.afterValidateLoadAndInvokeByIdCancels = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterValidateLoadAndInvokeByIdPointcutData) => void} eco.artd.IFirebaseJoinpointModel.__immediatelyAfterValidateLoadAndInvokeById
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__immediatelyAfterValidateLoadAndInvokeById<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._immediatelyAfterValidateLoadAndInvokeById */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.immediatelyAfterValidateLoadAndInvokeById} */
/**
 * Immediately after the async method returned its promise. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterValidateLoadAndInvokeByIdPointcutData} [data] Metadata passed to the pointcuts of _validateLoadAndInvokeById_ at the `immediatelyAfter` joinpoint.
 * - `promise` _!Promise&lt;void&gt;_ The promise with the return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.ValidateLoadAndInvokeByIdPointcutData*
 * - `args` _IFirebase.ValidateLoadAndInvokeByIdNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.ValidateLoadAndInvokeByIdPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.ValidateLoadAndInvokeByIdPointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.immediatelyAfterValidateLoadAndInvokeById = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.BeforeMakeInvokerPointcutData) => void} eco.artd.IFirebaseJoinpointModel.__beforeMakeInvoker
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__beforeMakeInvoker<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._beforeMakeInvoker */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.beforeMakeInvoker} */
/**
 * Before the method executes. Allows to cancel the method. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.BeforeMakeInvokerPointcutData} [data] Metadata passed to the pointcuts of _makeInvoker_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: IFirebase.MakeInvokerNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `makeInvoker` method from being executed.
 * - `sub` _(value: () =&gt; ?) =&gt; void_ Cancels a call to `makeInvoker` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.MakeInvokerPointcutData*
 * - `args` _IFirebase.MakeInvokerNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.MakeInvokerPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.MakeInvokerPointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.beforeMakeInvoker = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.AfterMakeInvokerPointcutData) => void} eco.artd.IFirebaseJoinpointModel.__afterMakeInvoker
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__afterMakeInvoker<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._afterMakeInvoker */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.afterMakeInvoker} */
/**
 * After the method. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterMakeInvokerPointcutData} [data] Metadata passed to the pointcuts of _makeInvoker_ at the `after` joinpoint.
 * - `res` _() =&gt; ?_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.MakeInvokerPointcutData*
 * - `args` _IFirebase.MakeInvokerNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.MakeInvokerPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.MakeInvokerPointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.afterMakeInvoker = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.AfterThrowsMakeInvokerPointcutData) => void} eco.artd.IFirebaseJoinpointModel.__afterMakeInvokerThrows
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__afterMakeInvokerThrows<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._afterMakeInvokerThrows */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.afterMakeInvokerThrows} */
/**
 * After the method throw. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterThrowsMakeInvokerPointcutData} [data] Metadata passed to the pointcuts of _makeInvoker_ at the `afterThrows` joinpoint.
 * - `err` _!Error_ The error with which the method failed.
 * - `hide` _() =&gt; void_ Hides the error in the `makeInvoker` (use only at the top-level of program flow).
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.MakeInvokerPointcutData*
 * - `args` _IFirebase.MakeInvokerNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.MakeInvokerPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.MakeInvokerPointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.afterMakeInvokerThrows = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.AfterReturnsMakeInvokerPointcutData) => void} eco.artd.IFirebaseJoinpointModel.__afterMakeInvokerReturns
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__afterMakeInvokerReturns<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._afterMakeInvokerReturns */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.afterMakeInvokerReturns} */
/**
 * After the method returned. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterReturnsMakeInvokerPointcutData} [data] Metadata passed to the pointcuts of _makeInvoker_ at the `afterReturns` joinpoint.
 * - `res` _() =&gt; ?_ The return of the method after it's successfully run.
 * - `sub` _(value: () =&gt; ?) =&gt; ?_ Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.MakeInvokerPointcutData*
 * - `args` _IFirebase.MakeInvokerNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.MakeInvokerPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.MakeInvokerPointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.afterMakeInvokerReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.AfterCancelsMakeInvokerPointcutData) => void} eco.artd.IFirebaseJoinpointModel.__afterMakeInvokerCancels
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__afterMakeInvokerCancels<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._afterMakeInvokerCancels */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.afterMakeInvokerCancels} */
/**
 * After the method cancels. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterCancelsMakeInvokerPointcutData} [data] Metadata passed to the pointcuts of _makeInvoker_ at the `afterCancels` joinpoint.
 * - `reasons` _!Set&lt;string&gt;_ The list of reasons why the method was cancelled.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.MakeInvokerPointcutData*
 * - `args` _IFirebase.MakeInvokerNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.MakeInvokerPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.MakeInvokerPointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.afterMakeInvokerCancels = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.BeforeLoadAndInvokeByIdPointcutData) => void} eco.artd.IFirebaseJoinpointModel.__beforeLoadAndInvokeById
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__beforeLoadAndInvokeById<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._beforeLoadAndInvokeById */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.beforeLoadAndInvokeById} */
/**
 * Before the method executes. Allows to cancel the method. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.BeforeLoadAndInvokeByIdPointcutData} [data] Metadata passed to the pointcuts of _loadAndInvokeById_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: IFirebase.LoadAndInvokeByIdNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `loadAndInvokeById` method from being executed.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.LoadAndInvokeByIdPointcutData*
 * - `args` _IFirebase.LoadAndInvokeByIdNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.LoadAndInvokeByIdPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.LoadAndInvokeByIdPointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.beforeLoadAndInvokeById = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.AfterLoadAndInvokeByIdPointcutData) => void} eco.artd.IFirebaseJoinpointModel.__afterLoadAndInvokeById
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__afterLoadAndInvokeById<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._afterLoadAndInvokeById */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.afterLoadAndInvokeById} */
/**
 * After the method. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterLoadAndInvokeByIdPointcutData} [data] Metadata passed to the pointcuts of _loadAndInvokeById_ at the `after` joinpoint.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.LoadAndInvokeByIdPointcutData*
 * - `args` _IFirebase.LoadAndInvokeByIdNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.LoadAndInvokeByIdPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.LoadAndInvokeByIdPointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.afterLoadAndInvokeById = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.AfterThrowsLoadAndInvokeByIdPointcutData) => void} eco.artd.IFirebaseJoinpointModel.__afterLoadAndInvokeByIdThrows
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__afterLoadAndInvokeByIdThrows<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._afterLoadAndInvokeByIdThrows */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.afterLoadAndInvokeByIdThrows} */
/**
 * After the method throw. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterThrowsLoadAndInvokeByIdPointcutData} [data] Metadata passed to the pointcuts of _loadAndInvokeById_ at the `afterThrows` joinpoint.
 * - `err` _!Error_ The error with which the method failed.
 * - `hide` _() =&gt; void_ Hides the error in the `loadAndInvokeById` (use only at the top-level of program flow).
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.LoadAndInvokeByIdPointcutData*
 * - `args` _IFirebase.LoadAndInvokeByIdNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.LoadAndInvokeByIdPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.LoadAndInvokeByIdPointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.afterLoadAndInvokeByIdThrows = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.AfterReturnsLoadAndInvokeByIdPointcutData) => void} eco.artd.IFirebaseJoinpointModel.__afterLoadAndInvokeByIdReturns
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__afterLoadAndInvokeByIdReturns<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._afterLoadAndInvokeByIdReturns */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.afterLoadAndInvokeByIdReturns} */
/**
 * After the method returned. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterReturnsLoadAndInvokeByIdPointcutData} [data] Metadata passed to the pointcuts of _loadAndInvokeById_ at the `afterReturns` joinpoint.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.LoadAndInvokeByIdPointcutData*
 * - `args` _IFirebase.LoadAndInvokeByIdNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.LoadAndInvokeByIdPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.LoadAndInvokeByIdPointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.afterLoadAndInvokeByIdReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.AfterCancelsLoadAndInvokeByIdPointcutData) => void} eco.artd.IFirebaseJoinpointModel.__afterLoadAndInvokeByIdCancels
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__afterLoadAndInvokeByIdCancels<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._afterLoadAndInvokeByIdCancels */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.afterLoadAndInvokeByIdCancels} */
/**
 * After the method cancels. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterCancelsLoadAndInvokeByIdPointcutData} [data] Metadata passed to the pointcuts of _loadAndInvokeById_ at the `afterCancels` joinpoint.
 * - `reasons` _!Set&lt;string&gt;_ The list of reasons why the method was cancelled.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.LoadAndInvokeByIdPointcutData*
 * - `args` _IFirebase.LoadAndInvokeByIdNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.LoadAndInvokeByIdPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.LoadAndInvokeByIdPointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.afterLoadAndInvokeByIdCancels = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.BeforeLoadAndInvokePointcutData) => void} eco.artd.IFirebaseJoinpointModel.__beforeLoadAndInvoke
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__beforeLoadAndInvoke<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._beforeLoadAndInvoke */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.beforeLoadAndInvoke} */
/**
 * Before the method executes. Allows to cancel the method. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.BeforeLoadAndInvokePointcutData} [data] Metadata passed to the pointcuts of _loadAndInvoke_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: IFirebase.LoadAndInvokeNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `loadAndInvoke` method from being executed.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.LoadAndInvokePointcutData*
 * - `args` _IFirebase.LoadAndInvokeNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.LoadAndInvokePointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.LoadAndInvokePointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.beforeLoadAndInvoke = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.AfterLoadAndInvokePointcutData) => void} eco.artd.IFirebaseJoinpointModel.__afterLoadAndInvoke
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__afterLoadAndInvoke<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._afterLoadAndInvoke */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.afterLoadAndInvoke} */
/**
 * After the method. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterLoadAndInvokePointcutData} [data] Metadata passed to the pointcuts of _loadAndInvoke_ at the `after` joinpoint.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.LoadAndInvokePointcutData*
 * - `args` _IFirebase.LoadAndInvokeNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.LoadAndInvokePointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.LoadAndInvokePointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.afterLoadAndInvoke = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.AfterThrowsLoadAndInvokePointcutData) => void} eco.artd.IFirebaseJoinpointModel.__afterLoadAndInvokeThrows
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__afterLoadAndInvokeThrows<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._afterLoadAndInvokeThrows */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.afterLoadAndInvokeThrows} */
/**
 * After the method throw. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterThrowsLoadAndInvokePointcutData} [data] Metadata passed to the pointcuts of _loadAndInvoke_ at the `afterThrows` joinpoint.
 * - `err` _!Error_ The error with which the method failed.
 * - `hide` _() =&gt; void_ Hides the error in the `loadAndInvoke` (use only at the top-level of program flow).
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.LoadAndInvokePointcutData*
 * - `args` _IFirebase.LoadAndInvokeNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.LoadAndInvokePointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.LoadAndInvokePointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.afterLoadAndInvokeThrows = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.AfterReturnsLoadAndInvokePointcutData) => void} eco.artd.IFirebaseJoinpointModel.__afterLoadAndInvokeReturns
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__afterLoadAndInvokeReturns<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._afterLoadAndInvokeReturns */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.afterLoadAndInvokeReturns} */
/**
 * After the method returned. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterReturnsLoadAndInvokePointcutData} [data] Metadata passed to the pointcuts of _loadAndInvoke_ at the `afterReturns` joinpoint.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.LoadAndInvokePointcutData*
 * - `args` _IFirebase.LoadAndInvokeNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.LoadAndInvokePointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.LoadAndInvokePointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.afterLoadAndInvokeReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.AfterCancelsLoadAndInvokePointcutData) => void} eco.artd.IFirebaseJoinpointModel.__afterLoadAndInvokeCancels
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__afterLoadAndInvokeCancels<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._afterLoadAndInvokeCancels */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.afterLoadAndInvokeCancels} */
/**
 * After the method cancels. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.AfterCancelsLoadAndInvokePointcutData} [data] Metadata passed to the pointcuts of _loadAndInvoke_ at the `afterCancels` joinpoint.
 * - `reasons` _!Set&lt;string&gt;_ The list of reasons why the method was cancelled.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.LoadAndInvokePointcutData*
 * - `args` _IFirebase.LoadAndInvokeNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.LoadAndInvokePointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.LoadAndInvokePointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.afterLoadAndInvokeCancels = function(data) {}

/**
 * @typedef {(this: THIS, data?: !eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterLoadAndInvokePointcutData) => void} eco.artd.IFirebaseJoinpointModel.__immediatelyAfterLoadAndInvoke
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseJoinpointModel.__immediatelyAfterLoadAndInvoke<!eco.artd.IFirebaseJoinpointModel>} eco.artd.IFirebaseJoinpointModel._immediatelyAfterLoadAndInvoke */
/** @typedef {typeof eco.artd.IFirebaseJoinpointModel.immediatelyAfterLoadAndInvoke} */
/**
 * Immediately after the async method returned its promise. `🔗 $combine`
 * @param {!eco.artd.IFirebaseJoinpointModel.ImmediatelyAfterLoadAndInvokePointcutData} [data] Metadata passed to the pointcuts of _loadAndInvoke_ at the `immediatelyAfter` joinpoint.
 * - `promise` _!Promise&lt;void&gt;_ The promise with the return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFirebaseJoinpointModel.LoadAndInvokePointcutData*
 * - `args` _IFirebase.LoadAndInvokeNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFirebaseJoinpointModel.LoadAndInvokePointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFirebaseJoinpointModel.LoadAndInvokePointcutData*
 * @return {void}
 */
eco.artd.IFirebaseJoinpointModel.immediatelyAfterLoadAndInvoke = function(data) {}

/**
 * @typedef {(this: THIS, time: number, args: eco.artd.IFirebase.GetPageDataFromFirestoreNArgs, proc: !Function, res: ?eco.artd.IFirebase.PageData) => (void|!Promise<void>)} eco.artd.IFirebaseProfilingAide.__reportGetPageDataFromFirestoreProfile
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseProfilingAide.__reportGetPageDataFromFirestoreProfile<!eco.artd.IFirebaseProfilingAide>} eco.artd.IFirebaseProfilingAide._reportGetPageDataFromFirestoreProfile */
/** @typedef {typeof eco.artd.IFirebaseProfilingAide.reportGetPageDataFromFirestoreProfile} */
/**
 * Reports the execution time of the _getPageDataFromFirestore_ method after its completion. `🔗 $combine`
 * @param {number} time The time for execution in milliseconds.
 * @param {eco.artd.IFirebase.GetPageDataFromFirestoreNArgs} args The arguments.
 * - `cid` _number_ The circuit id.
 * @param {!Function} proc Provides access to the task.
 * @param {?eco.artd.IFirebase.PageData} res What the method returned.
 * - `methodPaths` _!Map&lt;string, string&gt;_
 * - `methodNames` _!Map&lt;string, string&gt;_
 * - `dir` _string_
 * @return {void|!Promise<void>}
 */
eco.artd.IFirebaseProfilingAide.reportGetPageDataFromFirestoreProfile = function(time, args, proc, res) {}

/**
 * @typedef {(this: THIS, time: number, args: eco.artd.IFirebase.DownloadAndEvalPageDataNArgs, proc: !Function, res: *) => (void|!Promise<void>)} eco.artd.IFirebaseProfilingAide.__reportDownloadAndEvalPageDataProfile
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseProfilingAide.__reportDownloadAndEvalPageDataProfile<!eco.artd.IFirebaseProfilingAide>} eco.artd.IFirebaseProfilingAide._reportDownloadAndEvalPageDataProfile */
/** @typedef {typeof eco.artd.IFirebaseProfilingAide.reportDownloadAndEvalPageDataProfile} */
/**
 * Reports the execution time of the _downloadAndEvalPageData_ method after its completion. `🔗 $combine`
 * @param {number} time The time for execution in milliseconds.
 * @param {eco.artd.IFirebase.DownloadAndEvalPageDataNArgs} args The arguments.
 * - `id` _number_
 * @param {!Function} proc Provides access to the task.
 * @param {*} res What the method returned.
 * @return {void|!Promise<void>}
 */
eco.artd.IFirebaseProfilingAide.reportDownloadAndEvalPageDataProfile = function(time, args, proc, res) {}

/**
 * @typedef {(this: THIS, time: number, args: eco.artd.IFirebase.EvalBodyNArgs, proc: !Function, res: (arg0: !http.IncomingMessage, arg1: !http.ServerResponse) => (void|Promise<void>)) => (void|!Promise<void>)} eco.artd.IFirebaseProfilingAide.__reportEvalBodyProfile
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseProfilingAide.__reportEvalBodyProfile<!eco.artd.IFirebaseProfilingAide>} eco.artd.IFirebaseProfilingAide._reportEvalBodyProfile */
/** @typedef {typeof eco.artd.IFirebaseProfilingAide.reportEvalBodyProfile} */
/**
 * Reports the execution time of the _evalBody_ method after its completion. `🔗 $combine`
 * @param {number} time The time for execution in milliseconds.
 * @param {eco.artd.IFirebase.EvalBodyNArgs} args The arguments.
 * - `body` _string_ The config to run the method against.
 * @param {!Function} proc Provides access to the task.
 * @param {(arg0: !http.IncomingMessage, arg1: !http.ServerResponse) => (void|Promise<void>)} res What the method returned.
 * @return {void|!Promise<void>}
 */
eco.artd.IFirebaseProfilingAide.reportEvalBodyProfile = function(time, args, proc, res) {}

/**
 * @typedef {(this: THIS, time: number, args: eco.artd.IFirebase.FetchFunctionsFileNArgs, proc: !Function, res: eco.artd.IFirebase.fetchFunctionsFile.Return) => (void|!Promise<void>)} eco.artd.IFirebaseProfilingAide.__reportFetchFunctionsFileProfile
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseProfilingAide.__reportFetchFunctionsFileProfile<!eco.artd.IFirebaseProfilingAide>} eco.artd.IFirebaseProfilingAide._reportFetchFunctionsFileProfile */
/** @typedef {typeof eco.artd.IFirebaseProfilingAide.reportFetchFunctionsFileProfile} */
/**
 * Reports the execution time of the _fetchFunctionsFile_ method after its completion. `🔗 $combine`
 * @param {number} time The time for execution in milliseconds.
 * @param {eco.artd.IFirebase.FetchFunctionsFileNArgs} args The arguments.
 * - `file` _string_
 * @param {!Function} proc Provides access to the task.
 * @param {eco.artd.IFirebase.fetchFunctionsFile.Return} res What the method returned.
 * - `body` _string_
 * - `lastModified` _!Date_
 * - `headers` _!Map_
 * @return {void|!Promise<void>}
 */
eco.artd.IFirebaseProfilingAide.reportFetchFunctionsFileProfile = function(time, args, proc, res) {}

/**
 * @typedef {(this: THIS, time: number, args: eco.artd.IFirebase.DownloadDepsNArgs, proc: !Function, res: *) => (void|!Promise<void>)} eco.artd.IFirebaseProfilingAide.__reportDownloadDepsProfile
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseProfilingAide.__reportDownloadDepsProfile<!eco.artd.IFirebaseProfilingAide>} eco.artd.IFirebaseProfilingAide._reportDownloadDepsProfile */
/** @typedef {typeof eco.artd.IFirebaseProfilingAide.reportDownloadDepsProfile} */
/**
 * Reports the execution time of the _downloadDeps_ method after its completion. `🔗 $combine`
 * @param {number} time The time for execution in milliseconds.
 * @param {eco.artd.IFirebase.DownloadDepsNArgs} args The arguments.
 * - `deps` _!Array&lt;string&gt;_ </prop>,<prop name="methodName" type="string">
 * - `id` _number_
 * @param {!Function} proc Provides access to the task.
 * @param {*} res What the method returned.
 * @return {void|!Promise<void>}
 */
eco.artd.IFirebaseProfilingAide.reportDownloadDepsProfile = function(time, args, proc, res) {}

/**
 * @typedef {(this: THIS, time: number, args: eco.artd.IFirebase.DownloadMethodNArgs, proc: !Function, res: string) => (void|!Promise<void>)} eco.artd.IFirebaseProfilingAide.__reportDownloadMethodProfile
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseProfilingAide.__reportDownloadMethodProfile<!eco.artd.IFirebaseProfilingAide>} eco.artd.IFirebaseProfilingAide._reportDownloadMethodProfile */
/** @typedef {typeof eco.artd.IFirebaseProfilingAide.reportDownloadMethodProfile} */
/**
 * Reports the execution time of the _downloadMethod_ method after its completion. `🔗 $combine`
 * @param {number} time The time for execution in milliseconds.
 * @param {eco.artd.IFirebase.DownloadMethodNArgs} args The arguments.
 * - `cid` _number_ The id of the circuit.
 * - `methodName` _string_
 * @param {!Function} proc Provides access to the task.
 * @param {string} res What the method returned.
 * @return {void|!Promise<void>}
 */
eco.artd.IFirebaseProfilingAide.reportDownloadMethodProfile = function(time, args, proc, res) {}

/**
 * @typedef {(this: THIS, time: number, args: eco.artd.IFirebase.DownloadMethodDirectlyNArgs, proc: !Function, res: eco.artd.IFirebase.downloadMethodDirectly.Return) => (void|!Promise<void>)} eco.artd.IFirebaseProfilingAide.__reportDownloadMethodDirectlyProfile
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseProfilingAide.__reportDownloadMethodDirectlyProfile<!eco.artd.IFirebaseProfilingAide>} eco.artd.IFirebaseProfilingAide._reportDownloadMethodDirectlyProfile */
/** @typedef {typeof eco.artd.IFirebaseProfilingAide.reportDownloadMethodDirectlyProfile} */
/**
 * Reports the execution time of the _downloadMethodDirectly_ method after its completion. `🔗 $combine`
 * @param {number} time The time for execution in milliseconds.
 * @param {eco.artd.IFirebase.DownloadMethodDirectlyNArgs} args The arguments.
 * - `cid` _number_ The id of the circuit.
 * - `methodId` _string_
 * @param {!Function} proc Provides access to the task.
 * @param {eco.artd.IFirebase.downloadMethodDirectly.Return} res What the method returned.
 * - `body` _string_
 * - `lastModified` _!Date_
 * - `etag` _string_ The hash from hosting.
 * @return {void|!Promise<void>}
 */
eco.artd.IFirebaseProfilingAide.reportDownloadMethodDirectlyProfile = function(time, args, proc, res) {}

/**
 * @typedef {(this: THIS, time: number, args: eco.artd.IFirebase.DownloadPageDataNArgs, proc: !Function, res: string) => (void|!Promise<void>)} eco.artd.IFirebaseProfilingAide.__reportDownloadPageDataProfile
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseProfilingAide.__reportDownloadPageDataProfile<!eco.artd.IFirebaseProfilingAide>} eco.artd.IFirebaseProfilingAide._reportDownloadPageDataProfile */
/** @typedef {typeof eco.artd.IFirebaseProfilingAide.reportDownloadPageDataProfile} */
/**
 * Reports the execution time of the _downloadPageData_ method after its completion. `🔗 $combine`
 * @param {number} time The time for execution in milliseconds.
 * @param {eco.artd.IFirebase.DownloadPageDataNArgs} args The arguments.
 * - `cid` _number_
 * @param {!Function} proc Provides access to the task.
 * @param {string} res What the method returned.
 * @return {void|!Promise<void>}
 */
eco.artd.IFirebaseProfilingAide.reportDownloadPageDataProfile = function(time, args, proc, res) {}

/**
 * @typedef {(this: THIS, time: number, args: eco.artd.IFirebase.LoadRequirePageDataNArgs, proc: !Function, res: !eco.artd.IFirebase.PageData) => (void|!Promise<void>)} eco.artd.IFirebaseProfilingAide.__reportLoadRequirePageDataProfile
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseProfilingAide.__reportLoadRequirePageDataProfile<!eco.artd.IFirebaseProfilingAide>} eco.artd.IFirebaseProfilingAide._reportLoadRequirePageDataProfile */
/** @typedef {typeof eco.artd.IFirebaseProfilingAide.reportLoadRequirePageDataProfile} */
/**
 * Reports the execution time of the _loadRequirePageData_ method after its completion. `🔗 $combine`
 * @param {number} time The time for execution in milliseconds.
 * @param {eco.artd.IFirebase.LoadRequirePageDataNArgs} args The arguments.
 * - `cid` _number_
 * @param {!Function} proc Provides access to the task.
 * @param {!eco.artd.IFirebase.PageData} res What the method returned.
 * - `methodPaths` _!Map&lt;string, string&gt;_
 * - `methodNames` _!Map&lt;string, string&gt;_
 * - `dir` _string_
 * @return {void|!Promise<void>}
 */
eco.artd.IFirebaseProfilingAide.reportLoadRequirePageDataProfile = function(time, args, proc, res) {}

/**
 * @typedef {(this: THIS, time: number, args: eco.artd.IFirebase.LoadRequireNArgs, proc: !Function, res: *) => (void|!Promise<void>)} eco.artd.IFirebaseProfilingAide.__reportLoadRequireProfile
 * @template THIS
 */
/** @typedef {eco.artd.IFirebaseProfilingAide.__reportLoadRequireProfile<!eco.artd.IFirebaseProfilingAide>} eco.artd.IFirebaseProfilingAide._reportLoadRequireProfile */
/** @typedef {typeof eco.artd.IFirebaseProfilingAide.reportLoadRequireProfile} */
/**
 * Reports the execution time of the _loadRequire_ method after its completion. `🔗 $combine`
 * @param {number} time The time for execution in milliseconds.
 * @param {eco.artd.IFirebase.LoadRequireNArgs} args The arguments.
 * - `path` _string_ </prop>,<prop name="dir" type="string">
 * @param {!Function} proc Provides access to the task.
 * @param {*} res What the method returned.
 * @return {void|!Promise<void>}
 */
eco.artd.IFirebaseProfilingAide.reportLoadRequireProfile = function(time, args, proc, res) {}

/** @typedef {typeof eco.artd.FirebaseFactory} */
/**
 * Produces a configured hyper class.
 * @param {!eco.artd.FirebaseFactory.Config} [config] The config.
 * - `[aspectsInstallers]` _!Array?_
 * - `[subjects]` _!Array&lt;IFirebase&gt;?_
 * - `[hypers]` _!Array?_
 * - `[aspects]` _!Array&lt;!IFirebaseAspects&gt;&vert;!IFirebaseAspects?_
 * @return {typeof eco.artd.HyperFirebase}
 */
eco.artd.FirebaseFactory = function(config) {}

/**
 * @typedef {Object} eco.artd.FirebaseFactory.Config The config.
 * @prop {!Array} [aspectsInstallers]
 * @prop {!Array<eco.artd.IFirebase>} [subjects]
 * @prop {!Array} [hypers]
 * @prop {!Array<!eco.artd.IFirebaseAspects>|!eco.artd.IFirebaseAspects} [aspects]
 */

/**
 * @typedef {(this: THIS, request: http.IncomingMessage, response: http.ServerResponse, cb: !Function|number, meta?: !eco.artd.IFirebase.function.Meta) => !Promise<void>} eco.artd.IFirebase.__function
 * @template THIS
 */
/** @typedef {eco.artd.IFirebase.__function<!eco.artd.IFirebase>} eco.artd.IFirebase._function */
/** @typedef {typeof eco.artd.IFirebase.function} */
/**
 * Conditions the class against the invocation.
 * @param {http.IncomingMessage} request The req.
 * @param {http.ServerResponse} response The res.
 * @param {!Function|number} cb The callback.
 * @param {!eco.artd.IFirebase.function.Meta} [meta] The meta.
 * - `[region]` _string?_
 * @return {!Promise<void>}
 */
eco.artd.IFirebase.function = function(request, response, cb, meta) {}

/**
 * @typedef {Object} eco.artd.IFirebase.function.Meta The meta.
 * @prop {string} [region]
 */

/**
 * @typedef {(this: THIS, body: string) => eco.artd.IFirebase.evalBodyWithProfileTime.Return} eco.artd.IFirebase.__evalBodyWithProfileTime
 * @template THIS
 */
/** @typedef {eco.artd.IFirebase.__evalBodyWithProfileTime<!eco.artd.IFirebase>} eco.artd.IFirebase._evalBodyWithProfileTime */
/** @typedef {typeof eco.artd.IFirebase.evalBodyWithProfileTime} */
/**
 * @param {string} body
 * @return {eco.artd.IFirebase.evalBodyWithProfileTime.Return}
 */
eco.artd.IFirebase.evalBodyWithProfileTime = function(body) {}

/**
 * @typedef {Object} eco.artd.IFirebase.evalBodyWithProfileTime.Return
 * @prop {number} evalBodyTime
 * @prop {!Function} evalBody
 */

/**
 * @typedef {(this: THIS, cid: number) => !Promise<?eco.artd.IFirebase.PageData>} eco.artd.IFirebase.__getPageDataFromFirestore
 * @template THIS
 */
/** @typedef {eco.artd.IFirebase.__getPageDataFromFirestore<!eco.artd.IFirebase>} eco.artd.IFirebase._getPageDataFromFirestore */
/** @typedef {typeof eco.artd.IFirebase.getPageDataFromFirestore} */
/**
 * Looks up the source code for module mapping in the firestore.
 * @param {number} cid The circuit id.
 * @return {!Promise<?eco.artd.IFirebase.PageData>}
 */
eco.artd.IFirebase.getPageDataFromFirestore = function(cid) {}

/**
 * @typedef {(this: THIS, id: number) => !Promise} eco.artd.IFirebase.__downloadAndEvalPageData
 * @template THIS
 */
/** @typedef {eco.artd.IFirebase.__downloadAndEvalPageData<!eco.artd.IFirebase>} eco.artd.IFirebase._downloadAndEvalPageData */
/** @typedef {typeof eco.artd.IFirebase.downloadAndEvalPageData} */
/**
 * Evals body from downloaded page data.
 * @param {number} id
 * @return {!Promise}
 */
eco.artd.IFirebase.downloadAndEvalPageData = function(id) {}

/**
 * @typedef {(this: THIS, body: string) => (arg0: !http.IncomingMessage, arg1: !http.ServerResponse) => (void|Promise<void>)} eco.artd.IFirebase.__evalBody
 * @template THIS
 */
/** @typedef {eco.artd.IFirebase.__evalBody<!eco.artd.IFirebase>} eco.artd.IFirebase._evalBody */
/** @typedef {typeof eco.artd.IFirebase.evalBody} */
/**
 * Evaluates the body as string to produce an executable function.
 * @param {string} body The config to run the method against.
 * @return {(arg0: !http.IncomingMessage, arg1: !http.ServerResponse) => (void|Promise<void>)}
 */
eco.artd.IFirebase.evalBody = function(body) {}

/**
 * @typedef {(this: THIS, dateTime: !Date, times: eco.artd.IFirebase.Times, methods: eco.artd.IFirebase.Methods, invRes: { error: Error, res: null, req: null }) => !Promise<void>} eco.artd.IFirebase.__logRpcInvocation
 * @template THIS
 */
/** @typedef {eco.artd.IFirebase.__logRpcInvocation<!eco.artd.IFirebase>} eco.artd.IFirebase._logRpcInvocation */
/** @typedef {typeof eco.artd.IFirebase.logRpcInvocation} */
/**
 * Creates a record in the telemetry namespace firestore about method having
 * been invoked.
 * @param {!Date} dateTime The date.
 * @param {eco.artd.IFirebase.Times} times The times.
 * - `[pageDataDownloadTime]` _?number?_
 * - `[pageDataFirestoreTime]` _?number?_
 * - `[invokeTime]` _?number?_
 * - `[loadTime]` _?number?_
 * - `[region]` _?string?_
 * - `[downloadTime]` _?number?_
 * - `[downloadMethodDirectlyTime]` _?number?_ How long it took to download the method source code knowing
 * just its id.
 * @param {eco.artd.IFirebase.Methods} methods The methods.
 * - `[methodDate]` _?Date?_
 * - `[methodVersion]` _?string?_
 * - `[methodName]` _?string?_
 * - `[methodEtag]` _?string?_
 * - `[methodDeps]` _Map&lt;string, string&gt;?_
 * - `[methodId]` _?string?_
 * - `[methodCid]` _?number?_
 * @param {{ error: Error, res: null, req: null }} invRes
 * @return {!Promise<void>}
 */
eco.artd.IFirebase.logRpcInvocation = function(dateTime, times, methods, invRes) {}

/**
 * @typedef {(this: THIS, file: string) => !Promise<eco.artd.IFirebase.fetchFunctionsFile.Return>} eco.artd.IFirebase.__fetchFunctionsFile
 * @template THIS
 */
/** @typedef {eco.artd.IFirebase.__fetchFunctionsFile<!eco.artd.IFirebase>} eco.artd.IFirebase._fetchFunctionsFile */
/** @typedef {typeof eco.artd.IFirebase.fetchFunctionsFile} */
/**
 * Executes the main method via the class.
 * @param {string} file
 * @return {!Promise<eco.artd.IFirebase.fetchFunctionsFile.Return>}
 */
eco.artd.IFirebase.fetchFunctionsFile = function(file) {}

/**
 * @typedef {Object} eco.artd.IFirebase.fetchFunctionsFile.Return
 * @prop {string} body
 * @prop {!Date} lastModified
 * @prop {!Map} headers
 */

/**
 * @typedef {(this: THIS, deps: !Array<string>, methodName: string, id: number) => !Promise} eco.artd.IFirebase.__downloadDeps
 * @template THIS
 */
/** @typedef {eco.artd.IFirebase.__downloadDeps<!eco.artd.IFirebase>} eco.artd.IFirebase._downloadDeps */
/** @typedef {typeof eco.artd.IFirebase.downloadDeps} */
/**
 * Loads the deps.
 * @param {!Array<string>} deps
 * @param {string} methodName
 * @param {number} id
 * @return {!Promise}
 */
eco.artd.IFirebase.downloadDeps = function(deps, methodName, id) {}

/**
 * @typedef {(this: THIS, cid: number, methodName: string) => !Promise<string>} eco.artd.IFirebase.__downloadMethod
 * @template THIS
 */
/** @typedef {eco.artd.IFirebase.__downloadMethod<!eco.artd.IFirebase>} eco.artd.IFirebase._downloadMethod */
/** @typedef {typeof eco.artd.IFirebase.downloadMethod} */
/**
 * Returns the source code where dependencies have been replaced with
 * downloaded ones.
 * @param {number} cid The id of the circuit.
 * @param {string} methodName
 * @return {!Promise<string>}
 */
eco.artd.IFirebase.downloadMethod = function(cid, methodName) {}

/**
 * @typedef {(this: THIS, cid: number, methodId: string) => !Promise<eco.artd.IFirebase.downloadMethodDirectly.Return>} eco.artd.IFirebase.__downloadMethodDirectly
 * @template THIS
 */
/** @typedef {eco.artd.IFirebase.__downloadMethodDirectly<!eco.artd.IFirebase>} eco.artd.IFirebase._downloadMethodDirectly */
/** @typedef {typeof eco.artd.IFirebase.downloadMethodDirectly} */
/**
 * Downloads the method without knowing its name, directly by ID that came
 * from the `x-method` request.
 * @param {number} cid The id of the circuit.
 * @param {string} methodId
 * @return {!Promise<eco.artd.IFirebase.downloadMethodDirectly.Return>}
 */
eco.artd.IFirebase.downloadMethodDirectly = function(cid, methodId) {}

/**
 * @typedef {Object} eco.artd.IFirebase.downloadMethodDirectly.Return
 * @prop {string} body
 * @prop {!Date} lastModified
 * @prop {string} etag The hash from hosting.
 */

/**
 * @typedef {(this: THIS) => number} eco.artd.IFirebase.__getId
 * @template THIS
 */
/** @typedef {eco.artd.IFirebase.__getId<!eco.artd.IFirebase>} eco.artd.IFirebase._getId */
/** @typedef {typeof eco.artd.IFirebase.getId} */
/**
 * Returns the ID which decreases with time that helps debugging.
 * @return {number}
 */
eco.artd.IFirebase.getId = function() {}

/**
 * @typedef {(this: THIS, body: string) => !Promise<eco.artd.IFirebase.patchDeps.Return>} eco.artd.IFirebase.__patchDeps
 * @template THIS
 */
/** @typedef {eco.artd.IFirebase.__patchDeps<!eco.artd.IFirebase>} eco.artd.IFirebase._patchDeps */
/** @typedef {typeof eco.artd.IFirebase.patchDeps} */
/**
 * Updates the dependencies to their absolute paths and returns the map with
 * their versions.
 * @param {string} body
 * @return {!Promise<eco.artd.IFirebase.patchDeps.Return>}
 */
eco.artd.IFirebase.patchDeps = function(body) {}

/**
 * @typedef {Object} eco.artd.IFirebase.patchDeps.Return
 * @prop {string} patchedBody
 * @prop {!Map<string, string>} deps
 */

/**
 * @typedef {(this: THIS, cid: number) => !Promise<string>} eco.artd.IFirebase.__downloadPageData
 * @template THIS
 */
/** @typedef {eco.artd.IFirebase.__downloadPageData<!eco.artd.IFirebase>} eco.artd.IFirebase._downloadPageData */
/** @typedef {typeof eco.artd.IFirebase.downloadPageData} */
/**
 * Downloads metadata from hosting. The file needs to be evaled.
 * @param {number} cid
 * @return {!Promise<string>}
 */
eco.artd.IFirebase.downloadPageData = function(cid) {}

/**
 * @typedef {(this: THIS, cid: number) => !eco.artd.IFirebase.PageData} eco.artd.IFirebase.__loadRequirePageData
 * @template THIS
 */
/** @typedef {eco.artd.IFirebase.__loadRequirePageData<!eco.artd.IFirebase>} eco.artd.IFirebase._loadRequirePageData */
/** @typedef {typeof eco.artd.IFirebase.loadRequirePageData} */
/**
 * Requires the RPC metadata synchronously, from the `rpcDir` directory.
 * Otherwise, the page data can be loaded using the circuit id by the
 * `downloadPageData` method, in which case the data must be added to the
 * hosting.
 * @param {number} cid
 * @return {!eco.artd.IFirebase.PageData}
 */
eco.artd.IFirebase.loadRequirePageData = function(cid) {}

/**
 * @typedef {(this: THIS, path: string, dir: string) => ?} eco.artd.IFirebase.__loadRequire
 * @template THIS
 */
/** @typedef {eco.artd.IFirebase.__loadRequire<!eco.artd.IFirebase>} eco.artd.IFirebase._loadRequire */
/** @typedef {typeof eco.artd.IFirebase.loadRequire} */
/**
 * Loads the module using `require`. Requires the deployment of cloud
 * functions. Works synchronously.
 * @param {string} path
 * @param {string} dir
 * @return {?}
 */
eco.artd.IFirebase.loadRequire = function(path, dir) {}

/**
 * @typedef {(this: THIS, error: !Error, url: string) => void} eco.artd.IFirebase.__updateErrorStack
 * @template THIS
 */
/** @typedef {eco.artd.IFirebase.__updateErrorStack<!eco.artd.IFirebase>} eco.artd.IFirebase._updateErrorStack */
/** @typedef {typeof eco.artd.IFirebase.updateErrorStack} */
/**
 * Cleans the error stack to hide internals of this runtime, and updates
 * `eval at` lines with the pointer to actual files.
 * @param {!Error} error The error.
 * @param {string} url The url from which the method code was downloaded.
 * @return {void}
 */
eco.artd.IFirebase.updateErrorStack = function(error, url) {}

/**
 * @typedef {(this: THIS, methodsMap?: Map<string, string>) => string} eco.artd.IFirebase.__validateMethod
 * @template THIS
 */
/** @typedef {eco.artd.IFirebase.__validateMethod<!eco.artd.IFirebase>} eco.artd.IFirebase._validateMethod */
/** @typedef {typeof eco.artd.IFirebase.validateMethod} */
/**
 *       Checks that the method is present in the map. Otherwise rejects request.
 * The method that cane as `e3c51` will be checked for handler in the methods
 * map; if not found, the response will be 404.
 * @param {Map<string, string>} [methodsMap] If not passed, jsut returns method from the headers.
 * @return {string}
 */
eco.artd.IFirebase.validateMethod = function(methodsMap) {}

/**
 * @typedef {(this: THIS, pageData: !Object<string, eco.artd.IFirebase.PageData>, id: number) => !Promise<void>} eco.artd.IFirebase.__validateLoadAndInvoke
 * @template THIS
 */
/** @typedef {eco.artd.IFirebase.__validateLoadAndInvoke<!eco.artd.IFirebase>} eco.artd.IFirebase._validateLoadAndInvoke */
/** @typedef {typeof eco.artd.IFirebase.validateLoadAndInvoke} */
/**
 * @param {!Object<string, eco.artd.IFirebase.PageData>} pageData
 * @param {number} id
 * @return {!Promise<void>}
 */
eco.artd.IFirebase.validateLoadAndInvoke = function(pageData, id) {}

/**
 * @typedef {(this: THIS, cid: number) => !Promise<void>} eco.artd.IFirebase.__validateLoadAndInvokeById
 * @template THIS
 */
/** @typedef {eco.artd.IFirebase.__validateLoadAndInvokeById<!eco.artd.IFirebase>} eco.artd.IFirebase._validateLoadAndInvokeById */
/** @typedef {typeof eco.artd.IFirebase.validateLoadAndInvokeById} */
/**
 * Downloads without metadata, by method name from the functions folder.
 * @param {number} cid
 * @return {!Promise<void>}
 */
eco.artd.IFirebase.validateLoadAndInvokeById = function(cid) {}

/**
 * @typedef {(this: THIS, id: number) => () => ?} eco.artd.IFirebase.__makeInvoker
 * @template THIS
 */
/** @typedef {eco.artd.IFirebase.__makeInvoker<!eco.artd.IFirebase>} eco.artd.IFirebase._makeInvoker */
/** @typedef {typeof eco.artd.IFirebase.makeInvoker} */
/**
 * @param {number} id
 * @return {() => ?}
 */
eco.artd.IFirebase.makeInvoker = function(id) {}

/**
 * @typedef {(this: THIS, cid: number, mid: string) => ?} eco.artd.IFirebase.__loadAndInvokeById
 * @template THIS
 */
/** @typedef {eco.artd.IFirebase.__loadAndInvokeById<!eco.artd.IFirebase>} eco.artd.IFirebase._loadAndInvokeById */
/** @typedef {typeof eco.artd.IFirebase.loadAndInvokeById} */
/**
 * Downloads the method directly without metadatata.
 * @param {number} cid
 * @param {string} mid
 * @return {?}
 */
eco.artd.IFirebase.loadAndInvokeById = function(cid, mid) {}

/**
 * @typedef {(this: THIS, methodId: string, pageData: !Object<string, eco.artd.IFirebase.PageData>, id: number) => !Promise<void>} eco.artd.IFirebase.__loadAndInvoke
 * @template THIS
 */
/** @typedef {eco.artd.IFirebase.__loadAndInvoke<!eco.artd.IFirebase>} eco.artd.IFirebase._loadAndInvoke */
/** @typedef {typeof eco.artd.IFirebase.loadAndInvoke} */
/**
 * Loads and invokes.
 * @param {string} methodId
 * @param {!Object<string, eco.artd.IFirebase.PageData>} pageData
 * @param {number} id
 * @return {!Promise<void>}
 */
eco.artd.IFirebase.loadAndInvoke = function(methodId, pageData, id) {}

/** @typedef {import('http').IncomingMessage} */
http.IncomingMessage

/** @typedef {import('http').ServerResponse} */
http.ServerResponse

/**
 * @typedef {Object} eco.artd.firebase.Config Additional options for the program.
 * @prop {boolean} [shouldRun=true] A boolean option. Default `true`.
 * @prop {string} [text] A text to return.
 */