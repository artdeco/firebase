/** @nocompile */
eval(`var eco={}
eco.artd={}`)

/** @typedef {typeof eco.artd.firebase} */
/**
 * Various firebase utils and runtime to download JS files from HTTP hosting for execution in functions (helps to eliminate reploy every so often).
 * @param {!eco.artd.firebase.Config} config Additional options for the program.
 * - `[shouldRun=true]` _boolean?_ A boolean option. Default `true`.
 * - `[text]` _string?_ A text to return.
 * @return {!Promise<string>} The result of processing.
 */
eco.artd.firebase = function(config) {}