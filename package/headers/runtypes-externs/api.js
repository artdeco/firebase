/**
 * @param {!eco.artd.FirebaseFactory.Config} [config]
 * @return {typeof eco.artd.HyperFirebase}
 */
$eco.artd.FirebaseFactory = function(config) {}
/** @typedef {typeof $eco.artd.FirebaseFactory} */
eco.artd.FirebaseFactory

/**
 * @param {!eco.artd.firebase.Config} config
 * @return {!Promise<string>}
 */
$eco.artd.firebase = function(config) {}
/** @typedef {typeof $eco.artd.firebase} */
eco.artd.firebase