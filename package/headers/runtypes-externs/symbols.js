/** @const {?} */ eco.artd.AbstractFirebase
/** @const {?} */ eco.artd.IFirebase
/** @const {?} */ $eco.artd.IFirebase
/** @typedef {!eco.artd.IFirebase.getSymbols} */
eco.artd.AbstractFirebase.getSymbols

/** @typedef {!eco.artd.IFirebase.setSymbols} */
eco.artd.AbstractFirebase.setSymbols

/** @typedef {!eco.artd.IFirebase.Symbols} */
eco.artd.AbstractFirebase.Symbols

/** @record */
$eco.artd.IFirebase.SymbolsIn = function() {}
/** @type {string|undefined} */
$eco.artd.IFirebase.SymbolsIn.prototype._firestorePackage
/** @type {string|undefined} */
$eco.artd.IFirebase.SymbolsIn.prototype._rpcDir
/** @type {http.ServerResponse|undefined} */
$eco.artd.IFirebase.SymbolsIn.prototype._res
/** @type {http.IncomingMessage|undefined} */
$eco.artd.IFirebase.SymbolsIn.prototype._req
/** @type {string|undefined} */
$eco.artd.IFirebase.SymbolsIn.prototype._gCloudProject
/** @type {string|undefined} */
$eco.artd.IFirebase.SymbolsIn.prototype._functionTarget
/** @type {string|undefined} */
$eco.artd.IFirebase.SymbolsIn.prototype._functionsEmulator
/** @type {string|undefined} */
$eco.artd.IFirebase.SymbolsIn.prototype._db
/** @type {(?{ info: function(...*) })|undefined} */
$eco.artd.IFirebase.SymbolsIn.prototype._logger
/** @typedef {$eco.artd.IFirebase.SymbolsIn} */
eco.artd.IFirebase.SymbolsIn

/** @record */
$eco.artd.IFirebase.Symbols = function() {}
/** @type {symbol} */
$eco.artd.IFirebase.Symbols.prototype._firestorePackage
/** @type {symbol} */
$eco.artd.IFirebase.Symbols.prototype._rpcDir
/** @type {symbol} */
$eco.artd.IFirebase.Symbols.prototype._res
/** @type {symbol} */
$eco.artd.IFirebase.Symbols.prototype._req
/** @type {symbol} */
$eco.artd.IFirebase.Symbols.prototype._gCloudProject
/** @type {symbol} */
$eco.artd.IFirebase.Symbols.prototype._functionTarget
/** @type {symbol} */
$eco.artd.IFirebase.Symbols.prototype._functionsEmulator
/** @type {symbol} */
$eco.artd.IFirebase.Symbols.prototype._db
/** @type {symbol} */
$eco.artd.IFirebase.Symbols.prototype._logger
/** @typedef {$eco.artd.IFirebase.Symbols} */
eco.artd.IFirebase.Symbols

/** @record */
$eco.artd.IFirebase.SymbolsOut = function() {}
/** @type {string} */
$eco.artd.IFirebase.SymbolsOut.prototype._firestorePackage
/** @type {string} */
$eco.artd.IFirebase.SymbolsOut.prototype._rpcDir
/** @type {http.ServerResponse} */
$eco.artd.IFirebase.SymbolsOut.prototype._res
/** @type {http.IncomingMessage} */
$eco.artd.IFirebase.SymbolsOut.prototype._req
/** @type {string} */
$eco.artd.IFirebase.SymbolsOut.prototype._gCloudProject
/** @type {string} */
$eco.artd.IFirebase.SymbolsOut.prototype._functionTarget
/** @type {string} */
$eco.artd.IFirebase.SymbolsOut.prototype._functionsEmulator
/** @type {string} */
$eco.artd.IFirebase.SymbolsOut.prototype._db
/** @type {?{ info: function(...*) }} */
$eco.artd.IFirebase.SymbolsOut.prototype._logger
/** @typedef {$eco.artd.IFirebase.SymbolsOut} */
eco.artd.IFirebase.SymbolsOut

/** @typedef {function(!eco.artd.IFirebase, !eco.artd.IFirebase.SymbolsIn): void} */
eco.artd.IFirebase.setSymbols

/** @typedef {function(!eco.artd.IFirebase): !eco.artd.IFirebase.SymbolsOut} */
eco.artd.IFirebase.getSymbols