/* A custom license. Please make sure you have the rights to use this software. */

import {c} from '@artdeco/erte'

/**
 * Various firebase utils and runtime to download JS files from HTTP hosting for execution in functions (helps to eliminate reploy every so often).
 * @param {!_firebase.Config} [config] Options for the program.
 * @param {boolean} [config.shouldRun=true] A boolean option. Default `true`.
 * @param {string} [config.text] A text to return.
 */
export default async function firebase(config = {}) {
 const {
  shouldRun = true,
  text = '',
 } = config
 if (!shouldRun) return ''
 console.log('@artdeco/firebase called with %s', c(text, 'yellow'))
 return text
}

/* typal types/index.xml namespace */
/**
 * @typedef {_firebase.Config} Config `＠record` Options for the program.
 * @typedef {Object} _firebase.Config `＠record` Options for the program.
 * @prop {boolean} [shouldRun=true] A boolean option. Default `true`.
 * @prop {string} [text] A text to return.
 */
