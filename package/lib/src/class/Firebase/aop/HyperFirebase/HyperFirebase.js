import Firebase from '../../Firebase'
import AbstractFirebaseProfilingAide from '../../gen/aides/AbstractFirebaseProfilingAide'
import AbstractFirebaseAspects from '../../gen/aspects/AbstractFirebaseAspects'
import FirebaseProfilingAspects from '../../gen/aspects/FirebaseProfilingAspects'
import AbstractHyperFirebase from '../../gen/hyper/AbstractHyperFirebase'
import {FirebaseProc, PageData} from '../../lib/syms'
import FirebaseConditioningAspects from '../FirebaseConditioningAspects'
import FirebaseLoggingAspects from '../FirebaseLoggingAspects'

const EVAL=Symbol('eval')

/** @extends {eco.artd.HyperFirebase} */
export default class extends AbstractHyperFirebase
 .consults(
  // FirebaseGeneralAspects,
  FirebaseConditioningAspects,
  FirebaseProfilingAspects,
  AbstractFirebaseAspects.class.prototype=/**@type {!eco.artd.FirebaseAspects}*/({
   beforeValidateLoadAndInvoke:[
    function({args:{pageData},cancel}) {
     if(pageData===null) {
      cancel()
     }
    },
   ],
   beforeEvalBodyWithProfileTime:[
    ({proc})=>proc(EVAL),
   ],
   afterEvalBodyWithProfileTimeReturns({res,proc}){
    // this.asIFirebase.logger.info('after-eval-returns',{r})
    const l=proc(EVAL).lastEvalBodyProfile
    res.evalBodyTime=l
   },
   beforeDownloadDeps:[
    ({proc:proc})=>proc(FirebaseProc),
   ],
  }),
  FirebaseLoggingAspects,
 )
 .implements(
  Firebase,
  AbstractFirebaseProfilingAide.class.prototype=/**@type {!eco.artd.FirebaseProfilingAide}*/({
   reportGetPageDataFromFirestoreProfile(time,args,proc){
    proc(PageData,{getPageDataFromFirestoreTime:time})
   },
   reportDownloadMethodDirectlyProfile(time,_,proc){
    proc(PageData,{downloadMethodDirectlyTime:time})
   },
   reportDownloadPageDataProfile(time,args,proc){
    proc(PageData,{downloadPageDataTime:time})
   },
   reportDownloadDepsProfile(time,{deps},proc) {
    proc(FirebaseProc,{downloadDepsProfile:time,depsCount:deps.length})
   },
   reportEvalBodyProfile(time,abc,proc) {
    // this.asIFirebase.logger.info('report-eval-profile',{time})
    proc(EVAL,{lastEvalBodyProfile:time})
   },
  }),
  AbstractHyperFirebase.class.prototype=/**@type {!eco.artd.HyperFirebase}*/({
   function(req,res,cb,meta){
    if(typeof cb=='number') {
     const{asIFirebase:{makeInvoker:makeInvoker}}=this
     const invoker=makeInvoker(/** @type {number}*/(cb))
     return invoker()
     // return cb.call(this)
    }
    return cb.call(this)
   },
   async loadAndInvoke(methodId,pageData,id){
    const{
     asIFirebase:{
      loadRequire,downloadMethod,evalBodyWithProfileTime,logRpcInvocation,
      req:request,res:response,
     },
    }=this
    const{methodPaths,methodNames,dir}=pageData

    const FILE=methodPaths.get(methodId)

    // cloud function does not need cors?
    // sw4p33dev.web.app
    const time=new Date
    // could host be obtained from the hosting
    const date=new Date

    const canLoadFromUrl=true
    let evalTime
    /**@type {function(http.IncomingMessage,http.ServerResponse):{error:Error,req:null,res:null}}*/
    let d
    if(canLoadFromUrl) {
     // info("Loading from url")
     const body=await downloadMethod(id,FILE) // what about download time
     const{evalBodyTime,evalBody}=evalBodyWithProfileTime(body)
     d=evalBody
     evalTime=evalBodyTime
    }else{
     // info("Loading from file") // can pass acceptable version to load
     d=await loadRequire(FILE,dir) // add profiling time here also
    }

    // load could be execution of another one
    const loadTime=Date.now()-date.getTime()
    const invokeTimeStart=new Date
    const invRes=await d(request,response)
    const invokeTime=Date.now()-invokeTimeStart.getTime()

    // need to log region
    logRpcInvocation(time,{
     invokeTime:invokeTime,
     loadTime:canLoadFromUrl?evalTime:loadTime,
     downloadTime:canLoadFromUrl?loadTime:null,
    },{
     methodName:methodNames.get(methodId),
    },invRes)
   },
   evalBodyWithProfileTime(body) {
    const{asIFirebase:{evalBody}}=this
    const b=evalBody(body)
    return{evalBody:b,evalBodyTime:0}
   },
   // do something here
  }),
  // AbstractHyperFirebase.class.prototype=/**@type {!FirebaseProfilingAspects}*/({
  //  reportEvalBodyProfile(time,args,proc) {
  //   this.lastEvalBodyProfile=time
  //   const R=proc(EVAL)
  //   this.asIFirebase.logger.info(123,R)
  //   // this.lastEvalBodyProfile=time // need to put it to proc somehow maybe
  //  },
  //  // do something here
  // }),
 )
{}