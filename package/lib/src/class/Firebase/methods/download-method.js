import '../../../../types'

/** @type {eco.artd.IFirebase._downloadMethod} */
export default async function _downloadMethod(id,methodName){
 const{asIFirebase:{
  downloadDeps:downloadDeps,
  fetchFunctionsFile:fetchFunctionsFile,
 }}=this

 // does not fetch the methodNames/Paths map though as this is loaded manually now?
 let{body:index}=await fetchFunctionsFile(`${id}/${methodName}/index.js`)
 let deps=new Set
 const matches=new Map

 index.replace(/require\(['"]\.\/(.+?)['"]\)/g,(m,relPath)=>{
  matches.set(m,relPath)
  deps.add(relPath)
 })

 const rs=await downloadDeps([...deps],methodName,id)
 // deps could be recursive

 for(const m of matches.keys()) {
  index=index.replace(m,()=>{
   const r=matches.get(m)
   const b=rs.get(r)
   return b
  })
 }

 return index
}
