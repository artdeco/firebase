import { $advice, newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @abstract
 * @extends {eco.artd.AbstractFirebaseAspectsInstaller}
 */
class _FirebaseAspectsInstaller { }

_FirebaseAspectsInstaller.prototype[$advice]=true

/** @extends {eco.artd.AbstractFirebaseAspectsInstaller} ‎ */
class FirebaseAspectsInstaller extends newAbstract(
 _FirebaseAspectsInstaller,16817934095,null,{},false) {}

/** @type {typeof eco.artd.AbstractFirebaseAspectsInstaller} */
FirebaseAspectsInstaller.class=function(){}
/** @type {typeof eco.artd.AbstractFirebaseAspectsInstaller} */
function FirebaseAspectsInstallerClass(){}

export default FirebaseAspectsInstaller


FirebaseAspectsInstaller[$implementations]=[
 FirebaseAspectsInstallerClass.prototype=/**@type {!eco.artd.IFirebaseAspectsInstaller}*/({
  function(){
   this.beforeFunction=1
   this.afterFunction=2
   this.aroundFunction=3
   this.afterFunctionThrows=4
   this.afterFunctionReturns=5
   this.immediatelyAfterFunction=6
   this.afterFunctionCancels=7
   return {
    request:1,
    response:2,
    cb:3,
    meta:4,
   }
  },
  evalBodyWithProfileTime(){
   this.beforeEvalBodyWithProfileTime=1
   this.afterEvalBodyWithProfileTime=2
   this.aroundEvalBodyWithProfileTime=3
   this.afterEvalBodyWithProfileTimeThrows=4
   this.afterEvalBodyWithProfileTimeReturns=5
   this.afterEvalBodyWithProfileTimeCancels=7
   return {
    body:1,
   }
  },
  getPageDataFromFirestore(){
   this.beforeGetPageDataFromFirestore=1
   this.afterGetPageDataFromFirestore=2
   this.aroundGetPageDataFromFirestore=3
   this.afterGetPageDataFromFirestoreThrows=4
   this.afterGetPageDataFromFirestoreReturns=5
   this.immediatelyAfterGetPageDataFromFirestore=6
   this.afterGetPageDataFromFirestoreCancels=7
   return {
    cid:1,
   }
  },
  downloadAndEvalPageData(){
   this.beforeDownloadAndEvalPageData=1
   this.afterDownloadAndEvalPageData=2
   this.aroundDownloadAndEvalPageData=3
   this.afterDownloadAndEvalPageDataThrows=4
   this.afterDownloadAndEvalPageDataReturns=5
   this.immediatelyAfterDownloadAndEvalPageData=6
   this.afterDownloadAndEvalPageDataCancels=7
   return {
    id:1,
   }
  },
  evalBody(){
   this.beforeEvalBody=1
   this.afterEvalBody=2
   this.aroundEvalBody=3
   this.afterEvalBodyThrows=4
   this.afterEvalBodyReturns=5
   this.afterEvalBodyCancels=7
   return {
    body:1,
   }
  },
  logRpcInvocation(){
   this.beforeLogRpcInvocation=1
   this.afterLogRpcInvocation=2
   this.aroundLogRpcInvocation=3
   this.afterLogRpcInvocationThrows=4
   this.afterLogRpcInvocationReturns=5
   this.immediatelyAfterLogRpcInvocation=6
   this.afterLogRpcInvocationCancels=7
   return {
    dateTime:1,
    times:2,
    methods:3,
    invRes:4,
   }
  },
  fetchFunctionsFile(){
   this.beforeFetchFunctionsFile=1
   this.afterFetchFunctionsFile=2
   this.aroundFetchFunctionsFile=3
   this.afterFetchFunctionsFileThrows=4
   this.afterFetchFunctionsFileReturns=5
   this.immediatelyAfterFetchFunctionsFile=6
   this.afterFetchFunctionsFileCancels=7
   return {
    file:1,
   }
  },
  downloadDeps(){
   this.beforeDownloadDeps=1
   this.afterDownloadDeps=2
   this.aroundDownloadDeps=3
   this.afterDownloadDepsThrows=4
   this.afterDownloadDepsReturns=5
   this.immediatelyAfterDownloadDeps=6
   this.afterDownloadDepsCancels=7
   return {
    deps:1,
    methodName:2,
    id:3,
   }
  },
  downloadMethod(){
   this.beforeDownloadMethod=1
   this.afterDownloadMethod=2
   this.aroundDownloadMethod=3
   this.afterDownloadMethodThrows=4
   this.afterDownloadMethodReturns=5
   this.immediatelyAfterDownloadMethod=6
   this.afterDownloadMethodCancels=7
   return {
    cid:1,
    methodName:2,
   }
  },
  downloadMethodDirectly(){
   this.beforeDownloadMethodDirectly=1
   this.afterDownloadMethodDirectly=2
   this.aroundDownloadMethodDirectly=3
   this.afterDownloadMethodDirectlyThrows=4
   this.afterDownloadMethodDirectlyReturns=5
   this.immediatelyAfterDownloadMethodDirectly=6
   this.afterDownloadMethodDirectlyCancels=7
   return {
    cid:1,
    methodId:2,
   }
  },
  getId(){
   this.beforeGetId=1
   this.afterGetId=2
   this.aroundGetId=3
   this.afterGetIdThrows=4
   this.afterGetIdReturns=5
   this.afterGetIdCancels=7
  },
  patchDeps(){
   this.beforePatchDeps=1
   this.afterPatchDeps=2
   this.aroundPatchDeps=3
   this.afterPatchDepsThrows=4
   this.afterPatchDepsReturns=5
   this.immediatelyAfterPatchDeps=6
   this.afterPatchDepsCancels=7
   return {
    body:1,
   }
  },
  downloadPageData(){
   this.beforeDownloadPageData=1
   this.afterDownloadPageData=2
   this.aroundDownloadPageData=3
   this.afterDownloadPageDataThrows=4
   this.afterDownloadPageDataReturns=5
   this.immediatelyAfterDownloadPageData=6
   this.afterDownloadPageDataCancels=7
   return {
    cid:1,
   }
  },
  loadRequirePageData(){
   this.beforeLoadRequirePageData=1
   this.afterLoadRequirePageData=2
   this.aroundLoadRequirePageData=3
   this.afterLoadRequirePageDataThrows=4
   this.afterLoadRequirePageDataReturns=5
   this.afterLoadRequirePageDataCancels=7
   return {
    cid:1,
   }
  },
  loadRequire(){
   this.beforeLoadRequire=1
   this.afterLoadRequire=2
   this.aroundLoadRequire=3
   this.afterLoadRequireThrows=4
   this.afterLoadRequireReturns=5
   this.afterLoadRequireCancels=7
   return {
    path:1,
    dir:2,
   }
  },
  updateErrorStack(){
   this.beforeUpdateErrorStack=1
   this.afterUpdateErrorStack=2
   this.aroundUpdateErrorStack=3
   this.afterUpdateErrorStackThrows=4
   this.afterUpdateErrorStackReturns=5
   this.afterUpdateErrorStackCancels=7
   return {
    error:1,
    url:2,
   }
  },
  validateMethod(){
   this.beforeValidateMethod=1
   this.afterValidateMethod=2
   this.aroundValidateMethod=3
   this.afterValidateMethodThrows=4
   this.afterValidateMethodReturns=5
   this.afterValidateMethodCancels=7
   return {
    methodsMap:1,
   }
  },
  validateLoadAndInvoke(){
   this.beforeValidateLoadAndInvoke=1
   this.afterValidateLoadAndInvoke=2
   this.aroundValidateLoadAndInvoke=3
   this.afterValidateLoadAndInvokeThrows=4
   this.afterValidateLoadAndInvokeReturns=5
   this.immediatelyAfterValidateLoadAndInvoke=6
   this.afterValidateLoadAndInvokeCancels=7
   return {
    pageData:1,
    id:2,
   }
  },
  validateLoadAndInvokeById(){
   this.beforeValidateLoadAndInvokeById=1
   this.afterValidateLoadAndInvokeById=2
   this.aroundValidateLoadAndInvokeById=3
   this.afterValidateLoadAndInvokeByIdThrows=4
   this.afterValidateLoadAndInvokeByIdReturns=5
   this.immediatelyAfterValidateLoadAndInvokeById=6
   this.afterValidateLoadAndInvokeByIdCancels=7
   return {
    cid:1,
   }
  },
  makeInvoker(){
   this.beforeMakeInvoker=1
   this.afterMakeInvoker=2
   this.aroundMakeInvoker=3
   this.afterMakeInvokerThrows=4
   this.afterMakeInvokerReturns=5
   this.afterMakeInvokerCancels=7
   return {
    id:1,
   }
  },
  loadAndInvokeById(){
   this.beforeLoadAndInvokeById=1
   this.afterLoadAndInvokeById=2
   this.aroundLoadAndInvokeById=3
   this.afterLoadAndInvokeByIdThrows=4
   this.afterLoadAndInvokeByIdReturns=5
   this.afterLoadAndInvokeByIdCancels=7
   return {
    cid:1,
    mid:2,
   }
  },
  loadAndInvoke(){
   this.beforeLoadAndInvoke=1
   this.afterLoadAndInvoke=2
   this.aroundLoadAndInvoke=3
   this.afterLoadAndInvokeThrows=4
   this.afterLoadAndInvokeReturns=5
   this.immediatelyAfterLoadAndInvoke=6
   this.afterLoadAndInvokeCancels=7
   return {
    methodId:1,
    pageData:2,
    id:3,
   }
  },
 }),
]