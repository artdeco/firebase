import {ProfilingAide,profiling} from '@type.engineering/profiling'
import { newAbstract, $implementations, precombined } from '@type.engineering/type-engineer'

/**
 * @abstract
 * @extends {eco.artd.AbstractFirebaseProfilingAide}
 */
class _AbstractFirebaseProfilingAide { }
/**
 * Records the results of the profiling.
 * @extends {eco.artd.AbstractFirebaseProfilingAide} ‎
 */
class AbstractFirebaseProfilingAide extends newAbstract(
 _AbstractFirebaseProfilingAide,501525464513,null,{
  asIFirebaseProfilingAide:1,
  superFirebaseProfilingAide:2,
 },false) {}

/** @type {typeof eco.artd.AbstractFirebaseProfilingAide} */
AbstractFirebaseProfilingAide.class=function(){}
/** @type {typeof eco.artd.AbstractFirebaseProfilingAide} */
function AbstractFirebaseProfilingAideClass(){}

export default AbstractFirebaseProfilingAide


AbstractFirebaseProfilingAide[$implementations]=[
 AbstractFirebaseProfilingAideClass.prototype=/**@type {!eco.artd.IFirebaseProfilingAide}*/({
  reportGetPageDataFromFirestoreProfile:precombined,
  reportDownloadAndEvalPageDataProfile:precombined,
  reportEvalBodyProfile:precombined,
  reportFetchFunctionsFileProfile:precombined,
  reportDownloadDepsProfile:precombined,
  reportDownloadMethodProfile:precombined,
  reportDownloadMethodDirectlyProfile:precombined,
  reportDownloadPageDataProfile:precombined,
  reportLoadRequirePageDataProfile:precombined,
  reportLoadRequireProfile:precombined,
 }),
 ProfilingAide,
 profiling({
  reportGetPageDataFromFirestoreProfile:{
   id:24,
   beforeGetPageDataFromFirestore:1,
   afterGetPageDataFromFirestore:2,
   afterGetPageDataFromFirestoreThrows:3,
  },
  reportDownloadAndEvalPageDataProfile:{
   id:21,
   beforeDownloadAndEvalPageData:1,
   afterDownloadAndEvalPageData:2,
   afterDownloadAndEvalPageDataThrows:3,
  },
  reportEvalBodyProfile:{
   id:9,
   beforeEvalBody:1,
   afterEvalBody:2,
   afterEvalBodyThrows:3,
  },
  reportFetchFunctionsFileProfile:{
   id:20,
   beforeFetchFunctionsFile:1,
   afterFetchFunctionsFile:2,
   afterFetchFunctionsFileThrows:3,
  },
  reportDownloadDepsProfile:{
   id:17,
   beforeDownloadDeps:1,
   afterDownloadDeps:2,
   afterDownloadDepsThrows:3,
  },
  reportDownloadMethodProfile:{
   id:8,
   beforeDownloadMethod:1,
   afterDownloadMethod:2,
   afterDownloadMethodThrows:3,
  },
  reportDownloadMethodDirectlyProfile:{
   id:25,
   beforeDownloadMethodDirectly:1,
   afterDownloadMethodDirectly:2,
   afterDownloadMethodDirectlyThrows:3,
  },
  reportDownloadPageDataProfile:{
   id:22,
   beforeDownloadPageData:1,
   afterDownloadPageData:2,
   afterDownloadPageDataThrows:3,
  },
  reportLoadRequirePageDataProfile:{
   id:18,
   beforeLoadRequirePageData:1,
   afterLoadRequirePageData:2,
   afterLoadRequirePageDataThrows:3,
  },
  reportLoadRequireProfile:{
   id:4,
   beforeLoadRequire:1,
   afterLoadRequire:2,
   afterLoadRequireThrows:3,
  },
 },void 0,16817934091),
]