import AbstractFirebaseAspects from '../AbstractFirebaseAspects'

/** @extends {eco.artd.AbstractFirebaseLoggingAspects} ‎*/
export default class AbstractFirebaseLoggingAspects extends /**@type {typeof eco.artd.AbstractFirebaseLoggingAspects}*/(AbstractFirebaseAspects.clone()) {}

/** @type {typeof eco.artd.AbstractFirebaseLoggingAspects} */
AbstractFirebaseLoggingAspects.class=function(){}