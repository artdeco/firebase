import AbstractFirebaseProfilingAspects from '../AbstractFirebaseProfilingAspects'
import FirebaseProfilingAide from '../../aides/FirebaseProfilingAide'

/** @extends {eco.artd.FirebaseProfilingAspects} */
export class FirebaseProfilingAspects extends AbstractFirebaseProfilingAspects.continues(
 FirebaseProfilingAide,
) {}
export default FirebaseProfilingAspects