import AbstractFirebaseAspects from '../AbstractFirebaseAspects'

/** @extends {eco.artd.AbstractFirebaseConditioningAspects} ‎*/
export default class AbstractFirebaseConditioningAspects extends /**@type {typeof eco.artd.AbstractFirebaseConditioningAspects}*/(AbstractFirebaseAspects.clone()) {}

/** @type {typeof eco.artd.AbstractFirebaseConditioningAspects} */
AbstractFirebaseConditioningAspects.class=function(){}