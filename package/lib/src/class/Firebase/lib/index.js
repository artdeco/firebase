const getRandom=()=>{
 let rnd=Number((Math.random()*1000).toFixed())
 const pad=3-`${rnd}`.length
 rnd=`${`0`.repeat(pad)}${rnd}`
 return rnd
}

const PID=getRandom()

/**
 * Returns and ID which is always decreasing as time goes on.
 */
export const getId=()=>{
 const micro=`${process['hrtime']()[1]}`.slice(3,6)
 const precise=Date.now()+micro
 const N=('9'.repeat(`${precise}`.length))
 const v=Number(N)-Number(precise)
 return v+PID
}