/* eslint-disable no-unexpected-multiline */
import {join, relative, resolve} from 'path'
import findPackageJson from '@depack2/fpj'
import AbstractFirebase from './gen/AbstractFirebase'
import {getId as _getId} from './lib'
import _downloadMethod from './methods/download-method'
import _fetchFunctionsFile from './methods/fetch-file'

/** @type {eco.artd.IFirebase._downloadMethodDirectly} */
async function _downloadMethodDirectly(cid,methodId) {
 const{asIFirebase:{
  fetchFunctionsFile:fetchFunctionsFile,
 }}=this

 let{body:index,lastModified,headers,url:url}=await fetchFunctionsFile(`${cid}/${methodId}.js`)
 const etag=headers.get('etag')

 return{body:index,lastModified,etag,url:url}
}

/**@extends {eco.artd.Firebase}*/
export default class extends AbstractFirebase.implements(
 AbstractFirebase.class.prototype=/**@type {!eco.artd.Firebase}*/({
  // constructor() {},
  downloadMethod:_downloadMethod,
  downloadMethodDirectly:_downloadMethodDirectly,
  get host() {
   const{asIFirebase:{req}}=this
   return req.headers['x-forwarded-host']
  },
  async downloadDeps(deps,methodName,id){
   const rs=new Map
   const{asIFirebase:{fetchFunctionsFile}}=this
   for(const dep of deps) {
    const{body:depSrc}=await fetchFunctionsFile(`${id}/${methodName}/${dep}.js`)
    rs.set(dep,depSrc.replace(/^module.exports=/,''))
   }
   return rs
  },
  get isLocal() {
   const{asIFirebase:{host:host}}=this
   return host=='localhost:5000'
  },
  async getPageDataFromFirestore(cid) {
   const{asIFirebase:{firestorePackage}}=this
   const{'getFirestore':getFirestore}=require(firestorePackage)

   const firestore=getFirestore()
   const document = firestore['doc']('methods-metadata/'+cid)
   const g=await document['get']()
   if(!g['exists']) return null

   const data=g['data']()
   const R={
    methodNames:new Map(Object.entries(data['methodNames'])),
    methodPaths:new Map(Object.entries(data['methodPaths'])),
   }
   return R
  },
  async logRpcInvocation(date,{ // method date could come from last-modified header
   downloadTime,loadTime,invokeTime,
   pageDataDownloadTime:pageDataDownloadTime,
   pageDataFirestoreTime:pageDataFirestoreTime,
   downloadMethodDirectlyTime:downloadMethodDirectlyTime,
   region:region,
  },{
   methodName,methodEtag,methodDate,methodVersion,methodId,methodCid,methodDeps,
  },{error:error,req:request,res:res}) {
   const{asIFirebase:{
    host:host,
    getId:getId,
    isLocal:isLocal,
    gCloudProject:gCloudProject,
    functionTarget:functionTarget,
    firestorePackage:firestorePackage,
    functionsEmulator:functionsEmulator,
    req:{headers:{'referer':referer}},
   }}=this
   // can use conditioning aspects on a certain method to condition the instance
   // const HOST=

   const D={
    'date':date,
    'host':host,
    ...(region?{'region':region}:{}),
    ...(pageDataDownloadTime?{'pageDataDownloadTime':pageDataDownloadTime}:{}),
    ...(pageDataFirestoreTime?{'pageDataFirestoreTime':pageDataFirestoreTime}:{}),
    ...(downloadMethodDirectlyTime?{'downloadMethodDirectlyTime':downloadMethodDirectlyTime}:{}),
    ...(methodDate?{'methodDate':methodDate}:{}),
    ...(methodEtag?{'methodEtag':methodEtag}:{}),
    ...(methodVersion?{'methodVersion':methodVersion}:{}),
    ...(methodId?{'methodId':methodId}:{}),
    ...(methodCid?{'methodCid':methodCid}:{}),
    ...(referer?{'referer':referer}:{}),
    'downloadTime':downloadTime,
    'time':invokeTime,
    'loadTime':loadTime,
    'request':request,
    // debug:{...request.headers},
    // time:process.hrtime(),
    'project':gCloudProject,
    'cloudFunction':functionsEmulator?'emulator':functionTarget,
    // id:Number(process.hrtime.bigint()),
   }

   let id=getId()
   // const hex=id.toHexString()
   // 1224632806328676
   const{'getFirestore':getFirestore}=require(firestorePackage)

   // [...id.toHexString()].reverse().join('')
   if(error) {
    let{message,stack}=error
    await getFirestore()
     ['collection'](`${isLocal?`l`:''}te.${methodName}`)
     ['doc'](id)
     ['set']({
      'error':message,'errorStack':stack,...D,
      'methodDeps':Object.fromEntries(methodDeps.entries()),
     })
   }
   else await getFirestore()
    ['collection'](`${isLocal?`l`:''}t.${methodName}`)
    ['doc'](id)
    ['set']({...D,'result':res})
  },
  evalBody(body){
   const fn=new Function('module','require','__dirname',body)
   // const fn=eval(`(function firebaseEval(module,require,__dirname){${body}})`)
   const mod={exports:{}}

   fn(mod,require,__dirname) // dirname is irrelevant
   const{exports:FN}=mod
   return FN
   // return FN
  },
  getId(){
   // if(isLocal) return _getId(false)
   // how do we know if local or not.
   return _getId()
  },
  fetchFunctionsFile:_fetchFunctionsFile,
  // getLocalId(){
  //  return _getId(false)
  // },
  // add log to hard aspects
  loadRequire(path,dir){
   const{logger:{info:info}}=this
   let dd=new Date
   // info("loading "+path+" from",)
   const r=require(join(dir,path))
   const d=Date.now()-dd.getTime()
   // hard aspects, or logging aspects
   info("Loaded "+path+" from"+relative('',dir)+" in",d+'ms')
   return r
  },
  async downloadPageData(cid) {
   const{asIFirebase:{fetchFunctionsFile}}=this
   // does not fetch the methodNames/Paths map though as this is loaded manually now?
   let index=await fetchFunctionsFile(`${cid}/index.js`) // without method name?
   return index
   // debugger
   // let deps=new Set
   // const matches=new Map
  },
  loadRequirePageData(cid) {
   const{asIFirebase:{
    rpcDir:rpcDir='functions/_rpc',
   }}=this
   const pageData=/** @type {eco.artd.IFirebase.PageData} */(require(resolve(rpcDir,cid+''))) // require the folder for the functions dir
   return pageData
  },
  async downloadAndEvalPageData(id){
   const{asIFirebase:{
    downloadPageData:downloadPageData,
    evalBody:evalBody,
   }}=this
   const pageData=await downloadPageData(id)
   const R=evalBody(pageData)
   return R
  },
  makeInvoker(id) {
   const{asIFirebase:{
    // loadRequirePageData:loadRequirePageData,
    // downloadAndEvalPageData:downloadAndEvalPageData,
    // validateLoadAndInvoke:validateLoadAndInvoke,
    validateLoadAndInvokeById:validateLoadAndInvokeById,
    // getPageDataFromFirestore:getPageDataFromFirestore,
    // logger:{info},
    res,
   }}=this
   // The idea is to build cloud function container instance of JS isolates
   // from Oracle.
   const INVOKE=async()=>{
    await validateLoadAndInvokeById(id) // must explicitly end response?
    // not sure what is written or not to the validator
    try{
     res.end()
    }catch(err) {
     // nvm
    }
    // supported methods:
    // let pageData=null
    // 1. sync require loading
    // pageData=loadRequirePageData()
    // 2. async download from hosting
    // pageData=await downloadAndEvalPageData()
    // 3. load from firestore
    // pageData=await getPageDataFromFirestore(id)
    // if(!pageData) {
    //  info({'no page data':true,'cid':id});{
    //   res.statusCode=404
    //   res.end()
    //   return
    //  }
    // }
    // 4.
    // loaded from the same host as firebase.
    // pageData=await downloadAndEvalPageData(id)

    // return validateLoadAndInvoke(pageData,id)
   }
   return INVOKE
  },
  async validateLoadAndInvokeById(id) {
   const{
    asIFirebase:{
     validateMethod:validateMethod,loadAndInvokeById:loadAndInvokeById,
    },
   }=this

   const methodId=validateMethod()
   await loadAndInvokeById(id,methodId)
  },
  async patchDeps(body) {
   const rs=[]
   const deps=new Map
   body=body.replace(/require\(["'](.+?)['"]\)/g,(m,pckg)=>{
    rs.push(pckg)
    return`require`+`('__REQUIRE__${rs.length}')`
   })
   let i=0
   for(const pckg of rs) {
    const{entry,version}=await findPackageJson(process.cwd(),pckg,{
     preferModule:false,
    })
    body=body.replace(`__REQUIRE__${i+1}`,join(process.cwd(),entry))
    deps.set(pckg,version)
    i++
   }

   return{
    patchedBody:body,
    deps:deps,
   }
  },
  async loadAndInvokeById(cid,mid){
   const{
    asIFirebase:{
     patchDeps,
     downloadMethodDirectly,evalBodyWithProfileTime,logRpcInvocation,
     req:request,res:response,
     updateErrorStack,
    },
   }=this
   const time=new Date
   const date=new Date

   let evalTime
   let{body:body,lastModified:lastModified,etag:etag,url}=await downloadMethodDirectly(cid,mid)
   const{patchedBody:patchedBody,deps:deps}=await patchDeps(body)
   body=patchedBody
   const{evalBodyTime,evalBody}=evalBodyWithProfileTime(body)
   /**@type {function(http.IncomingMessage,http.ServerResponse):Promise<{error:Error,req:null,res:null}>}*/
   const d=evalBody
   evalTime=evalBodyTime

   // generate execution depth of n trees to see where conditioning stops
   // working
   const methodName=d['methodName']
   const methodVersion=d['methodVersion']||''
   let methodDate=lastModified

   // // last-modified won't be in request headers
   // if('last-modified' in request.headers){
   //  const dd=request.headers['last-modified']
   //  const parsedD=new Date(dd)
   //  methodDate=parsedD
   // }
   // if(!methodDate) methodDate=d['methodDate']||null
   // if(methodDate) methodDate=new Date(methodDate)

   // load could be execution of another one
   const loadTime=Date.now()-date.getTime()
   const invokeTimeStart=new Date
   const invRes=await d(request,response)
   if(invRes.error) {
    updateErrorStack(invRes.error,url)
   }
   const invokeTime=Date.now()-invokeTimeStart.getTime()

   // const methodNames=new Map
   // where would it get method map? could be in the same module?

   // const methodName=methodNames.get(mid)||mid

   logRpcInvocation(time,{
    invokeTime:invokeTime,
    loadTime:true?evalTime:loadTime,
    downloadTime:true?loadTime:null,
   },{
    methodName,methodDate,
    methodVersion,
    methodId:mid,
    methodCid:cid,
    methodEtag:etag,
    methodDeps:deps,
   },invRes)
  },
  updateErrorStack(error,url) {
   let r=error.stack.split('\n').reverse()
   const i=r.findIndex((line)=>{
    return /eval at evalBody/.test(line)
   })
   r.splice(0,i)
   const newStack=r.reverse().join('\n')
   error.stack=newStack.replace(/\(eval at evalBody \(.+?\),\s*<anonymous>:(\d+):(\d+)\)/g,(m,l,c)=>{
    return `${url}:${l-2}:${c}`
   })
  },
  async validateLoadAndInvoke(pageData,id) {
   const{methodPaths}=pageData

   const{
    asIFirebase:{
     logger:{info},
     loadAndInvoke,validateMethod,
    },
   }=this
   const methodId=validateMethod(methodPaths)
   if(!methodId) {
    info(404)
    return
   }
   await loadAndInvoke(methodId,pageData,id)
  },
  // getMethod
  // this is an aide's method
  validateMethod(methodsMap){
   const{asIFirebase:{req,res}}=this
   const METHOD=req.headers['x-method']

   if(!methodsMap) return METHOD||''

   if(!methodsMap.has(METHOD)) {
    res['status']=404
    res.end('')
    return ''
   }

   return METHOD
  },
 }),
) {}
