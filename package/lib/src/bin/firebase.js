/* A custom license. Please make sure you have the rights to use this software. */

import {reduceUsage} from '@artdeco/argufy'
import {c} from '@artdeco/erte/colors'
import {writeFileSync} from 'fs'
import usually from 'usually'
import firebase from '../api/run'
import Firebase from '../class/Firebase'
import Init from './commands/init'
import {argsConfig, _help, _init, _input, _output, _version} from './get-args'

if (_help) {
 const usage = usually({
  description: 'Various firebase utils and runtime to download JS files from HTTP hosting for execution in functions (helps to eliminate reploy every so often).',
  example: 'firebase example.txt -o out.txt',
  line: 'firebase input [-o output] [-ihv]',
  usage: reduceUsage(argsConfig),
 })
 console.log(usage)
 process.exit(0)
} else if (_version) {
 console.log(require('../../package.json').version)
 process.exit(0)
}

(async () => {
 try {
  if (_init) return await Init()
  if (!_input) throw new Error('Please pass an input file.')
  // const content = /** @type {string} */ (readFileSync(_input, 'utf8'))
  const firebase=new Firebase({
   symbol:'test',
  })
  const{
   asIFirebase:{
    example:content,
   },
  }=firebase
  const output=await firebase({
   shouldRun: true,
   text: content,
  })
  if (_output == '-') console.log(output)
  else writeFileSync(_output, output)
  console.error('File %s successfully processed.', c(_input, 'yellow'))
 } catch (err) {
  if (process.env['DEBUG']) console.error(err.stack)
  else console.log(err.message)
 }
})()