import {c} from '@artdeco/erte/colors'
import '../../types'

/** @type {eco.artd.firebase} */
export default async function firebase(config = {}) {
 const {
  shouldRun = true,
  text = '',
 } = config
 if (!shouldRun) return ''
 console.log('@artdeco/firebase called with %s', c(text, 'yellow'))
 return text
}