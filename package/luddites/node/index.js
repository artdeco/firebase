import Module from './node'

/**
@license
@LICENSE @artdeco/firebase (c) by Art Deco™ 2024.
Please make sure you have a Commercial License to use this library.
*/

/** @type {typeof eco.artd.Firebase} */
export const Firebase=Module['50152546451']
/** @type {typeof eco.artd.FirebaseAspectsInstaller} */
export const FirebaseAspectsInstaller=Module['50152546452']
/**@extends {eco.artd.AbstractFirebaseAspects}*/
export class AbstractFirebaseAspects extends Module['50152546453'] {}
/** @type {typeof eco.artd.AbstractFirebaseAspects} */
AbstractFirebaseAspects.class=function(){}
/**@extends {eco.artd.AbstractHyperFirebase}*/
export class AbstractHyperFirebase extends Module['50152546455'] {}
/** @type {typeof eco.artd.AbstractHyperFirebase} */
AbstractHyperFirebase.class=function(){}
/** @type {typeof eco.artd.HyperFirebase} */
export const HyperFirebase=Module['50152546456']
/** @type {!eco.artd.FirebaseFactory} */
export const FirebaseFactory=Module['50152546457']
/** @type {typeof eco.artd.FirebaseLoggingAspects} */
export const FirebaseLoggingAspects=Module['50152546458']
/** @type {typeof eco.artd.FirebaseProfilingAspects} */
export const FirebaseProfilingAspects=Module['50152546459']
/**@extends {eco.artd.AbstractFirebaseProfilingAide}*/
export class AbstractFirebaseProfilingAide extends Module['501525464510'] {}
/** @type {typeof eco.artd.AbstractFirebaseProfilingAide} */
AbstractFirebaseProfilingAide.class=function(){}
/** @type {typeof eco.artd.FirebaseProfilingAide} */
export const FirebaseProfilingAide=Module['501525464511']
/** @type {typeof eco.artd.FirebaseConditioningAspects} */
export const FirebaseConditioningAspects=Module['501525464512']

/** Allows to embed the object code directly into other packages. */