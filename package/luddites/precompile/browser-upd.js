import {getPackageName, isShared} from '../version'
/**
@license
@LICENSE                      Warning!
This file links to (or embeds, in which case you will see object code
boundaries comments before and after), proprietary code from the package(s):
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
Please make sure you have a Commercial License to use this library.
*/
const packageName=getPackageName()

/** @nosideeffects */
function getEmbeddedModule(exports,require,module={},__filename='',__dirname='') {
 const fn=new Function('exports,require,module,__filename,__dirname',`
/*! @embed-object-start {@artdeco/firebase} */
/*

[? Any reverse engineering of source code is strictly prohibited !]
@LICENSE @type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
Please make sure you have a Commercial License to use this library.
*/

const b=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@type.engineering/type-engineer"'))}();
const c=b["372700389810"],d=b["372700389811"];function e(a,f,g,h){return b["372700389812"](a,f,null,g,!1,h)}const k=b.iu,l=b.$advice;class m{__$constructor(){this.example="ok"}}class n extends e(m,16817934091,{asIFirebase:1,superFirebase:2},{symbol:{g:1}}){}function p(){}const q=n.getSymbols,r=n.setSymbols;n[d]=[{constructor:function(){const {g:a}=q(this);r(this,{g:k(a,"")})}},{[c]:{symbol:1},initializer({symbol:a}){r(this,{g:a})}}];var t=class extends n.implements(p.prototype={constructor(){}}){};class u{}u.prototype[l]=!0;class v extends e(u,16817934095,{}){}v[d]=[{run(){this.beforeRun=1;this.afterRun=2;this.afterRunThrows=4;this.afterRunReturns=5;this.immediatelyAfterRun=6;this.afterRunCancels=7;return{conf:1}}}];class w{}class x extends b["372700389817"](w,16817934097,null,{},!1,void 0,void 0){};class y{}class z extends e(y,16817934098,{asIHyperFirebase:v,superHyperFirebase:2}){};var A=class extends x.continues(){};var B=class extends z.consults(A).implements(t){};module.exports["50152546450"]=t;module.exports["50152546451"]=t;module.exports["50152546452"]=v;module.exports["50152546453"]=x;module.exports["50152546455"]=z;module.exports["50152546456"]=B;module.exports["50152546457"]=function({aspectsInstallers:a,aspects:f,subjects:g=[],hypers:h}){return z.extends(h).installs(a).consults(f).implements(t,...g)};
/*! @embed-object-end {@artdeco/firebase} */`)
 module['exports']=module.exports||exports||{}
 fn(exports,require,module,__filename,__dirname)
 const m=module['exports']
 if(require) require[packageName]=m
 return m
}

/** @nosideeffects */
function getSharedModule(exports,req) {
 return req(eval(`/*dequire*/"${packageName}"`))
}

const shared=isShared()

export default shared?getSharedModule:getEmbeddedModule