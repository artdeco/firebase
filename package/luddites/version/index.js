/**
 * @suppress {uselessCode}
 */
export const isShared = () => { try {
 return ECO_ARTD_FIREBASE_COMPILE_SHARED
} catch (err) {
 return false
}}

/**
 * @suppress {uselessCode}
 */
export const getPackageName = () => { try {
 return ECO_ARTD_FIREBASE_PACKAGE_NAME
} catch (err) {
 return '@artdeco/firebase'
}}
