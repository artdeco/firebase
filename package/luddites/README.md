<div align="center"><p align="center">

# @artdeco/firebase

[![npm version](https://badge.fury.io/js/@artdeco/firebase.svg)](https://www.npmjs.com/package/@artdeco/firebase)
[![Pipeline Badge](https://gitlab.com/artdeco/firebase/nodejs/badges/master/pipeline.svg)](https://gitlab.com/artdeco/firebase/nodejs/-/commits/master)
</p></div>

`@artdeco/firebase` is: Various firebase utils and runtime to download JS files from HTTP hosting for execution in functions (helps to eliminate reploy every so often).

```sh
yarn add @artdeco/firebase
npm i @artdeco/firebase
```

## Table Of Contents

- [Table Of Contents](#table-of-contents)
- [API](#api)
- [`async firebase(config: !FirebaseConfig): !Promise<string>`](#async-mynewpackageconfig-mynewpackageconfig-promisestring)
  * [`FirebaseConfig`](#type-mynewpackageconfig)
- [`Firebase`](#type-mynewpackage)
- [CLI](#cli)
- [Copyright & License](#copyright--license)

<div align="center"><p align="center"><a href="#table-of-contents">
  <img alt="section break" src="https://artdeco.gitlab.io/firebase/nodejs/section-breaks/0.svg">
</a></p></div>

## API

The package is available by importing its default function and named class:

```js
import firebase, { Firebase } from '@artdeco/firebase'
```

<div align="center"><p align="center"><a href="#table-of-contents">
  <img alt="section break" src="https://artdeco.gitlab.io/firebase/nodejs/section-breaks/1.svg">
</a></p></div>

## <code>async <ins>firebase</ins>(</code><sub><br/>&nbsp;&nbsp;`config: !FirebaseConfig,`<br/></sub><code>): <i>!Promise&lt;string&gt;</i></code>

Various firebase utils and runtime to download JS files from HTTP hosting for execution in functions (helps to eliminate reploy every so often).

 - <kbd><strong>config*</strong></kbd> <em><code>![FirebaseConfig](#type-mynewpackageconfig "Additional options for the program.")</code></em>: Additional options for the program.

__<a name="type-mynewpackageconfig">`FirebaseConfig`</a>__: Additional options for the program.

|   Name    |   Type    |    Description    | Default |
| --------- | --------- | ----------------- | ------- |
| shouldRun | _boolean_ | A boolean option. | `true`  |
| text      | _string_  | A text to return. | -       |

```js
import firebase from '@artdeco/firebase'

(async () => {
  const res = await firebase({
    text: 'example',
  })
  console.log(res)
})()
```
```
@artdeco/firebase called with example
example
```

<div align="center"><p align="center"><a href="#table-of-contents">
  <img alt="section break" src="https://artdeco.gitlab.io/firebase/nodejs/section-breaks/2.svg">
</a></p></div>

__<a name="type-mynewpackage">`Firebase`</a>__: A representation of a package.

|      Name       |                                                               Type                                                               |             Description             | Initial |
| --------------- | -------------------------------------------------------------------------------------------------------------------------------- | ----------------------------------- | ------- |
| __constructor__ | _new () =&gt; [Firebase](#type-mynewpackage "A representation of a package.")_                                               | Constructor method.                 | -       |
| __example__     | _string_                                                                                                                         | An example property.                | `ok`    |
| __run__         | _(conf: &#33;[FirebaseConfig](#type-mynewpackageconfig "Additional options for the program.")) =&gt; !Promise&lt;string&gt;_ | Executes main method via the class. | -       |

<div align="center"><p align="center"><a href="#table-of-contents">
  <img alt="section break" src="https://artdeco.gitlab.io/firebase/nodejs/section-breaks/3.svg">
</a></p></div>

## Copyright & License

SEE LICENSE IN LICENSE

<table>
  <tr>
    <td><img src="https://avatars3.githubusercontent.com/u/38815725?v=4&amp;s=100" alt="artdeco"></td>
    <td>© <a href="https://artd.eco">Art Deco™</a> 2020</td>
  </tr>
</table>

<div align="center"><p align="center"><a href="#table-of-contents">
  <img alt="section break" src="https://artdeco.gitlab.io/firebase/nodejs/section-breaks/-1.svg">
</a></p></div>