/*

@LICENSE @type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
Please make sure you have a Commercial License to use this library.
*/
/*

[? Any reverse engineering of source code is strictly prohibited !]
@LICENSE @type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
Please make sure you have a Commercial License to use this library.
*/
/*

@LICENSE                      Warning!

This file links to (or embeds, in which case you will see object code
boundaries comments before and after), proprietary code from the package(s):
*/
const p=function(){return require(eval('/*dequire*/"@type.engineering/type-engineer"'))}();const da=p["37270038981"],ea=p["37270038983"],fa=p["372700389810"],q=p["372700389811"];function t(a,b,c,d){return p["372700389812"](a,b,null,c,!1,d)}const v=p.iu,ha=p.$advice,ia=p.$processes,w=p.precombined;module.exports={get t(){return t},get q(){return q},get v(){return v},get fa(){return fa},get ha(){return ha},get w(){return w},get p(){return p},get ea(){return ea},get da(){return da},get ia(){return ia}};