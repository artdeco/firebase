const os = require('os')
const fs = require('fs')
const stream = require('stream')
const $$DEPACK_BUILT_INS = { os,fs,stream }
'use strict';
const path = require('path');'use strict';const g=path.dirname,l=path.join,n=path.relative;const p={black:30,red:31,green:32,yellow:33,blue:34,magenta:35,cyan:36,white:37,grey:90},q={reset:0,bold:1,i:4,reverse:7,h:8},r=b=>{Array.isArray(b)&&(b=b.join(";"));return`\x1b[${b}m`};function t(b,c){c=[p[c],...Object.keys({}).map(e=>q[e.toLowerCase()])].filter(Boolean);if(!c.length)return b;c=r(c);const f=r(0);return`${c}${b}${f}`};/*

 -= Depack Runtime =-
 Art Deco EULA: Not Open Source License.
 For unlimited free use without warranty
 as part of acquired distributed packages,
 but

 Not for publication on public registries
 in any form, whether distributed on its own
 or as part of a bundled package offering,
 except the package marketplace Ludds.io,
 unless with prior written permission.

 (c) 2022 Art Deco Code Ltd
     London, UK
*/
const u=require("./rename.map"),v=Object.getOwnPropertyNames(global).reduce((b,c)=>{if(c.startsWith("$"))return b;b[c]=!0;return b},{module:!0,require:!0,__dirname:!0,__filename:!0}),w={[Symbol.unscopables]:v},{COMPILER_PROFILE:x}=process.env;function y(b,c,f){var e=z;const d=n(__dirname,c.filename);f={f:b,c:f,rel:d,filename:c.filename,get g(){return b.inner}};e.a[c.filename]=f;e.b[d]=f}
function A(b){var c=z,f=c.a[b];if(!f)return[];var e=f.c;f=[];for(const d of e)e=l(g(b),d),f.push(c.a[e]);return f}class B{constructor(){this.a={};this.b={}}}
class C{constructor(b){const c=A(b.filename),f=new Map,e={};return new Proxy(w,{has(d,a){return["eval","$$COMPILER_EVAL"].includes(a)||a in e?!1:!0},get:(d,a)=>{if(a in d)return d[a];if(a in $$DEPACK_BUILT_INS)return $$DEPACK_BUILT_INS[a];if(a in global)return global[a];if(f.has(a))return f.get(a);e[a]=!0;let k;x&&console.error("%s %s needs %s.",t("[\u2a7c]","grey"),t(n("",b.filename),"magenta"),a);d=u[a];let m;d&&(d=z.b[d])&&(m=[d]);let h;if(m)try{[k,h]=D(a,m)}catch(E){[k,h]=D(a,c,!0)}else[k,h]=
D(a,c,!0);!h&&a in global&&(k=global[a],h="global");h&&(x&&(console.error("%s %s got %s from inner scope of %s",t("[\u2a7b]","grey"),t(n("",h),"green"),t(a,"cyan"),n("",h)),console.error("%s Hashing %s.",t("[#]","grey"),t(a,"blue"))),delete e[a],f.set(a,k));return k}})}}function D(b,c,f=!1){let e,d;for(const a of c)try{e=a.f(b);d=a.filename;break}catch(k){try{e=a.g(b);d=a.filename;break}catch(m){if(!f)throw m;}}return[e,d]}const z=new B;
module.exports=function(b,c,f,...e){for(const d of e)try{require(l(g(b.filename),d))}catch(a){throw console.log(t("Depack Runtime Error","red")),console.error("Could not require dependency chunk %s for compiled module %s",t(d,"blue"),t(n("",b.filename)," yellow")),a;}y(c,b,e);return new C(b)};