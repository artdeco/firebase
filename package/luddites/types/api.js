/* @typal-type {types/design/IFirebase.xml} eco.artd.FirebaseFactory factories 50e8bbc1fe335ed01681ebb85ea90d26 */
/**
 * @param {!eco.artd.FirebaseFactory.Config} [config]
 * @return {typeof eco.artd.HyperFirebase}
 */
$eco.artd.FirebaseFactory = function(config) {}
/** @typedef {typeof $eco.artd.FirebaseFactory} */
eco.artd.FirebaseFactory

// nss:eco.artd,$eco.artd
/* @typal-end */

/* @typal-type {types/api.xml} eco.artd.firebase functions e426e0b5402727bbd85fe26b051a8029 */
/**
 * @param {!eco.artd.firebase.Config} config
 * @return {!Promise<string>}
 */
$eco.artd.firebase = function(config) {}
/** @typedef {typeof $eco.artd.firebase} */
eco.artd.firebase

// nss:eco.artd,$eco.artd
/* @typal-end */